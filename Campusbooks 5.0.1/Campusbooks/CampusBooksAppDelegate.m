//
//  CampusBooksAppDelegate.m
//  CampusBooks
//
//  Created by Admin on 06/06/09.
//  Copyright __MyCompanyName__ 2009. All rights reserved.
//

#import "CampusBooksAppDelegate.h"
#import"PopViewController.h"
#import"Schools.h"
#include <sys/types.h>
#include <sys/sysctl.h>
#define kTransitionDuration	0.50
#define IPHONE_1G_NAMESTRING @"iPhone 1G"
#define IPHONE_3G_NAMESTRING @"iPhone 3G"
#define IPHONE_3GS_NAMESTRING @"iPhone 3GS"
#define IPOD_1G_NAMESTRING @"iPod touch 1G"
#define IPOD_2G_NAMESTRING @"iPod touch 2G"

#define API_NEW_KEY @"DlkIbOJ8GnTEnRkqP1BF"
#define API_OLD_KEY @"KrIyLTZ4eZfTUckh6oJx"

static const NSInteger kGANDispatchPeriodSec = 30;
static const NSInteger kGANDispatchPeriodManual = 0;

@implementation CampusBooksAppDelegate
@synthesize window = _window;
@synthesize internetConnectionStatus, database,database_local,masterViewController, currentLocation, currentZipcode , barcodeViewController;
@synthesize homeViewController,searchViewController,  bookListViewController, bookBagViewController, infoViewController, merchantViewController, tabBarController,facebookObject,  scanHistoryViewController;

- (void)applicationDidFinishLaunching:(UIApplication *)application {    

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//    self.window.backgroundColor = [UIColor redColor];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"blanck-header.png"] forBarMetrics:UIBarMetricsDefault];
    
    NSMutableDictionary *barButtonAttributes = [NSMutableDictionary dictionary];
    [barButtonAttributes setValue:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    [barButtonAttributes setValue:[UIColor clearColor] forKey:UITextAttributeTextShadowColor];
    [barButtonAttributes setValue:[NSValue valueWithUIOffset:UIOffsetMake(0.0, 0.0)] forKey:UITextAttributeTextShadowOffset];

    [[UIBarButtonItem appearance] setTitleTextAttributes:barButtonAttributes forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:26.0/255 green:73.0/255 blue:90.0/255 alpha:1]];
    
    NSMutableDictionary *navTitleAttributes = [NSMutableDictionary dictionary];
    [navTitleAttributes setValue:[UIColor colorWithRed:26.0/255 green:73.0/255 blue:90.0/255 alpha:1] forKey:UITextAttributeTextColor];
    [navTitleAttributes setValue:[UIColor clearColor] forKey:UITextAttributeTextShadowColor];
    [navTitleAttributes setValue:[NSValue valueWithUIOffset:UIOffsetMake(0.0, 0.0)] forKey:UITextAttributeTextShadowOffset];

    [[UINavigationBar appearance] setTitleTextAttributes:navTitleAttributes];

    [[UIActivityIndicatorView appearance] setColor:[UIColor colorWithRed:26.0/255 green:73.0/255 blue:90.0/255 alpha:1]];

//    
//    [[UINavigationBar appearance] setBarStyle:UIBarStyleBlackTranslucent];
//    [[UINavigationBar appearance] setAlpha:0.7];

//    [[UIToolbar appearance] setBackgroundImage:[UIImage imageNamed:@"myToolBar.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];

	[self updateStatus];
	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"CampusBooks" message:@"No Network Available.\n This application requires a Wi-Fi or cellular data network access."
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
	}
	
    // Override point for customization after application launch
	[self createEditableCopyOfDatabaseIfNeeded];
	[self initializeObjects];
	[self createEditableCopyOfLocalDatabaseIfNeeded];
	
	[MyCLController sharedInstance].delegate = self;
	[[MyCLController sharedInstance].locationManager startUpdatingLocation];

	barcodeViewController = [[Pic2BarCodeScanViewController alloc]init];

	rootView = [[TransitionView alloc] initWithFrame:[[UIScreen mainScreen] bounds]] ;
	homeViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil] ;
//	[rootView addSubview:homeViewController.view];
	
//	ProgrammaticAdViewController* adViewController = [[ProgrammaticAdViewController alloc]init];
//	[self.window addSubview:adViewController.view];
	facebookObject=[FacebookMyLib sharedInstance];
	searchViewController = [[SearchViewController alloc] init];
	bookBagViewController = [[BookBagViewController alloc]init];
	masterViewController = [[InfoViewController alloc] init];
	merchantViewController = [[MerchantViewController alloc]init];
	
	UIColor* titleColor =[UIColor blackColor];///[UIColor colorWithRed:0.21484375 green:0.21484375 blue:0.21484375 alpha:1];
	
	UINavigationController* navController1 = [[[UINavigationController alloc] initWithRootViewController:homeViewController] autorelease];
	navController1.navigationBar.tintColor = titleColor;
	navController1.title = @"Search";
    navController1.tabBarItem.image = [UIImage imageNamed:@"tabbar_search.png"];
	
	UINavigationController* navController3 = nil;
	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
	{
		scanHistoryViewController =[[ScanHistoryViewController alloc] init];

		navController3 = [[[UINavigationController alloc] initWithRootViewController:scanHistoryViewController] autorelease];
		navController3.navigationBar.tintColor = titleColor;
		navController3.title = @"Scan History";
		navController3.tabBarItem.image = [UIImage imageNamed:@"tabbar_scanhistory.png"];
	//	[scanViewController release];
	}
	
	NSMutableArray* booksList = nil;

	bookBagViewController.selectedType = 1;
	BookListViewController* wishListViewController = [[BookListViewController alloc]initWithBooksList:booksList fromSource:bookBagViewController];
	[wishListViewController refreshBookListViewController:booksList fromSource:bookBagViewController];

	
	UINavigationController* navController4 = [[[UINavigationController alloc] initWithRootViewController:wishListViewController] autorelease];
	navController4.navigationBar.tintColor = titleColor;
	navController4.title = @"Wishlist";
	navController4.tabBarItem.image  = [UIImage imageNamed:@"tabbar_wishlist.png"];
	[wishListViewController release];
	
	UINavigationController* navController5 = [[[UINavigationController alloc] initWithRootViewController:masterViewController] autorelease];
	navController5.navigationBar.tintColor = titleColor;
	navController5.title = @"Info";
	navController5.tabBarItem.image  = [UIImage imageNamed:@"tabbar_info.png"];
	
	self.tabBarController = [[UITabBarController alloc]init];
	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
	{
		self.tabBarController.viewControllers = [NSArray arrayWithObjects:navController1, navController3, navController4, navController5, nil];	
	}
	else 
	{
		self.tabBarController.viewControllers = [NSArray arrayWithObjects:navController1, navController4, navController5, nil];
	}	


	self.tabBarController.tabBar.backgroundColor = [UIColor redColor];
	self.tabBarController.tabBarController.tabBar.backgroundColor = [UIColor greenColor];
	[self.tabBarController setSelectedIndex: 0];

//	[rootView addSubview:tabBarController.view];
//	[self.window addSubview:tabBarController.view];
    self.window.rootViewController = self.tabBarController;
//    [window addSubview:navController1.view];
    [self.window makeKeyAndVisible];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [facebookObject handleOpenURL:url]; 
}

-(void)addLocalOptionsToDatabaseIfNeeded
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"booksdb_local.sql"];
	
	if (sqlite3_open([path UTF8String], &database_local) == SQLITE_OK) 
	{	
		//printf("\n sucesfuly opened the database");
	}
	else 
	{
		sqlite3_close(database);
		NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(database));
	}
	
/*	BOOL isSchoolsAvailable = YES;
	sqlite3_stmt* statement;
	printf("\n About to check if schools is available");
	const char *sql =  "select max(pk) from schools10;";
	if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
		//NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		isSchoolsAvailable = NO;
		printf("\n assertion failed");

	}
	
	@try{
	
		sqlite3_step(statement);
				
	 }
	 @catch (NSException * e) {
		 printf("\n caught an exception:" );
		 isSchoolsAvailable = NO;
	 }
	 @finally {
	 
	 }
 
	sqlite3_finalize(statement);
	printf("\n Completed checking value for schools");
	if(!isSchoolsAvailable)
	{
		printf("\n Schools not found");
		sqlite3_stmt* create_statement;
		const char *sql_create =  "create table schools10 (pk INTEGER PRIMARY KEY AUTOINCREMENT, schoolName VARCHAR(128), zipCode VARCHAR(128), phoneNumber VARCHAR(128), latValue VARCHAR(128), langValue VARCHAR(128)) ";
		if (sqlite3_prepare_v2(database, sql_create, -1, &create_statement, NULL) != SQLITE_OK) {
			//NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			printf("%s",sqlite3_errmsg(database));
		}
		
		sqlite3_step(create_statement);
		sqlite3_finalize(create_statement);
	}
	else
	{
		printf("\n Schools found");
	}

	NSMutableArray* insertSchoolsDataStatements = [[NSMutableArray alloc] init];
		
	[insertSchoolsDataStatements addObject:@"insert into schools10 values(1, 'test school', 'test zipcode', '1234567890', '2.09', '2,12');"];
	[insertSchoolsDataStatements addObject:@"insert into schools10 values(2, 'test school', 'test zipcode', '1234567890', '2.09', '2,12');"];
	[insertSchoolsDataStatements addObject:@"insert into schools10 values(3, 'test school', 'test zipcode', '1234567890', '2.09', '2,12');"];
	
	NSDate* startDate = [[NSDate alloc ] init];
					
//	for (NSString* sqlStatementStr in insertSchoolsDataStatements)
	for(int i = 100; i < 7000; i++)
	{
		NSString* sqlStatementStr = [NSString stringWithFormat:@"insert into schools10 values(%d, 'test school', 'test zipcode', '1234567890', '2.09', '2,12');", i];
		sqlite3_stmt* insert_statement;
		const char *sql_insert =  [sqlStatementStr UTF8String];
		if (sqlite3_prepare_v2(database, sql_insert, -1, &insert_statement, NULL) != SQLITE_OK) {
			//NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			printf("%s",sqlite3_errmsg(database));
		}
		
		sqlite3_step(insert_statement);
		sqlite3_finalize(insert_statement);
	}
	NSDate* endDate = [[NSDate alloc ] init];
	printf("\n Time taken to insert recods:%f", [endDate timeIntervalSince1970] - [startDate timeIntervalSince1970]);
	
	sqlite3_stmt* select_statement;
	const char *sql_select =  "select count(*) from schools10";
	if (sqlite3_prepare_v2(database, sql_select, -1, &select_statement, NULL) != SQLITE_OK) {
		//NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		printf("%s",sqlite3_errmsg(database));
	}
	

	while (sqlite3_step(select_statement) == SQLITE_ROW) 
	{
		int noOfRows = sqlite3_column_int(select_statement, 0);
		printf("\n No Of Schools:%d", noOfRows);
	}
	sqlite3_finalize(select_statement);
	
*/		
}
-(void)initializeObjects
{
	
	localOptions = nil;
	booksInfoParserXml = [[BooksInfoParserXml alloc] init];
	booksInfoParserXmlForSearch = [[BooksInfoParserXml alloc] init] ;
	bookOffersParserXmlForSearch = [[BooksOfferParserXml alloc] init];
	
	booksInfoParserXmlForBookBag = [[BooksInfoParserXml alloc] init] ;
	bookOffersParserXmlForBookBag = [[BooksOfferParserXml alloc] init];
    
    bookOffersParserXmlForBookBagLocalOptions = [[BooksLocalOfferParserXml alloc] init];
	bookOffersParserXmlForSearchLocalOptions = [[BooksLocalOfferParserXml alloc] init];
	
	
	bookBuybackOffersParserXmlForSearch = [[BooksBuybackOfferParserXml alloc] init];
	bookBuybackOffersParserXmlForBookBag = [[BooksBuybackOfferParserXml alloc] init];
	
	
	booksListParserXml=[[BooksListParserXml alloc]init];
	bookSearchParserXml = [[BookSearchParserXml alloc] init];
	isbnSearchParserXml = [[BookSearchParserXml alloc] init];

	wishList = [[NSMutableArray alloc] init];
	myCollection = [[NSMutableArray alloc] init];
	
	schoolsList=[[NSMutableArray alloc]init];

	scanListList = [[NSMutableArray alloc] init];
	scanItemList = [[NSMutableArray alloc] init];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"booksdb.sql"];
	
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{	
		//printf("\n sucesfuly opened the database");
	}
	else 
	{
		sqlite3_close(database);
		NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(database));
	}
	
	
}
- (void)createEditableCopyOfDatabaseIfNeeded {
	
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"booksdb.sql"];
    success = [fileManager fileExistsAtPath:writableDBPath];
    if (success) return;
    
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"booksdb.sql"];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
	printf("\n Success:%s", success ? "Yes" :"NO" );
   
}
- (void)createEditableCopyOfLocalDatabaseIfNeeded {
	
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"booksdb_local.sql"];
    success = [fileManager fileExistsAtPath:writableDBPath];
    if (!success)
	{
    
		NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"booksdb_local.sql"];
		success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
		
		if (!success) {
			NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
		}
	}
		
	NSString *localDbPath = [documentsDirectory stringByAppendingPathComponent:@"booksdb_local.sql"];
		
	if (sqlite3_open([localDbPath UTF8String], &database_local) == SQLITE_OK) 
	{	
		printf("\n sucesfuly opened the local database");
	}
	else 
	{
		sqlite3_close(database_local);
		NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(database));
	}

	
	
}
-(Schools*)getSchoolByName:(NSString*)aSchooName
{
	sqlite3_stmt* statement= nil;
	Schools* schoolsObj = nil;
	//select id from winetypes
	const char *sql = "select * from schools where schoolName = ?";
	if (sqlite3_prepare_v2(database_local, sql, -1, &statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database_local));
	}
	sqlite3_bind_text(statement, 1, [aSchooName UTF8String], -1, SQLITE_TRANSIENT);
	
	if (sqlite3_step(statement) == SQLITE_ROW) 
	{
		int pk = sqlite3_column_int(statement, 0);
		
		schoolsObj = [[[Schools alloc] initWithPrimaryKey:pk database:database_local] autorelease];	
	}
	
	sqlite3_finalize(statement);
	
	return schoolsObj ;
}
-(NSMutableArray*)getSchoolsList
{
	
	[schoolsList removeAllObjects];	
	
	sqlite3_stmt* statement;
	
	//select id from winetypes
	const char *sql = "select pk, schoolName,latValue,langValue from schools";
	if (sqlite3_prepare_v2(database_local, sql, -1, &statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database_local));
	}
	while (sqlite3_step(statement) == SQLITE_ROW) 
	{
		Schools* schoolsObj = [[Schools alloc] init];	
		
		int intpk = sqlite3_column_int(statement, 0);
		schoolsObj.primaryKey  = (intpk != 0 ? intpk:0);
		
		char *strSchoolName= (char *)sqlite3_column_text(statement, 1);
		schoolsObj.schoolName = (strSchoolName != nil ? [NSString stringWithUTF8String:strSchoolName] : @"");
		
		char *strLatValue = (char *)sqlite3_column_text(statement, 2);
		schoolsObj.latValue = (strLatValue != nil ? [NSString stringWithUTF8String:strLatValue] : @"");
		
		
		char *strLangValue= (char *)sqlite3_column_text(statement, 3);
		schoolsObj.langValue = (strLangValue != nil ? [NSString stringWithUTF8String:strLangValue] : @"");
		
		[schoolsList addObject:schoolsObj];
		[schoolsObj release];
	}
	
	sqlite3_finalize(statement);
	
	return schoolsList;
}
-(LocalOptions*)getLocalOptions
{
	
	if (localOptions == nil) {
		sqlite3_stmt* statement = nil;
		
		const char *sql = "select * from localOptions";
		if (sqlite3_prepare_v2(database_local, sql, -1, &statement, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database_local));
		}
		if (sqlite3_step(statement) == SQLITE_ROW) 
		{
			int pk = sqlite3_column_int(statement, 0);
			
			localOptions = [[LocalOptions alloc] initWithPrimaryKey:pk database:database_local];	
		}
		
		sqlite3_finalize(statement);
	}	
	return localOptions;
	
}
- (void)updateLocalOptions:(LocalOptions *)aupdateObj
{
	sqlite3_stmt *update_statement;
	
	const char *sql = "UPDATE localOptions SET options=?,radius=?,zipCode=?,schoolName=?,isFirstTime=? WHERE pk=? ;";
	if (sqlite3_prepare_v2(database_local, sql, -1, &update_statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database_local));
	}
	sqlite3_bind_int(update_statement, 1, [aupdateObj optionsValue]);
	sqlite3_bind_int(update_statement, 2, [aupdateObj radiusValue]);
	sqlite3_bind_text(update_statement, 3, [[aupdateObj zipCode]UTF8String] , -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, 4, [[aupdateObj schoolName]UTF8String] , -1, SQLITE_TRANSIENT);
	sqlite3_bind_int(update_statement, 5,[aupdateObj isFirstTime]);
	sqlite3_bind_int(update_statement, 6,[aupdateObj primaryKey]);
	int success = sqlite3_step(update_statement);
	sqlite3_finalize(update_statement);
	if (success == SQLITE_ERROR) {
		NSAssert1(0, @"Error: failed to update to  the database with message '%s'.", sqlite3_errmsg(database_local));
	} 
	
}
- (void)addToWishList:(WishList *)awish 
{
	sqlite3_stmt *insert_statement;
	static char *sql = "INSERT INTO wishlist (bookName,isbnId10,authorName, favoritedDate, imageUrl) VALUES (?,?,?,?,?)";
	if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
	}
	sqlite3_bind_text(insert_statement, 1, [[awish bookName]UTF8String] , -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, 2, [[awish isbnId10]UTF8String] , -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, 3, [[awish authorName] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_double(insert_statement, 4, [[NSDate date] timeIntervalSinceReferenceDate]);
	sqlite3_bind_text(insert_statement, 5, [[awish imageUrl] UTF8String], -1, SQLITE_TRANSIENT);

	int success = sqlite3_step(insert_statement);
	sqlite3_finalize(insert_statement);
	if (success == SQLITE_ERROR) {
		NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
	} 
	else {
		primaryKey = sqlite3_last_insert_rowid(database);
	}
}

- (void)removeFromWishList:(NSString*)aIsbnId
{
	sqlite3_stmt *insert_statement;
	static char *sql = " delete from wishlist where isbnId10 = ?";
	if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
	}
	sqlite3_bind_text(insert_statement, 1, [aIsbnId UTF8String] , -1, SQLITE_TRANSIENT);

	
	int success = sqlite3_step(insert_statement);
	sqlite3_finalize(insert_statement);
	if (success == SQLITE_ERROR) {
		NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
	} 
}
- (void)addToMyCollection:(MyCollection *)awish
{
	sqlite3_stmt *insert_statement;
	static char *sql = "INSERT INTO MyCollection (bookName,isbnId10,authorName, favoritedDate, imageUrl) VALUES (?,?,?,?,?)";
	if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
	}
	sqlite3_bind_text(insert_statement, 1, [[awish bookName]UTF8String] , -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, 2, [[awish isbnId10]UTF8String] , -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, 3, [[awish authorName] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_double(insert_statement, 4, [[NSDate date] timeIntervalSinceReferenceDate]);
	sqlite3_bind_text(insert_statement, 5, [[awish imageUrl] UTF8String], -1, SQLITE_TRANSIENT);
	
	int success = sqlite3_step(insert_statement);
	sqlite3_finalize(insert_statement);
	if (success == SQLITE_ERROR) {
		NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
	} 
	else {
		primaryKey = sqlite3_last_insert_rowid(database);
	}
}

- (void)removeFromMyCollection:(NSString *)aIsbnId
{
	sqlite3_stmt *insert_statement;
	static char *sql = " delete from MyCollection where isbnId10 = ?";
	if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
	}
	sqlite3_bind_text(insert_statement, 1, [aIsbnId UTF8String] , -1, SQLITE_TRANSIENT);
	
	
	int success = sqlite3_step(insert_statement);
	sqlite3_finalize(insert_statement);
	if (success == SQLITE_ERROR) {
		NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
	}
}					  
-(BOOL)isItemInWishList:(NSString*)aIsbnId
{
	BOOL isInWishList = NO;
	sqlite3_stmt* statement;
	const char *sql =  "SELECT pk  FROM wishlist where isbnId10 = ? ";
	if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
	}
	sqlite3_bind_text(statement, 1, [aIsbnId UTF8String] , -1, SQLITE_TRANSIENT);

	if (sqlite3_step(statement) == SQLITE_ROW) 
	{
		isInWishList = YES;
	}
	sqlite3_finalize(statement);
	return isInWishList ;
}

-(BOOL)isItemInMyCollection:(NSString*)aIsbnId
{
	BOOL isItemPurchased = NO;
	sqlite3_stmt* statement;
	const char *sql =  "SELECT pk  FROM MyCollection where isbnId10 = ? ";
	if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
	}
	sqlite3_bind_text(statement, 1, [aIsbnId UTF8String] , -1, SQLITE_TRANSIENT);
	
	if (sqlite3_step(statement) == SQLITE_ROW) 
	{
		isItemPurchased = YES;
	}
	sqlite3_finalize(statement);
	return isItemPurchased ;
}

-(NSMutableArray*) getMyCollection:(NSInteger)aOffset
{
	if(aOffset ==0)
	{
		[myCollection removeAllObjects];
	}
	
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc ]init];
	sqlite3_stmt* statement;
	const char *sql =  "SELECT pk,bookName,isbnId10,authorName, favoritedDate, imageUrl  FROM MyCollection  limit 10 offset ?  ";
	if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
	}
	sqlite3_bind_int(statement, 1,  aOffset );	

	while (sqlite3_step(statement) == SQLITE_ROW) 
	{
		Book* book = [[Book alloc] init];
		
		char *strBookName= (char *)sqlite3_column_text(statement, 1);
		book.title = (strBookName != nil ? [NSString stringWithUTF8String:strBookName] : @"");
		
		char *strIsbnId10= (char *)sqlite3_column_text(statement, 2);
		book.isbnId10  = (strIsbnId10 != nil ? [NSString stringWithUTF8String:strIsbnId10] : @"");;
		
		char *strAuthorName = (char *)sqlite3_column_text(statement, 3);
		book.author = (strAuthorName != nil ? [NSString stringWithUTF8String:strAuthorName] : @"");
		
		book.isInMyCollection = YES;
		
		book.isInWishList = NO;
		
		book.favoriteDate = (sqlite3_column_double(statement, 4) != 0) ? [NSDate dateWithTimeIntervalSinceReferenceDate:sqlite3_column_double(statement, 5)] : nil;
		
		char *strImageurl = (char *)sqlite3_column_text(statement, 5);
		book.imageUrl = (strImageurl != nil ? [NSString stringWithUTF8String:strImageurl] : @"");

		[myCollection addObject:book];
		[book release];
	}
	sqlite3_finalize(statement);
	[pool release];
	return myCollection;
}

-(NSMutableArray*) getMyWishList:(NSInteger)aOffset
{
	if(aOffset ==0)
	{
		[wishList removeAllObjects];
	}	
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc ]init];
	sqlite3_stmt* statement;
	const char *sql =  "SELECT pk,bookName,isbnId10,authorName, favoritedDate, imageUrl  FROM wishlist  limit 10 offset ?  ";
	if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
	}
	sqlite3_bind_int(statement, 1,  aOffset );	

	while (sqlite3_step(statement) == SQLITE_ROW) 
	{
		Book* book = [[Book alloc] init];
	
		char *strBookName= (char *)sqlite3_column_text(statement, 1);
		book.title = (strBookName != nil ? [NSString stringWithUTF8String:strBookName] : @"");
		
//		printf("\n Retrieving :%s", [book.title UTF8String]);
		
		char *strIsbnId10= (char *)sqlite3_column_text(statement, 2);
		book.isbnId10  = (strIsbnId10 != nil ? [NSString stringWithUTF8String:strIsbnId10] : @"");;
		
		char *strAuthorName = (char *)sqlite3_column_text(statement, 3);
		book.author = (strAuthorName != nil ? [NSString stringWithUTF8String:strAuthorName] : @"");
		
		
		book.isInWishList = YES;
		book.isInMyCollection = NO;

		book.favoriteDate = (sqlite3_column_double(statement, 4) != 0) ? [NSDate dateWithTimeIntervalSinceReferenceDate:sqlite3_column_double(statement, 5)] : nil;

		char *strImageurl = (char *)sqlite3_column_text(statement, 5);
		book.imageUrl = (strImageurl != nil ? [NSString stringWithUTF8String:strImageurl] : @"");

		[wishList addObject:book];
		[book release];
	}
	sqlite3_finalize(statement);
	[pool release];
	
	return wishList;
}
- (void)dealloc 
{

	[booksInfoParserXml release];
	[booksInfoParserXmlForSearch release]; 
	[bookOffersParserXmlForSearch release]; 
	[booksInfoParserXmlForBookBag release];
	[bookOffersParserXmlForBookBag release];
	[bookBuybackOffersParserXmlForSearch release];
	[bookBuybackOffersParserXmlForBookBag release];
	[booksListParserXml release];
	[bookSearchParserXml release];
	[isbnSearchParserXml release];
	[window release];
	
    [super dealloc];
}

- (void)addToScanList:(ScanList*)aList 
{
	sqlite3_stmt *insert_statement;
	static char *sql = "INSERT INTO scanlist (name,scanDate) VALUES (?,?)";
	if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
	}
	sqlite3_bind_text(insert_statement, 1, [[aList name]UTF8String] , -1, SQLITE_TRANSIENT);
	sqlite3_bind_double(insert_statement, 2, [[NSDate date] timeIntervalSinceReferenceDate]);
	
	int success = sqlite3_step(insert_statement);
	sqlite3_finalize(insert_statement);
	if (success == SQLITE_ERROR) {
		NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
	} 
	else {
		aList.primaryKey = sqlite3_last_insert_rowid(database);
	}
}

- (void)addToScanItem:(ScanItem*)aScanItem 
{
	sqlite3_stmt *insert_statement;
	static char *sql = "INSERT INTO scanitem (bookName,isbnId10,authorName, imageUrl, scanListId) VALUES (?,?,?,?,?)";
	if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
	}
	
	sqlite3_bind_text(insert_statement, 1, [[aScanItem bookName]UTF8String] , -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, 2, [[aScanItem isbnId10]UTF8String] , -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, 3, [[aScanItem authorName] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, 4, [[aScanItem imageUrl] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_int(insert_statement, 5,  aScanItem.scanListId);	

	int success = sqlite3_step(insert_statement);
	sqlite3_finalize(insert_statement);
	if (success == SQLITE_ERROR) {
		NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
	} 
	else {
		aScanItem.primaryKey = sqlite3_last_insert_rowid(database);
	}
}
-(NSMutableArray*) getScanItemList:(NSInteger)aScanListId
{
	[scanItemList removeAllObjects];
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc ]init];
	sqlite3_stmt* statement;
	const char *sql =  "SELECT pk,bookName,isbnId10,authorName, imageUrl, scanListId  FROM scanitem where scanListId = ? order by pk ";
	if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
	}
	sqlite3_bind_int(statement, 1,  aScanListId );	
	
	while (sqlite3_step(statement) == SQLITE_ROW) 
	{
		ScanItem* scanItem = [[ScanItem alloc] init];
		
		scanItem.primaryKey = sqlite3_column_int(statement, 0);

		char *strBookName= (char *)sqlite3_column_text(statement, 1);
		scanItem.bookName = (strBookName != nil ? [NSString stringWithUTF8String:strBookName] : @"");
		
		char *strIsbnId10= (char *)sqlite3_column_text(statement, 2);
		scanItem.isbnId10  = (strIsbnId10 != nil ? [NSString stringWithUTF8String:strIsbnId10] : @"");;
		
		char *strAuthorName = (char *)sqlite3_column_text(statement, 3);
		scanItem.authorName = (strAuthorName != nil ? [NSString stringWithUTF8String:strAuthorName] : @"");
		
		char *strImageurl = (char *)sqlite3_column_text(statement, 4);
		scanItem.imageUrl = (strImageurl != nil ? [NSString stringWithUTF8String:strImageurl] : @"");
		
		scanItem.scanListId = sqlite3_column_int(statement, 5);
		
	//	printf("\n Scan Item Details\n:%d, %s, %s,  %s, %s,%d ", scanItem.primaryKey, [scanItem.bookName UTF8String],  [scanItem.isbnId10 UTF8String], [scanItem.authorName UTF8String], [scanItem.imageUrl UTF8String], scanItem.scanListId);
		[scanItemList addObject:scanItem];
		[scanItem release];
	}
	sqlite3_finalize(statement);
	[pool release];
	
	return scanItemList;
}

-(NSMutableArray*) getScanListList
{
	[scanListList removeAllObjects];
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc ]init];
	sqlite3_stmt* statement;
	const char *sql =  "select scanlist.pk, scanlist.name, scanlist.scanDate, count(  distinct scanitem.pk) from scanlist, scanitem where scanitem.scanListId = scanlist.pk group by scanlist.pk, scanlist.name, scanlist.scanDate order by scanlist.pk desc ";
//	const char *sql =  "select scanList.pk, scanList.name, scanList.scanDate, 2 from scanlist order by scanlist.pk desc";	
	if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
	}
	
	while (sqlite3_step(statement) == SQLITE_ROW) 
	{
		ScanList* scanList = [[ScanList alloc] init];
		
		scanList.primaryKey = sqlite3_column_int(statement, 0);
		
		
		char *strName= (char *)sqlite3_column_text(statement, 1);
		scanList.name = (strName != nil ? [NSString stringWithUTF8String:strName] : @"");
		
		scanList.scanDate = (sqlite3_column_double(statement, 2) != 0) ? [NSDate dateWithTimeIntervalSinceReferenceDate:sqlite3_column_double(statement, 2)] : nil;

		scanList.noOfItems = sqlite3_column_int(statement, 3);

	//	printf("\n Scan List PK:%d NoOfValues:%d", scanList.primaryKey, scanList.noOfItems);
		
		[scanListList addObject:scanList];
		[scanList release];
	}
	sqlite3_finalize(statement);
	[pool release];
	
	//printf("\n APP Delegate: Returned Object Count:%d", [scanListList count]);
	return scanListList;
	
}

-(void)closeInfoViewController
{
	//	printf("\n Closing Info View Controller");
	[rootView replaceSubview:infoViewController.view  withSubview: homeViewController.view
				  transition:kCATransitionFade
				   direction:kCATransitionFade 
					duration:kTransitionDuration];
	
}

-(NSMutableArray *)doBookSearch:(NSString*) aTitle byAuthor:(NSString*) aAuthor withKeyword:(NSString*)aKeyword forISBN:(NSString*)aISBNId forPage:(NSInteger)aPage 
{  

	if ([aISBNId length]  == 0)
	{
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		
	//	//http://api.campusbooks.com/3/rest/bookinfo?key=KrIyLTZ4eZfTUckh6oJx&isbn=0824828917
		
	//	http://api.campusbooks.com/3/rest/search?key=KrIyLTZ4eZfTUckh6oJx&title=%s
//		NSString *surl=@"http://api.campusbooks.com/8/rest/search?key=KrIyLTZ4eZfTUckh6oJx";
        
        
		NSString *surl = [NSString stringWithFormat:@"http://api2.campusbooks.com/13/rest/books?key=%@&f=search", API_NEW_KEY];
		if([aTitle length]>0)
		{
			surl = [surl stringByAppendingString:@"&title="];
			surl = [surl stringByAppendingString:aTitle ];
		}
		if ([aAuthor length] >0)
		{
			surl = [surl stringByAppendingString:@"&author="];
			surl = [surl stringByAppendingString:aAuthor ];
		}
		if ([aKeyword length] >0)
		{
			surl = [surl stringByAppendingString:@"&keywords="];
			surl = [surl stringByAppendingString:aKeyword ];
		}
		if(aPage > 0)
		{
			surl = [surl stringByAppendingString:[NSString stringWithFormat:@"&page=%d", aPage]];
		}
		surl = [surl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		printf("\n Searching url: %s", [surl UTF8String]);
		[bookSearchParserXml parseXMLFileAtURL:[NSURL URLWithString:surl] forPage:aPage];
		NSMutableArray* bookslist =[bookSearchParserXml getitemsList];
		
		[pool release];

		return bookslist;
	}
	else
	{
		NSMutableArray* booksList = [[NSMutableArray alloc] init];
		Book* book =  [self getBookInfo:aISBNId];
		if(book != nil)
		{
			[booksList addObject:book];
		}
		return [booksList autorelease] ;
	}
	return nil;
}
/*
-(NSString *)getCampusBooksCoverURLByISBN:(NSString*) isbnId
{  
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	//http://api.campusbooks.com/3/rest/bookinfo?key=KrIyLTZ4eZfTUckh6oJx&isbn=0824828917
	
	NSString *surl=[NSString stringWithFormat:@"http://api.campusbooks.com/8/rest/bookinfo?key=KrIyLTZ4eZfTUckh6oJx&isbn=%s",[isbnId UTF8String]];
//	//printf("Books info url:%s",[surl UTF8String]);
	
	[booksInfoParserXml parseXMLFileAtURL:[NSURL URLWithString:surl]];
	NSMutableArray* booksInfoList=[booksInfoParserXml getitemsList];
	if([booksInfoList count] > 0)
	{
		BookInfo* aBookInfo = [booksInfoList objectAtIndex:0];
		return [aBookInfo.imageUrl copy];
	}
	
	[pool release];
	return @"";
}
*/
-(Book*)getBookInfo:(NSString*)isbnId
{
	
//	NSString *surl=[NSString stringWithFormat:@"http://api.campusbooks.com/8/rest/bookinfo?key=KrIyLTZ4eZfTUckh6oJx&isbn=%s",[isbnId UTF8String]];
	
	NSString *surl=[NSString stringWithFormat:@"http://api2.campusbooks.com/13/rest/books?key=%@&f=search&isbn=%s",API_NEW_KEY,[isbnId UTF8String]];
    
    
    NSLog(@"Books info url:%s",[surl UTF8String]);
	
	[booksInfoParserXml parseXMLFileAtURL:[NSURL URLWithString:surl]];
	NSMutableArray* booksInfoList=[booksInfoParserXml getitemsList];
	if ([booksInfoList count] >0)
	{
		return [booksInfoList objectAtIndex:0];
	}
	
	return nil;
}
-(Book*)getBookInfoForSearch:(NSString*)isbnId
{

	//http://api.campusbooks.com/3/rest/bookinfo?key=KrIyLTZ4eZfTUckh6oJx&isbn=0824828917
	
	NSString *surl=[NSString stringWithFormat:@"http://api2.campusbooks.com/13/rest/books?key=%@&f=search&isbn=%s",API_NEW_KEY, [isbnId UTF8String]];
    
    NSLog(@"URL:%@", surl);
	
	[booksInfoParserXmlForSearch parseXMLFileAtURL:[NSURL URLWithString:surl]];
	NSMutableArray* booksInfoList=[booksInfoParserXmlForSearch getitemsList];
	if ([booksInfoList count] >0)
	{
		return [booksInfoList objectAtIndex:0];
	}
	return nil;
}

-(NSMutableArray*)getBookOffersListForSearch:(NSString*)isbnId
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	//http://api.campusbooks.com/3/rest/bookinfo?key=KrIyLTZ4eZfTUckh6oJx&isbn=0824828917

//	NSString *surl=[NSString stringWithFormat:@"http://api.campusbooks.com/8/rest/prices?&key=KrIyLTZ4eZfTUckh6oJx&isbn=%s",[isbnId UTF8String]];
	//NSString* surl = @"http://api.campusbooks.com/8/rest/bookprices?isbn=0316024961&key=PA52HnTGaTSyizTOq4j1&lat=33.9230790&lon=-83.3389880";
	
    NSString *surl=[NSString stringWithFormat:@"http://api2.campusbooks.com/13/rest/books?f=prices&type=buy&format=json&isbn=%s&coupons=1",[isbnId UTF8String]];

    NSString* newAPIURL = [surl stringByAppendingFormat:@"&key=%@", API_NEW_KEY]; 

	NSLog(@"URL:%@", newAPIURL);
    
    
    [bookOffersParserXmlForSearch parseXMLFileAtURL:[NSURL URLWithString:newAPIURL] includeLocalOptionsOnly:NO];
	NSMutableArray* offersList=[bookOffersParserXmlForSearch getBookOffersList];
    
    NSLog(@"No of results from new API :%d", [offersList count]);
    
    
	//PA52HnTGaTSyizTOq4j1&lat=33.9230790&lon=-83.3389880
	
	NSString* localSearchParameters = @"";
//	NSString* latValue = @"33.9230790";
//	NSString* longValue = @"-83.3389880";

	NSString* latValue = @"";
	NSString* longValue = @"";

	LocalOptions* localOption = [self getLocalOptions];
//	localOption.optionsValue = 2;
//    localOption.zipCode = @"60062";
    
    if (localOption.optionsValue != 0) {
        
        NSString *localOptionsBaseUrl=[NSString stringWithFormat:@"http://api.campusbooks.com/11/rest/bookprices?isbn=%@",isbnId];
    
        if (localOption.optionsValue ==1)
        {
            if (currentLocation != nil)
            {
                latValue = [NSString stringWithFormat:@"%f", currentLocation.coordinate.latitude];
                longValue = [NSString stringWithFormat:@"%f", currentLocation.coordinate.longitude];
            }
            
            
            localSearchParameters = [NSString stringWithFormat:@"&lat=%s&lon=%s&miles=%d", [latValue UTF8String], [longValue UTF8String], localOption.radiusValue]; 
        }
        else if (localOption.optionsValue ==2)
        {
            localSearchParameters = [NSString stringWithFormat:@"&zip=%s", [localOption.zipCode UTF8String]]; 
        }
        else if (localOption.optionsValue ==3)
        {
            Schools* school = [self getSchoolByName:localOption.schoolName];
            if (school != nil)
            {
                latValue = school.latValue;
                longValue = school.langValue;
                localSearchParameters = [NSString stringWithFormat:@"&lat=%s&lon=%s", [latValue UTF8String], [longValue UTF8String]]; 
            }
        }
        
        NSString* localOptionsURL = [localOptionsBaseUrl stringByAppendingFormat:@"&key=%@%@", API_OLD_KEY, localSearchParameters];
        
        [bookOffersParserXmlForSearchLocalOptions parseXMLFileAtURL:[NSURL URLWithString:localOptionsURL] ];
        NSMutableArray* offersFromLocalOptions = [bookOffersParserXmlForSearchLocalOptions getBookOffersList];

        NSLog(@"URL For Older API :%@", localOptionsURL);
        NSLog(@"No of results from Older API :%d", [offersFromLocalOptions count]);
        [offersList addObjectsFromArray:offersFromLocalOptions];
    }

	
//    NSData* responseData = [NSData dataWithContentsOfURL:[NSURL URLWithString:surl]];
//    NSError* error;
//    NSDictionary* json = [NSJSONSerialization 
//                          JSONObjectWithData:responseData //1
//                          
//                          options:kNilOptions 
//                          error:&error];
//    
//    NSArray* offersList = [json objectForKey:@"loans"]; //2
//    
//    NSLog(@"loans: %@", latestLoans); //3
    
	
	[pool release];
	return offersList;
}
-(Book*)getBookInfoForBookBag:(NSString*)isbnId
{
	//NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	NSString *surl=[NSString stringWithFormat:@"http://api2.campusbooks.com/13/rest/books?key=%@&f=search&isbn=%s",API_NEW_KEY,[isbnId UTF8String]];
	
	[booksInfoParserXmlForBookBag parseXMLFileAtURL:[NSURL URLWithString:surl]];
	NSMutableArray* booksInfoList=[booksInfoParserXmlForBookBag getitemsList];
	if ([booksInfoList count] >0)
	{
		//[pool release];
		return [booksInfoList objectAtIndex:0];
	}
	else
	{
	//[pool release];	
	}

	return nil;
}

-(NSMutableArray*)getBookOffersListForBookBag:(NSString*)isbnId
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	//http://api.campusbooks.com/3/rest/bookinfo?key=KrIyLTZ4eZfTUckh6oJx&isbn=0824828917
    
    //	NSString *surl=[NSString stringWithFormat:@"http://api.campusbooks.com/8/rest/prices?&key=KrIyLTZ4eZfTUckh6oJx&isbn=%s",[isbnId UTF8String]];
	//NSString* surl = @"http://api.campusbooks.com/8/rest/bookprices?isbn=0316024961&key=PA52HnTGaTSyizTOq4j1&lat=33.9230790&lon=-83.3389880";
	
    NSString *surl=[NSString stringWithFormat:@"http://api2.campusbooks.com/13/rest/books?f=prices&type=buy&format=json&isbn=%s&coupons=1",[isbnId UTF8String]];
    
    NSString* newAPIURL = [surl stringByAppendingFormat:@"&key=%@", API_NEW_KEY]; 
    
	NSLog(@"URL:%@", newAPIURL);
    
    
    [bookOffersParserXmlForBookBag parseXMLFileAtURL:[NSURL URLWithString:newAPIURL] includeLocalOptionsOnly:NO];
	NSMutableArray* offersList=[bookOffersParserXmlForBookBag getBookOffersList];
    
    NSLog(@"No of results from new API :%d", [offersList count]);
    
    
	//PA52HnTGaTSyizTOq4j1&lat=33.9230790&lon=-83.3389880
	
	NSString* localSearchParameters = @"";
//	NSString* latValue = @"33.9230790";
//	NSString* longValue = @"-83.3389880";
    
	NSString* latValue = @"";
	NSString* longValue = @"";
    
	LocalOptions* localOption = [self getLocalOptions];
//	localOption.optionsValue = 2;
//    localOption.zipCode = @"60062";
    
    if (localOption.optionsValue != 0) {
        
        NSString *localOptionsBaseUrl=[NSString stringWithFormat:@"http://api.campusbooks.com/11/rest/bookprices?isbn=%@",isbnId];
        
        if (localOption.optionsValue ==1)
        {
            if (currentLocation != nil)
            {
                latValue = [NSString stringWithFormat:@"%f", currentLocation.coordinate.latitude];
                longValue = [NSString stringWithFormat:@"%f", currentLocation.coordinate.longitude];
            }
            
            
            localSearchParameters = [NSString stringWithFormat:@"&lat=%s&lon=%s&miles=%d", [latValue UTF8String], [longValue UTF8String], localOption.radiusValue]; 
        }
        else if (localOption.optionsValue ==2)
        {
            localSearchParameters = [NSString stringWithFormat:@"&zip=%s", [localOption.zipCode UTF8String]]; 
        }
        else if (localOption.optionsValue ==3)
        {
            Schools* school = [self getSchoolByName:localOption.schoolName];
            if (school != nil)
            {
                latValue = school.latValue;
                longValue = school.langValue;
                localSearchParameters = [NSString stringWithFormat:@"&lat=%s&lon=%s", [latValue UTF8String], [longValue UTF8String]]; 
            }
        }
        
        NSString* localOptionsURL = [localOptionsBaseUrl stringByAppendingFormat:@"&key=%@%@", API_OLD_KEY, localSearchParameters];
        
        [bookOffersParserXmlForBookBagLocalOptions parseXMLFileAtURL:[NSURL URLWithString:localOptionsURL] ];
        NSMutableArray* offersFromLocalOptions = [bookOffersParserXmlForBookBagLocalOptions getBookOffersList];
        
        NSLog(@"URL For Older API :%@", localOptionsURL);
        NSLog(@"No of results from Older API :%d", [offersFromLocalOptions count]);
        [offersList addObjectsFromArray:offersFromLocalOptions];
    }
    
	
    //    NSData* responseData = [NSData dataWithContentsOfURL:[NSURL URLWithString:surl]];
    //    NSError* error;
    //    NSDictionary* json = [NSJSONSerialization 
    //                          JSONObjectWithData:responseData //1
    //                          
    //                          options:kNilOptions 
    //                          error:&error];
    //    
    //    NSArray* offersList = [json objectForKey:@"loans"]; //2
    //    
    //    NSLog(@"loans: %@", latestLoans); //3
    
	
	[pool release];
	return offersList;
}
/*
-(void)testBookDetails
{
	Book* currentBookInfo = [self getBookInfo:@"0321356683"];
	NSMutableArray* bookOffersList = [self getBookOffersList:@"0321356683"];
//	printf("\n Displaying the details ");
	BookDetailsViewController* detailsViewControler = [[BookDetailsViewController alloc] initWithBook:currentBookInfo withOffers:bookOffersList fromSource:nil];
	[window addSubview:detailsViewControler.view];
	[window makeKeyAndVisible];
	
}
*/

-(NSString *)hostName
{
	// Don't include a scheme. 'http://' will break the reachability checking.
	// Change this value to test the reachability of a different host.
	return @"www.apple.com";
}
- (void)updateStatus
{
	
	self.internetConnectionStatus	= [[Reachability sharedReachability] internetConnectionStatus];
	//	[tableView reloadData];
}

-(NSMutableArray*)getBookBuybackOffersListForSearch:(NSString*)isbnId
{
	//printf("\n getBookBuybackOffersListForSearch");
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	//http://api.campusbooks.com/3/rest/bookinfo?key=KrIyLTZ4eZfTUckh6oJx&isbn=0824828917
	
    NSString *surl=[NSString stringWithFormat:@"http://api2.campusbooks.com/13/rest/books?key=%@&f=prices&format=json&type=buyback&isbn=%s&coupons=1",API_NEW_KEY, [isbnId UTF8String]];
		NSLog(@"Book Offers info url:%s",[surl UTF8String]);
	
	[bookBuybackOffersParserXmlForSearch parseXMLFileAtURL:[NSURL URLWithString:surl]];
	NSMutableArray* offersList=[bookBuybackOffersParserXmlForSearch getBookOffersList];
	[pool release];
	return offersList;
}

-(NSMutableArray*)getBookBuybackOffersListForBookBag:(NSString*)isbnId
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
    NSString *surl=[NSString stringWithFormat:@"http://api2.campusbooks.com/13/rest/books?key=%@&f=prices&format=json&type=buyback&isbn=%s&coupons=1",API_NEW_KEY, [isbnId UTF8String]];
	NSLog(@"Book Offers info url:%s",[surl UTF8String]);
	
	[bookBuybackOffersParserXmlForBookBag parseXMLFileAtURL:[NSURL URLWithString:surl]];
	NSMutableArray* offersList=[bookBuybackOffersParserXmlForBookBag getBookOffersList];
	[pool release];
	return offersList;
}

-(void)deleteFromScanList:(ScanList*)aScanList
{
	sqlite3_stmt *delete_scan_item_statement;
	static char *sqlScanItem = " delete from scanitem where scanListId = ?";
	if (sqlite3_prepare_v2(database, sqlScanItem, -1, &delete_scan_item_statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
	}
	sqlite3_bind_int(delete_scan_item_statement, 1, aScanList.primaryKey);

	
	int success = sqlite3_step(delete_scan_item_statement);
	sqlite3_finalize(delete_scan_item_statement);
	if (success == SQLITE_ERROR) {
		NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
	}
	
	
	sqlite3_stmt *delete_scan_list_statement;
	static char *sqlScanList = " delete from scanlist where pk = ?";
	if (sqlite3_prepare_v2(database, sqlScanList, -1, &delete_scan_list_statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
	}
	sqlite3_bind_int(delete_scan_list_statement, 1, aScanList.primaryKey);
	
	
	success = sqlite3_step(delete_scan_list_statement);
	sqlite3_finalize(delete_scan_list_statement);
	if (success == SQLITE_ERROR) {
		NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
	}
}


-(void)updateScanList:(ScanList*)aScanList
{
	sqlite3_stmt *insert_statement;
	static char *sql = "update  scanlist  set name = ? where pk = ? ";
	if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
		NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
	}
	
	sqlite3_bind_text(insert_statement, 1, [[aScanList name]UTF8String] , -1, SQLITE_TRANSIENT);
	sqlite3_bind_int(insert_statement, 2, aScanList.primaryKey);
	
	int success = sqlite3_step(insert_statement);
	sqlite3_finalize(insert_statement);
	if (success == SQLITE_ERROR) {
		NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
	} 
	
}

- (NSString *) platform
{
	size_t size;
	sysctlbyname("hw.machine", NULL, &size, NULL, 0);
	char *machine = malloc(size);
	sysctlbyname("hw.machine", machine, &size, NULL, 0);
	NSString *platform = [NSString stringWithUTF8String:machine];
	free(machine);
	return platform;
}
- (NSString *) platformString
{
	NSString *platform = [self platform];
	if ([platform isEqualToString:@"iPhone1,1"]) return IPHONE_1G_NAMESTRING;
	if ([platform isEqualToString:@"iPhone1,2"]) return IPHONE_3G_NAMESTRING;
	if ([platform isEqualToString:@"iPhone2,1"]) return IPHONE_3GS_NAMESTRING;
	if ([platform isEqualToString:@"iPod1,1"]) return IPOD_1G_NAMESTRING;
	if ([platform isEqualToString:@"iPod2,1"]) return IPOD_2G_NAMESTRING;
	
	return NULL;
}
/*
-(NSMutableArray*)getDummyList
{
	
	NSMutableArray* dummyList = [[NSMutableArray alloc]init];
	
	[dummyList addObject:@"All Coffee Shops"];
	[dummyList addObject:@"Caffino"];
	[dummyList addObject:@"Caribou"];
	[dummyList addObject:@"CoffeeBeans"];
	[dummyList addObject:@"Community"];
	[dummyList addObject:@"DunkinDonuts"];
	[dummyList addObject:@"DunnBros"];
	[dummyList addObject:@"DutchBros"];
	[dummyList addObject:@"Peets"];
	[dummyList addObject:@"Saxbys"];
	[dummyList addObject:@"Starbucks"];
	[dummyList addObject:@"TimHottons"];
	[dummyList addObject:@"Tully"];
	
	return dummyList;
	
}
 */

-(void)newLocationUpdate:(CLLocation *)newLocation 
{
	[currentLocation release];
	currentLocation=[newLocation copy];
//	printf("\n=====currentLocation=== (%f, %f)",currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
	CLLocationCoordinate2D myLocation ;
	myLocation.latitude = currentLocation.coordinate.latitude;
	myLocation.longitude = currentLocation.coordinate.longitude;
	
//	MKReverseGeocoder* reverseGeocoder =[ [[MKReverseGeocoder alloc]initWithCoordinate:myLocation] autorelease];
//	reverseGeocoder.delegate=self;
//	[reverseGeocoder start];
    
    CLGeocoder *locationGeocoded = [[CLGeocoder alloc] init];
    [locationGeocoded reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        if (!placemark){
            currentZipCode = @"";
        }else{
            // got location but not accurate.
            currentZipcode=[placemark.postalCode copy];
        }
    }];

	
}
-(void)newError:(NSString *)text 
{
	printf("\n Error:%s",[text UTF8String]);
}

-(void)getCurrentLocationZipCode
{

}

@end
