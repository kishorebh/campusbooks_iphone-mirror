//
//  CampusBooksAppDelegate.h
//  CampusBooks
//
//  Created by Admin on 06/06/09.
//  Copyright __MyCompanyName__ 2009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"HomeViewController.h"
#import"ScanHistoryViewController.h"
#import"TransitionView.h"
#import "SearchViewController.h"
#import <QuartzCore/QuartzCore.h>
#import"BooksListParserXml.h"
#import"BookSearchParserXml.h"
#import"BooksOfferParserXml.h"
#import "BooksLocalOfferParserXml.h"
#import"BookSearch.h"
#import"BookInfo.h"
#import"BooksInfoParserXml.h"
#import"Book.h"
#import"Schools.h"
#import"BookListViewController.h"
#import"BookDetailsViewController.h"
#import"BookOfferViewController.h"
#import"BookBagViewController.h"
#import <sqlite3.h>
#import"WishList.h"
#import"MyCollection.h"
#import"ScanList.h"
#import"ScanItem.h"
#import"Reachability.h"
#import"InfoViewController.h"
#import"BookBagViewController.h"
#import"MerchantViewController.h"
#import"BooksBuybackOfferParserXml.h"
#import"FacebookMyLib.h"
#import <MapKit/MapKit.h>
#import"LocalOptions.h"
#import "MyCLController.h"
#import"Pic2BarCodeScanViewController.h"
@interface CampusBooksAppDelegate : NSObject <UIApplicationDelegate, MyCLControllerDelegate> {
	
	sqlite3 *database;
	sqlite3 *database_local;	
	int primaryKey;
	NSMutableArray* wishList;
	NSMutableArray* myCollection;
	FacebookMyLib* facebookObject;
	
	CLLocation* currentLocation;
	NSString* currentZipCode;
	
	NSMutableArray* schoolsList;

	
	UITabBarController* tabBarController ;
	UIViewController* masterViewController;
	TransitionView* rootView;
	HomeViewController* homeViewController;
	SearchViewController* searchViewController;
	InfoViewController* infoViewController;
	BookBagViewController* bookBagViewController;
	BookListViewController* bookListViewController;
	MerchantViewController* merchantViewController;
	UIWindow* window;
	
	UINavigationController* popViewNavigationController;

	BooksListParserXml* booksListParserXml;
	BookSearchParserXml* bookSearchParserXml;
	BookSearchParserXml* isbnSearchParserXml;
	
	BooksInfoParserXml* booksInfoParserXml;

    BooksLocalOfferParserXml* bookOffersParserXmlForSearchLocalOptions;
	BooksLocalOfferParserXml* bookOffersParserXmlForBookBagLocalOptions;

	BooksOfferParserXml* bookOffersParserXmlForSearch;
	BooksInfoParserXml* booksInfoParserXmlForSearch;
	BooksOfferParserXml* bookOffersParserXmlForBookBag;
	BooksInfoParserXml* booksInfoParserXmlForBookBag;

	BooksBuybackOfferParserXml* bookBuybackOffersParserXmlForSearch;
	BooksBuybackOfferParserXml* bookBuybackOffersParserXmlForBookBag;

	NetworkStatus internetConnectionStatus;
	
	NSMutableArray* scanListList;
	NSMutableArray* scanItemList;
	LocalOptions* localOptions;
	Pic2BarCodeScanViewController* barcodeViewController;
	ScanHistoryViewController* scanHistoryViewController;
}
@property NetworkStatus internetConnectionStatus;
@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,retain)UIViewController* masterViewController;
@property(nonatomic,retain)FacebookMyLib* facebookObject;
@property(nonatomic,retain) HomeViewController* homeViewController;
@property(nonatomic,retain) SearchViewController* searchViewController;
@property(nonatomic, retain) BookListViewController* bookListViewController;
@property(nonatomic, retain) BookBagViewController* bookBagViewController;
@property(nonatomic, retain) InfoViewController* infoViewController;
@property(nonatomic, retain)ScanHistoryViewController* scanHistoryViewController;
@property(nonatomic, retain) MerchantViewController* merchantViewController;
@property(nonatomic, retain) UITabBarController* tabBarController ;
@property(nonatomic) sqlite3 *database;
@property(nonatomic) sqlite3 *database_local;
@property(nonatomic, retain) CLLocation* currentLocation;
@property(nonatomic, retain)NSString* currentZipcode;
@property(nonatomic, retain) Pic2BarCodeScanViewController* barcodeViewController;
- (void)updateStatus;
-(NSString *)hostName;


-(NSMutableArray *)doBookSearch:(NSString*) aTitle byAuthor:(NSString*) aAuthor withKeyword:(NSString*)aKeyword forISBN:(NSString*)aIsbnId forPage:(NSInteger)aPage;

-(void)initializeObjects;
- (void)createEditableCopyOfDatabaseIfNeeded;

-(Book*)getBookInfoForSearch:(NSString*)isbnId;
-(Book*)getBookInfoForBookBag:(NSString*)isbnId;

-(NSMutableArray*)getBookOffersListForSearch:(NSString*)isbnId;
-(NSMutableArray*)getBookOffersListForBookBag:(NSString*)isbnId;

//-(NSString *)getCampusBooksCoverURLByISBN:(NSString*) isbnId;

-(NSMutableArray*) getMyWishList:(NSInteger)aOffset;
-(NSMutableArray*) getMyCollection:(NSInteger)aOffset;


- (void)addToWishList:(WishList *)awish ;
- (void)removeFromWishList:(NSString*)aIsbnId;
- (void)addToMyCollection:(MyCollection *)awish ;
- (void)removeFromMyCollection:(NSString *)aIsbnId;
-(BOOL)isItemInWishList:(NSString*)aIsbnId;
-(BOOL)isItemInMyCollection:(NSString*)aIsbnId;

-(Book*)getBookInfo:(NSString*)isbnId;
-(NSMutableArray*)getBookBuybackOffersListForSearch:(NSString*)isbnId;
-(NSMutableArray*)getBookBuybackOffersListForBookBag:(NSString*)isbnId;

-(void)deleteFromScanList:(ScanList*)aScanList;
- (void)addToScanItem:(ScanItem*)aScanItem ;
- (void)addToScanList:(ScanList*)aList ;
-(NSMutableArray*) getScanItemList:(NSInteger)aScanListId;
-(NSMutableArray*) getScanListList;
-(void)updateScanList:(ScanList*)aScanList;
- (NSString *) platformString;

-(void)addLocalOptionsToDatabaseIfNeeded;


-(NSMutableArray*)getSchoolsList;

-(LocalOptions*)getLocalOptions;
- (void)updateLocalOptions:(LocalOptions *)aupdateObj;
//-(NSMutableArray*)getDummyList;
- (void)createEditableCopyOfLocalDatabaseIfNeeded;
-(Schools*)getSchoolByName:(NSString*)aSchooName;
//-(ScanDetailsViewController*)getScanDetailsViewController;
@end

