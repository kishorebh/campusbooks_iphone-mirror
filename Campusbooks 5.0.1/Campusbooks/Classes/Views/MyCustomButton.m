/*
 * Created by Admin on 06/08/08.
 * Copyright 2008 Bluepal. All rights reserved.
 *
 */

#import "MyCustomButton.h"


@implementation MyCustomButton
//@synthesize index;
@synthesize indexPath, integer1, coffeeShopName;
/*
-(MyCustomButton*) initWithIndex:(NSInteger)aIndex
{
	if (self = [super init])
	{
		self.index = aIndex;
		
	}
	return self;
}
*/

-(MyCustomButton*) initWithIndexPath:(NSIndexPath*)aIndexPath
{
	if (self = [super init])
	{
		self.indexPath = [aIndexPath copy];
	}
	return self;
}
-(MyCustomButton*) initWithInteger:(NSInteger)aInteger1 shopName:(NSString*)shopName
{
	if (self = [super init])
	{
		self.integer1 = aInteger1;
		self.coffeeShopName = [shopName copy];
	}
	return self;
}

-(void)dealloc
{
	[indexPath release];
	[super dealloc];
}
@end
