/*
 * Created by Admin on 06/08/08.
 * Copyright 2008 Bluepal. All rights reserved.
 *
 */

#import <UIKit/UIKit.h>


@interface MyCustomButton : UIButton {
	//NSInteger index ;
	NSIndexPath* indexPath;
	NSString* coffeeShopName;
	NSInteger integer1;
	
}
//@property (nonatomic) NSInteger index ;
@property (nonatomic, retain) NSIndexPath* indexPath;
@property (nonatomic, retain) NSString* coffeeShopName;
@property (nonatomic) NSInteger integer1;
//-(MyCustomButton*) initWithIndex:(NSInteger)aIndex ;
-(MyCustomButton*) initWithIndexPath:(NSIndexPath*)aIndexPath ;
-(MyCustomButton*) initWithInteger:(NSInteger)aInteger1 shopName:(NSString*)shopName;
@end


  