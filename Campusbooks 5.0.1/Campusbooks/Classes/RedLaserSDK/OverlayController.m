/* (c) 2009 eBay Inc. */

#import "OverlayController.h"
#if defined(RL_LOCALIZED)
#import "Localization.h"
#endif

@implementation OverlayController

//@synthesize parentPicker;
@synthesize overlayImage;
@synthesize cameraView;

- (void)barcodePickerController:(BarcodePickerController*)picker statusUpdated:(NSDictionary*)status
{
	//self._EAN = [status objectForKey:@"EAN"];
	//self._Message = [status objectForKey:@"Message"];
	//self._Image = [status objectForKey:@"Image"];
	
	BOOL isValid = [(NSNumber*)[status objectForKey:@"Valid"] boolValue];
	BOOL inRange = [(NSNumber*)[status objectForKey:@"InRange"] boolValue];
	
	[self setArrows:inRange animated:TRUE];
	
	BOOL saveDebug = [[NSUserDefaults standardUserDefaults] boolForKey:@"save_pref"];
	
	if(isValid & !saveDebug && isScanning) {
		isScanning  = NO;
		[self beepOrVibrate];

		[progressInd startAnimating];
		fetchResultsLabel.text = @"Retrieving results...";
        
        NSLog(@"Status;%@", status);

        [self.parentPicker doneScanning];

//		[picker returnBarcode:[[status objectForKey:@"EAN"] copy] withInfo:status];
	}
	
}

-(void)resetScanning
{
	fetchResultsLabel.text = @"";
	[progressInd stopAnimating];
	isScanning = YES;
}
- (void)barcodePickerControllerWillAppear:(BarcodePickerController*)picker {
	//NSLog(@"I am in barcodePickerController");
	[self resetScanning];
	if (![picker hasFlash]) {
		[flashButton setEnabled:NO];
		NSMutableArray * items = [[toolBar items] mutableCopy];
		[items removeObject:flashButton];
		[toolBar setItems:items animated:NO];
		[items release];
	}else{
		[flashButton setEnabled:YES];
	}
}

// Checks if the phone is in vibrate mode, in which case the scanner
// vibrates instead of beeps.

-(void)beepOrVibrate
{
	if(!_isSilent)
	{
		UInt32 routeSize = sizeof (CFStringRef);
		CFStringRef route = NULL;
		AudioSessionGetProperty (
								 kAudioSessionProperty_AudioRoute,
								 &routeSize,
								 &route
								 );
		
		if (CFStringGetLength(route) == 0) {
			AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
		}
		else {
			AudioServicesPlaySystemSound(_scanSuccessSound);
		}
	}
}

-(IBAction)cancelPressed
{
    [self.parentPicker doneScanning];
    
    NSLog(@"Done Scanning is called on:%@", self.parentPicker);
    fetchResultsLabel.text = @"";
//    self.parentPicker.delegate
}

- (IBAction) flashPressed 
{
	if ([flashButton style] == UIBarButtonItemStyleBordered) 
	{
		[flashButton setStyle:UIBarButtonItemStyleDone];
		[self.parentPicker setTorchState:YES];
	} else 
	{
		[flashButton setStyle:UIBarButtonItemStyleBordered];
		[self.parentPicker setTorchState:NO];
	}
}
-(void)setArrows:(BOOL)green animated:(BOOL)animate
{	
	if(green && isGreen) return;
	
	isGreen = green;
	textCue.textColor = [UIColor whiteColor];
	//textCue.backgroundColor = [UIColor lightGrayColor];
	textCue.font = [UIFont boldSystemFontOfSize:14];

	UIImageView* otherview1, *otherview2;
	UIImageView* focusview1, *focusview2;

	if(isGreen)
	{
		otherview1 = whiteTopArrow;	otherview2 = whiteBottomArrow;
		focusview1 = greenTopArrow; focusview2 = greenBottomArrow;
	}
	else
	{
		otherview1 = greenTopArrow; otherview2 = greenBottomArrow;
		focusview1 = whiteTopArrow; focusview2 = whiteBottomArrow;
	}
	
	if(animate)
	{
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.15];
		[UIView setAnimationBeginsFromCurrentState: YES];
	}
	
	[focusview1 setAlpha:1];
	[focusview2 setAlpha:1];
	
	[otherview1 setAlpha:0];
	[otherview2 setAlpha:0];
	
	if(animate) {
		[UIView commitAnimations];
	}
	
}


- (void)viewDidLoad {
    [super viewDidLoad];
	isGreen = FALSE;
	[self setArrows:false animated:NO];
	
	self->_isSilent = [[NSUserDefaults standardUserDefaults] boolForKey:@"silent_pref"];
		
	if(!_isSilent) // If silent, no need to do this.
	{
		NSURL* aFileURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"beep" ofType:@"wav"] isDirectory:NO]; 
		AudioServicesCreateSystemSoundID((CFURLRef)aFileURL, &_scanSuccessSound);
		
		UInt32 flag = 0;
		OSStatus error = AudioServicesSetProperty(kAudioServicesPropertyIsUISound,
												  sizeof(UInt32),
												  &_scanSuccessSound,
												  sizeof(UInt32),
												  &flag);
		
		float aBufferLength = 1.0; // In seconds
		error = AudioSessionSetProperty(kAudioSessionProperty_PreferredHardwareIOBufferDuration, 
								sizeof(aBufferLength), &aBufferLength);
		
		/* Create and warm up an audio session */
		AudioSessionInitialize(NULL, NULL, NULL, NULL);
		AudioSessionSetActive(TRUE);
	}
	
	CGRect activityViewframe = CGRectMake(15.0,442.5, 35,35);
	progressInd = [[UIActivityIndicatorView alloc] initWithFrame:activityViewframe];
	progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
	progressInd.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
									UIViewAutoresizingFlexibleRightMargin |
									UIViewAutoresizingFlexibleTopMargin |
									UIViewAutoresizingFlexibleBottomMargin);
	progressInd.hidesWhenStopped = YES;
	progressInd.hidden = YES;
	[self.view addSubview:progressInd];
	[progressInd release];
	

	
	fetchResultsLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, 422,140, 33)];
	fetchResultsLabel.font = [UIFont boldSystemFontOfSize:15];
	fetchResultsLabel.textAlignment = UITextAlignmentLeft;
	fetchResultsLabel.textColor = [UIColor whiteColor];
	fetchResultsLabel.backgroundColor = [UIColor clearColor];
	[self.view addSubview:fetchResultsLabel];
	//[fetchResultsLabel release];
	
//	[parentPicker setCameraView:cameraView];
}

- (void)viewDidUnload {
	
	self.overlayImage = nil;
	
	AudioServicesDisposeSystemSoundID(_scanSuccessSound);
	if(!_isSilent) { AudioSessionSetActive(FALSE); }
}

- (void)dealloc {
//	[cameraView release];
    [super dealloc];
}

@end
