/* (c) 2009 eBay Inc. */

#import <UIKit/UIKit.h>
#import "RedLaserSDK.h"
#import "AudioToolbox/AudioServices.h"

@interface OverlayController : CameraOverlayViewController {
	
	IBOutlet UIImageView * greenTopArrow;
	IBOutlet UIImageView * greenBottomArrow;
	
	IBOutlet UIImageView * whiteTopArrow;
	IBOutlet UIImageView * whiteBottomArrow;
	
	IBOutlet UIImageView * overlayImage;
	
	IBOutlet UILabel * textCue;
	
	BOOL isGreen;
	
	SystemSoundID _scanSuccessSound;
	BOOL _isSilent;
	
	IBOutlet UILabel * latestResultLabel;

//	BarcodePickerController * parentPicker;
	IBOutlet UIView * cameraView;
	IBOutlet UIBarButtonItem *flashButton;
	IBOutlet UIToolbar *toolBar;
	IBOutlet UIBarButtonItem *cancelButton;
	UILabel* fetchResultsLabel;
	UIActivityIndicatorView* progressInd;
	BOOL isScanning;

}

//@property ( assign) BarcodePickerController * parentPicker;
@property (nonatomic, assign) UIImageView * overlayImage;
@property (nonatomic, retain) UIView *cameraView;

-(void)resetScanning;
-(IBAction)cancelPressed;
-(IBAction)flashPressed;
-(void)beepOrVibrate;
-(void) setArrows:(BOOL)green animated:(BOOL)animate;


@end
