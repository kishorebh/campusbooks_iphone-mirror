//
//  UIDevice-hardware.m
//  CampusBooks
//
//  Created by Admin on 24/03/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "UIDevice-hardware.h"


#import "UIDevice-hardware.h"
#include <sys/types.h>
#include <sys/sysctl.h>
@implementation UIDevice (Hardware)

 /*
 8. Platforms
 9. iPhone1,1 -> iPhone 1G
 10. iPhone1,2 -> iPhone 3G
 11. iPod1,1 -> iPod touch 1G
 12. iPod2,1 -> iPod touch 2G
 13. */

 - (NSString *) platform
{
	 size_t size;
	 sysctlbyname("hw.machine", NULL, &size, NULL, 0);
	 char *machine = malloc(size);
	 sysctlbyname("hw.machine", machine, &size, NULL, 0);
	 NSString *platform = [NSString stringWithUTF8String:machine];
	 free(machine);
	 return platform;
 }

 - (NSString *) platformString
{
	 NSString *platform = [self platform];
	 if ([platform isEqualToString:@"iPhone1,1"]) return IPHONE_1G_NAMESTRING;
	 if ([platform isEqualToString:@"iPhone1,2"]) return IPHONE_3G_NAMESTRING;
	 if ([platform isEqualToString:@"iPod1,1"]) return IPOD_1G_NAMESTRING;
	if ([platform isEqualToString:@"iPod2,1"]) return IPOD_2G_NAMESTRING;
	return NULL;
}

@end
