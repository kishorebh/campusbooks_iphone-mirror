//
//  HTMLUtil.m
//  Dictionary
//
//  Created by Admin on 02/11/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "HTMLUtil.h"


@implementation HTMLUtil
static NSMutableDictionary* escapeSequenceValues;
static NSMutableDictionary* decodeSequenceValues;
+ (void)initialize

{
	escapeSequenceValues =[ [NSMutableDictionary alloc] init]; 
	[escapeSequenceValues setObject:@"€" forKey:@"&euro;"];
	 [escapeSequenceValues setObject:@"&" forKey:@"&amp;"];
	 [escapeSequenceValues setObject:@"<" forKey:@"&lt;"];
	 [escapeSequenceValues setObject:@">" forKey:@"&gt;"];
	 [escapeSequenceValues setObject:@"" forKey:@"&nbsp;"];
	 [escapeSequenceValues setObject:@"¡" forKey:@"&iexcl;"];
	[escapeSequenceValues setObject:@"¢" forKey:@"&cent;"];
	[escapeSequenceValues setObject:@"£" forKey:@"&pound;"];
	[escapeSequenceValues setObject:@"¤" forKey:@"&curren;"];
	[escapeSequenceValues setObject:@"¥" forKey:@"&yen;"];
	[escapeSequenceValues setObject:@"¦" forKey:@"&brvbar;"];
	[escapeSequenceValues setObject:@"§" forKey:@"&sect;"];
	[escapeSequenceValues setObject:@"¨" forKey:@"&uml;"];
	[escapeSequenceValues setObject:@"©" forKey:@"&copy;"];
	[escapeSequenceValues setObject:@"ª" forKey:@"&ordf;"];
	[escapeSequenceValues setObject:@"¬" forKey:@"&not;"];
	[escapeSequenceValues setObject:@"­" forKey:@"&shy;"];
	[escapeSequenceValues setObject:@"®" forKey:@"&reg;"];
	[escapeSequenceValues setObject:@"¯" forKey:@"&macr;"];
	[escapeSequenceValues setObject:@"°" forKey:@"&deg;"];
	[escapeSequenceValues setObject:@"±" forKey:@"&plusmn;"];
	[escapeSequenceValues setObject:@"²" forKey:@"&sup2;"];
	[escapeSequenceValues setObject:@"³" forKey:@"&sup3;"];
	[escapeSequenceValues setObject:@"´" forKey:@"&acute;"];
	[escapeSequenceValues setObject:@"µ" forKey:@"&micro;"];
	[escapeSequenceValues setObject:@"¶" forKey:@"&para;"];
	[escapeSequenceValues setObject:@"·" forKey:@"&middot;"];
	[escapeSequenceValues setObject:@"¸" forKey:@"&cedil;"];
	[escapeSequenceValues setObject:@"¹" forKey:@"&sup1;"];
	[escapeSequenceValues setObject:@"º" forKey:@"&ordm;"];
	[escapeSequenceValues setObject:@"»" forKey:@"&raquo;"];
	[escapeSequenceValues setObject:@"¼" forKey:@"&frac14;"];
	[escapeSequenceValues setObject:@"½" forKey:@"&frac12;"];
	[escapeSequenceValues setObject:@"¾" forKey:@"&frac34;"];
	[escapeSequenceValues setObject:@"¿" forKey:@"&iquest;"];
	[escapeSequenceValues setObject:@"À" forKey:@"&Agrave;"];
	[escapeSequenceValues setObject:@"Á" forKey:@"&Aacute;"];
	[escapeSequenceValues setObject:@"Â" forKey:@" Â"];
	[escapeSequenceValues setObject:@"Ã" forKey:@"&Atilde;"];
	[escapeSequenceValues setObject:@"Ä" forKey:@"&Auml;"];
	[escapeSequenceValues setObject:@"Å" forKey:@"&Aring;"];
	[escapeSequenceValues setObject:@"Æ" forKey:@"&AElig;"];
	[escapeSequenceValues setObject:@"Ç" forKey:@"&Ccedil;"];
	[escapeSequenceValues setObject:@"È" forKey:@"&Egrave;"];
	[escapeSequenceValues setObject:@"É" forKey:@"&Eacute;"];
	[escapeSequenceValues setObject:@"Ê" forKey:@"&Ecirc;"];
	[escapeSequenceValues setObject:@"Ë" forKey:@" Ë"];
	[escapeSequenceValues setObject:@"Ì" forKey:@"&Igrave;"];
	[escapeSequenceValues setObject:@"Í" forKey:@"&Iacute;"];
	[escapeSequenceValues setObject:@"Î" forKey:@"&Icirc;"];
	[escapeSequenceValues setObject:@"Ï" forKey:@"&Iuml;"];
	[escapeSequenceValues setObject:@"Ð" forKey:@"&ETH;"];
	[escapeSequenceValues setObject:@"Ñ" forKey:@"&Ntilde;"];
	[escapeSequenceValues setObject:@"Ò" forKey:@"&Ograve;"];
	[escapeSequenceValues setObject:@"Ó" forKey:@"&Oacute;"];
	[escapeSequenceValues setObject:@"Ô" forKey:@"&Ocirc;"];
	[escapeSequenceValues setObject:@"Õ" forKey:@"&Otilde;"];
	[escapeSequenceValues setObject:@"Ö" forKey:@"&Ouml;"];
	[escapeSequenceValues setObject:@"×" forKey:@"&times;"];
	[escapeSequenceValues setObject:@"Ø" forKey:@"&Oslash;"];
	[escapeSequenceValues setObject:@"Ù" forKey:@"&Ugrave;"];
	[escapeSequenceValues setObject:@"Ú" forKey:@"&Uacute;"];
	[escapeSequenceValues setObject:@"Û" forKey:@"&Ucirc;"];
	[escapeSequenceValues setObject:@"Ü" forKey:@"&Uuml;"];
	[escapeSequenceValues setObject:@"Ý" forKey:@"&Yacute;"];
	[escapeSequenceValues setObject:@"Þ" forKey:@"&THORN;"];
	[escapeSequenceValues setObject:@"ß" forKey:@"&szlig;"];
	[escapeSequenceValues setObject:@"à" forKey:@"&agrave;"];
	[escapeSequenceValues setObject:@"á" forKey:@"&aacute;"];
	[escapeSequenceValues setObject:@"â" forKey:@"&acirc;"];
	[escapeSequenceValues setObject:@"ã" forKey:@"&atilde;"];
	[escapeSequenceValues setObject:@"ä" forKey:@"&auml;"];
	[escapeSequenceValues setObject:@"å" forKey:@"&aring;"];
	[escapeSequenceValues setObject:@"æ" forKey:@"&aelig;"];
	[escapeSequenceValues setObject:@"ç" forKey:@"&ccedil;"];
	[escapeSequenceValues setObject:@"è" forKey:@"&egrave;"];
	[escapeSequenceValues setObject:@"é" forKey:@"&eacute;"];
	[escapeSequenceValues setObject:@"ê" forKey:@"&ecirc;"];
	[escapeSequenceValues setObject:@"ë" forKey:@"&euml;"];
	[escapeSequenceValues setObject:@"ì" forKey:@"&igrave;"];
	[escapeSequenceValues setObject:@"í" forKey:@"&iacute;"];
	[escapeSequenceValues setObject:@"î" forKey:@"&icirc;"];
	[escapeSequenceValues setObject:@"ï" forKey:@"&iuml;"];
	[escapeSequenceValues setObject:@"ð" forKey:@"&eth;"];
	[escapeSequenceValues setObject:@"ñ" forKey:@"&ntilde;"];
	[escapeSequenceValues setObject:@"ò" forKey:@"&ograve;"];
	[escapeSequenceValues setObject:@"ó" forKey:@"&oacute;"];
	[escapeSequenceValues setObject:@"ô" forKey:@"&ocirc;"];
	[escapeSequenceValues setObject:@"õ" forKey:@"&otilde;"];
	[escapeSequenceValues setObject:@"ö" forKey:@"&ouml;"];
	[escapeSequenceValues setObject:@"÷" forKey:@"&divide;"];
	[escapeSequenceValues setObject:@"ø" forKey:@"&oslash;"];
	[escapeSequenceValues setObject:@"ù" forKey:@"&ugrave;"];
	[escapeSequenceValues setObject:@"ú" forKey:@"&uacute;"];
	[escapeSequenceValues setObject:@"û" forKey:@"&ucirc;"];
	[escapeSequenceValues setObject:@"ü" forKey:@"&uuml;"];
	[escapeSequenceValues setObject:@"ý" forKey:@"&yacute;"];
	[escapeSequenceValues setObject:@"þ" forKey:@"&thorn;"];
	[escapeSequenceValues setObject:@"'" forKey:@"&apos;"];	
	

	
	decodeSequenceValues =[ [NSMutableDictionary alloc] init]; 
	[decodeSequenceValues	setObject:@"&euro;" forKey:@"€"];
	[decodeSequenceValues	setObject:@"&amp;" forKey:@"&"];
	[decodeSequenceValues	setObject:@"&lt;" forKey:@"<"];
	[decodeSequenceValues	setObject:@"&gt;" forKey:@">"];
	[decodeSequenceValues	setObject:@"&nbsp;" forKey:@""];
	[decodeSequenceValues	setObject:@"&iexcl;" forKey:@"¡"];
	[decodeSequenceValues	setObject:@"&cent;" forKey:@"¢"];
	[decodeSequenceValues	setObject:@"&pound;" forKey:@"£"];
	[decodeSequenceValues	setObject:@"&curren;" forKey:@"¤"];
	[decodeSequenceValues	setObject:@"&yen;" forKey:@"¥"];
	[decodeSequenceValues	setObject:@"&brvbar;" forKey:@"¦"];
	[decodeSequenceValues	setObject:@"&sect;" forKey:@"§"];
	[decodeSequenceValues	setObject:@"&uml;" forKey:@"¨"];
	[decodeSequenceValues	setObject:@"&copy;" forKey:@"©"];
	[decodeSequenceValues	setObject:@"&ordf;" forKey:@"ª"];
	[decodeSequenceValues	setObject:@"&not;" forKey:@"¬"];
	[decodeSequenceValues	setObject:@"&shy;" forKey:@"­"];
	[decodeSequenceValues	setObject:@"&reg;" forKey:@"®"];
	[decodeSequenceValues	setObject:@"&macr;" forKey:@"¯"];
	[decodeSequenceValues	setObject:@"&deg;" forKey:@"°"];
	[decodeSequenceValues	setObject:@"&plusmn;" forKey:@"±"];
	[decodeSequenceValues	setObject:@"&sup2;" forKey:@"²"];
	[decodeSequenceValues	setObject:@"&sup3;" forKey:@"³"];
	[decodeSequenceValues	setObject:@"&acute;" forKey:@"´"];
	[decodeSequenceValues	setObject:@"&micro;" forKey:@"µ"];
	[decodeSequenceValues	setObject:@"&para;" forKey:@"¶"];
	[decodeSequenceValues	setObject:@"&middot;" forKey:@"·"];
	[decodeSequenceValues	setObject:@"&cedil;" forKey:@"¸"];
	[decodeSequenceValues	setObject:@"&sup1;" forKey:@"¹"];
	[decodeSequenceValues	setObject:@"&ordm;" forKey:@"º"];
	[decodeSequenceValues	setObject:@"&raquo;" forKey:@"»"];
	[decodeSequenceValues	setObject:@"&frac14;" forKey:@"¼"];
	[decodeSequenceValues	setObject:@"&frac12;" forKey:@"½"];
	[decodeSequenceValues	setObject:@"&frac34;" forKey:@"¾"];
	[decodeSequenceValues	setObject:@"&iquest;" forKey:@"¿"];
	[decodeSequenceValues	setObject:@"&Agrave;" forKey:@"À"];
	[decodeSequenceValues	setObject:@"&Aacute;" forKey:@"Á"];
	[decodeSequenceValues	setObject:@"&Atilde;" forKey:@"Ã"];
	[decodeSequenceValues	setObject:@"&Auml;" forKey:@"Ä"];
	[decodeSequenceValues	setObject:@"&Aring;" forKey:@"Å"];
	[decodeSequenceValues	setObject:@"&AElig;" forKey:@"Æ"];
	[decodeSequenceValues	setObject:@"&Ccedil;" forKey:@"Ç"];
	[decodeSequenceValues	setObject:@"&Egrave;" forKey:@"È"];
	[decodeSequenceValues	setObject:@"&Eacute;" forKey:@"É"];
	[decodeSequenceValues	setObject:@"&Ecirc;" forKey:@"Ê"];
	[decodeSequenceValues	setObject:@"&Igrave;" forKey:@"Ì"];
	[decodeSequenceValues	setObject:@"&Iacute;" forKey:@"Í"];
	[decodeSequenceValues	setObject:@"&Icirc;" forKey:@"Î"];
	[decodeSequenceValues	setObject:@"&Iuml;" forKey:@"Ï"];
	[decodeSequenceValues	setObject:@"&ETH;" forKey:@"Ð"];
	[decodeSequenceValues	setObject:@"&Ntilde;" forKey:@"Ñ"];
	[decodeSequenceValues	setObject:@"&Ograve;" forKey:@"Ò"];
	[decodeSequenceValues	setObject:@"&Oacute;" forKey:@"Ó"];
	[decodeSequenceValues	setObject:@"&Ocirc;" forKey:@"Ô"];
	[decodeSequenceValues	setObject:@"&Otilde;" forKey:@"Õ"];
	[decodeSequenceValues	setObject:@"&Ouml;" forKey:@"Ö"];
	[decodeSequenceValues	setObject:@"&times;" forKey:@"×"];
	[decodeSequenceValues	setObject:@"&Oslash;" forKey:@"Ø"];
	[decodeSequenceValues	setObject:@"&Ugrave;" forKey:@"Ù"];
	[decodeSequenceValues	setObject:@"&Uacute;" forKey:@"Ú"];
	[decodeSequenceValues	setObject:@"&Ucirc;" forKey:@"Û"];
	[decodeSequenceValues	setObject:@"&Uuml;" forKey:@"Ü"];
	[decodeSequenceValues	setObject:@"&Yacute;" forKey:@"Ý"];
	[decodeSequenceValues	setObject:@"&THORN;" forKey:@"Þ"];
	[decodeSequenceValues	setObject:@"&szlig;" forKey:@"ß"];
	[decodeSequenceValues	setObject:@"&agrave;" forKey:@"à"];
	[decodeSequenceValues	setObject:@"&aacute;" forKey:@"á"];
	[decodeSequenceValues	setObject:@"&acirc;" forKey:@"â"];
	[decodeSequenceValues	setObject:@"&atilde;" forKey:@"ã"];
	[decodeSequenceValues	setObject:@"&auml;" forKey:@"ä"];
	[decodeSequenceValues	setObject:@"&aring;" forKey:@"å"];
	[decodeSequenceValues	setObject:@"&aelig;" forKey:@"æ"];
	[decodeSequenceValues	setObject:@"&ccedil;" forKey:@"ç"];
	[decodeSequenceValues	setObject:@"&egrave;" forKey:@"è"];
	[decodeSequenceValues	setObject:@"&eacute;" forKey:@"é"];
	[decodeSequenceValues	setObject:@"&ecirc;" forKey:@"ê"];
	[decodeSequenceValues	setObject:@"&euml;" forKey:@"ë"];
	[decodeSequenceValues	setObject:@"&igrave;" forKey:@"ì"];
	[decodeSequenceValues	setObject:@"&iacute;" forKey:@"í"];
	[decodeSequenceValues	setObject:@"&icirc;" forKey:@"î"];
	[decodeSequenceValues	setObject:@"&iuml;" forKey:@"ï"];
	[decodeSequenceValues	setObject:@"&eth;" forKey:@"ð"];
	[decodeSequenceValues	setObject:@"&ntilde;" forKey:@"ñ"];
	[decodeSequenceValues	setObject:@"&ograve;" forKey:@"ò"];
	[decodeSequenceValues	setObject:@"&oacute;" forKey:@"ó"];
	[decodeSequenceValues	setObject:@"&ocirc;" forKey:@"ô"];
	[decodeSequenceValues	setObject:@"&otilde;" forKey:@"õ"];
	[decodeSequenceValues	setObject:@"&ouml;" forKey:@"ö"];
	[decodeSequenceValues	setObject:@"&divide;" forKey:@"÷"];
	[decodeSequenceValues	setObject:@"&oslash;" forKey:@"ø"];
	[decodeSequenceValues	setObject:@"&ugrave;" forKey:@"ù"];
	[decodeSequenceValues	setObject:@"&uacute;" forKey:@"ú"];
	[decodeSequenceValues	setObject:@"&ucirc;" forKey:@"û"];
	[decodeSequenceValues	setObject:@"&uuml;" forKey:@"ü"];
	[decodeSequenceValues	setObject:@"&yacute;" forKey:@"ý"];
	[decodeSequenceValues	setObject:@"&thorn;" forKey:@"þ"];
	[decodeSequenceValues	setObject:@"&apos;" forKey:@"'"];
	
}
+(NSString*) decodeEscapeSequences:(NSString*) encodedStr
{
	NSString* tmpString =[ [encodedStr copy] autorelease];
	NSMutableArray* array = (NSMutableArray*)[escapeSequenceValues allKeys];
	for (NSString* currentElement in array)
	{
		NSString* replacementString = [ escapeSequenceValues objectForKey:currentElement];
		//printf("replacing %s with %s", [currentElement UTF8String], [replacementString UTF8String]);
		tmpString = [tmpString stringByReplacingOccurrencesOfString:currentElement withString:replacementString];
	}
	
	return tmpString ? tmpString : @"";
}
+(NSString*) encodeEscapeSequences:(NSString*) encodedStr
{
	NSString* tmpString = [[encodedStr copy] autorelease];
	NSMutableArray* array = (NSMutableArray*)[decodeSequenceValues allKeys];
	for (NSString* currentElement in array)
	{
		NSString* replacementString = [ decodeSequenceValues objectForKey:currentElement];
		//printf("replacing %s with %s", [currentElement UTF8String], [replacementString UTF8String]);
		tmpString = [tmpString stringByReplacingOccurrencesOfString:currentElement withString:replacementString];
	}
	
	return tmpString;
}
@end
