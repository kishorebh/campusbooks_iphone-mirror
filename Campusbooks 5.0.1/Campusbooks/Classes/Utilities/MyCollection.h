//
//  WishList.h
//  CampusBooks
//
//  Created by Admin on 20/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface MyCollection : NSObject {
	
	NSString* bookName;
	NSString* isbnId10;
	NSString* authorName;
		NSString* imageUrl;
	int primaryKey;
	sqlite3 *database;
}
@property(nonatomic)NSInteger primaryKey;
@property(nonatomic,retain)NSString* bookName;
@property(nonatomic, retain)NSString* isbnId10;
@property(nonatomic,retain)NSString* authorName;
@property(nonatomic,retain)NSString* imageUrl;
- (id)initWithPrimaryKey:(NSInteger)apk database:(sqlite3 *)db;
@end
