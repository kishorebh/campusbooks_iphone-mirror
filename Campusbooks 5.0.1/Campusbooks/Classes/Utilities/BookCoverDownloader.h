//
//  BookCoverDownloader.h
//  CampusBooks
//
//  Created by Admin on 17/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import"Book.h"

@class BookCoverDownloader;

@protocol BookCoverDownloaderDelegate<NSObject>

-(void)didReceiveImage:(UIImage*)aImage forIsbn:(NSString*)aISBN;

@end

@interface BookCoverDownloader : NSObject {
	
	NSMutableData*	result;
	id <BookCoverDownloaderDelegate> delegate;
	NSString* isbnId;
	NSURLConnection *connection;
	BOOL usingCoversAPI;
	BOOL canceled;
}
- (void)getCoverForBook:(NSString*)aISBN withUrl:(NSString*)aUrl delegate:(id <BookCoverDownloaderDelegate>)delegate;
-(void)getImageCover:(NSString*)aUrl;

- (void)cancel;
- (BOOL)canceled;


@property (nonatomic, retain) NSURLConnection *connection;
@property (nonatomic, retain) NSString* isbnId;
@end
