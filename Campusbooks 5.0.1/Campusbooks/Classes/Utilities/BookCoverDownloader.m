//
//  BookCoverDownloader.m
//  CampusBooks
//
//  Created by Admin on 17/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "BookCoverDownloader.h"
#import"CampusBooksAppDelegate.h"


@implementation BookCoverDownloader

@synthesize connection;
@synthesize isbnId;

-(id)init
{
	self = [super init];
	if(self)
	{
		result = [[NSMutableData alloc] initWithCapacity:128];
		canceled = NO;
		usingCoversAPI = YES;
	}
	return self;
}

-(void)dealloc
{
	if(delegate)
		[delegate release];
	if(isbnId)
		[isbnId release];
	self.connection = nil;
	[self.connection release];
	[result  release];
	[super dealloc];
}

-(void)getImageCover:(NSString*)aUrl
{
	printf("\n in getImageCover ");

	NSURL* url = [NSURL URLWithString:aUrl];
	NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
	
	self.connection = [[NSURLConnection alloc] initWithRequest:req 
													   delegate:self 
											   startImmediately:YES];
	if (!self.connection) 
	{
		printf("\n Connection is not made properly");
		[delegate didReceiveImage:nil forIsbn:isbnId];
	}
}
- (void)getCoverForBook:(NSString*)aISBN  withUrl:(NSString*)aUrl delegate:(id <BookCoverDownloaderDelegate>)dlgt;
{
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];
	[self retain];
	delegate = [dlgt retain];
	isbnId = [aISBN copy];
	[self getImageCover:aUrl];
	[pool release];
	
}

#pragma mark NSURLConnection delegate methods


- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	printf("\n Connection Reponse is Received:");
    [result setLength:0];
}


- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	printf("\n Data Received");
    [result appendData:data];
}


- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
		printf("\n Error Received");
	[delegate didReceiveImage:nil forIsbn:isbnId];
}


- (NSCachedURLResponse *) connection:(NSURLConnection *)aConnection 
                   willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
		printf("\n Cache Received");
	return cachedResponse;
}


- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
	printf("\n in connectionDidFinishLoading ");
	BOOL isImageDownloadFailed = FALSE;
	if([result length] >0)
	{
		UIImage* image = [UIImage imageWithData:result];
		printf("\n Received Image for book :%s,",[isbnId UTF8String]);
		[delegate didReceiveImage:image forIsbn:isbnId];
		
		if(image.size.height == 1 && image.size.width == 1)
		{
			isImageDownloadFailed  = TRUE;
		}
		//[image release]; 
	}
	else
	{
		isImageDownloadFailed = TRUE;
	}
	if(isImageDownloadFailed)
	{
		UIImage* image = [UIImage imageNamed:@"MissingCover.png"];
		[delegate didReceiveImage:image forIsbn:isbnId];
	}

	[self release];
}

- (void)cancel
{
	canceled = YES;
	[connection cancel];
	[delegate didReceiveImage:nil forIsbn:isbnId];
	[self release];
}

- (BOOL)canceled
{
	return canceled;
}

@end
