//
//  BookCoverLoader.m
//  CampusBooks
//
//  Created by Admin on 17/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "BookCoverLoader.h"


@implementation BookCoverLoader
- (id)init
{
	self = [super init];
	if(self)
	{
		_cache = [[NSMutableDictionary alloc] initWithCapacity:80];
	}
	
	return self;
}

- (UIImage*)getBookCover:(Book*)aBook  delegate:(id <BookCoverLoaderDelegate>)dlg
{
	delegate = [dlg retain];
	printf("\n Downloading cover for image :%s",[aBook.imageUrl UTF8String]);
	UIImage *image = [_cache objectForKey:aBook.isbnId10];
	if(image)
	{
		printf("\n returning image from cache");
		return image;
	}
	else if(aBook.imageUrl != nil 
			&&( [[aBook.imageUrl stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]  > 0)
			&& ![aBook.imageUrl isEqualToString:@"http://partners.campusbooks.com/images/no_image.gif"]  
			&& ![aBook.imageUrl isEqualToString:@"http://www.campusbooks.com/images/no_image.gif"] )
	{

		NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
		
		printf("\n Creat BookCoverDownloader and assign it");
		BookCoverDownloader *downloader = [[BookCoverDownloader alloc] init];
		[downloader getCoverForBook:aBook.isbnId10 withUrl: aBook.imageUrl delegate:self];
		[downloader release];
		
		[pool release];

	}
	else
	{
		return [UIImage imageNamed:@"MissingCover.png"];
	}
	return nil;
}


-(void)didReceiveImage:(UIImage*)aImage forIsbn:(NSString*)aISBN
{
	if(aImage)
	{
		[_cache setObject:aImage forKey:aISBN];
	}
	[delegate didReceiveBookCover:aISBN];
}
+ (BookCoverLoader*)sharedLoader
{
	static BookCoverLoader *loader;
	if(!loader)
		loader = [[BookCoverLoader alloc] init];
	
	return loader;
}

@end
