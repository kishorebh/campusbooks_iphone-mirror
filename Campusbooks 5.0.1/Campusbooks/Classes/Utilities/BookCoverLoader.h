//
//  BookCoverLoader.h
//  CampusBooks
//
//  Created by Admin on 17/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import"BookCoverDownloader.h"
@class BookCoverLoader;

@protocol BookCoverLoaderDelegate <NSObject>
-(void)didReceiveBookCover:(NSString*)aISBN;
@end

@interface BookCoverLoader : NSObject <BookCoverDownloaderDelegate>{
		NSMutableDictionary *_cache;
		id <BookCoverLoaderDelegate> delegate;
}

+ (BookCoverLoader*)sharedLoader;
- (UIImage*)getBookCover:(Book*)aBook delegate:(id <BookCoverLoaderDelegate>)dlg;

@end
