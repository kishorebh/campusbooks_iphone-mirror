//
//  HTMLUtil.h
//  Dictionary
//
//  Created by Admin on 02/11/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HTMLUtil : NSObject {

}
+(NSString*) decodeEscapeSequences:(NSString*) encodedStr;
+(NSString*) encodeEscapeSequences:(NSString*) encodedStr;

@end
