//
//  BooksInfoParserXml.h
//  CampusBooks
//
//  Created by Admin on 13/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import"Book.h"
#import"HTMLUtil.h"
@interface BooksInfoParserXml : NSObject <NSXMLParserDelegate> {
	
	
	NSString* responce;
	NSString* responceStatus;
	NSString* responceVersion;
	NSString* label;
	NSString* labelPlid;
	NSString* labelName;
	NSString* page;
	NSString* pageName;
	NSString* isbn10;
	NSString* isbn13;
	NSString* title;
	NSString* author;
	NSString* binding;
	NSString* msrp;
	NSString* pages;
	NSString* publisher;
	NSString* publishedDate;
	NSString* edition;
	NSString* rank;
	NSString* rating;
	NSString* amazonItem;
	NSString* image;
	NSString* imageWidth;
	NSString* imageHeight;

	
	Book* bookInfo;
	NSMutableArray* bookInfoList;
	
	NSXMLParser *myParser;
	
	BOOL isRootNode;
	BOOL isresponce;
	BOOL isresponceStatus;
	BOOL isresponceVersion;
	BOOL islabel;
	BOOL islabelPlid;
	BOOL islabelName;
	BOOL ispage;
	BOOL ispageName;
	BOOL isisbn10;
	BOOL isisbn13;
	BOOL istitle;
	BOOL isauthor;
	BOOL isbinding;
	BOOL ismsrp;
	BOOL ispages;
	BOOL ispublisher;
	BOOL ispublishedDate;
	BOOL isedition;
	BOOL isrank;
	BOOL israting;
	BOOL isamazonItem;
	BOOL isimage;
	BOOL isimageWidth;
	BOOL isimageHeight;
	
	
}
- (void)parseXMLFileAtURL:(NSURL *)URL;
-(NSMutableArray*)getitemsList;
@end
