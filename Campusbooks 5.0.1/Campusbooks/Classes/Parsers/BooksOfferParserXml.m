//
//  BooksOfferParserXml.m
//  Books
//
//  Created by bluepal on 14/06/09.
//  Copyright 2009 bluepal. All rights reserved.
//

#import "BooksOfferParserXml.h"
#import"CampusBooksAppDelegate.h"
#import "Coupon.h"

@implementation BooksOfferParserXml

- (void)parseXMLFileAtURL:(NSURL *)URL includeLocalOptionsOnly:(BOOL)aIncludeLocalOptionsOnly
{	
    
    includeLocalOptionsOnly = aIncludeLocalOptionsOnly;
	
	[booksOfferList release];
	booksOfferList = [[NSMutableArray alloc] init];
	
    NSData* responseData = [NSData dataWithContentsOfURL:URL];
    NSError* error = [[NSError alloc] init ];
    
    NSDictionary* json = nil;
    if (responseData) {
        json = [NSJSONSerialization 
                JSONObjectWithData:responseData //1
                
                options:kNilOptions 
                error:&error];
    }
    
    
    
    NSArray* bookItem = [[[[json objectForKey:@"response"] objectForKey:@"page"] objectForKey:@"books"] objectForKey:@"book"]; //2
    
    NSArray* offersByConditionsArray = [[[bookItem objectAtIndex:0] objectForKey:@"offers"] objectForKey:@"group"];
    
    NSLog(@"No of Conditions:%d", [offersByConditionsArray count]);
    
    for (NSDictionary* groupOfOffersByCondition in offersByConditionsArray) {
        
        NSLog(@"PRocessing:%@", groupOfOffersByCondition);
        NSString* conditionId = [NSString stringWithFormat:@"%@", [groupOfOffersByCondition objectForKey:@"id"]] ;
        NSString* conditionName = [groupOfOffersByCondition objectForKey:@"name"];
        

        NSArray* offersForThisCondition = [groupOfOffersByCondition objectForKey:@"offer"];
        
        NSLog(@"Processing :<%@-%@>, %d", conditionId, conditionName, [offersForThisCondition count]);        
        
        //Ignore international listings.
        if ([conditionId isEqualToString:@"7"]) {
            continue;
        }

        for (NSDictionary* offerInfo in offersForThisCondition) {
            BooksOffer* offer = [[[BooksOffer alloc] init] autorelease];
            
            offer.availabilityId = [NSString stringWithFormat:@"%@", [[offerInfo objectForKey:@"availability"] objectForKey:@"id"]];
            offer.availabilityText =  [HTMLUtil decodeEscapeSequences:[[offerInfo objectForKey:@"availability"] objectForKey:@"availability"]];            
            offer.conditionId = conditionId;
            offer.conditionName = conditionName;
            offer.offerConditionId = conditionId;
            offer.offerConditionText = conditionName;      
            offer.merchantId = [NSString stringWithFormat:@"%@", [[offerInfo objectForKey:@"merchant"] objectForKey:@"id"]];
            offer.merchantName =  [HTMLUtil decodeEscapeSequences:[[offerInfo objectForKey:@"merchant"] objectForKey:@"name"]];  
            
            NSArray* couponsDictionaryArray = [[offerInfo objectForKey:@"coupon_available"] objectForKey:@"coupon"];
            
            NSMutableArray* coupons = [[[NSMutableArray alloc] init]autorelease];
            for (NSDictionary* couponItem in couponsDictionaryArray) {
                Coupon* coupon = [[[Coupon alloc] init]autorelease];
                coupon.code = [couponItem objectForKey:@"code"];
                coupon.description = [couponItem objectForKey:@"desc"];
                coupon.shortDescription = [couponItem objectForKey:@"desc_short"];
                coupon.expirationDate = [couponItem objectForKey:@"expires"];
                
                [coupons addObject:coupon];
            }
            offer.coupons = coupons;
            
            NSString* couponText = @"";
            for (Coupon* coupon in offer.coupons) {
                couponText = [couponText stringByAppendingFormat:@"%@\n\n", coupon];
            }
            if ([offer.coupons count] > 0) {
                offer.couponText = couponText;
            }
            else {
                offer.couponText = @"N/A";
            }
            
            offer.isbn10 = [offerInfo objectForKey:@"isbn10"];
            offer.isbn13 = [offerInfo objectForKey:@"isbn13"];
            offer.comments = [HTMLUtil decodeEscapeSequences:[offerInfo objectForKey:@"comment"]];
            offer.price = [offerInfo objectForKey:@"price"];
            offer.shippingPrice = [offerInfo objectForKey:@"shipping"];
            offer.totalPrice = [offerInfo objectForKey:@"total"];
            offer.link = [offerInfo objectForKey:@"link"];
            offer.shoppingMode = 0;
            offer.shippingMode = [offerInfo objectForKey:@"shipping_method"];
            offer.isbn13 = [offerInfo objectForKey:@""];
            
//            if (!includeLocalOptionsOnly || (includeLocalOptionsOnly && ([offer.conditionId isEqualToString:@"8"] || [offer.conditionId isEqualToString:@"9"]))) {

                [booksOfferList addObject:offer];

//            }
        }
    }

}
- (void)dealloc 
{
	[booksOfferList release];
	[super dealloc];
}
//- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
//{
//
//    if (qName) {
//        elementName = qName;
//    }
//	if([elementName isEqualToString:@"offer"])
//	{
//		[booksOffer release];
//		booksOffer = [[BooksOffer alloc] init] ;
//
//		isRootNode=YES;
//		isresponce=NO;
//		isresponceStatus=NO;
//		isresponceVersion=NO;
//		islabel=NO;
//		isoffers=NO;
//		islabelPlid=NO;
//		islabelName=NO;
//		ispage=NO;
//		ispangName=NO;
//		iscondition=NO;
//		isconditionId=NO;
//		isconditionText=NO;
//		isoffer=YES;
//		isisbn13=NO;
//		isisbn10=NO;
//		ismerchantId=NO;
//		ismerchantName=NO;
//		isprice=NO;
//		isshippingGround=NO;
//		istotalPrice=NO;
//		islink=NO;
//		isofferConditionId=NO;
//		isofferConditionText=NO;
//		isavailabilityId=NO;
//		isavailabilityText=NO;
//		iscomments=NO;
//		isstoreName=NO;
//		isstorePhone=NO;
//		isstoreAddress1=NO;
//		isstoreAddress2=NO;
//		isstoreCity=NO;
//		isstoreState=NO;
//		isstoreZip=NO;
//		isstoreLat=NO;
//		isstoreLong=NO;
//		isstoreMiles=NO;
//		
//		responce=@"";
//		responceStatus=@"";
//		responceVersion=@"";
//		label=@"";
//		labelPlid=@"";
//		labelName=@"";
//		page=@"";
//		pageName=@"";
//		condition=@"";
//		//conditionId=@"";
//		//conditionName=@"";
//		offer=@"";
//		isbn13=@"";
//		isbn10=@"";
//		merchantId=@"";
//		merchantName=@"";
//		price=@"";
//		shippingGround=@"";
//		totalPrice=@"";
//		link=@"";
//		offerConditionId=@"";
//		offerConditionText=@"";
//		availabilityId=@"";
//		availabilityText=@"";
//		comments=@"";
//		storeName=@"";
//		storePhone=@"";
//		storeAddress1=@"";
//		storeAddress2=@"";
//		storeCity=@"";
//		storeState=@"";
//		storeZip=@"";
//		storeLat=@"";
//		storeLong=@"";
//		storeMiles=@"";
//		
//	}
//	
//	else if([elementName isEqualToString:@"respose"])
//	{
//		responceStatus=@"";
//		responceStatus=(NSString*)[attributeDict objectForKey:@"status"];
//		
//		responceVersion=@"";
//		responceVersion=(NSString*)[attributeDict objectForKey:@"version"];
//		
//		isresponce=YES;
//	}
//	else if([elementName isEqualToString:@"label"])
//	{
//		labelPlid=@"";
//		labelPlid=(NSString*)[attributeDict objectForKey:@"plid"];
//		
//		labelName=@"";
//		labelName=(NSString*)[attributeDict objectForKey:@"name"];
//		
//		islabel=YES;
//	}
//	else if([elementName isEqualToString:@"page"])
//	{
//		pageName=@"";
//		pageName=(NSString*)[attributeDict objectForKey:@"name"];
//		
//		ispage=YES;
//	}
//	else if([elementName isEqualToString:@"offers"])
//	{
//		offersId=@"";
//		offersId=(NSString*)[attributeDict objectForKey:@"id"];
//		isoffers=YES;
//	}
//	else if([elementName isEqualToString:@"condition"])
//	{
//        NSLog(@"Parsing:%@", attributeDict);
//		conditionId=@"";
//		conditionId=(NSString*)[attributeDict objectForKey:@"id"];
//		
//		conditionName=@"";
//		[booksOffer setConditionId:conditionId];
//
//		iscondition=YES;
//	}
//	else if([elementName isEqualToString:@"isbn13"])
//	{
//		isisbn13=YES;
//	}
//	else if([elementName isEqualToString:@"isbn10"])
//	{
//		isisbn10=YES;
//	}
//	else if([elementName isEqualToString:@"merchant_id"])
//	{
//		ismerchantId=YES;
//	}
//	else if([elementName isEqualToString:@"merchant_name"])
//	{
//		ismerchantName=YES;
//	}
//	else if([elementName isEqualToString:@"price"])
//	{
//		isprice=YES;
//	}
//	else if([elementName isEqualToString:@"shipping_ground"])
//	{
//		isshippingGround=YES;
//	}
//	else if([elementName isEqualToString:@"total_price"])
//	{
//		istotalPrice=YES;
//	}
//	else if([elementName isEqualToString:@"link"])
//	{
//		islink=YES;
//	}
//	else if([elementName isEqualToString:@"condition_id"])
//	{
//		isofferConditionId=YES;
//	}
//	else if([elementName isEqualToString:@"condition_text"])
//	{
//		isofferConditionText=YES;
//	}
//	else if([elementName isEqualToString:@"availability_id"])
//	{
//		isavailabilityId=YES;
//	}
//	else if([elementName isEqualToString:@"availability_text"])
//	{
//		isavailabilityText=YES;
//	}
//	else if([elementName isEqualToString:@"comments"])
//	{
//		iscomments=YES;
//	}
//	else if([elementName isEqualToString:@"store_name"])
//	{
//		isstoreName=YES;
//	}
//	else if([elementName isEqualToString:@"store_phone"])
//	{
//		isstorePhone=YES;
//	}
//	else if([elementName isEqualToString:@"store_addr1"])
//	{
//		isstoreAddress1=YES;
//	}
//	else if([elementName isEqualToString:@"store_addr2"])
//	{
//		isstoreAddress2=YES;
//	}
//	else if([elementName isEqualToString:@"store_city"])
//	{
//		isstoreCity=YES;
//	}
//	else if([elementName isEqualToString:@"store_state"])
//	{
//		isstoreState=YES;
//	}
//	else if([elementName isEqualToString:@"store_zip"])
//	{
//		isstoreZip=YES;
//	}
//	else if([elementName isEqualToString:@"store_lat"])
//	{
//		isstoreLat=YES;
//	}
//	else if([elementName isEqualToString:@"store_lon"])
//	{
//		isstoreLong=YES;
//	}
//	else if([elementName isEqualToString:@"store_miles"])
//	{
//		isstoreMiles=YES;
//	}
//	
//}
//- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
//{  
//	if (qName) {
//		elementName = qName;
//	}	
//	else if([elementName isEqualToString:@"respose"])
//	{
//		
//		isresponce=YES;
//	}
//	else if([elementName isEqualToString:@"label"])
//	{
//		islabel=YES;
//	}
//	else if([elementName isEqualToString:@"page"])
//	{
//		
//		ispage=NO;
//	}
//	else if([elementName isEqualToString:@"offers"])
//	{
//		isoffers=NO;
//	}
//	
//	else if([elementName isEqualToString:@"condition"])
//	{
//		[booksOffer setConditionName:conditionName];
//
//		iscondition=NO;
//	}
//	else if([elementName isEqualToString:@"isbn13"])
//	{
//		[booksOffer setIsbn13:isbn13];
//		isisbn13=NO;
//	}
//	else if([elementName isEqualToString:@"isbn10"])
//	{
//		[booksOffer setIsbn10:isbn10];
//		isisbn10=NO;
//	}
//	else if([elementName isEqualToString:@"merchant_id"])
//	{
//		[booksOffer setMerchantId:merchantId];
//		ismerchantId=NO;
//	}
//	else if([elementName isEqualToString:@"merchant_name"])
//	{
//		[booksOffer setMerchantName:[HTMLUtil decodeEscapeSequences:merchantName]];
//		ismerchantName=NO;
//	}
//	else if([elementName isEqualToString:@"price"])
//	{
//		[booksOffer setPrice:price];
//		isprice=NO;
//	}
//	else if([elementName isEqualToString:@"shipping_ground"])
//	{
//		[booksOffer setShippingGround:[HTMLUtil decodeEscapeSequences:shippingGround]];
//		isshippingGround=NO;
//	}
//	else if([elementName isEqualToString:@"total_price"])
//	{
//		[booksOffer setTotalPrice:totalPrice];
//		istotalPrice=NO;
//	}
//	else if([elementName isEqualToString:@"link"])
//	{
//		[booksOffer setLink:link];
//		islink=NO;
//	}
//	else if([elementName isEqualToString:@"condition_id"])
//	{
//		[booksOffer setOfferConditionId:offerConditionId];
//		isofferConditionId=NO;
//	}
//	else if([elementName isEqualToString:@"condition_text"])
//	{
//		[booksOffer setOfferConditionText:[HTMLUtil decodeEscapeSequences:offerConditionText]];
//		isofferConditionText=NO;
//	}
//	else if([elementName isEqualToString:@"availability_id"])
//	{
//		[booksOffer setAvailabilityId:availabilityId];
//		isavailabilityId=NO;
//	}
//	else if([elementName isEqualToString:@"availability_text"])
//	{
//		[booksOffer setAvailabilityText:[HTMLUtil decodeEscapeSequences:availabilityText]];
//		isavailabilityText=NO;
//	}
//	else if([elementName isEqualToString:@"comments"])
//	{
//		[booksOffer setComments:[HTMLUtil decodeEscapeSequences:comments]];
//		iscomments=NO;
//	}	
//	else if([elementName isEqualToString:@"store_name"])
//	{
//		[booksOffer setStoreName:storeName];
//		isstoreName=NO;
//	}
//	else if([elementName isEqualToString:@"store_phone"])
//	{
//		[booksOffer setStorePhone:storePhone];
//		isstorePhone=NO;
//	}
//	else if([elementName isEqualToString:@"store_addr1"])
//	{
//		[booksOffer setStoreAddress1:storeAddress1];
//		isstoreAddress1=NO;
//	}
//	else if([elementName isEqualToString:@"store_addr2"])
//	{
//		[booksOffer setStoreAddress2:storeAddress2];
//		isstoreAddress2=NO;
//	}
//	else if([elementName isEqualToString:@"store_city"])
//	{
//		[booksOffer setStoreCity:storeCity];
//		isstoreCity=NO;
//	}
//	else if([elementName isEqualToString:@"store_state"])
//	{
//		[booksOffer setStoreState:storeState];
//		isstoreState=NO;
//	}
//	else if([elementName isEqualToString:@"store_zip"])
//	{
//		[booksOffer setStoreZip:storeZip];
//		isstoreZip=NO;
//	}
//	else if([elementName isEqualToString:@"store_lat"])
//	{
//		[booksOffer setStoreLat:storeLat];
//		isstoreLat=NO;
//	}
//	else if([elementName isEqualToString:@"store_lon"])
//	{
//		[booksOffer setStoreLong:storeLong];
//		isstoreLong=NO;
//	}
//	else if([elementName isEqualToString:@"store_miles"])
//	{
//		//printf("\n Miles:%s", [storeMiles UTF8String]);
//		[booksOffer setStoreMiles:storeMiles];
//		isstoreMiles=NO;
//	}
//	if([elementName isEqualToString:@"offer"])
//	{
//		booksOffer.responceStatus=responceStatus;
//		booksOffer.responceVersion=responceVersion;
//		booksOffer.labelPlid=labelPlid;
//		booksOffer.labelName=labelName;
//		booksOffer.pageName=pageName;
//		booksOffer.conditionId=conditionId;
//		booksOffer.conditionName=conditionName;
///*		printf("\n Set ConditionID:%s", [conditionId UTF8String]);
//		printf("\n Object ConditionID:%s", [booksOffer.conditionId UTF8String]);
//		printf("\n Set ConditionName:%s", [conditionName UTF8String]);
//		printf("\n Object Condition Name:%s", [booksOffer.conditionName UTF8String]);
//*/		
//		booksOffer.shippingGround = shippingGround;
//		booksOffer.shoppingMode = 0;
//		booksOffer.offersId=offersId;
//	//	printf("\n storeName :%s",[booksOffer.storeName UTF8String]);
//	//	printf("\n storePhone :%s",[booksOffer.storePhone UTF8String]);
//	//	printf("\n storeAddress1 :%s",[booksOffer.storeAddress1 UTF8String]);
//		
//		[booksOfferList addObject:booksOffer];
//		
//		isRootNode=NO;
//		isresponce=NO;
//		isresponceStatus=NO;
//		isresponceVersion=NO;
//		islabel=NO;
//		islabelPlid=NO;
//		islabelName=NO;
//		ispage=NO;
//		ispangName=NO;
//		iscondition=NO;
//		isconditionId=NO;
//		isconditionText=NO;
//		isoffer=NO;
//		isisbn13=NO;
//		isisbn10=NO;
//		ismerchantId=NO;
//		ismerchantName=NO;
//		isprice=NO;
//		isshippingGround=NO;
//		istotalPrice=NO;
//		islink=NO;
//		isoffers=NO;
//		isofferConditionId=NO;
//		isofferConditionText=NO;
//		isavailabilityId=NO;
//		isavailabilityText=NO;
//		iscomments=NO;
//		isstoreName=NO;
//		isstorePhone=NO;
//		isstoreAddress1=NO;
//		isstoreAddress2=NO;
//		isstoreCity=NO;
//		isstoreState=NO;
//		isstoreZip=NO;
//		isstoreLat=NO;
//		isstoreLong=NO;
//		isstoreMiles=NO;		
//
//		
//	}
//}
//- (void)parserDidEndDocument:(NSXMLParser *)parser{
//	
//}
//- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
//{	
//	
//	if (isRootNode == YES)
//	{
//		if(isisbn13==YES || isisbn10==YES || ismerchantId==YES || ismerchantName==YES || isprice==YES
//		   || isshippingGround==YES || istotalPrice==YES || islink==YES 
//		   || isofferConditionId==YES || isofferConditionText==YES || isavailabilityId==YES 
//		   || isavailabilityText==YES || iscomments==YES  || isstoreName==YES ||  isstorePhone==YES || isstoreAddress1==YES || 
//		   isstoreAddress2==YES ||  isstoreCity==YES ||  isstoreState==YES || isstoreZip==YES || isstoreLat==YES || 
//		   isstoreLong==YES || isstoreMiles)
//		{
//			string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
//			string = [string stringByReplacingOccurrencesOfString:@"\t" withString:@" "];
//			string = [string stringByReplacingOccurrencesOfString:@"           " withString:@" "];
//			string = [string stringByReplacingOccurrencesOfString:@"         " withString:@" "];
//			string = [string stringByReplacingOccurrencesOfString:@"      " withString:@"\n\n\t"];
//			
//		}
//		if (isisbn13== YES) 
//		{
//			isbn13 = [ isbn13 stringByAppendingString:string];
//		}
//		else if (isisbn10 == YES) {
//			isbn10 = [ isbn10 stringByAppendingString:string];
//		}
//		else if(ismerchantId==YES)
//		{
//			merchantId = [ merchantId stringByAppendingString:string];
//		}
//		else if (ismerchantName == YES) {
//			merchantName = [ merchantName stringByAppendingString:string];
//		}
//		else if (isprice == YES) {
//			price = [ price stringByAppendingString:string];
//		}
//		else if(isshippingGround==YES)
//		{
//			shippingGround = [ shippingGround stringByAppendingString:string];
//		}
//		else if (istotalPrice == YES) {
//			totalPrice = [ totalPrice stringByAppendingString:string];
//		}
//		else if(islink==YES)
//		{
//			link = [ link stringByAppendingString:string];
//		}
//		else if (iscondition == YES) {
//			conditionName = [ conditionName stringByAppendingString:string];
//		}
//		else if (isofferConditionId == YES) {
//			offerConditionId = [ offerConditionId stringByAppendingString:string];
//		}
//		else if(isofferConditionText==YES)
//		{
//			offerConditionText = [ offerConditionText stringByAppendingString:string];
//		}
//		else if(isavailabilityId==YES)
//		{
//			availabilityId = [ availabilityId stringByAppendingString:string];
//		}
//		else if (isavailabilityText == YES) {
//			availabilityText = [ availabilityText stringByAppendingString:string];
//		}
//		else if(iscomments==YES)
//		{
//			comments = [ comments stringByAppendingString:string];
//		}
//		else if(isstoreName==YES)
//		{
//			storeName=[storeName stringByAppendingString:string];
//		}
//		else if(isstorePhone==YES)
//		{
//			storePhone=[storePhone stringByAppendingString:string];
//		}
//		else if(isstoreAddress1==YES)
//		{
//			storeAddress1=[storeAddress1 stringByAppendingString:string];
//		}
//		else if(isstoreAddress2==YES)
//		{
//			storeAddress2=[storeAddress2 stringByAppendingString:string];
//		}
//		else if(isstoreCity==YES)
//		{
//			storeCity=[storeCity stringByAppendingString:string];
//		}
//		else if(isstoreState==YES)
//		{
//			storeState=[storeState stringByAppendingString:string];
//		}
//		else if(isstoreZip==YES)
//		{
//			storeZip=[storeZip stringByAppendingString:string];
//		}
//		else if(isstoreLat==YES)
//		{
//			storeLat=[storeLat stringByAppendingString:string];
//		}
//		else if(isstoreLong==YES)
//		{
//			storeLong=[storeLong stringByAppendingString:string];
//		}
//		else if(isstoreMiles==YES)
//		{
//			storeMiles=[storeMiles stringByAppendingString:string];
//		}
//		
//	}
//}
-(NSMutableArray*)getBookOffersList
{

	return booksOfferList ;
}

@end

	
