//
//  BooksOfferParserXml.h
//  Books
//
//  Created by bluepal on 14/06/09.
//  Copyright 2009 bluepal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"BooksOffer.h"
#import"HTMLUtil.h"
@interface BooksOfferParserXml : NSObject <NSXMLParserDelegate>{
	
	
//	NSString* responce;
//	NSString* offersId;
//	NSString* responceStatus;
//	NSString* responceVersion;
//	NSString* offers;
//	NSString* label;
//	NSString* labelPlid;
//	NSString* labelName;
//	NSString* page;
//	NSString* pageName;
//	NSString* condition;
//	NSString* conditionId;
//	NSString* conditionName;
//	NSString* offer;
//	NSString* isbn13;
//	NSString* isbn10;
//	NSString* merchantId;
//	NSString* merchantName;
//	NSString* price;
//	NSString* shippingGround;
//	NSString* totalPrice;
//	NSString* link;
//	NSString* offerConditionId;
//	NSString* offerConditionText;
//	NSString* availabilityId;
//	NSString* availabilityText;
//	NSString* comments;
//	NSString* storeName;
//	NSString* storePhone;
//	NSString* storeAddress1;
//	NSString* storeAddress2;
//	NSString* storeCity;
//	NSString* storeState;
//	NSString* storeZip;
//	NSString* storeLat;
//	NSString* storeLong;
//	NSString* storeMiles;	
//	
//	BooksOffer* booksOffer;
	NSMutableArray* booksOfferList;
    
    BOOL includeLocalOptionsOnly;
	
//	NSXMLParser *myParser;
//	
//	BOOL isRootNode;
//	BOOL isresponce;
//	BOOL isresponceStatus;
//	BOOL isresponceVersion;
//	BOOL islabel;
//	BOOL islabelPlid;
//	BOOL islabelName;
//	BOOL ispage;
//	BOOL isoffers;
//	BOOL ispangName;
//	BOOL iscondition;
//	BOOL isconditionId;
//	BOOL isconditionText;
//	BOOL isoffer;
//	BOOL isisbn13;
//	BOOL isisbn10;
//	BOOL ismerchantId;
//	BOOL ismerchantName;
//	BOOL isprice;
//	BOOL isshippingGround;
//	BOOL istotalPrice;
//	BOOL islink;
//	BOOL isofferConditionId;
//	BOOL isofferConditionText;
//	BOOL isavailabilityId;
//	BOOL isavailabilityText;
//	BOOL iscomments;
//	BOOL isstoreName;
//	BOOL isstorePhone;
//	BOOL isstoreAddress1;
//	BOOL isstoreAddress2;
//	BOOL isstoreCity;
//	BOOL isstoreState;
//	BOOL isstoreZip;
//	BOOL isstoreLat;
//	BOOL isstoreLong;
//	BOOL isstoreMiles;	

}
- (void)parseXMLFileAtURL:(NSURL *)URL includeLocalOptionsOnly:(BOOL)aIncludeLocalOptionsOnly;
-(NSMutableArray*)getBookOffersList;
@end
