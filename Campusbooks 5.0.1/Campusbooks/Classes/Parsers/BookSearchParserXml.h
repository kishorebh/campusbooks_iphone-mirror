//
//  BookSearchParserXml.h
//  Books
//
//  Created by bluepal on 14/06/09.
//  Copyright 2009 bluepal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"Book.h"
#import"HTMLUtil.h"
@interface BookSearchParserXml : NSObject <NSXMLParserDelegate> {
	
	NSXMLParser *myParser;
	
	NSString* responce;
	NSString* responceStatus;
	NSString* responceVersion;
	NSString* label;
	NSString* labelPlid;
	NSString* labelName;
	NSString* page;
	NSString* pageName;
	NSString* count;
	NSString* currentPage;
	NSString* result;
	NSString* book;
	NSString* isbn10;
	NSString* isbn13;
	NSString* title;
	NSString* author;
	NSString* binding;
	NSString* msrp;
	NSString* pages;
	NSString* publisher;
	NSString* publishedDate;
	NSString* edition;
	NSString* rank;
	NSString* rating;
	NSString* amazonItem;
	NSString* image;
	NSString* imageWidth;
	NSString* imageHeight;
	
	Book* bookObject;
	NSMutableArray* bookSearchList;
	
	BOOL isRootNode;
	BOOL isresponce;
	BOOL isresponceStatus;
	BOOL isresponceVersion;
	BOOL islabel;
	BOOL islabelPlid;
	BOOL islabelName;
	BOOL ispage;
	BOOL ispageName;
	BOOL iscount;
	BOOL iscurrentPage;
	BOOL isresult;
	BOOL isbook;
	BOOL isisbn10;
	BOOL isisbn13;
	BOOL istitle;
	BOOL isauthor;
	BOOL isbinding;
	BOOL ismsrp;
	BOOL ispages;
	BOOL ispublisher;
	BOOL ispublishedDate;
	BOOL isedition;
	BOOL isrank;
	BOOL israting;
	BOOL isamazonItem;
	BOOL isimage;
	BOOL isimageWidth;
	BOOL isimageHeight;
}

- (void)parseXMLFileAtURL:(NSURL *)URL forPage:(NSInteger)aPage;
-(NSMutableArray*)getitemsList;

@end
