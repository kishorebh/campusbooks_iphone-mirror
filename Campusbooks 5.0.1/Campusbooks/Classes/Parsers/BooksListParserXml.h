//
//  BooksListParserXml.h
//  Books
//
//  Created by bluepal on 13/06/09.
//  Copyright 2009 bluepal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"Book.h"
#import"HTMLUtil.h"
@interface BooksListParserXml : NSObject <NSXMLParserDelegate>{
	
	NSXMLParser *myParser;
	
	Book* book;
	NSString* rss;
	NSString* channel;
	NSString* title;
	NSString* link;
	NSString* description;
	NSString* pubdate;
	NSString* lastBuildDate;
	NSString* ttl;
	NSString* generator;
	NSString* language;
	NSString* copyRight;
	NSString* item;
	NSString* itemTitle;
	NSString* itemGuid;
	NSString* itemLink;
	NSString* itemPubDate;
	NSString* itemDescription;
	
	NSMutableArray* itemsList;
	BOOL isRootNode;
	BOOL isrss;
	BOOL ischannel;
	BOOL istitle;
	BOOL islink;
	BOOL isdescription;
	BOOL ispubdate;
	BOOL islastBuildDate;
	BOOL isttl;
	BOOL isgenerator;
	BOOL islanguage;
	BOOL iscopyRight;
	BOOL isitem;
	BOOL isitemTitle;
	BOOL isitemGuid;
	BOOL isitemLink;
	BOOL isitemPubDate;
	BOOL isitemDescription;

}
- (void)parseXMLFileAtURL:(NSURL *)URL;
-(NSMutableArray*)getitemsList;
@end
