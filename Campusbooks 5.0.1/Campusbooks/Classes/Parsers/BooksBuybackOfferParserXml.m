//
//  BooksBuybackOfferParserXml.m
//  Books
//
//  Created by bluepal on 14/06/09.
//  Copyright 2009 bluepal. All rights reserved.
//

#import "BooksBuybackOfferParserXml.h"
#import"CampusBooksAppDelegate.h"

@implementation BooksBuybackOfferParserXml

//- (void)parserDidStartDocument:(NSXMLParser *)parser
//{
//	responce=@"";
//	responceStatus=@"";
//	responceVersion=@"";
//	label=@"";
//	offersId=@"";
//	offers=@"";
//	labelPlid=@"";
//	labelName=@"";
//	page=@"";
//	pageName=@"";
//	condition=@"";
//	conditionId=@"";
//	conditionName=@"";
//	offer=@"";
//	isbn13=@"";
//	isbn10=@"";
//	merchantId=@"";
//	merchantName=@"";
//	prices=@"";
//	pricesCondition=@"";
//	priceNew=@"";
//	priceUsed=@"";
//	shippingGround=@"";
//	totalPrice=@"";
//	link=@"";
//	offerConditionId=@"";
//	offerConditionText=@"";
//	availabilityId=@"";
//	availabilityText=@"";
//	comments=@"";
//}
- (void)parseXMLFileAtURL:(NSURL *)URL
{	
	[booksOfferList release];
	booksOfferList = [[NSMutableArray alloc] init];
	
	NSData* responseData = [NSData dataWithContentsOfURL:URL];
    NSError* error;
    NSDictionary* json = [NSJSONSerialization 
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions 
                          error:&error];
    
    NSArray* bookItem = [[[[json objectForKey:@"response"] objectForKey:@"page"] objectForKey:@"books"] objectForKey:@"book"]; //2
    
    NSArray* offersByConditionsArray = [[[bookItem objectAtIndex:0] objectForKey:@"offers"] objectForKey:@"group"];
    
    NSLog(@"No of Conditions:%d", [offersByConditionsArray count]);
    
    for (NSDictionary* groupOfOffersByCondition in offersByConditionsArray) {
        
        NSLog(@"PRocessing:%@", groupOfOffersByCondition);
        NSString* conditionId = [NSString stringWithFormat:@"%@", [groupOfOffersByCondition objectForKey:@"id"]] ;
        NSString* conditionName = [groupOfOffersByCondition objectForKey:@"name"];
        
        
        NSArray* offersForThisCondition = [groupOfOffersByCondition objectForKey:@"offer"];
        
        NSLog(@"Processing :<%@-%@>, %d", conditionId, conditionName, [offersForThisCondition count]);        
        
        //Ignore New & International listings.
        if ([conditionId isEqualToString:@"7"] || [conditionId isEqualToString:@"1"]) {
            continue;
        }
        for (NSDictionary* offerInfo in offersForThisCondition) {
            BooksOffer* offer = [[[BooksOffer alloc] init] autorelease];
            
            offer.availabilityId = [NSString stringWithFormat:@"%@", [[offerInfo objectForKey:@"availability"] objectForKey:@"id"]];
            offer.availabilityText =  [HTMLUtil decodeEscapeSequences:[[offerInfo objectForKey:@"availability"] objectForKey:@"availability"]];            
            offer.conditionId = conditionId;
            offer.conditionName = conditionName;
            offer.offerConditionId = conditionId;
            offer.offerConditionText = conditionName;      
            offer.merchantId = [NSString stringWithFormat:@"%@", [[offerInfo objectForKey:@"merchant"] objectForKey:@"id"]];
            offer.merchantName =  [HTMLUtil decodeEscapeSequences:[[offerInfo objectForKey:@"merchant"] objectForKey:@"name"]];            
            
            offer.isbn10 = [offerInfo objectForKey:@"isbn10"];
            offer.isbn13 = [offerInfo objectForKey:@"isbn13"];
            offer.comments = [HTMLUtil decodeEscapeSequences:[offerInfo objectForKey:@"comment"]];
            offer.price = [offerInfo objectForKey:@"price"];
            offer.shippingPrice = [offerInfo objectForKey:@"shipping"];
            offer.totalPrice = [offerInfo objectForKey:@"price"];
            offer.link = [offerInfo objectForKey:@"link"];
            offer.shoppingMode = 1;
            offer.shippingMode = [offerInfo objectForKey:@"shipping_method"];
            offer.isbn13 = [offerInfo objectForKey:@""];
            
            [booksOfferList addObject:offer];
        }
    }	
	
}
- (void)dealloc {
//	[booksOffer release];
	[booksOfferList release];
//	[offersArrayList release];
//	[myParser release];
	[super dealloc];
}
//- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
//{
//	
//    if (qName) {
//        elementName = qName;
//    }
//	
//	if([elementName isEqualToString:@"merchant"])
//	{
//		[booksOffer release];
//		booksOffer = [[BookBuyBack alloc] init] ;
//
//		isRootNode=YES;
//		isresponce=NO;
//		isresponceStatus=NO;
//		isresponceVersion=NO;
//		islabel=NO;
//		islabelPlid=NO;
//		islabelName=NO;
//		ispage=NO;
//		ispangName=NO;
//		iscondition=NO;
//		isconditionId=NO;
//		isconditionText=NO;
//		isoffer=YES;
//		isisbn13=NO;
//		isisbn10=NO;
//		isoffers=NO;
//		ismerchantId=NO;
//		ismerchantName=NO;
//		isprice=NO;
//		ispriceNew=NO;
//		ispriceUsed=NO;
//		ismerchantNotes=NO;
//		isshippingGround=NO;
//		istotalPrice=NO;
//		islink=NO;
//		isofferConditionId=NO;
//		isofferConditionText=NO;
//		isavailabilityId=NO;
//		isavailabilityText=NO;
//		iscomments=NO;
//		
//		responce=@"";
//		responceStatus=@"";
//		responceVersion=@"";
//		label=@"";
//		labelPlid=@"";
//		labelName=@"";
//		page=@"";
//		pageName=@"";
//		condition=@"";
//		conditionId=@"";
//		conditionName=@"";
//		offer=@"";
//		isbn13=@"";
//		isbn10=@"";
//		merchantId=@"";
//		merchantName=@"";
//		price=@"";
//		priceNew=@"";
//		priceUsed=@"";
//		shippingGround=@"";
//		totalPrice=@"";
//		link=@"";
//		offerConditionId=@"";
//		offerConditionText=@"";
//		availabilityId=@"";
//		availabilityText=@"";
//		comments=@"";
//		
//	}
//	else if([elementName isEqualToString:@"respose"])
//	{
//		responceStatus=@"";
//		responceStatus=(NSString*)[attributeDict objectForKey:@"status"];
//		
//		responceVersion=@"";
//		responceVersion=(NSString*)[attributeDict objectForKey:@"version"];
//		
//		isresponce=YES;
//	}
//	else if([elementName isEqualToString:@"label"])
//	{
//		labelPlid=@"";
//		labelPlid=(NSString*)[attributeDict objectForKey:@"plid"];
//		
//		labelName=@"";
//		labelName=(NSString*)[attributeDict objectForKey:@"name"];
//		
//		islabel=YES;
//	}
//	else if([elementName isEqualToString:@"offers"])
//	{
//		offersId=@"";
//		offersId=(NSString*)[attributeDict objectForKey:@"id"];
//		//printf("\n offersid in the start element:%s",[offersId UTF8String]);
//		isoffers=YES;
//	}
//	
//	else if([elementName isEqualToString:@"page"])
//	{
//		pageName=@"";
//		pageName=(NSString*)[attributeDict objectForKey:@"name"];
//		
//		ispage=YES;
//	}
//	else if([elementName isEqualToString:@"condition"])
//	{
//		conditionId=@"";
//		conditionId=(NSString*)[attributeDict objectForKey:@"id"];
//		
//		conditionName=@"";
//		conditionName=(NSString*)[attributeDict objectForKey:@"name"];
//		
//		iscondition=YES;
//	}
//	else if([elementName isEqualToString:@"isbn13"])
//	{
//		isisbn13=YES;
//	}
//	else if([elementName isEqualToString:@"isbn10"])
//	{
//		isisbn10=YES;
//	}
//	else if([elementName isEqualToString:@"merchant_id"])
//	{
//		ismerchantId=YES;
//	}
//	else if([elementName isEqualToString:@"name"])
//	{
//		ismerchantName=YES;
//	}
//	else if([elementName isEqualToString:@"notes"])
//	{
//		ismerchantNotes=YES;
//	}
//	else if([elementName isEqualToString:@"prices"])
//	{
//		isprices=YES;
//	}
//	else if([elementName isEqualToString:@"price"])
//	{
//		pricesCondition=@"";
//		pricesCondition=(NSString*)[attributeDict objectForKey:@"condition"];
//		if([pricesCondition isEqualToString:@"new"])
//		{
//			ispriceNew=YES;
//		}
//		else if([pricesCondition isEqualToString:@"used"])
//		{
//			ispriceUsed=YES;
//		}
//		isprice=YES;
//	}
//		
//	else if([elementName isEqualToString:@"shipping_ground"])
//	{
//		isshippingGround=YES;
//	}
//	else if([elementName isEqualToString:@"total_price"])
//	{
//		istotalPrice=YES;
//	}
//	else if([elementName isEqualToString:@"link"])
//	{
//		islink=YES;
//	}
//	else if([elementName isEqualToString:@"condition_id"])
//	{
//		isofferConditionId=YES;
//	}
//	else if([elementName isEqualToString:@"condition_text"])
//	{
//		isofferConditionText=YES;
//	}
//	else if([elementName isEqualToString:@"availability_id"])
//	{
//		isavailabilityId=YES;
//	}
//	else if([elementName isEqualToString:@"availability_text"])
//	{
//		isavailabilityText=YES;
//	}
//	else if([elementName isEqualToString:@"comments"])
//	{
//		iscomments=YES;
//	}
//}
//- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
//{  
//	
//	if (qName) {
//		elementName = qName;
//	}	
//	else if([elementName isEqualToString:@"respose"])
//	{
//		
//		isresponce=YES;
//	}
//	else if([elementName isEqualToString:@"label"])
//	{
//		islabel=YES;
//	}
//	else if([elementName isEqualToString:@"page"])
//	{
//		
//		ispage=NO;
//	}
//	else if([elementName isEqualToString:@"condition"])
//	{
//		iscondition=NO;
//	}
//	else if([elementName isEqualToString:@"isbn13"])
//	{
//		[booksOffer setIsbn13:isbn13];
//		isisbn13=NO;
//	}
//	else if([elementName isEqualToString:@"isbn10"])
//	{
//		[booksOffer setIsbn10:isbn10];
//		isisbn10=NO;
//	}
//	else if([elementName isEqualToString:@"offers"])
//	{
//		isoffers=NO;
//	}
//	
//	else if([elementName isEqualToString:@"merchant_id"])
//	{
//		[booksOffer setMerchantId:merchantId];
//		ismerchantId=NO;
//	}
//	else if([elementName isEqualToString:@"name"])
//	{
//		[booksOffer setMerchantName:merchantName];
//		ismerchantName=NO;
//	}
//	else if([elementName isEqualToString:@"notes"])
//	{
//		[booksOffer setComments:comments];
//		ismerchantNotes=NO;
//	}
//	else if([elementName isEqualToString:@"prices"])
//	{
//		isprices=NO;
//	}
//	else if([elementName isEqualToString:@"price"])
//	{
//		if(ispriceNew==YES)
//		{
//			[booksOffer setPriceNew:priceNew];
//			ispriceNew=NO;
//		}
//		else if(ispriceUsed==YES)
//		{
//			[booksOffer setPriceUsed:priceUsed];
//			ispriceUsed=NO;
//		}
//		isprice=NO;
//	}
//	else if([elementName isEqualToString:@"shipping_ground"])
//	{
//		[booksOffer setShippingGround:shippingGround];
//		isshippingGround=NO;
//	}
//	else if([elementName isEqualToString:@"total_price"])
//	{
//		[booksOffer setTotalPrice:totalPrice];
//		istotalPrice=NO;
//	}
//	else if([elementName isEqualToString:@"link"])
//	{
//		[booksOffer setLink:link];
//		islink=NO;
//	}
//	else if([elementName isEqualToString:@"condition_id"])
//	{
//		[booksOffer setConditionId:conditionId];
//		isofferConditionId=NO;
//	}
//	else if([elementName isEqualToString:@"condition_text"])
//	{
//		[booksOffer setOfferConditionText:offerConditionText];
//		isofferConditionText=NO;
//	}
//	else if([elementName isEqualToString:@"availability_id"])
//	{
//		[booksOffer setAvailabilityId:availabilityId];
//		isavailabilityId=NO;
//	}
//	else if([elementName isEqualToString:@"availability_text"])
//	{
//		[booksOffer setAvailabilityText:availabilityText];
//		isavailabilityText=NO;
//	}
//	else if([elementName isEqualToString:@"comments"])
//	{
//		//[booksOffer setComments:comments];
//		iscomments=NO;
//	}	
//	if([elementName isEqualToString:@"merchant"])
//	{
//		booksOffer.responceStatus=responceStatus;
//		booksOffer.responceVersion=responceVersion;
//		booksOffer.labelPlid=labelPlid;
//		booksOffer.labelName=labelName;
//		booksOffer.pageName=pageName;
//		booksOffer.conditionId=conditionId;
//		booksOffer.conditionName=conditionName;
//		booksOffer.shippingGround = 0;
//		//printf("\n offers id before we set it to the object:%s",[offersId UTF8String]);
//		booksOffer.offersId=offersId;
//		//printf("\n books offers id:%s",[booksOffer.offersId UTF8String]);
//		[booksOfferList addObject:booksOffer];
//		
//		isRootNode=NO;
//		isresponce=NO;
//		isresponceStatus=NO;
//		isresponceVersion=NO;
//		islabel=NO;
//		islabelPlid=NO;
//		islabelName=NO;
//		ispage=NO;
//		ispangName=NO;
//		iscondition=NO;
//		isconditionId=NO;
//		isconditionText=NO;
//		isoffer=NO;
//		isoffers=NO;
//		isisbn13=NO;
//		isisbn10=NO;
//		ismerchantId=NO;
//		ismerchantName=NO;
//		isprice=NO;
//		isshippingGround=NO;
//		istotalPrice=NO;
//		islink=NO;
//		isofferConditionId=NO;
//		isofferConditionText=NO;
//		isavailabilityId=NO;
//		isavailabilityText=NO;
//		iscomments=NO;
//		
//
//		
//	}
//}
//- (void)parserDidEndDocument:(NSXMLParser *)parser{
//	
//}
//- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
//{	
//	
//	if (isRootNode == YES)
//	{
//		if(isisbn13==YES || isisbn10==YES || ismerchantId==YES || ismerchantName==YES
//		   || isshippingGround==YES || ispriceNew==YES || ispriceUsed==YES || islink==YES 
//		   || isofferConditionId==YES || isofferConditionText==YES || isavailabilityId==YES 
//		   || isavailabilityText==YES || iscomments==YES || ismerchantNotes == YES)
//		{
//			string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
//			string = [string stringByReplacingOccurrencesOfString:@"\t" withString:@" "];
//			string = [string stringByReplacingOccurrencesOfString:@"           " withString:@" "];
//			string = [string stringByReplacingOccurrencesOfString:@"         " withString:@" "];
//			string = [string stringByReplacingOccurrencesOfString:@"      " withString:@"\n\n\t"];
//			
//		}
//		if (isisbn13== YES) 
//		{
//			isbn13 = [ isbn13 stringByAppendingString:string];
//		}
//		else if (isisbn10 == YES) {
//			isbn10 = [ isbn10 stringByAppendingString:string];
//		}
//		else if(ismerchantId==YES)
//		{
//			merchantId = [ merchantId stringByAppendingString:string];
//		}
//		else if (ismerchantName == YES) {
//			merchantName = [ merchantName stringByAppendingString:string];
//		}
//		else if(isshippingGround==YES)
//		{
//			shippingGround = [ shippingGround stringByAppendingString:string];
//			
//		}
//		else if (ispriceNew == YES) {
//			priceNew = [ priceNew stringByAppendingString:string];
//		}
//		else if (ispriceUsed == YES) {
//			priceUsed = [ priceUsed stringByAppendingString:string];
//		}
//		else if(islink==YES)
//		{
//			link = [ link stringByAppendingString:string];
//		}
//		else if (isofferConditionId == YES) {
//			offerConditionId = [ offerConditionId stringByAppendingString:string];
//		}
//		else if(isofferConditionText==YES)
//		{
//			offerConditionText = [ offerConditionText stringByAppendingString:string];
//		}
//		else if(isavailabilityId==YES)
//		{
//			availabilityId = [ availabilityId stringByAppendingString:string];
//		}
//		else if (isavailabilityText == YES) {
//			availabilityText = [ availabilityText stringByAppendingString:string];
//		}
//		else if(iscomments==YES)
//		{
//			comments = [ comments stringByAppendingString:string];
//		}
//		else if(ismerchantNotes==YES)
//		{
//			comments = [ comments stringByAppendingString:string];
//		}
//		
//	}
//}
//-(NSMutableArray*)getBookOffersList
//{
//	for(int i=0;i<[booksOfferList count];i++)
//	{
//
//		BookBuyBack* obj=[booksOfferList objectAtIndex:i];
//		
//		//printf("\n Merchant: %s, Old Price :%s New Price:%s",  [obj.merchantName UTF8String],[obj.priceUsed UTF8String], [obj.priceNew UTF8String]);
//		if ([obj.priceUsed floatValue]> 0.0 || [obj.priceNew floatValue]> 0.0)
//		{
//		//	printf("\n Added to list");
//			BooksOffer* offerObj1=[[BooksOffer alloc]init];
//			
//			offerObj1.price=obj.price;
//			offerObj1.isbn10=obj.isbn10;
//			offerObj1.link=obj.link;
//			offerObj1.offersId  = obj.offersId;
//			offerObj1.isbn13=obj.isbn13;
//			offerObj1.comments= [HTMLUtil decodeEscapeSequences: obj.comments];
//			offerObj1.pageName=[HTMLUtil decodeEscapeSequences:obj.pageName];
//			offerObj1.labelName=[HTMLUtil decodeEscapeSequences:obj.labelName];
//			offerObj1.labelPlid=obj.labelPlid;
//			offerObj1.totalPrice=obj.priceNew;
//			offerObj1.conditionId=obj.conditionId;
//			offerObj1.merchantId=obj.merchantId;
//			offerObj1.merchantName=[HTMLUtil decodeEscapeSequences:obj.merchantName];
//			offerObj1.shoppingMode=1;
//			offerObj1.conditionName=@"New";
//			offerObj1.shippingMode=[HTMLUtil decodeEscapeSequences:obj.shippingGround];
//			offerObj1.availabilityId=obj.availabilityId;
//			offerObj1.responceStatus=obj.responceStatus;
//			offerObj1.responceVersion=obj.responceVersion;
//			offerObj1.availabilityText=[HTMLUtil decodeEscapeSequences:obj.availabilityText];
//			offerObj1.offerConditionText=@"New";
//			offerObj1.offerConditionId=obj.offerConditionId;
//			
//			[offersArrayList addObject:offerObj1];
//			[offerObj1 release];
//			
//			BooksOffer* offerObj2=[[BooksOffer alloc]init];
//			
//			offerObj2.price=obj.price;
//			offerObj2.isbn10=obj.isbn10;
//			offerObj2.link=obj.link;
//			offerObj2.isbn13=obj.isbn13;
//			offerObj2.offersId  = obj.offersId;
//			offerObj2.comments=[HTMLUtil decodeEscapeSequences:obj.comments];
//			offerObj2.pageName=[HTMLUtil decodeEscapeSequences:obj.pageName];
//			offerObj2.labelName=[HTMLUtil decodeEscapeSequences:obj.labelName];
//			offerObj2.labelPlid=obj.labelPlid;
//			offerObj2.totalPrice=obj.priceUsed;
//			offerObj2.conditionId=obj.conditionId;
//			offerObj2.merchantId=obj.merchantId;
//			offerObj2.merchantName=[HTMLUtil decodeEscapeSequences:obj.merchantName];
//			offerObj2.shoppingMode=1;
//			offerObj2.conditionName=@"Used";
//			offerObj2.shippingMode=[HTMLUtil decodeEscapeSequences:obj.shippingGround];
//			offerObj2.availabilityId=obj.availabilityId;
//			offerObj2.responceStatus=obj.responceStatus;
//			offerObj2.responceVersion=obj.responceVersion;
//			offerObj2.availabilityText=[HTMLUtil decodeEscapeSequences:obj.availabilityText];
//			offerObj2.offerConditionText=@"Used";
//			offerObj2.offerConditionId=obj.offerConditionId;
//			
//			[offersArrayList addObject:offerObj2];
//			[offerObj2 release];	
//		}
//	}
//	
//	return offersArrayList;
//	
//}

-(NSMutableArray*)getBookOffersList
{
    
	return booksOfferList ;
}

@end

	
