//
//  BooksListParserXml.m
//  Books
//
//  Created by bluepal on 13/06/09.
//  Copyright 2009 bluepal. All rights reserved.
//

#import "BooksListParserXml.h"


@implementation BooksListParserXml

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
	rss=@"";
	channel=@"";
	title=@"";
	link=@"";
	description=@"";
	pubdate=@"";
	lastBuildDate=@"";
	ttl=@"";
	generator=@"";
	language=@"";
	copyRight=@"";
	item=@"";
	itemTitle=@"";
	itemGuid=@"";
	itemLink=@"";
	itemPubDate=@"";
	itemDescription=@"";
}
- (void)parseXMLFileAtURL:(NSURL *)URL
{	
	
	[itemsList release];
	itemsList = [[NSMutableArray alloc] init];
	
	isRootNode=NO;
	isrss=NO;
	ischannel=NO;
	istitle=NO;
	islink=NO;
	isdescription=NO;
	ispubdate=NO;
	islastBuildDate=NO;
	isttl=NO;
	isgenerator=NO;
	islanguage=NO;
	iscopyRight=NO;
	isitem=NO;
	isitemTitle=NO;
	isitemGuid=NO;
	isitemLink=NO;
	isitemPubDate=NO;
	isitemDescription=NO;

	[myParser release];
	myParser = [[NSXMLParser alloc] initWithContentsOfURL:URL] ;
	[myParser setDelegate:self];
	[myParser setShouldProcessNamespaces:NO];
	[myParser setShouldReportNamespacePrefixes:NO];
	[myParser setShouldResolveExternalEntities:NO];
	[myParser parse];	
	
		
	
}
- (void)dealloc 
{
	[book release];
	[itemsList release];
	[myParser release];
	[super dealloc];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	
    if (qName) {
        elementName = qName;
    }
	
	//ƒ[myParser abortParsing];
	if([elementName isEqualToString:@"item"])
	{
		[book release];
		book = [[Book alloc] init] ;
		
		isRootNode=YES;
		isrss=NO;
		ischannel=NO;
		istitle=NO;
		islink=NO;
		isdescription=NO;
		ispubdate=NO;
		islastBuildDate=NO;
		isttl=NO;
		isgenerator=NO;
		islanguage=NO;
		iscopyRight=NO;
		isitem=YES;
		isitemTitle=NO;
		isitemGuid=NO;
		isitemLink=NO;
		isitemPubDate=NO;
		isitemDescription=NO;
		
		
		rss=@"";
		channel=@"";
		title=@"";
		link=@"";
		description=@"";
		pubdate=@"";
		lastBuildDate=@"";
		ttl=@"";
		generator=@"";
		language=@"";
		copyRight=@"";
		item=@"";
		itemTitle=@"";
		itemGuid=@"";
		itemLink=@"";
		itemPubDate=@"";
		itemDescription=@"";
	}
	else if([elementName isEqualToString:@"channel"])
	{
		ischannel=YES;
	}
	else if([elementName isEqualToString:@"title"])
	{
		if(isitem==YES)
		{
			isitemTitle=YES;
		}
		else
		{
			istitle=YES;
		}
	}
	else if([elementName isEqualToString:@"link"])
	{
		if(isitem==YES)
		{
			isitemLink=YES;
		}
		else
		{
			islink=YES;
		}
	}
	else if([elementName isEqualToString:@"description"])
	{
		if(isitem==YES)
		{
			isitemDescription=YES;
		}
		else
		{
			isdescription=YES;
		}
	}
	else if([elementName isEqualToString:@"lastBuildDate"])
	{
		islastBuildDate=YES;
	}
	else if([elementName isEqualToString:@"pubDate"])
	{
		if(isitem==YES)
		{
			isitemPubDate=YES;
		}
		else
		{
			ispubdate=YES;
		}
	}
	else if([elementName isEqualToString:@"ttl"])
	{
		isttl=YES;
	}
	else if([elementName isEqualToString:@"guid"])
	{
		isitemGuid=YES;
	}
	else if([elementName isEqualToString:@"generator"])
	{
		isgenerator=YES;
	}
	else if([elementName isEqualToString:@"language"])
	{
		islanguage=YES;
	}
	else if([elementName isEqualToString:@"copyright"])
	{
		iscopyRight=YES;
	}
	else if([elementName isEqualToString:@"rss"])
	{
		isrss=YES;
	}
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{  
	if (qName) {
		elementName = qName;
	}	
	else if([elementName isEqualToString:@"channel"])
	{
		ischannel=NO;
	}
	else if([elementName isEqualToString:@"title"])
	{
		if(isitem==YES)
		{

		//	printf("\n TItle before parsing:%s", [itemTitle UTF8String]);
			NSString* bookTitle = @"";
			NSArray* compontents = [itemTitle componentsSeparatedByString:@":"];
			if ([compontents count] ==0)
			{
				bookTitle = itemTitle;
				book.rank = 0;
			}
			else
			{
				int noOfComponents = [compontents count];
				NSString* rankStr = [[compontents objectAtIndex:0] stringByReplacingOccurrencesOfString:@"#" withString:@""];
				book.rank =[ rankStr  intValue];
				for (int i=1; i < noOfComponents; i++)
				{
					NSString* currentComponentElem = [compontents objectAtIndex:i];
					if ( i !=1)
					{
						bookTitle = [bookTitle stringByAppendingString:@":"];
					}
					bookTitle = [bookTitle stringByAppendingString:currentComponentElem];;
				}
			}
			[book setTitle:[HTMLUtil decodeEscapeSequences:bookTitle]];
			//printf("\n TItle after parsing:%s", [book.title UTF8String]);

			isitemTitle=NO;
		}
		else
		{
			istitle=NO;
		}
	}
	else if([elementName isEqualToString:@"link"])
	{
		if(isitem==YES)
		{
		//	[book setItemLink:itemLink];
			isitemLink=NO;
		}
		else
		{
			islink=NO;
		}
	}
	else if([elementName isEqualToString:@"description"])
	{
		if(isitem==YES)
		{
			NSString* authorNamePart = [itemDescription stringByReplacingOccurrencesOfString:@"" withString:@"__@__"];
			NSRange startRange = [authorNamePart rangeOfString:@"<span class=\"riRssContributor\">"];
			if (startRange.length >0)
			{
				authorNamePart = [authorNamePart substringFromIndex:startRange.location + startRange.length];
				NSRange endRange = [authorNamePart rangeOfString:@"</span>"];
				authorNamePart = [authorNamePart substringToIndex:endRange.location];
				authorNamePart = [authorNamePart stringByReplacingOccurrencesOfString:@"by " withString:@""];
				authorNamePart = [authorNamePart stringByReplacingOccurrencesOfString:@"(Author)" withString:@""];

				NSRange anchorStartRange = [authorNamePart rangeOfString:@"<a href=\""];
				if (anchorStartRange.length >0)
				{
					NSString* tmpString = [authorNamePart substringFromIndex:anchorStartRange.location];
					NSRange anchorEndRange = [tmpString rangeOfString:@"\">"];
					NSInteger endLocation = anchorEndRange.location + [@"\">" length];
					tmpString = [tmpString substringToIndex:endLocation];
					authorNamePart = [authorNamePart stringByReplacingOccurrencesOfString:tmpString withString:@""];
					authorNamePart = [authorNamePart stringByReplacingOccurrencesOfString:@"</a>" withString:@""];
				}
				authorNamePart = [authorNamePart stringByReplacingOccurrencesOfString:@"\n" withString:@""];
				authorNamePart = [authorNamePart stringByReplacingOccurrencesOfString:@"\t" withString:@""];
				
				book.author = [HTMLUtil decodeEscapeSequences:authorNamePart];
			}
				
			NSString* coverImagePart = [itemDescription stringByReplacingOccurrencesOfString:@"" withString:@"__@__"];
			NSRange coverImagestartRange = [coverImagePart rangeOfString:@"<img src=\""];
			if (startRange.length >0)
			{
				coverImagePart = [coverImagePart substringFromIndex:coverImagestartRange.location + coverImagestartRange.length];
				NSRange endRange = [coverImagePart rangeOfString:@"\""];
				coverImagePart = [coverImagePart substringToIndex:endRange.location];
				
				NSRange coverSizeStartRange = [coverImagePart rangeOfString:@"._SL"];
				if(coverSizeStartRange.length > 0)
				{
					NSString* coverSizePart = [coverImagePart substringFromIndex:coverSizeStartRange.location ];
					
					coverImagePart = [coverImagePart stringByReplacingOccurrencesOfString:coverSizePart withString:@"._SL180_.jpg"];
					book.imageUrl = coverImagePart;
				}
			}
			
			isitemDescription=NO;
		}
		else
		{
			isdescription=NO;
		}
	}
	else if([elementName isEqualToString:@"lastBuildDate"])
	{
		islastBuildDate=NO;
	}
	else if([elementName isEqualToString:@"pubDate"])
	{
		if(isitem==YES)
		{
		//	[book setItemPubDate:itemPubDate];

			isitemPubDate=NO;
		}
		else
		{
			ispubdate=NO;
		}
	}
	else if([elementName isEqualToString:@"ttl"])
	{
		isttl=NO;
	}
	else if([elementName isEqualToString:@"generator"])
	{
		isgenerator=NO;
	}
	else if([elementName isEqualToString:@"guid"])
	{
		NSString* isbn10 = @"";
		NSArray* components = [itemGuid componentsSeparatedByString:@"_"];
		if (components != nil && [components count] >0)
		{
			isbn10 = [components objectAtIndex:[components count]-1];
		}
		[book setIsbnId10:isbn10];
		isitemGuid=NO;
	}
	else if([elementName isEqualToString:@"language"])
	{
		islanguage=NO;
	}
	else if([elementName isEqualToString:@"copyright"])
	{
		iscopyRight=NO;
	}
	else if([elementName isEqualToString:@"rss"])
	{
		isrss=NO;
	}
	if([elementName isEqualToString:@"item"])
	{		
		
		if ([[book.title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0)
		{
			[itemsList addObject:book];	
		}
		
		isRootNode=NO;
		isrss=NO;
		ischannel=NO;
		istitle=NO;
		islink=NO;
		isdescription=NO;
		ispubdate=NO;
		islastBuildDate=NO;
		isttl=NO;
		isgenerator=NO;
		islanguage=NO;
		iscopyRight=NO;
		isitem=NO;
		isitemTitle=NO;
		isitemGuid=NO;
		isitemLink=NO;
		isitemPubDate=NO;
		isitemDescription=NO;
		
	}
}

- (void)parserDidEndDocument:(NSXMLParser *)parser{
	
	
}
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{	
	
	if (isRootNode == YES)
	{
		if(isitemTitle==YES || isitemPubDate==YES || isitemLink==YES || isitemGuid==YES || isitemDescription==YES)
		{
			string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
			string = [string stringByReplacingOccurrencesOfString:@"\t" withString:@" "];
			string = [string stringByReplacingOccurrencesOfString:@"           " withString:@" "];
			string = [string stringByReplacingOccurrencesOfString:@"         " withString:@" "];
			string = [string stringByReplacingOccurrencesOfString:@"      " withString:@"\n\n\t"];
			
		}
		if (isitemTitle == YES) {
			itemTitle = [ itemTitle stringByAppendingString:string];
			
			
		}
		else if (isitemPubDate== YES) 
		{
			
			
			itemPubDate = [ itemPubDate stringByAppendingString:string];
		}
		
		else if (isitemLink== YES) 
		{
			itemLink = [ itemLink stringByAppendingString:string];
		}
		else if (isitemGuid== YES) 
			
		{
			
			itemGuid = [ itemGuid stringByAppendingString:string];
		}
		else if (isitemDescription== YES) 
		{
			itemDescription = [ itemDescription stringByAppendingString:string];
		}

		
	}
}
-(NSMutableArray*)getitemsList
{
	return itemsList ;
}

@end

 