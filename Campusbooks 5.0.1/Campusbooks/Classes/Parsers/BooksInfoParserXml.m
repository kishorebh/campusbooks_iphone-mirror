//
//  BooksInfoParserXml.m
//  CampusBooks
//
//  Created by Admin on 13/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "BooksInfoParserXml.h"


@implementation BooksInfoParserXml

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
	responce=@"";
	responceStatus=@"";
	responceVersion=@"";
	label=@"";
	labelPlid=@"";
	labelName=@"";
	page=@"";
	pageName=@"";
	isbn10=@"";
	isbn13=@"";
	title=@"";
	author=@"";
	binding=@"";
	msrp=@"";
	pages=@"";
	publisher=@"";
	publishedDate=@"";
	edition=@"";
	rank=@"";
	rating=@"";
	amazonItem=@"";
	image=@"";
	imageWidth=@"";
	imageHeight=@"";
}
- (void)parseXMLFileAtURL:(NSURL *)URL
{	
	
	[bookInfoList release];
	bookInfoList = [[NSMutableArray alloc] init];
	
	isRootNode=NO;
	isresponce=NO;
	isresponceStatus=NO;
	isresponceVersion=NO;
	islabel=NO;
	islabelPlid=NO;
	islabelName=NO;
	ispage=NO;
	ispageName=NO;
	isisbn10=NO;
	isisbn13=NO;
	istitle=NO;
	isauthor=NO;
	isbinding=NO;
	ismsrp=NO;
	ispages=NO;
	ispublisher=NO;
	ispublishedDate=NO;
	isedition=NO;
	isrank=NO;
	israting=NO;
	isamazonItem=NO;
	isimage=NO;
	isimageWidth=NO;
	isimageHeight=NO;
	
	[myParser release];
	myParser = [[NSXMLParser alloc] initWithContentsOfURL:URL] ;
	[myParser setDelegate:self];
	[myParser setShouldProcessNamespaces:NO];
	[myParser setShouldReportNamespacePrefixes:NO];
	[myParser setShouldResolveExternalEntities:NO];
	[myParser parse];	
}
- (void)dealloc {
	[bookInfo release];
	[bookInfoList release];
	[myParser release];
	[super dealloc];
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	
    if (qName) {
        elementName = qName;
    }
	if([elementName isEqualToString:@"response"])
	{
		[bookInfo release];
		bookInfo = [[Book alloc] init] ;
		
		responceStatus=@"";
		responceStatus=(NSString*)[attributeDict objectForKey:@"status"];
		
		responceVersion=@"";
		responceVersion=(NSString*)[attributeDict objectForKey:@"version"];
		
		
		
		isRootNode=YES;
		isresponce=NO;
		isresponceStatus=NO;
		isresponceVersion=NO;
		islabel=NO;
		islabelPlid=NO;
		islabelName=NO;
		ispage=NO;
		ispageName=NO;
		isisbn10=NO;
		isisbn13=NO;
		istitle=NO;
		isauthor=NO;
		isbinding=NO;
		ismsrp=NO;
		ispages=NO;
		ispublisher=NO;
		ispublishedDate=NO;
		isedition=NO;
		isrank=NO;
		israting=NO;
		isamazonItem=NO;
		isimage=NO;
		isimageWidth=NO;
		isimageHeight=NO;
		
		responce=@"";
		responceStatus=@"";
		responceVersion=@"";
		label=@"";
		labelPlid=@"";
		labelName=@"";
		page=@"";
		pageName=@"";
		isbn10=@"";
		isbn13=@"";
		title=@"";
		author=@"";
		binding=@"";
		msrp=@"";
		pages=@"";
		publisher=@"";
		publishedDate=@"";
		edition=@"";
		rank=@"";
		rating=@"";
		amazonItem=@"";
		image=@"";
		imageWidth=@"";
		imageHeight=@"";
	}
	else if([elementName isEqualToString:@"label"])
	{
		labelPlid=@"";
		labelPlid=(NSString*)[attributeDict objectForKey:@"plid"];
		
		labelName=@"";
		labelName=(NSString*)[attributeDict objectForKey:@"name"];
		
		islabel=YES;
	}
	else if([elementName isEqualToString:@"page"])
	{
		pageName=@"";
		pageName=(NSString*)[attributeDict objectForKey:@"name"];
		
		ispage=YES;
	}
	
	else if([elementName isEqualToString:@"isbn10"])
	{
		isisbn10=YES;
	}
	else if([elementName isEqualToString:@"isbn13"])
	{
		isisbn13=YES;
	}
	else if([elementName isEqualToString:@"title"])
	{
		istitle=YES;
	}
	else if([elementName isEqualToString:@"author"])
	{
		isauthor=YES;
	}
	else if([elementName isEqualToString:@"binding"])
	{
		isbinding=YES;
	}
	else if([elementName isEqualToString:@"msrp"])
	{
		ismsrp=YES;
	}
	else if([elementName isEqualToString:@"pages"])
	{
		ispages=YES;
	}
	else if([elementName isEqualToString:@"publisher"])
	{
		ispublisher=YES;
	}
	else if([elementName isEqualToString:@"publish_date"])
	{
		ispublishedDate=YES;
	}
	else if([elementName isEqualToString:@"edition"])
	{
		isedition=YES;
	}
	else if([elementName isEqualToString:@"rank"])
	{
		isrank=YES;
	}
	else if([elementName isEqualToString:@"rating"])
	{
		israting=YES;
	}
	else if([elementName isEqualToString:@"_amazon_item"])
	{
		isamazonItem=YES;
	}
	else if([elementName isEqualToString:@"image"])
	{
		imageWidth=@"";
		imageWidth=(NSString*)[attributeDict objectForKey:@"width"];
		
		imageHeight=@"";
		imageHeight=(NSString*)[attributeDict objectForKey:@"height"];
		
		isimage=YES;
	}
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{  
	if (qName) {
		elementName = qName;
	}	
	else if([elementName isEqualToString:@"respose"])
	{
		isresponce=NO;
	}
	else if([elementName isEqualToString:@"label"])
	{
		islabel=NO;
	}
	else if([elementName isEqualToString:@"page"])
	{
		ispage=NO;
	}

	else if([elementName isEqualToString:@"pages"])
	{
		bookInfo.numberOfPages = [pages intValue];
		ispages=NO;
	}
	else if([elementName isEqualToString:@"isbn10"])
	{
		[bookInfo setIsbnId10:isbn10];
		isisbn10=NO;
	}
	else if([elementName isEqualToString:@"isbn13"])
	{
		[bookInfo setIsbnId13:isbn13];
		isisbn13=NO;
	}
	else if([elementName isEqualToString:@"title"])
	{
		[bookInfo setTitle:[HTMLUtil decodeEscapeSequences: title]];
		istitle=NO;
	}
	else if([elementName isEqualToString:@"author"])
	{
		[bookInfo setAuthor:[HTMLUtil decodeEscapeSequences: author]];
		isauthor=NO;
	}
	else if([elementName isEqualToString:@"binding"])
	{
		[bookInfo setBinding:binding];
		isbinding=NO;
	}
	else if([elementName isEqualToString:@"msrp"])
	{
		[bookInfo setMsrp:msrp];
		ismsrp=NO;
	}
	else if([elementName isEqualToString:@"publisher"])
	{
		[bookInfo setPublisher:[HTMLUtil decodeEscapeSequences: publisher]];
		ispublisher=NO;
	}
	else if([elementName isEqualToString:@"publish_date"])
	{
		[bookInfo setPublishedDate:publishedDate];
		ispublishedDate=NO;
	}
	else if([elementName isEqualToString:@"edition"])
	{
		[bookInfo setEdition:edition];

		isedition=NO;
	}
	else if([elementName isEqualToString:@"rank"])
	{
		if (rank != nil)
		{
			[bookInfo setRank:[rank intValue]];
		}
		isrank=NO;
	}
	else if([elementName isEqualToString:@"rating"])
	{
		[bookInfo setRating:[rating floatValue]];
		israting=NO;
	}
	else if([elementName isEqualToString:@"_amazon_item"])
	{
		isamazonItem=NO;
	}
	else if([elementName isEqualToString:@"image"])
	{
		NSRange coverSizeStartRange = [image rangeOfString:@"._SL"];
		if ( coverSizeStartRange.length > 0)
		{
			NSString* coverSizePart = [image substringFromIndex:coverSizeStartRange.location ];
			
			image = [image stringByReplacingOccurrencesOfString:coverSizePart withString:@"._SL160_.jpg"];
			
			bookInfo.imageUrl = image;
		}
		else 
		{
			bookInfo.imageUrl = nil;
		}
	//	printf("\n Books Image URL :%s",[bookInfo.imageUrl UTF8String]);
		isimage=NO;
	}
	if([elementName isEqualToString:@"page"])
	{
		
		 bookInfo.responceStatus=responceStatus;
		 bookInfo.responceVersion=responceVersion;
		 bookInfo.labelPlid=labelPlid;
		 bookInfo.labelName=[HTMLUtil decodeEscapeSequences: labelName];
		 bookInfo.pageName=pageName;
		 bookInfo.imageWidth=imageWidth;
		 bookInfo.imageHeight=imageHeight;
	 		
		[bookInfoList addObject:bookInfo];
		
		isRootNode=NO;
		isresponce=NO;
		isresponceStatus=NO;
		isresponceVersion=NO;
		islabel=NO;
		islabelPlid=NO;
		islabelName=NO;
		ispage=NO;
		ispageName=NO;
		isisbn10=NO;
		isisbn13=NO;
		istitle=NO;
		isauthor=NO;
		isbinding=NO;
		ismsrp=NO;
		ispages=NO;
		ispublisher=NO;
		ispublishedDate=NO;
		isedition=NO;
		isrank=NO;
		israting=NO;
		isamazonItem=NO;
		isimage=NO;
		isimageWidth=NO;
		isimageHeight=NO;
		
	}
	
}
- (void)parserDidEndDocument:(NSXMLParser *)parser{
	
}
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{	
	
	if (isRootNode == YES)
	{
		if(isisbn10==YES || isisbn13==YES || istitle==YES || isauthor==YES || isbinding==YES
		   || ismsrp==YES || ispages==YES || ispublisher==YES 
		   || ispublishedDate==YES || isedition==YES || isrank==YES 
		   || israting==YES || isimage==YES)
		{
			string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
			string = [string stringByReplacingOccurrencesOfString:@"\t" withString:@" "];
			string = [string stringByReplacingOccurrencesOfString:@"           " withString:@" "];
			string = [string stringByReplacingOccurrencesOfString:@"         " withString:@" "];
			string = [string stringByReplacingOccurrencesOfString:@"      " withString:@"\n\n\t"];
			
		}
		if (isisbn13== YES) 
		{
			isbn13 = [ isbn13 stringByAppendingString:string];
		}
		else if (isisbn10 == YES) {
			isbn10 = [ isbn10 stringByAppendingString:string];
		}
		
		else if (istitle == YES) {
			title = [ title stringByAppendingString:string];
		}
		else if (isauthor == YES) {
			author = [ author stringByAppendingString:string];
		}
		else if (isbinding == YES) {
			binding = [ binding stringByAppendingString:string];
		}
		else if (ismsrp == YES) {
			msrp = [ msrp stringByAppendingString:string];
		}else if (ispages == YES) {
			pages = [ pages stringByAppendingString:string];
		}
		else if (ispublisher == YES) {
			publisher = [ publisher stringByAppendingString:string];
		}
		else if (ispublishedDate == YES) {
			publishedDate = [publishedDate stringByAppendingString:string];
		}
		else if (isedition == YES) {
			edition = [ edition stringByAppendingString:string];
		}
		else if (isrank == YES) {
			rank = [ rank stringByAppendingString:string];
		}
		else if (israting == YES) {
			rating = [ rating stringByAppendingString:string];
		}
		else if (isimage == YES) {
			image = [ image stringByAppendingString:string];
		}
	}
}
-(NSMutableArray*)getitemsList
{
	return bookInfoList;
}

@end

