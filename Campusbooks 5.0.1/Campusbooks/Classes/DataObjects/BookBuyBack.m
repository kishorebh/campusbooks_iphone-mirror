//
//  BookBuyBack.m
//  CampusBooks
//
//  Created by bluepal on 18/11/09.
//  Copyright 2009 bluepal. All rights reserved.
//

#import "BookBuyBack.h"


@implementation BookBuyBack


@synthesize responceStatus,responceVersion,labelPlid,labelName,pageName,conditionId,conditionName,isbn13,isbn10,merchantId,merchantName,
price,priceNew,priceUsed,shippingGround,totalPrice,link,offerConditionId,
offerConditionText,availabilityId,availabilityText,comments,offersId;
@synthesize shoppingMode;


- (NSString *)offersId {
    return offersId;
}
- (void)setOffersId:(NSString *) aOffersId {
    if ((!offersId && ! aOffersId ) || (offersId && aOffersId)) return;
	
    [offersId release];
    offersId = [aOffersId copy]  ;
}
- (NSString *)comments {
    return comments;
}
- (void)setComments:(NSString *) aComments {
    if ((!comments && ! aComments ) || (comments && aComments)) return;
	
    [comments release];
    comments = [aComments copy]  ;
}
- (NSString *)availabilityText {
    return availabilityText;
}

- (void)setAvailabilityText:(NSString *) aAvailabilityText {
    if ((!availabilityText && ! aAvailabilityText ) || (availabilityText && aAvailabilityText)) return;
	
    [availabilityText release];
    availabilityText = [aAvailabilityText copy]  ;
}
- (NSString *)availabilityId {
    return availabilityId;
}

- (void)setAvailabilityId:(NSString *) aAvailabilityId {
    if ((!availabilityId && ! aAvailabilityId ) || (availabilityId && aAvailabilityId)) return;
	
    [availabilityId release];
    availabilityId = [aAvailabilityId copy]  ;
}
- (NSString *)offerConditionText {
    return offerConditionText;
}

- (void)setOfferConditionText:(NSString *) aOfferConditionText {
    if ((!offerConditionText && ! aOfferConditionText ) || (offerConditionText && aOfferConditionText)) return;
	
    [offerConditionText release];
    offerConditionText = [aOfferConditionText copy]  ;
}
- (NSString *)offerConditionId {
    return offerConditionId;
}

- (void)setOfferConditionId:(NSString *) aOfferConditionId {
    if ((!offerConditionId && ! aOfferConditionId ) || (offerConditionId && aOfferConditionId)) return;
	
    [offerConditionId release];
    offerConditionId = [aOfferConditionId copy]  ;
}
- (NSString *)link {
    return link;
}

- (void)setLink:(NSString *) aLink {
    if ((!link && ! aLink ) || (link && aLink)) return;
	
    [link release];
    link = [aLink copy]  ;
}

- (NSString *)shippingGround {
    return shippingGround;
}

- (void)setShippingGround:(NSString *) aShippingGround {
    if ((!shippingGround && ! aShippingGround ) || (shippingGround && aShippingGround)) return;
	
    [shippingGround release];
    shippingGround = [aShippingGround copy]  ;
}
- (NSString *)price {
    return price;
}

- (void)setPrice:(NSString *) aPrice {
    if ((!price && ! aPrice ) || (price && aPrice)) return;
	
    [price release];
    price = [aPrice copy]  ;
}
- (NSString *)merchantName {
    return merchantName;
}

- (void)setMerchantName:(NSString *) aMerchantName {
    if ((!merchantName && ! aMerchantName ) || (merchantName && aMerchantName)) return;
	
    [merchantName release];
    merchantName = [aMerchantName copy]  ;
}
- (NSString *)merchantId {
    return merchantId;
}

- (void)setMerchantId:(NSString *) aMerchantId {
    if ((!merchantId && ! aMerchantId ) || (merchantId && aMerchantId)) return;
	
    [merchantId release];
    merchantId = [aMerchantId copy]  ;
}

- (NSString *) aIsbn13 {
    return isbn13;
}

- (void)setIsbn13:(NSString *) aIsbn13 {
    if ((!isbn13 && ! aIsbn13 ) || (isbn13 && aIsbn13)) return;
	
    [isbn13 release];
    isbn13 = [aIsbn13 copy]  ;
}
- (NSString *) aIsbn10 {
    return isbn10;
}

- (void)setIsbn10:(NSString *) aIsbn10 {
    if ((!isbn10 && ! aIsbn10 ) || (isbn10 && aIsbn10)) return;
	
    [isbn10 release];
    isbn10 = [aIsbn10 copy]  ;
}
- (NSString *)conditionName {
    return conditionName;
}

- (void)setConditionName:(NSString *) aConditionName {
    if ((!conditionName && ! aConditionName ) || (conditionName && aConditionName)) return;
	
    [conditionName release];
    conditionName = [aConditionName copy]  ;
}
- (NSString *)conditionId {
    return conditionId;
}

- (void)setConditionId:(NSString *) aConditionId {
    if ((!conditionId && ! aConditionId ) || (conditionId && aConditionId)) return;
	
    [conditionId release];
    conditionId = [aConditionId copy]  ;
}

- (NSString *)responceVersion {
    return responceVersion;
}

- (void)setResponceVersion:(NSString *) aResponceVersion {
    if ((!responceVersion && ! aResponceVersion ) || (responceVersion && aResponceVersion)) return;
	
    [responceVersion release];
    responceVersion = [aResponceVersion copy]  ;
}
- (NSString *)pageName {
    return pageName;
}

- (void)setPageName:(NSString *) aPageName {
    if ((!pageName && ! aPageName ) || (pageName && aPageName)) return;
	
    [pageName release];
    pageName = [aPageName copy]  ;
}


- (NSString *)labelPlid {
    return labelPlid;
}

- (void)setLabelPlid:(NSString *) aLabelPlid {
    if ((!labelPlid && ! aLabelPlid ) || (labelPlid && aLabelPlid)) return;
	
    [labelPlid release];
    labelPlid = [aLabelPlid copy]  ;
}
- (NSString *)priceNew {
    return priceNew;
}

- (void)setPriceNew:(NSString *) aPriceNew {
    if ((!priceNew && ! aPriceNew ) || (priceNew && aPriceNew)) return;
	
    [priceNew release];
    priceNew = [aPriceNew copy]  ;
}
- (NSString *)priceUsed {
    return priceUsed;
}

- (void)setPriceUsed:(NSString *) aPriceUsed {
    if ((!priceUsed && ! aPriceUsed ) || (priceUsed && aPriceUsed)) return;
	
    [priceUsed release];
    priceUsed = [aPriceUsed copy]  ;
}
- (NSString *)responceStatus {
    return responceStatus;
}

- (void)setResponceStatus:(NSString *) aResponceStatus {
    if ((!responceStatus && ! aResponceStatus ) || (responceStatus && aResponceStatus)) return;
	
    [responceStatus release];
    responceStatus = [aResponceStatus copy]  ;
}
- (NSString *) labelName {
    return labelName;
}

- (void)setLabelName:(NSString *) aLabelName {
    if ((!labelName && ! aLabelName) || (labelName && aLabelName)) return;
	
    [labelName release];
    labelName = [aLabelName copy]  ;
}

-(void)dealloc
{
	
	[ responceStatus release];
	[ responceVersion release];
	[ labelPlid release];
	[ labelName release];
	[ pageName release];
	[ conditionId release];
	[ conditionName release];
	[ isbn13 release];
	[ isbn10 release];
	[ merchantId release];
	[ merchantName release];
	[ price release];
	[priceNew release];
	[priceUsed release];
	[ shippingGround release];
	[ totalPrice release];
	[ link release];
	[offersId release];
	[ offerConditionId release];
	[ offerConditionText release];
	[ availabilityId release];
	[ availabilityText release];
	[ comments release];
	[super dealloc];
}
@end




