//
//  BooksOffer.h
//  Books
//
//  Created by bluepal on 14/06/09.
//  Copyright 2009 bluepal. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BooksOffer : NSObject {
	
	
	NSString* responceStatus;
	NSString* responceVersion;
	NSString* labelPlid;
	NSString* offersId;
	NSString* labelName;
	NSString* pageName;
	NSString* conditionId;
	NSString* conditionName;
	NSString* isbn13;
	NSString* isbn10;
	NSString* merchantId;
	NSString* merchantName;
	NSString* price;
	NSString* shippingPrice;
	NSString* shippingMode;
	NSString* totalPrice;
	NSString* link;
	NSString* offerConditionId;
	NSString* offerConditionText;
	NSString* availabilityId;
	NSString* availabilityText;
	NSString* comments;
	
	NSInteger shoppingMode;
	NSString* storeName;
	NSString* storePhone;
	NSString* storeAddress1;
	NSString* storeAddress2;
	NSString* storeCity;
	NSString* storeState;
	NSString* storeZip;
	NSString* storeLat;
	NSString* storeLong;
	NSString* storeMiles;

    NSMutableArray* coupons;
    
    NSString* couponText;
	
	float distance;

}

@property(nonatomic,retain)NSString*responceStatus;
@property(nonatomic,retain)NSString*responceVersion;
@property(nonatomic,retain)NSString*labelPlid;
@property(nonatomic,retain)NSString* offersId;
@property(nonatomic,retain)NSString*labelName;
@property(nonatomic,retain)NSString*pageName;
@property(nonatomic,retain)NSString*conditionId;
@property(nonatomic,retain)NSString*conditionName;
@property(nonatomic,retain)NSString*isbn13;
@property(nonatomic,retain)NSString*isbn10;
@property(nonatomic,retain)NSString*merchantId;
@property(nonatomic,retain)NSString*merchantName;
@property(nonatomic,retain)NSString*price;
@property(nonatomic,retain)NSString*shippingMode;
@property(nonatomic,retain)NSString*totalPrice;
@property(nonatomic,retain)NSString*shippingPrice;
@property(nonatomic,retain)NSString*link;
@property(nonatomic,retain)NSString*offerConditionId;
@property(nonatomic,retain)NSString*offerConditionText;
@property(nonatomic,retain)NSString*availabilityId;
@property(nonatomic,retain)NSString*availabilityText;
@property(nonatomic,retain)NSString*comments;
@property(nonatomic)NSInteger shoppingMode;
@property(nonatomic,retain)NSString* storeName;
@property(nonatomic,retain)NSString* storePhone;
@property(nonatomic,retain)NSString* storeAddress1;
@property(nonatomic,retain)NSString* storeAddress2;
@property(nonatomic,retain)NSString* storeCity;
@property(nonatomic,retain)NSString* storeState;
@property(nonatomic,retain)NSString* storeZip;
@property(nonatomic,retain)NSString* storeLat;
@property(nonatomic,retain)NSString* storeLong;
@property(nonatomic) float distance;
@property(nonatomic,retain)NSString* storeMiles;
@property(nonatomic,retain)NSMutableArray* coupons;
@property(nonatomic,retain)NSString* couponText;
@end
