//
//  AddressAnnotation.h
//  Doctors
//
//  Created by admin bluepal on 04/12/09.
//  Copyright 2009 Company. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

#import "BooksOffer.h"

@interface AddressAnnotation : NSObject<MKAnnotation> 
{
	CLLocationCoordinate2D coordinate;
	NSString *mTitle;
	NSString *mSubTitle;
	BooksOffer *obj;
	
}
@property(nonatomic,readonly)CLLocationCoordinate2D coordinate;
@property(nonatomic,retain)NSString *mTitle;
@property(nonatomic,retain)NSString *mSubTitle;
@property(nonatomic,retain)BooksOffer *obj;
-(id)initWithCoordinate:(CLLocationCoordinate2D) c aObj:(BooksOffer *)aObj;


@end
