//
//  Coupon.h
//  Campusbooks
//
//  Created by sekhar bethalam on 01/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Coupon : NSObject {
    NSString* code;
    NSString* description;
    NSString* shortDescription;
    NSString* expirationDate;
}
@property(nonatomic, retain) NSString* code;
@property(nonatomic, retain) NSString* description;
@property(nonatomic, retain) NSString* shortDescription;
@property(nonatomic, retain) NSString* expirationDate;
@end
