//
//  BooksOffer.m
//  Books
//
//  Created by bluepal on 14/06/09.
//  Copyright 2009 bluepal. All rights reserved.
//

#import "BooksOffer.h"


@implementation BooksOffer

@synthesize responceStatus,responceVersion,labelPlid,labelName,pageName,conditionId,conditionName,isbn13,isbn10,merchantId,merchantName,
		  price,shippingMode,shippingPrice, totalPrice,link,offerConditionId,
                   offerConditionText,availabilityId,availabilityText,comments,offersId;
@synthesize shoppingMode,storeName,storePhone,storeAddress1,storeAddress2,storeCity,storeState,storeZip,distance,
				storeLat,storeLong, storeMiles, coupons, couponText;


- (NSString *)storeLong {
    return storeLong;
}
- (void)setStoreLong:(NSString *) aStoreLong {
    if ((!storeLong && ! aStoreLong ) || (storeLong && aStoreLong)) return;
	
    [storeLong release];	
    storeLong = [aStoreLong copy]  ;
}

- (NSString *)storeLat {
    return storeLat;
}
- (void)setStoreLat:(NSString *) aStoreLat {
    if ((!storeLat && ! aStoreLat ) || (storeLat && aStoreLat)) return;
	
    [storeLat release];	
    storeLat = [aStoreLat copy]  ;
}

- (NSString *)storeZip {
    return storeZip;
}
- (void)setStoreZip:(NSString *) aStoreZip {
    if ((!storeZip && ! aStoreZip ) || (storeZip && aStoreZip)) return;
	
    [storeZip release];	
    storeZip = [aStoreZip copy]  ;
}
- (NSString *)storeState {
    return storeState;
}
- (void)setStoreState:(NSString *) aStoreState {
    if ((!storeState && ! aStoreState ) || (storeState && aStoreState)) return;
	
    [storeState release];	
    storeState = [aStoreState copy]  ;
}

- (NSString *)storeCity {
    return storeCity;
}
- (void)setStoreCity:(NSString *) aStoreCity {
    if ((!storeCity && ! aStoreCity ) || (storeCity && aStoreCity)) return;
	
    [storeCity release];	
    storeCity = [aStoreCity copy]  ;
}

- (NSString *)storeAddress2 {
    return storeAddress2;
}
- (void)setStoreAddress2:(NSString *) aStoreAddress2 {
    if ((!storeAddress2 && ! aStoreAddress2 ) || (storeAddress2 && aStoreAddress2)) return;
	
    [storeAddress2 release];	
    storeAddress2 = [aStoreAddress2 copy]  ;
}

- (NSString *)storeAddress1 {
    return storeAddress1;
}
- (void)setStoreAddress1:(NSString *) aStoreAddress1 {
    if ((!storeAddress1 && ! aStoreAddress1 ) || (storeAddress1 && aStoreAddress1)) return;
	
    [storeAddress1 release];
    storeAddress1 = [aStoreAddress1 copy]  ;
}

- (NSString *)storePhone {
    return storePhone;
}
- (void)setStorePhone:(NSString *) aStorePhone {
    if ((!storePhone && ! aStorePhone ) || (storePhone && aStorePhone)) return;
	
    [storePhone release];
    storePhone = [aStorePhone copy]  ;
}
- (NSString *)storeName {
    return storeName;
}
- (void)setStoreName:(NSString *) aStoreName {
    if ((!storeName && ! aStoreName ) || (storeName && aStoreName)) return;
	
    [storeName release];
    storeName = [aStoreName copy]  ;
}
- (NSString *)offersId {
    return offersId;
}
- (void)setOffersId:(NSString *) aOffersId {
    if ((!offersId && ! aOffersId ) || (offersId && aOffersId)) return;
	
    [offersId release];
    offersId = [aOffersId copy]  ;
}
- (NSString *)comments {
    return comments;
}
- (void)setComments:(NSString *) aComments {
    if ((!comments && ! aComments ) || (comments && aComments)) return;
	
    [comments release];
    comments = [aComments copy]  ;
}
- (NSString *)availabilityText {
    return availabilityText;
}

- (void)setAvailabilityText:(NSString *) aAvailabilityText {
    if ((!availabilityText && ! aAvailabilityText ) || (availabilityText && aAvailabilityText)) return;
	
    [availabilityText release];
    availabilityText = [aAvailabilityText copy]  ;
}
- (NSString *)availabilityId {
    return availabilityId;
}

- (void)setAvailabilityId:(NSString *) aAvailabilityId {
    if ((!availabilityId && ! aAvailabilityId ) || (availabilityId && aAvailabilityId)) return;
	
    [availabilityId release];
    availabilityId = [aAvailabilityId copy]  ;
}
- (NSString *)offerConditionText {
    return offerConditionText;
}

- (void)setOfferConditionText:(NSString *) aOfferConditionText {
    if ((!offerConditionText && ! aOfferConditionText ) || (offerConditionText && aOfferConditionText)) return;
	
    [offerConditionText release];
    offerConditionText = [aOfferConditionText copy]  ;
}
- (NSString *)offerConditionId {
    return offerConditionId;
}

- (void)setOfferConditionId:(NSString *) aOfferConditionId {
    if ((!offerConditionId && ! aOfferConditionId ) || (offerConditionId && aOfferConditionId)) return;
	
    [offerConditionId release];
    offerConditionId = [aOfferConditionId copy]  ;
}
- (NSString *)link {
    return link;
}

- (void)setLink:(NSString *) aLink {
    if ((!link && ! aLink ) || (link && aLink)) return;
	
    [link release];
    link = [aLink copy]  ;
}

- (NSString *)shippingMode {
    return shippingMode;
}

- (void)setShippingMode:(NSString *)aShippingMode {
    if ((!shippingMode && ! aShippingMode ) || (shippingMode && aShippingMode)) return;
	
    [shippingMode release];
    shippingMode = [aShippingMode copy]  ;
}
- (NSString *)price {
    return price;
}

- (void)setPrice:(NSString *) aPrice {
    if ((!price && ! aPrice ) || (price && aPrice)) return;
	
    [price release];
    price = [aPrice copy]  ;
}
- (NSString *)merchantName {
    return merchantName;
}

- (void)setMerchantName:(NSString *) aMerchantName {
    if ((!merchantName && ! aMerchantName ) || (merchantName && aMerchantName)) return;
	
    [merchantName release];
    merchantName = [aMerchantName copy]  ;
}
- (NSString *)merchantId {
    return merchantId;
}

- (void)setMerchantId:(NSString *) aMerchantId {
    if ((!merchantId && ! aMerchantId ) || (merchantId && aMerchantId)) return;
	
    [merchantId release];
    merchantId = [aMerchantId copy]  ;
}

- (NSString *) aIsbn13 {
    return isbn13;
}

- (void)setIsbn13:(NSString *) aIsbn13 {
    if ((!isbn13 && ! aIsbn13 ) || (isbn13 && aIsbn13)) return;
	
    [isbn13 release];
    isbn13 = [aIsbn13 copy]  ;
}
- (NSString *) aIsbn10 {
    return isbn10;
}

- (void)setIsbn10:(NSString *) aIsbn10 {
    if ((!isbn10 && ! aIsbn10 ) || (isbn10 && aIsbn10)) return;
	
    [isbn10 release];
    isbn10 = [aIsbn10 copy]  ;
}
- (NSString *)conditionName {
    return conditionName;
}

- (void)setConditionName:(NSString *) aConditionName {
    if ((!conditionName && ! aConditionName ) || (conditionName && aConditionName)) return;
	
    [conditionName release];
    conditionName = [aConditionName copy]  ;
}
- (NSString *)conditionId {
    return conditionId;
}

- (void)setConditionId:(NSString *) aConditionId {
    if ((!conditionId && ! aConditionId ) || (conditionId && aConditionId)) return;
	
    [conditionId release];
    conditionId = [aConditionId copy]  ;
}

- (NSString *)responceVersion {
    return responceVersion;
}

- (void)setResponceVersion:(NSString *) aResponceVersion {
    if ((!responceVersion && ! aResponceVersion ) || (responceVersion && aResponceVersion)) return;
	
    [responceVersion release];
    responceVersion = [aResponceVersion copy]  ;
}
- (NSString *)pageName {
    return pageName;
}

- (void)setPageName:(NSString *) aPageName {
    if ((!pageName && ! aPageName ) || (pageName && aPageName)) return;
	
    [pageName release];
    pageName = [aPageName copy]  ;
}


- (NSString *)labelPlid {
    return labelPlid;
}

- (void)setLabelPlid:(NSString *) aLabelPlid {
    if ((!labelPlid && ! aLabelPlid ) || (labelPlid && aLabelPlid)) return;
	
    [labelPlid release];
    labelPlid = [aLabelPlid copy]  ;
}

- (NSString *)responceStatus {
    return responceStatus;
}

- (void)setResponceStatus:(NSString *) aResponceStatus {
    if ((!responceStatus && ! aResponceStatus ) || (responceStatus && aResponceStatus)) return;
	
    [responceStatus release];
    responceStatus = [aResponceStatus copy]  ;
}
- (NSString *) labelName {
    return labelName;
}

- (void)setLabelName:(NSString *) aLabelName {
    if ((!labelName && ! aLabelName) || (labelName && aLabelName)) return;
	
    [labelName release];
    labelName = [aLabelName copy]  ;
}
-(NSComparisonResult)compareByConditionAndTotalPriceReverse:(BooksOffer*)aBookOffer
{
	if ([self.offerConditionText compare:aBookOffer.offerConditionText]  ==0 )
	{
		if( [totalPrice floatValue] < [aBookOffer.totalPrice floatValue]) 
		{
			return 1;
		}
		else
		{
			return -1;
		}	
	}
	else 
	{
		return [self.offerConditionText compare:aBookOffer.offerConditionText] ;
	}
	return 0;
}

-(NSComparisonResult)compareByTotalPriceReverse:(BooksOffer*)aBookOffer
{
	if( [totalPrice floatValue] < [aBookOffer.totalPrice floatValue]) 
	{
		return 1;
	}
	else
	{
		return -1;
	}
	return 0;
}
-(NSComparisonResult)compareByDistance:(BooksOffer*)aBookOffer
{
	if( distance > aBookOffer.distance) 
	{
		return 1;
	}
	else
	{
		return -1;
	}
	return 0;
}
-(NSComparisonResult)compareByMiles:(BooksOffer*)aBookOffer
{
	if( [storeMiles floatValue] > [aBookOffer.storeMiles floatValue] ) 
	{
		return 1;
	}
	else
	{
		return -1;
	}
	return 0;
}

-(NSComparisonResult)compareByConditionAndTotalPrice:(BooksOffer*)aBookOffer
{
	if ([self.offerConditionText compare:aBookOffer.offerConditionText]  ==0 )
	{
		if( [totalPrice floatValue] > [aBookOffer.totalPrice floatValue]) 
		{
			return 1;
		}
		else
		{
			return -1;
		}	
	}
	else 
	{
		return [self.offerConditionText compare:aBookOffer.offerConditionText] ;
	}
	return 0;
}

-(NSComparisonResult)compareByTotalPrice:(BooksOffer*)aBookOffer
{
	if( [totalPrice floatValue] > [aBookOffer.totalPrice floatValue]) 
	{
		return 1;
	}
	else
	{
		return -1;
	}
	return 0;
}

-(void)dealloc
{
	
	[ responceStatus release];
	[ responceVersion release];
	[ labelPlid release];
	[ labelName release];
	[ pageName release];
	[ conditionId release];
	[ conditionName release];
	[ isbn13 release];
	[ isbn10 release];
	[offersId release];
	[ merchantId release];
	[ merchantName release];
	[ price release];
	[ shippingMode release];
	[ totalPrice release];
	[ link release];
	[ offerConditionId release];
	[ offerConditionText release];
	[ availabilityId release];
	[ availabilityText release];
	[ comments release];
//	[storeName release];
	[storePhone release];
//	[storeAddress1 release];
	[storeAddress2 release];
	[storeCity release];
	[storeState release];
	[storeZip release];
	[storeLat release];
	[storeLong release];
    [coupons release];
    [couponText release];
	[super dealloc];
}
@end



