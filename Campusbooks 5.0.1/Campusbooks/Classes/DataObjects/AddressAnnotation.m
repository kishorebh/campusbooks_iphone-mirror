//
//  AddressAnnotation.m
//  Doctors
//
//  Created by admin bluepal on 04/12/09.
//  Copyright 2009 Company. All rights reserved.
//
#import "AddressAnnotation.h"


@implementation AddressAnnotation

@synthesize coordinate,mTitle,mSubTitle,obj;


- (NSString *)subtitle
{
	return mSubTitle;
}

- (NSString *)title
{
	return mTitle;
}
 

-(id)initWithCoordinate:(CLLocationCoordinate2D) c aObj:(BooksOffer *)aObj
{
	self = [super init];
	if (self != nil) 
	{
		 obj = aObj;
		
		mTitle=aObj.storeName;
		mSubTitle=aObj.storeCity;
		coordinate=c;
	}
	return self;
}

 -(void)dealloc
{
	[super dealloc];
}
 

@end
