//
//  LocalOptions.m
//  CampusBooks
//
//  Created by Admin on 17/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "LocalOptions.h"


@implementation LocalOptions
@synthesize  primaryKey,optionsValue,radiusValue,zipCode,schoolName,isFirstTime;

- (NSInteger)isFirstTime {
    return isFirstTime;
}
- (void)setIsFirstTime:(NSInteger )aIsFirstTime{
    isFirstTime = aIsFirstTime;
}
- (NSInteger)primaryKey {
    return primaryKey;
}
- (void)setPrimaryKey:(NSInteger )aPrimaryKey{
    primaryKey = aPrimaryKey;
}

- (NSInteger)optionsValue {
    return optionsValue;
}
- (void)setOptionsValue:(NSInteger )aOptionsValue{
    optionsValue = aOptionsValue;
}

- (NSInteger)radiusValue {
    return radiusValue;
}
- (void)setRadiusValue:(NSInteger )aRadiusValue
{
    radiusValue = aRadiusValue;
}



- (NSString *)zipCode {
    return zipCode;
}
- (void)setZipCode:(NSString *) aZipCode {
//    if ((!zipCode && ! aZipCode ) || (zipCode && aZipCode)) return;
	
  //  [zipCode release];
    zipCode = [aZipCode copy]  ;
}


- (NSString *)schoolName {
    return schoolName;
}
- (void)setSchoolName:(NSString *) aSchoolName {
	
    [schoolName release];
    schoolName = [aSchoolName copy]  ;
}
- (id)initWithPrimaryKey:(NSInteger)apk database:(sqlite3 *)db {
	
	if (self = [super init]) 
	{
		primaryKey = apk;
		database = db;
		sqlite3_stmt* init_statement;
		
		const char *sql = "SELECT pk,options,radius,zipCode,schoolName,isFirstTime FROM localOptions WHERE pk=?";
		
		if (sqlite3_prepare_v2(database, sql, -1, &init_statement, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
		
		sqlite3_bind_int(init_statement, 1, primaryKey);
		
		if (sqlite3_step(init_statement) == SQLITE_ROW) {
			
			int intpk = sqlite3_column_int(init_statement, 0);
			self.primaryKey  = (intpk != 0 ? intpk:0);
			
			self.optionsValue  = sqlite3_column_int(init_statement, 1);
			
			self.radiusValue  = sqlite3_column_int(init_statement, 2);
			
			char *strZipCode= (char *)sqlite3_column_text(init_statement, 3);
			self.zipCode = (strZipCode != nil ? [NSString stringWithUTF8String:strZipCode] : @"");
			
		
			
			char *strSchoolName= (char *)sqlite3_column_text(init_statement, 4);
			self.schoolName  = (strSchoolName != nil ? [NSString stringWithUTF8String:strSchoolName] : @"");;
			
			self.isFirstTime  = sqlite3_column_int(init_statement, 5);
				
		} 
		else
		{
			
			self.optionsValue=0;
			self.radiusValue=0;
			self.isFirstTime=0;
			self.zipCode=@"";
			self.schoolName=@"";
			
		}
		
		sqlite3_finalize(init_statement);
	}
	return self;
}

-(void) dealloc
{
	self.schoolName=nil;
	self.zipCode=nil;
	[self.schoolName release];
	[self.zipCode release];
	[super dealloc];
}
@end



