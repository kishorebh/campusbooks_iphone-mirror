//
//  LocalOptions.h
//  CampusBooks
//
//  Created by Admin on 17/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface LocalOptions : NSObject
{
	NSInteger primaryKey;
	NSInteger optionsValue;
	NSInteger radiusValue;
	NSString* zipCode;
	NSString* schoolName;
	NSInteger isFirstTime;
	sqlite3 *database;
}
@property(nonatomic)NSInteger primaryKey;
@property(nonatomic)NSInteger optionsValue;
@property(nonatomic)NSInteger radiusValue;
@property(nonatomic,retain)NSString* zipCode;
@property(nonatomic,retain)NSString* schoolName;
@property(nonatomic)NSInteger isFirstTime;

- (id)initWithPrimaryKey:(NSInteger)apk database:(sqlite3 *)db ;

@end
