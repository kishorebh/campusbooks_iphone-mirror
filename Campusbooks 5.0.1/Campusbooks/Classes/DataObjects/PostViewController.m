//
//  PostViewController.m
//  GolfGames
//
//  Created by bluepal on 22/03/10.
//  Copyright 2010 bluepal. All rights reserved.
//

#import "PostViewController.h"
#import"CampusBooksAppDelegate.h"

@implementation PostViewController
@synthesize isFromTwitter;

- (id)init {
	if (self = [super init]) {
		self.title = @"Review message";				
		
		UIView 	*myView = [[[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]]autorelease];
		myView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
		myView.autoresizesSubviews = YES;	
		
		UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]
											   initWithTitle:@"Share"
											   style:UIBarButtonItemStyleBordered	
											   target:self
											   action:@selector(sentAction:)];
		
		self.navigationItem.rightBarButtonItem = rightBarButtonItem;
		[rightBarButtonItem release];
		
		UIView* subView=[[UIView alloc] initWithFrame:CGRectMake(0,0,320, 440)];
		subView.backgroundColor= [UIColor clearColor];
		
		UIImageView* imageView1;
		imageView1=[[UIImageView alloc] initWithFrame:CGRectMake(10,20,300,180)];
		imageView1.image = [UIImage imageNamed:@"description.png"];
		imageView1.backgroundColor=[UIColor clearColor];
		[subView addSubview:imageView1];
		[imageView1 release];
		
		descriptionView=[[UITextView alloc]initWithFrame:CGRectMake(10,30, 300, 160)];
		descriptionView.font = [UIFont boldSystemFontOfSize:16];
		descriptionView.textColor=[UIColor blackColor];
		descriptionView.backgroundColor = [UIColor clearColor];   
		descriptionView.returnKeyType = UIReturnKeyDefault;
		descriptionView.text=@"I'm saving money on books using CampusBooks for iPhone! I recommend you do too. Get the free app at http://tinyurl.com/ibookstore ";
		descriptionView.keyboardAppearance  = UIKeyboardAppearanceDefault;
		descriptionView.keyboardType = UIKeyboardTypeDefault;
		[subView addSubview:descriptionView];
		[descriptionView release];
		
		[myView addSubview:subView];
		[subView release];
		self.view =myView;
		[myView release];
		
	}
	return self;
}
- (void)viewWillAppear:(BOOL)animated 
{
	[descriptionView becomeFirstResponder];
}
-(void)sentAction:(id)sender
{
	CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [ [UIApplication sharedApplication] delegate];
	NSString* status;
	if([descriptionView.text length]!=0)
	{
		status=descriptionView.text;
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!" message:@"Please Enter your comment"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		return;
	}
	if(!isFromTwitter)
	{
		[appDelegate.facebookObject tryToUpdateStatus:status];
	}
		
      [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
		//	printf("\n In PostViewCOntroller Dealloc");
    [super dealloc];
}


@end
