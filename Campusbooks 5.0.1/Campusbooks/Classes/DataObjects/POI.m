//
//  POI.m
//  SampleApplication
//
//  Created by Admin Bluepal on 24/04/09.
//  Copyright 2009 Bluepal Soultions Pvt Ltd. All rights reserved.
//

#import "POI.h"


@implementation POI
@synthesize coordinate;
@synthesize booksOffer;

- (id) initWithCoords:(CLLocationCoordinate2D) coords forOffer:(BooksOffer*)aBooksOffer{
	self = [super init];
	if (self != nil) {
		booksOffer = aBooksOffer;
		
		//printf("\n books offer at poi:%s",[booksOffer.storeName UTF8String]);
		if (aBooksOffer != nil)
		{
			title = booksOffer.storeName;
			subtitle = booksOffer.storeAddress1;
		}
		else
		{
			title =@"Current Location";
			subtitle = @"";
		}
		coordinate = coords; 
	}
	return self;
}
-(NSString*)title {
    return title;
}
-(NSString*)subtitle {
    return subtitle;
}
- (void) dealloc
{
	
//	[booksOffer release];
//	[title release];
//	[subtitle release];
	[super dealloc];
}


@end
