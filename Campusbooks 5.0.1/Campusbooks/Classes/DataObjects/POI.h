//
//  POI.h
//  SampleApplication
//
//  Created by Admin Bluepal on 24/04/09.
//  Copyright 2009 Bluepal Soultions Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "BooksOffer.h"
@interface POI : NSObject<MKAnnotation> {
	
	CLLocationCoordinate2D coordinate;
	NSString *subtitle; 
	NSString *title;
	BooksOffer* booksOffer;

}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic,retain) BooksOffer* booksOffer;

- (id) initWithCoords:(CLLocationCoordinate2D) coords forOffer:(BooksOffer*)aBooksOffer ;


@end
