//
//  ScanList.h
//  CampusBooks
//
//  Created by Admin on 20/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface ScanList : NSObject {
	
	NSString* name;
	NSDate* scanDate;
	int primaryKey;
	int noOfItems;
	sqlite3 *database;
}
@property(nonatomic)NSInteger primaryKey;
@property(nonatomic,retain)NSString* name;
@property (copy, nonatomic) NSDate *scanDate;
@property(nonatomic)NSInteger noOfItems;
- (id)initWithPrimaryKey:(NSInteger)apk database:(sqlite3 *)db;
@end
