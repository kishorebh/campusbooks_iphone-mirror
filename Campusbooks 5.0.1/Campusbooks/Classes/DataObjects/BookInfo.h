//
//  BookInfo.h
//  CampusBooks
//
//  Created by Admin on 13/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BookInfo : NSObject {
	
	
	NSString* responceStatus;
	NSString* responceVersion;
	NSString* labelPlid;
	NSString* labelName;
	NSString* pageName;
	NSString* count;
	NSString* currentPage;
	NSString* result;
	NSString* isbn10;
	NSString* isbn13;
	NSString* title;
	NSString* author;
	NSString* binding;
	NSString* msrp;
	NSString* pages;
	NSString* publisher;
	NSString* publishedDate;
	NSString* edition;
	NSString* rank;
	NSString* rating;
	NSString* image;
	NSString* imageWidth;
	NSString* imageHeight;
	NSInteger numberOfPages;
	NSString* imageUrl;
	
}
@property(nonatomic,retain)NSString* imageUrl;
@property(nonatomic)NSInteger numberOfPages;
@property(nonatomic,retain)NSString* responceStatus;
@property(nonatomic,retain)NSString* responceVersion;
@property(nonatomic,retain)NSString* labelPlid;
@property(nonatomic,retain)NSString* labelName;
@property(nonatomic,retain)NSString* pageName;
@property(nonatomic,retain)NSString* count;
@property(nonatomic,retain)NSString* currentPage;
@property(nonatomic,retain)NSString* result;
@property(nonatomic,retain)NSString* isbn10;
@property(nonatomic,retain)NSString* isbn13;
@property(nonatomic,retain)NSString* title;
@property(nonatomic,retain)NSString* author;
@property(nonatomic,retain)NSString* binding;
@property(nonatomic,retain)NSString* msrp;
@property(nonatomic,retain)NSString* pages;
@property(nonatomic,retain)NSString* publisher;
@property(nonatomic,retain)NSString* publishedDate;
@property(nonatomic,retain)NSString* edition;
@property(nonatomic,retain)NSString* rank;
@property(nonatomic,retain)NSString* rating;
@property(nonatomic,retain)NSString* image;
@property(nonatomic,retain)NSString* imageWidth;
@property(nonatomic,retain)NSString* imageHeight;
@end