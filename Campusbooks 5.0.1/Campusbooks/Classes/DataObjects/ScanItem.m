//
//  ScanItem.m
//  CampusBooks
//
//  Created by Admin on 20/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "ScanItem.h"


@implementation ScanItem
@synthesize 	 primaryKey,bookName,isbnId10,authorName, scanListId;


- (NSInteger)primaryKey {
    return primaryKey;
}

- (void)setPrimaryKey:(NSInteger )aPrimaryKey{
    primaryKey = aPrimaryKey;
}
- (NSString *)authorName {
    return authorName;
}

- (void)setAuthorName:(NSString *) aAuthorName {
    if ((!authorName && ! aAuthorName ) || (authorName && aAuthorName)) return;
	
    [authorName release];
    authorName = [aAuthorName copy]  ;
}
- (NSString *)bookName {
    return bookName;
}

- (void)setBookName:(NSString *) aBookName {
    if ((!bookName && ! aBookName ) || (bookName && aBookName)) return;
	
    [bookName release];
    bookName = [aBookName copy]  ;
}

- (NSString *)isbnId10 {
    return isbnId10;
}

- (void)setIsbnId10:(NSString *) aIsbnId10{
    if ((!isbnId10 && ! aIsbnId10 ) || (isbnId10 && aIsbnId10)) return;
	
    [isbnId10 release];
    isbnId10 = [aIsbnId10 copy]  ;
}

- (NSString *)imageUrl {
    return imageUrl;
}

- (void)setImageUrl:(NSString *) aImageUrl {
    if ((!imageUrl && ! aImageUrl ) || (imageUrl && aImageUrl)) return;
	
    [imageUrl release];
    imageUrl = [aImageUrl copy]  ;
}

- (NSInteger)scanListId {
    return scanListId;
}

- (void)setScanListId:(NSInteger )aScanListId{
    scanListId = aScanListId;
}
- (id)initWithPrimaryKey:(NSInteger)apk database:(sqlite3 *)db {
	
	if (self = [super init]) 
	{
		primaryKey = apk;
		database = db;
		sqlite3_stmt* init_statement;
		
		const char *sql = "SELECT pk,bookName,isbnId10,authorName , imageUrl, scanListId  FROM scanitem WHERE pk=?";
		
		if (sqlite3_prepare_v2(database, sql, -1, &init_statement, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
		
		sqlite3_bind_int(init_statement, 1, primaryKey);
		
		if (sqlite3_step(init_statement) == SQLITE_ROW) {
			
			int intpk = sqlite3_column_int(init_statement, 0);
			self.primaryKey  = (intpk != 0 ? intpk:0);

			char *strBookName= (char *)sqlite3_column_text(init_statement, 1);
			self.bookName = (strBookName != nil ? [NSString stringWithUTF8String:strBookName] : @"");

			
			char *strIsbnId10= (char *)sqlite3_column_text(init_statement, 2);
			self.isbnId10  = (strIsbnId10 != nil ? [NSString stringWithUTF8String:strIsbnId10] : @"");;

			char *strAuthorName = (char *)sqlite3_column_text(init_statement, 3);
			self.authorName = (strAuthorName != nil ? [NSString stringWithUTF8String:strAuthorName] : @"");

			
			char *strImageUrl = (char *)sqlite3_column_text(init_statement, 4);
			self.imageUrl = (strImageUrl != nil ? [NSString stringWithUTF8String:strImageUrl] : @"");

			int intScanListId = sqlite3_column_int(init_statement, 5);
			self.scanListId  = (intScanListId != 0 ? intScanListId:0);
			
			//printf("\n Retrieved scan item isbn id :%s", [self.isbnId10 UTF8String]);

		} 
		else {
			
		}
		
		sqlite3_finalize(init_statement);
	}
	return self;
}

-(void) dealloc
{
	[bookName release];
	[isbnId10 release];
	[authorName release];
	[imageUrl release];
	[super dealloc];
}
@end