//
//  Book.h
//  CampusBooks
//
//  Created by Admin on 14/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Book : NSObject {
	
	NSString* isbnId10;
	NSString* isbnId13;
	NSString* description;
	NSInteger numberOfPages;
	float rating;
	NSInteger rank;
	UIImage* image;
	NSString* responceStatus;
	NSString* responceVersion;
	NSString* labelPlid;
	NSString* labelName;
	NSString* pageName;
	NSString* count;
	NSString* currentPage;
	NSString* result;
	NSString* title;
	NSString* author;
	NSString* binding;
	NSString* msrp;
	NSString* pages;
	NSString* publisher;
	NSString* publishedDate;
	NSString* edition;
	NSString* imageHeight;
	NSString* imageWidth;
	NSString* imageUrl;
	BOOL isInMyCollection;
	BOOL isInWishList;
	NSInteger awardYear;
	NSDate* favoriteDate;
	BOOL isImageLoaded;
	
}
@property(nonatomic, retain) NSString* isbnId10;
@property(nonatomic, retain) NSString* isbnId13;
@property(nonatomic, retain) NSString* description;
@property(nonatomic)NSInteger numberOfPages;
@property(nonatomic)float rating;
@property(nonatomic)NSInteger rank;
@property(nonatomic, retain)UIImage* image;
@property(nonatomic, retain) NSString* responceStatus;
@property(nonatomic, retain) NSString* responceVersion;
@property(nonatomic, retain) NSString* labelPlid;
@property(nonatomic, retain) NSString* labelName;
@property(nonatomic, retain) NSString* pageName;
@property(nonatomic, retain) NSString* count;
@property(nonatomic, retain) NSString* currentPage;
@property(nonatomic, retain) NSString* result;
@property(nonatomic, retain) NSString* title;
@property(nonatomic, retain) NSString* author;
@property(nonatomic, retain) NSString* binding;
@property(nonatomic, retain) NSString* msrp;
@property(nonatomic, retain) NSString* pages;
@property(nonatomic, retain) NSString* publisher;
@property(nonatomic, retain) NSString* publishedDate;
@property(nonatomic, retain) NSString* edition;
@property(nonatomic, retain) NSString* imageWidth;
@property(nonatomic, retain) NSString* imageHeight;
@property(nonatomic, retain) NSString* imageUrl;
@property(nonatomic) BOOL isInMyCollection;
@property(nonatomic) BOOL isInWishList;
@property(nonatomic) BOOL isImageLoaded;
@property(nonatomic) NSInteger awardYear;
@property(nonatomic, retain)	NSDate* favoriteDate;
@end
