//
//  ScanList.m
//  CampusBooks
//
//  Created by Admin on 20/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "ScanList.h"


@implementation ScanList
@synthesize 	 primaryKey,name,scanDate, noOfItems;


- (NSInteger)primaryKey {
    return primaryKey;
}

- (void)setPrimaryKey:(NSInteger )aPrimaryKey{
    primaryKey = aPrimaryKey;
}
- (NSString *)name {
    return name;
}

- (void)setName:(NSString *) aName {
 //   if ((!name && ! aName ) || (name && aName)) return;
	
    [name release];
    name = [aName copy]  ;
}

- (NSDate *)scanDate{
    return scanDate;
}

- (void)setDueDate:(NSDate *)aScanDate {
    if ((!scanDate && !aScanDate) || (scanDate && aScanDate && [scanDate isEqualToDate:aScanDate])) return;
    [scanDate release];
    scanDate = [aScanDate copy] ;
}

- (NSInteger)noOfItems {
    return noOfItems;
}

- (void)setNoOfItems:(NSInteger )aNoOfItems{
    noOfItems = aNoOfItems;
}
- (id)initWithPrimaryKey:(NSInteger)apk database:(sqlite3 *)db {
	
	if (self = [super init]) 
	{
		primaryKey = apk;
		database = db;
		sqlite3_stmt* init_statement;
		
		const char *sql = "SELECT pk,name, scanDate  FROM scanlist WHERE pk=?";
		
		if (sqlite3_prepare_v2(database, sql, -1, &init_statement, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
		
		sqlite3_bind_int(init_statement, 1, primaryKey);
		
		if (sqlite3_step(init_statement) == SQLITE_ROW) {
			
			int intpk = sqlite3_column_int(init_statement, 0);
			self.primaryKey  = (intpk != 0 ? intpk:0);

			char *strName= (char *)sqlite3_column_text(init_statement, 1);
			self.name = (strName != nil ? [NSString stringWithUTF8String:strName] : @"");

			self.scanDate = (sqlite3_column_double(init_statement, 2) != 0) ? [NSDate dateWithTimeIntervalSinceReferenceDate:sqlite3_column_double(init_statement, 2)] : nil;
			
		} 
		else {
			
		}
		
		sqlite3_finalize(init_statement);
	}
	return self;
}

-(void) dealloc
{
	[name release];
	[scanDate release];
	[super dealloc];
}
@end