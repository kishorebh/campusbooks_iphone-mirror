//
//  Book.m
//  CampusBooks
//
//  Created by Admin on 14/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "Book.h"


@implementation Book
@synthesize isbnId10,isbnId13,description,numberOfPages,rating,rank, 
		      image,responceStatus,responceVersion,labelPlid,labelName,
                        pageName,count,currentPage,result,title,author,
                        binding,msrp,pages,publisher,publishedDate,edition,imageHeight,
                        imageWidth,imageUrl, isInMyCollection,isInWishList, awardYear,favoriteDate, isImageLoaded;

- (NSDate *)favoriteDate {
    return favoriteDate;
}

- (void)setFavoriteDate:(NSDate *) aFavoriteDate {
	
    [favoriteDate release];
    favoriteDate = [aFavoriteDate copy]  ;
}

- (NSString *)result {
    return result;
}

- (void)setResult:(NSString *) aResult {
	
    [result release];
    result = [aResult copy]  ;
}
- (NSString *)currentPage {
    return currentPage;
}

- (void)setCurrentPage:(NSString *) aCurrentPage {
	
    [currentPage release];
    currentPage = [aCurrentPage copy]  ;
}
- (NSString *)count {
    return count;
}

- (void)setCount:(NSString *) aCount {
	
    [count release];
    count = [aCount copy]  ;
}
- (NSString *) imageHeight {
    return imageHeight;
}

- (void)setImageHeight:(NSString *) aImageHeight {
	
    [imageHeight release];
    imageHeight = [aImageHeight copy]  ;
}
- (NSString *) imageWidth {
    return imageWidth;
}

- (void)setImageWidth:(NSString *) aImageWidth {
	
    [imageWidth release];
    imageWidth = [aImageWidth copy]  ;
}
- (UIImage *) image {
    return image;
}

- (void)setImage:(UIImage *) aImage {
 //   if ((!image && ! aImage ) || (image && aImage)) return;
	
  // [image release];
    image = [aImage retain];
}

- (float ) rating {
    return rating;
}

- (void)setRating:(float)aRating {
    rating = aRating ;
}
- (NSInteger ) awardYear {
    return awardYear;
}
- (void)setAwardYear:(NSInteger)aAwardYear {
    awardYear = aAwardYear ;
}
- (NSString *) edition {
    return edition;
}

- (void)setEdition:(NSString *) aEdition {
	
    [edition release];
    edition = [aEdition copy]  ;
}
- (NSString *) publishedDate {
    return publishedDate;
}

- (void)setPublishedDate:(NSString *) aPublishedDate {
	
    [publishedDate release];
    publishedDate = [aPublishedDate copy]  ;
}
- (NSString *) publisher {
    return publisher;
}

- (void)setPublisher:(NSString *) aPublisher {
	
    [publisher release];
    publisher = [aPublisher copy]  ;
}
- (NSString *) pages {
    return pages;
}

- (void)setPages:(NSString *) aPages {
	
    [pages release];
    pages = [aPages copy]  ;
}
- (NSString *) msrp {
    return msrp;
}

- (void)setMsrp:(NSString *) aMsrp {
	
    [msrp release];
    msrp = [aMsrp copy]  ;
}
- (NSString *) binding {
    return binding;
}

- (void)setBinding:(NSString *) aBinding {
	
    [binding release];
    binding = [aBinding copy]  ;
}
- (NSString *) author {
    return author;
}

- (void)setAuthor:(NSString *) aAuthor {
	
    [author release];
    author = [aAuthor copy]  ;
}
- (NSString *) title {
    return title;
}

- (void)setTitle:(NSString *) aTitle {
	
    [title release];
    title = [aTitle copy]  ;
}

- (NSString *)pageName {
    return pageName;
}

- (void)setPageName:(NSString *) aPageName {
	
    [pageName release];
    pageName = [aPageName copy]  ;
}

- (void)setIsbnId10:(NSString *) aIsbnId10 {
    [isbnId10 release];
    isbnId10 = [aIsbnId10 copy]  ;
}

- (NSString *)responceVersion {
    return responceVersion;
}

- (void)setResponceVersion:(NSString *) aResponceVersion {
	
    [responceVersion release];
    responceVersion = [aResponceVersion copy]  ;
}
- (NSString *)labelPlid {
    return labelPlid;
}

- (void)setLabelPlid:(NSString *) aLabelPlid {
	
    [labelPlid release];
    labelPlid = [aLabelPlid copy]  ;
}

- (NSString *)responceStatus {
    return responceStatus;
}

- (void)setResponceStatus:(NSString *) aResponceStatus {
	
    [responceStatus release];
    responceStatus = [aResponceStatus copy]  ;
}
- (NSString *) labelName {
    return labelName;
}

- (void)setLabelName:(NSString *) aLabelName {
	
    [labelName release];
    labelName = [aLabelName copy]  ;
}

-(NSComparisonResult)compareByRankAndRating:(Book*)aBook
{
	if (rank != aBook.rank)
	{
		if (rank < aBook.rank )
		{
			return -1;
		}
		else 
		{
			return 1;
		}
	}
	else 
	{
		if( rating > aBook.rating) 
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}
	return 0;
}

-(NSComparisonResult)compareByRank:(Book*)aBook
{
	if (rank < aBook.rank)
	{
		return -1;
	}
	else
	{
		return 1;
	}
	return 0;
}

-(NSComparisonResult)compareByAwardYear:(Book*)aBook
{
	if( awardYear <  aBook.awardYear)
	{
		return 1;
	}
	else
	{
		return -1;
	}
	return 0;
}
-(NSComparisonResult)compareByFavoriteDate:(Book*)aBook
{

	if( [favoriteDate timeIntervalSinceReferenceDate]  >  [aBook.favoriteDate timeIntervalSinceReferenceDate])
	{
		return -1;
	}
	else
	{
		return 1;
	}
	return 0;
}
-(void) dealloc
{

	
	[isbnId10 release];
	[isbnId13 release];
	[description release];
//	[image release];
	[responceStatus release];
	[responceVersion release];
	[labelPlid release];
	[labelName release];
	[pageName release];
	[count release];
	[currentPage release];
	[result release];
	[title release];
	[author release];
	[binding release];
	[msrp release];
	[pages release];
	[publisher release];
	[publishedDate release];
	[edition release];
	[imageHeight release];
	[imageWidth release];
	[imageUrl release];
	[favoriteDate release];
	
	[super dealloc];
}
@end

