//
//  Coupon.m
//  Campusbooks
//
//  Created by sekhar bethalam on 01/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Coupon.h"

@implementation Coupon 
@synthesize code, description, shortDescription, expirationDate;

-(void)dealloc {
    
//    [self.code release];
//    [self.description release];
//    [self.shortDescription release];
//    [self.expirationDate release];
    [super dealloc];
}
-(NSString*)description {
    NSLog(@"%@", code);
    NSLog(@"%@", description);
        NSLog(@"%@", expirationDate);

    return [NSString stringWithFormat:@"* %@ - Use Code: %@ – Expires: %@", [description stringByReplacingOccurrencesOfString:@"\n" withString:@""], code, expirationDate];
}
@end
