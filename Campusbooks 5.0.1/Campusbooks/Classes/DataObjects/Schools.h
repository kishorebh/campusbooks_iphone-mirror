//
//  Schools.h
//  CampusBooks
//
//  Created by Admin on 17/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface Schools : NSObject 
{
	NSInteger primaryKey;
	NSString* schoolName;
	NSString* zipCode;
	NSString* phoneNumber;
	NSString* latValue;
	NSString* langValue;
	sqlite3 *database;
}
@property(nonatomic)NSInteger primaryKey;
@property(nonatomic,retain)NSString* schoolName;
@property(nonatomic,retain)NSString* zipCode;
@property(nonatomic,retain)NSString* phoneNumber;
@property(nonatomic,retain)NSString* latValue;
@property(nonatomic,retain)NSString* langValue;
- (id)initWithPrimaryKey:(NSInteger)apk database:(sqlite3 *)db ;
@end
