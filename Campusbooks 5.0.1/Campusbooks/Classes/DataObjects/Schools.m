//
//  Schools.m
//  CampusBooks
//
//  Created by Admin on 17/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Schools.h"


@implementation Schools
@synthesize  schoolName,zipCode,phoneNumber,latValue,langValue,primaryKey;

- (NSString *)langValue {
    return langValue;
}

- (void)setLangValue:(NSString *) aLangValue {
    if ((!langValue && ! aLangValue ) || (langValue && aLangValue)) return;
	
    [langValue release];
    langValue = [aLangValue copy]  ;
}
- (NSString *)latValue {
    return latValue;
}

- (void)setLatValue:(NSString *) aLatValue {
    if ((!latValue && ! aLatValue ) || (latValue && aLatValue)) return;
	
    [latValue release];
    latValue = [aLatValue copy]  ;
}
- (NSString *)phoneNumber {
    return phoneNumber;
}

- (void)setPhoneNumber:(NSString *) aPhoneNumber {
    if ((!phoneNumber && ! aPhoneNumber ) || (phoneNumber && aPhoneNumber)) return;
	
    [phoneNumber release];
    phoneNumber = [aPhoneNumber copy]  ;
}
- (NSString *)schoolName {
    return schoolName;
}

- (void)setSchoolName:(NSString *) aSchoolName {
    if ((!schoolName && ! aSchoolName ) || (schoolName && aSchoolName)) return;
	
    [schoolName release];
    schoolName = [aSchoolName copy]  ;
}
- (NSString *)zipCode {
    return zipCode;
}
- (void)setZipCode:(NSString *) aZipCode {
    if ((!zipCode && ! aZipCode ) || (zipCode && aZipCode)) return;
	
    [zipCode release];
    zipCode = [aZipCode copy]  ;
}
- (id)initWithPrimaryKey:(NSInteger)apk database:(sqlite3 *)db {
	
	if (self = [super init]) 
	{
		primaryKey = apk;
		database = db;
		sqlite3_stmt* init_statement;
		
		const char *sql = "SELECT pk,schoolName,zipCode,phoneNumber,latValue,langValue FROM schools WHERE pk=?";
		
		if (sqlite3_prepare_v2(database, sql, -1, &init_statement, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
		
		sqlite3_bind_int(init_statement, 1, primaryKey);
		
		if (sqlite3_step(init_statement) == SQLITE_ROW) {
			
			int intpk = sqlite3_column_int(init_statement, 0);
			self.primaryKey  = (intpk != 0 ? intpk:0);
			
			
			char *strSchoolName= (char *)sqlite3_column_text(init_statement, 1);
			self.schoolName = (strSchoolName != nil ? [NSString stringWithUTF8String:strSchoolName] : @"");
			
			char *getZipCode= (char *)sqlite3_column_text(init_statement, 2);
			self.zipCode = (getZipCode != nil ? [NSString stringWithUTF8String:getZipCode] : @"");
			
			char *strPhoneNumber= (char *)sqlite3_column_text(init_statement, 3);
			self.phoneNumber  = (strPhoneNumber != nil ? [NSString stringWithUTF8String:strPhoneNumber] : @"");;
			
			char *strLatValue = (char *)sqlite3_column_text(init_statement, 4);
			self.latValue = (strLatValue != nil ? [NSString stringWithUTF8String:strLatValue] : @"");
			
			
			char *strLangValue= (char *)sqlite3_column_text(init_statement, 5);
			self.langValue = (strLangValue != nil ? [NSString stringWithUTF8String:strLangValue] : @"");
			
			
		} 
		else
		{
			self.schoolName=@"";
			self.zipCode=@"";
			self.phoneNumber=@"";
			self.latValue=@"";
			self.langValue=@"";
		}
		
		sqlite3_finalize(init_statement);
	}
	return self;
}

-(void) dealloc
{
	self.schoolName=nil;
	self.phoneNumber=nil;
	self.latValue=nil;
	self.langValue=nil;
	self.zipCode=nil;
	[self.zipCode release];
	[self.schoolName release];
	[self.phoneNumber release];
	[self.latValue release];
	[self.langValue release];
	[super dealloc];
}
@end
