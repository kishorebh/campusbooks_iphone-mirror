//
//  BookBuyBack.h
//  CampusBooks
//
//  Created by bluepal on 18/11/09.
//  Copyright 2009 bluepal. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BookBuyBack : NSObject {
	
	NSString* responceStatus;
	NSString* responceVersion;
	NSString* labelPlid;
	NSString* labelName;
	NSString* pageName;
	NSString* conditionId;
	NSString* conditionName;
	NSString* isbn13;
	NSString* isbn10;
	NSString* offersId;
	NSString* merchantId;
	NSString* merchantName;
	NSString* price;
	NSString* priceNew;
	NSString* priceUsed;
	NSString* shippingGround;
	NSString* totalPrice;
	NSString* link;
	NSString* offerConditionId;
	NSString* offerConditionText;
	NSString* availabilityId;
	NSString* availabilityText;
	NSString* comments;
	
	NSInteger shoppingMode;
	
}

@property(nonatomic,retain)NSString*responceStatus;
@property(nonatomic,retain)NSString*responceVersion;
@property(nonatomic,retain)NSString*labelPlid;
@property(nonatomic,retain)NSString*labelName;
@property(nonatomic,retain)NSString*pageName;
@property(nonatomic,retain)NSString*conditionId;
@property(nonatomic,retain)NSString*conditionName;
@property(nonatomic,retain)NSString*isbn13;
@property(nonatomic,retain)NSString*isbn10;
@property(nonatomic,retain)NSString* offersId;
@property(nonatomic,retain)NSString*merchantId;
@property(nonatomic,retain)NSString*merchantName;
@property(nonatomic,retain)NSString*price;
@property(nonatomic,retain)NSString* priceNew;
@property(nonatomic,retain)NSString* priceUsed;
@property(nonatomic,retain)NSString*shippingGround;
@property(nonatomic,retain)NSString*totalPrice;
@property(nonatomic,retain)NSString*link;
@property(nonatomic,retain)NSString*offerConditionId;
@property(nonatomic,retain)NSString*offerConditionText;
@property(nonatomic,retain)NSString*availabilityId;
@property(nonatomic,retain)NSString*availabilityText;
@property(nonatomic,retain)NSString*comments;
@property(nonatomic)NSInteger shoppingMode;

@end
