//
//  BookInfo.m
//  CampusBooks
//
//  Created by Admin on 13/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "BookInfo.h"


@implementation BookInfo

@synthesize responceStatus,responceVersion,labelPlid,labelName,pageName,count,
currentPage,result,isbn10,isbn13,title,author,binding,msrp,pages,
publisher,publishedDate,edition,rank,rating,image,imageWidth,
numberOfPages,imageHeight,imageUrl;


- (NSString *)result {
    return result;
}

- (void)setResult:(NSString *) aResult {
    if ((!result && ! aResult ) || (result && aResult)) return;
	
    [result release];
    result = [aResult copy]  ;
}
- (NSString *)currentPage {
    return currentPage;
}

- (void)setCurrentPage:(NSString *) aCurrentPage {
    if ((!currentPage && ! aCurrentPage ) || (currentPage && aCurrentPage)) return;
	
    [currentPage release];
    currentPage = [aCurrentPage copy]  ;
}
- (NSString *)count {
    return count;
}

- (void)setCount:(NSString *) aCount {
    if ((!count && ! aCount ) || (count && aCount)) return;
	
    [count release];
    count = [aCount copy]  ;
}
- (NSString *) imageHeight {
    return imageHeight;
}

- (void)setImageHeight:(NSString *) aImageHeight {
    if ((!imageHeight && ! aImageHeight) || (imageHeight && aImageHeight)) return;
	
    [imageHeight release];
    imageHeight = [aImageHeight copy]  ;
}
- (NSString *) imageWidth {
    return imageWidth;
}

- (void)setImageWidth:(NSString *) aImageWidth {
    if ((!imageWidth && ! aImageWidth) || (imageWidth && aImageWidth)) return;
	
    [imageWidth release];
    imageWidth = [aImageWidth copy]  ;
}
- (NSString *) image {
    return image;
}

- (void)setImage:(NSString *) aImage {
    if ((!image && ! aImage ) || (image && aImage)) return;
	
    [image release];
    image = [aImage copy]  ;
}

- (NSString *) rating {
    return rating;
}

- (void)setRating:(NSString *) aRating {
    if ((!rating && ! aRating ) || (rating && aRating)) return;
	
    [rating release];
    rating = [aRating copy]  ;
}

- (NSString *) rank {
    return rank;
}

- (void)setRank:(NSString *) aRank {
    if ((!rank && ! aRank ) || (rank && aRank)) return;
	
    [rank release];
    rank = [aRank copy]  ;
}

- (NSString *) edition {
    return edition;
}

- (void)setEdition:(NSString *) aEdition {
    if ((!edition && ! aEdition ) || (edition && aEdition)) return;
	
    [edition release];
    edition = [aEdition copy]  ;
}
- (NSString *) publishedDate {
    return publishedDate;
}

- (void)setPublishedDate:(NSString *) aPublishedDate {
    if ((!publishedDate && ! aPublishedDate ) || (publishedDate && aPublishedDate)) return;
	
    [publishedDate release];
    publishedDate = [aPublishedDate copy]  ;
}
- (NSString *) publisher {
    return publisher;
}

- (void)setPublisher:(NSString *) aPublisher {
    if ((!publisher && ! aPublisher ) || (publisher && aPublisher)) return;
	
    [publisher release];
    publisher = [aPublisher copy]  ;
}
- (NSString *) pages {
    return pages;
}

- (void)setPages:(NSString *) aPages {
    if ((!pages && ! aPages ) || (pages && aPages)) return;
	
    [pages release];
    pages = [aPages copy]  ;
}
- (NSString *) msrp {
    return msrp;
}

- (void)setMsrp:(NSString *) aMsrp {
    if ((!msrp && ! aMsrp ) || (msrp && aMsrp)) return;
	
    [msrp release];
    msrp = [aMsrp copy]  ;
}
- (NSString *) binding {
    return binding;
}

- (void)setBinding:(NSString *) aBinding {
    if ((!binding && ! aBinding ) || (binding && aBinding)) return;
	
    [binding release];
    binding = [aBinding copy]  ;
}
- (NSString *) author {
    return author;
}

- (void)setAuthor:(NSString *) aAuthor {
    if ((!author && ! aAuthor ) || (author && aAuthor)) return;
	
    [author release];
    author = [aAuthor copy]  ;
}
- (NSString *) title {
    return title;
}

- (void)setTitle:(NSString *) aTitle {
    if ((!title && ! aTitle ) || (title && aTitle)) return;
	
    [title release];
    title = [aTitle copy]  ;
}

- (NSString *)pageName {
    return pageName;
}

- (void)setPageName:(NSString *) aPageName {
    if ((!pageName && ! aPageName ) || (pageName && aPageName)) return;
	
    [pageName release];
    pageName = [aPageName copy]  ;
}
- (NSString *) aIsbn13 {
    return isbn13;
}

- (void)setIsbn13:(NSString *) aIsbn13 {
    if ((!isbn13 && ! aIsbn13 ) || (isbn13 && aIsbn13)) return;
	
    [isbn13 release];
    isbn13 = [aIsbn13 copy]  ;
}
- (NSString *) aIsbn10 {
    return isbn10;
}

- (void)setIsbn10:(NSString *) aIsbn10 {
    if ((!isbn10 && ! aIsbn10 ) || (isbn10 && aIsbn10)) return;
	
    [isbn10 release];
    isbn10 = [aIsbn10 copy]  ;
}
- (NSString *)responceVersion {
    return responceVersion;
}

- (void)setResponceVersion:(NSString *) aResponceVersion {
    if ((!responceVersion && ! aResponceVersion ) || (responceVersion && aResponceVersion)) return;
	
    [responceVersion release];
    responceVersion = [aResponceVersion copy]  ;
}
- (NSString *)labelPlid {
    return labelPlid;
}

- (void)setLabelPlid:(NSString *) aLabelPlid {
    if ((!labelPlid && ! aLabelPlid ) || (labelPlid && aLabelPlid)) return;
	
    [labelPlid release];
    labelPlid = [aLabelPlid copy]  ;
}

- (NSString *)responceStatus {
    return responceStatus;
}

- (void)setResponceStatus:(NSString *) aResponceStatus {
    if ((!responceStatus && ! aResponceStatus ) || (responceStatus && aResponceStatus)) return;
	
    [responceStatus release];
    responceStatus = [aResponceStatus copy]  ;
}
- (NSString *) labelName {
    return labelName;
}

- (void)setLabelName:(NSString *) aLabelName {
    if ((!labelName && ! aLabelName) || (labelName && aLabelName)) return;
	
    [labelName release];
    labelName = [aLabelName copy]  ;
}

-(void)dealloc
{
	
	[ responceStatus release];
	[ responceVersion release];
	[ labelPlid release];
	[ labelName release];
	[ pageName release];
	[ count release];
	[ currentPage release];
	[ result release];
	[ isbn10 release];
	[ isbn13 release];
	[ title release];
	[ author release];
	[ binding release];
	[ msrp release];
	[ pages release];
	[ publisher release];
	[ publishedDate release];
	[ edition release];
	[ rank release];
	[ rating release];
	[ image release];
	[ imageWidth release];
	[ imageHeight release];
	[ imageUrl release];
	[super dealloc];
	
}
@end