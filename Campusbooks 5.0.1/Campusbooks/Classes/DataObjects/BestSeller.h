//
//  BestSeller.h
//  CampusBooks
//
//  Created by Admin on 05/08/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@interface BestSeller : NSObject {
	
	NSInteger year;
	NSString* bookName;
	NSString* isbnId;
	NSString* authorName;
	NSInteger categoryId;
	int primaryKey;
	sqlite3 *database;
	
}
@property(nonatomic)NSInteger primaryKey;
@property(nonatomic)NSInteger year;
@property(nonatomic,retain)NSString* bookName;
@property(nonatomic,retain)NSString* authorName;
@property(nonatomic)NSInteger categoryId;
@property(nonatomic,retain)NSString* isbnId;
- (id)initWithPrimaryKey:(NSInteger)apk database:(sqlite3 *)db;


@end
