//
//  BookDetailsViewController.h
//  CampusBooks
//
//  Created by Admin on 19/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import"Reachability.h"
#import"Book.h"
#import"WishList.h"
#import"MyCollection.h"
#import"HTMLUtil.h"
#import"BookOfferViewController.h"
#import"BookBuyViewController.h"
#import"BooKSellViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface BookDetailsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, MFMailComposeViewControllerDelegate>{
	Book* currentBookInfo;
	UIViewController* bookListViewController;
	UIView* contentView ;
	UIActivityIndicatorView* progressInd;

	NetworkStatus internetConnectionStatus;
	
	UITableView* tableView;
	
	UIImageView* titleView;
	NSArray* authorNamesList;
	
	NSDateFormatter* originalDateFormatter;
	NSDateFormatter* newDateFormatter;
	
	UIViewController* sourceViewController;
	BookBuyViewController* bookBuyViewController;
	BookSellViewController* bookSellViewController;
	
	NSMutableArray* bookOffersList ;
}
@property NetworkStatus internetConnectionStatus;
- (void)updateStatus;
-(NSString *)hostName;

//-(id)initWithBook:(Book*)aBook withOffers:(NSMutableArray*)aOffersList fromSource:(UIViewController*)aBookListViewController;
-(id)initWithBook:(Book*)aBook fromSource:(UIViewController*)aBookListViewController;
-(void)refreshBookDetailsViewController:(Book*)aBook fromSource:(UIViewController*)sourceViewController ;

- (CGFloat)getTextHeightForSystemFontOfSize:(CGFloat)size ofWidth:(CGFloat)maxWidth forText:(NSString*)aText ;


-(void)addToWishList;
-(void)removeFromWishList;
-(void)addToMyCollection;
-(void)removeFromMyCollection;
-(void)buyItAction;
-(void)sellItAction;
@end
