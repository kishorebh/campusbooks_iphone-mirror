//
//  Pic2BarCodeScanViewController.h
//  CampusBooks
//
//  Created by Admin on 21/11/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"MyCustomButton.h"
#import"ScanItem.h"
#import"ScanList.h"
#import"Reachability.h"
#import"Book.h"
#import"BookCoverLoader.h"
#import"OverlayController.h"
@interface Pic2BarCodeScanViewController : UIViewController<UINavigationControllerDelegate, UIAlertViewDelegate, BookCoverLoaderDelegate, BarcodePickerControllerDelegate> {
	UIView* contentView;

	BOOL allowScan;
	
	UIActivityIndicatorView* progressInd;

	NSMutableArray* currScanedBooksList;
	
	NSString* scannedISBN;
	
	UIAlertView* pic2ShopAlert;
	NetworkStatus internetConnectionStatus;
	NSMutableArray* bookOffersList;
	Book* currentBook;
	NSString* scannedEan;
	
	BOOL isFoundEan;
	
//	OverlayController * overlayController;

}
-(void)openBarCodeReader;
@property NetworkStatus internetConnectionStatus;
@property (nonatomic, retain) UILabel *resultsLabel;

- (void)updateStatus;
-(NSString *)hostName;

-(NSMutableArray*)getBooksListFromScannedItems:(NSMutableArray*)aScanItemsList;
-(void)buyItAction;
- (void)barcodeFound:(NSString*)ean pretty:(NSString*)prettyean;
- (void)liveScannerDidCancel;
- (void)dismissLiveScanner;

@end
