//
//  Pic2BarCodeScanViewController.m
//  CampusBooks
//
//  Created by Admin on 21/11/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "ScanHistoryViewController.h"
#import"CampusBooksAppDelegate.h"
#import"Book.h"


@implementation ScanHistoryViewController

- (id)init{
    if (self = [super init]) {
	//	overlayController = [[OverlayController alloc] init];
		
		[UIApplication sharedApplication].statusBarHidden = NO;
		
		contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
		contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
		[self.view addSubview: contentView];
		self.title = @"Scan History";
		
//		self.navigationItem.rightBarButtonItem =[[ [UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(closeViewController)] autorelease];
		
		noItemsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, 320,50)];
		noItemsLabel.text = @"No history of scans is available";
		noItemsLabel.numberOfLines = 0;
		noItemsLabel.textAlignment = UITextAlignmentCenter;
		noItemsLabel.textColor =[UIColor darkTextColor];// [UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];;
		noItemsLabel.font = [UIFont boldSystemFontOfSize:18];
		noItemsLabel.backgroundColor = [UIColor clearColor];

		
		singleScanButton = [[UIButton alloc] initWithFrame:CGRectMake(95, 320, 133, 45)];
		[singleScanButton setImage:[UIImage imageNamed:@"Scan.png"] forState:UIControlStateNormal];
	//	[singleScanButton setImage:[UIImage imageNamed:@"scanbook_press.png"] forState:UIControlStateHighlighted];
		singleScanButton.backgroundColor = [UIColor clearColor];
		[singleScanButton addTarget:self action:@selector(displaySingleScan) forControlEvents:UIControlEventTouchUpInside];
		[contentView addSubview:singleScanButton];
		
		
		tableView = [[UITableView alloc] initWithFrame:CGRectMake(5, 8, 310,300) style:UITableViewStyleGrouped];
		tableView.delegate = self;
		tableView.dataSource = self;
		tableView.rowHeight = 45;
		tableView.sectionHeaderHeight = 5;
		tableView.sectionFooterHeight = 0;		
		tableView.backgroundColor = [UIColor clearColor];
		[contentView addSubview:tableView];
		
		dateFormatter = [[NSDateFormatter alloc] init];
		
		[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
		[dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
		
		booksList = [[NSMutableArray alloc] init] ;
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication] delegate];
	scanListList = [appDelegate getScanListList];
	
	for (UIView* subview in contentView.subviews)
	{
		[subview removeFromSuperview];
	}
	[contentView addSubview:tableView];
	[contentView addSubview:singleScanButton];
	if([scanListList count]==0)
	{
		[contentView addSubview:noItemsLabel];
	}
	
		
	[tableView reloadData];

}
/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	//printf("\n In ScanHistory View Controller Dealloc");
    [super dealloc];
	[currScanedBooksList release];
	[dateFormatter release];
	[contentView release];
	[tableView release];
	[singleScanButton release];
	[booksList release]; 
}

-(IBAction) displaySingleScan
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	[self.navigationController pushViewController:appDelegate.barcodeViewController animated:YES];
}


#pragma mark Table Delegate and Data Source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tv {
	
	return 1;
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)section {
	
	return [scanListList count] ;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
	if (cell == nil)
	{
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyIdentifier"] autorelease];
		cell.selectionStyle = UITableViewCellSelectionStyleGray;
		cell.backgroundColor = [UIColor whiteColor];
		
		UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0,0, 300,30)];
		myView.tag = 0;
		[cell.contentView addSubview:myView];
		[myView release];
	}
	
	UIView* cellContentView = [cell.contentView viewWithTag:0];
	for(UIView* subView in cellContentView.subviews)
	{
		[subView removeFromSuperview];
	}
	UIFont* labelFont =  [ UIFont boldSystemFontOfSize:16];
	
	UIColor* backgroundColor = cell.backgroundColor=[UIColor colorWithRed:0.92578125 green:0.92578125 blue:0.92578125 alpha:1];
	//UIColor* singleRowTextColor =[UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1] ;
	UIColor* singleRowTextColor =[UIColor blackColor];
	cell.backgroundColor = backgroundColor;
	cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;

	if ([scanListList count ] > 0)
	{
		ScanList* scanList = [scanListList objectAtIndex:indexPath.row];

		NSString* dateStr = (scanList.scanDate != nil) ? [dateFormatter stringFromDate:scanList.scanDate] : @"";
		NSString* labelDisplayStr = [scanList.name length] > 0 ? scanList.name : dateStr;

		UILabel*listDate =  [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 250,25)];
		listDate.numberOfLines = 0;
		listDate.font = labelFont;
		listDate.text = labelDisplayStr;
		listDate.backgroundColor =backgroundColor;
		listDate.textAlignment = UITextAlignmentLeft;
		listDate.textColor = singleRowTextColor;
		[cellContentView addSubview:listDate];
		[listDate release];
		
		
			
//		NSString* noOfObjectsStr = [NSString stringWithFormat:@"%d", scanList.noOfItems];
//
//		MyCustomButton* customButton =  [ [MyCustomButton alloc] initWithIndexPath:indexPath];
//		 [customButton setFrame:CGRectMake(210,7.5, 40.0,30.0)];	
//		[customButton addTarget:self action:@selector(countButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//		 [customButton setBackgroundImage:[UIImage imageNamed:@"backgroundImage.png"] forState:UIControlStateNormal ];
//		 [customButton setTitle:noOfObjectsStr forState:UIControlStateNormal];
//		customButton.titleLabel.textColor=[UIColor colorWithRed:0.2421875 green:0.2421875 blue:0.2421875 alpha:1];
//		 [cellContentView addSubview:customButton];
//		 [customButton release];
	}
	return cell;
}


-(void)countButtonAction:(MyCustomButton*)customButton
{
	ScanList* scanList = [scanListList objectAtIndex:customButton.indexPath.row];
	[self displayBooksFromScanList:scanList];
}
- (void)tableView:(UITableView *)tv accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
	[self tableView:tv willSelectRowAtIndexPath:indexPath];	
}
- (NSIndexPath *)tableView:(UITableView *)tv willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if ([scanListList count] > 0)
	{
		ScanList* scanList = [scanListList objectAtIndex:indexPath.row];
		[self displayBooksFromScanList:scanList];
	}
	[tableView deselectRowAtIndexPath:indexPath animated:NO];

	return nil;

}
-(void)displayBooksFromScanList:(ScanList*)aScanList
{
	//printf("\n Display Books from Scan List:%d", aScanList.primaryKey);
	CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [[UIApplication sharedApplication]delegate];
	NSMutableArray* scanItemsArray = [appDelegate getScanItemList:aScanList.primaryKey];
	BookListViewController* bookListViewController = [[BookListViewController alloc] initWithBooksList:nil fromSource:self];
	booksList = [self  getBooksListFromScannedItems:scanItemsArray];
	[bookListViewController refreshBookListViewController:booksList fromSource:appDelegate.scanHistoryViewController];
	//printf("\n No of objects returned :%d", [booksList count]);
	if ([booksList count] >0)
	{
		//printf("\n About to display bookList ViewController. \n is Book List view Controller nil ? :%s", (bookListViewController== nil? "Yes" :"No"));
		
		[[self navigationController] pushViewController:bookListViewController animated:YES];
	}
	[bookListViewController release];

}
-(NSMutableArray*)getBooksListFromScannedItems:(NSMutableArray*)aScanItemsList
{
	[booksList removeAllObjects];
	for(ScanItem* scanItem in aScanItemsList)
	{
		Book* book = [[Book alloc] init];
		book.author = scanItem.authorName;
		book.title= scanItem.bookName;
		book.isbnId10 = scanItem.isbnId10;
		book.imageUrl = scanItem.imageUrl;
		[booksList addObject:book];
		[book release];
	}
	return booksList;
}

- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath 
{
	if ([scanListList count] >0)
	{
		ScanList* scanList = [scanListList objectAtIndex:indexPath.row];
		CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [[UIApplication sharedApplication]delegate];
		[appDelegate deleteFromScanList:scanList];
		[scanListList removeObjectAtIndex:indexPath.row];
		[aTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
		
		if([scanListList count] == 0){
			[aTableView reloadData];
		}
	}
}


@end
