//
//  CampusBooksViewController.m
//  CampusBooks
//
//  Created by Admin on 13/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "CampusBooksViewController.h"
#import "CampusBooksAppDelegate.h"
#import "PopViewController.h"
#define IPHONE_1G_NAMESTRING @"iPhone 1G"
#define IPHONE_3G_NAMESTRING @"iPhone 3G"
#define IPHONE_3GS_NAMESTRING @"iPhone 3GS"
#define IPOD_1G_NAMESTRING @"iPod touch 1G"
#define IPOD_2G_NAMESTRING @"iPod touch 2G"
@implementation CampusBooksViewController
@synthesize internetConnectionStatus, strKeyword;;
//static UISegmentedControl *__segmentControl = nil;

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)init {
    if (self = [super init]) {
        // Custom initialization
		contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, 320, 420)];
		contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
		[self.view addSubview: contentView];
		self.title = @"Search";
        
        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header-Small-Logo.png"]];
	//	self.navigationItem.titleView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@""]];
		
		bookListViewController = [[BookListViewController alloc] initWithBooksList:nil fromSource:self];
//		__segmentControl = [self createSegmentControl];
		
		imageView =[[UIImageView alloc] initWithFrame:CGRectMake(100, 10, 100, 100)];
		imageView.image = [UIImage imageNamed:@"icon-home.png"];
		[contentView addSubview:imageView];
		//[imageView release];
		
		compareLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 125, 320, 15)];
		compareLabel.text = @"Compare prices on any book";
		compareLabel.textColor = [UIColor whiteColor];
		compareLabel.font = [UIFont boldSystemFontOfSize:15];
		compareLabel.backgroundColor = [UIColor clearColor];
		compareLabel.textAlignment = UITextAlignmentCenter;
		[contentView addSubview:compareLabel];
		
		txtFldBookBG =[ [UIImageView alloc] initWithFrame:CGRectMake(20, 150, 280, 54)];
		txtFldBookBG.image = [UIImage imageNamed:@"textField.png"];
		[contentView addSubview:txtFldBookBG];
		[txtFldBookBG release];
		
		txtFldBook = [[UITextField alloc ] initWithFrame:CGRectMake(30, 165, 250, 25)];
		txtFldBook.returnKeyType = UIReturnKeySearch;
		txtFldBook.autocorrectionType = UITextAutocorrectionTypeNo;
		txtFldBook.autocapitalizationType = UITextAutocapitalizationTypeNone;
		txtFldBook.clearButtonMode = UITextFieldViewModeWhileEditing;
		txtFldBook.delegate = self;
		txtFldBook.textAlignment = UITextAlignmentLeft;
		txtFldBook.font = [UIFont boldSystemFontOfSize:16];
		//txtFldBook.backgroundColor = [UIColor clearColor];
		txtFldBook.textColor = [UIColor darkTextColor];//[UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1] ;
		txtFldBook.placeholder = @"Book Name";
	//	[txtFldBook becomeFirstResponder];
		[contentView addSubview:txtFldBook];

/*		theTableView = [[UITableView alloc]initWithFrame:CGRectMake(5, 110, 310,245) style:UITableViewStyleGrouped];
		theTableView.delegate = self;
		theTableView.dataSource = self;
		theTableView.scrollEnabled = NO;
		theTableView.backgroundColor = [UIColor clearColor];
		[contentView addSubview:theTableView];
		[theTableView release];
		[theTableView reloadData];
*/		
		
		if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
		{
			orLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 210, 320, 15)];
			orLabel.text = @"OR";
			orLabel.font = [UIFont boldSystemFontOfSize:15];
			orLabel.textColor = [UIColor whiteColor];
			orLabel.backgroundColor = [UIColor clearColor];
			orLabel.textAlignment = UITextAlignmentCenter;
			[contentView addSubview:orLabel];
			
			barcodeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 230, 320, 15)];
			barcodeLabel.text = @"Scan book barcode to compare prices";
			barcodeLabel.font = [UIFont boldSystemFontOfSize:15];
			barcodeLabel.textColor = [UIColor whiteColor];
			barcodeLabel.backgroundColor = [UIColor clearColor];
			barcodeLabel.textAlignment = UITextAlignmentCenter;
			[contentView addSubview:orLabel];
			
			pic2ShopScanButton = [[UIButton alloc] initWithFrame:CGRectMake(101.5, 250, 117, 35)];
			[pic2ShopScanButton setImage:[UIImage imageNamed:@"Scan.png"] forState:UIControlStateNormal];
		//	[pic2ShopScanButton setImage:[UIImage imageNamed:@"scanbook_press.png"] forState:UIControlStateHighlighted];
			[pic2ShopScanButton addTarget:self action:@selector(displayP2SBarCodeReaderScreen) forControlEvents:UIControlEventTouchUpInside];
			[contentView addSubview:pic2ShopScanButton];
		}

		searchButton = [[UIButton alloc] initWithFrame:CGRectMake(101.5, 295, 117, 35)];
		[searchButton setImage:[UIImage imageNamed:@"advanced-search.png"] forState:UIControlStateNormal];
		[searchButton addTarget:self action:@selector(displaySearchViewController) forControlEvents:UIControlEventTouchUpInside];
		//	searchButton.font = [UIFont boldSystemFontOfSize:18];
		[contentView addSubview:searchButton];
		
		localOptionsButton = [[UIButton alloc] initWithFrame:CGRectMake(101.5, 325, 117, 35)];
		[localOptionsButton setImage:[UIImage imageNamed:@"localoptions.png"] forState:UIControlStateNormal];
		[localOptionsButton addTarget:self action:@selector(displayLocalOptions) forControlEvents:UIControlEventTouchUpInside];
		//	searchButton.font = [UIFont boldSystemFontOfSize:18];
		[contentView addSubview:localOptionsButton];
		
		CGRect activityViewframe = CGRectMake(150.0,0, 35,35);
		progressInd = [[UIActivityIndicatorView alloc] initWithFrame:activityViewframe];
		progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
		progressInd.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
										UIViewAutoresizingFlexibleRightMargin |
										UIViewAutoresizingFlexibleTopMargin |
										UIViewAutoresizingFlexibleBottomMargin);
		[progressInd startAnimating];
		
		progressInd.hidden = NO;
		self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:progressInd ] autorelease];

		isLoadOptionsOnWillAppear = YES;
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
	
	/*if (isLoadOptionsOnWillAppear) {
		CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate* )[[UIApplication sharedApplication] delegate];
		LocalOptions* localObject=[appDelegate getLocalOptions];
		if (localObject.isFirstTime ==0) {
			[self displayLocalOptions];
			isLoadOptionsOnWillAppear = NO;
		}
	}
	 */
	isSearchInProgress = NO;
	[self refreshView];
	
}

-(void) displaySearchViewController
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	[[self navigationController] pushViewController:appDelegate.searchViewController animated:YES];
}
-(void)refreshView
{
	self.title = @"CampusBooks";
	for(UIView* subview in contentView.subviews)
	{
		[subview removeFromSuperview];
	}

//	[contentView addSubview:__segmentControl];
	[contentView addSubview:imageView];

	[contentView addSubview:txtFldBookBG];
	[contentView addSubview:txtFldBook];
	[contentView addSubview:compareLabel];
	[contentView addSubview:orLabel];
	[contentView addSubview:barcodeLabel];
	
	[contentView addSubview:searchButton];
	[contentView addSubview:pic2ShopScanButton];
	[contentView addSubview:localOptionsButton];	

	txtFldBook.placeholder = @" ISBN, Title, Author or Keyword";
/*	if(currentSearchSelection == 0)
	{
		txtFldBook.placeholder = @"ISBN";
	}
	else 	if(currentSearchSelection == 1)
	{
		txtFldBook.placeholder = @"Author";
	}
	else 	if(currentSearchSelection == 2)
	{
		txtFldBook.placeholder = @"Title";
	}
	else 	if(currentSearchSelection == 3)
	{
		txtFldBook.placeholder = @"Keyword";
	}
	
	
	[contentView addSubview:theTableView];
*/	[progressInd removeFromSuperview];
	if(isSearchInProgress)
	{
		self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:progressInd ] autorelease];
	}
	else
	{
		self.navigationItem.rightBarButtonItem = nil;
	}
}

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)dealloc {
		//	printf("\n In CampusbooksViewController Dealloc");
	[bookListViewController release];
	[searchButton release];
	[imageView release];
    [super dealloc];
	
}
-(void)performSearch
{
	
	
	isSearchInProgress = YES;
	[self refreshView];
	
	[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(doSearch) userInfo:nil repeats:NO];
}
-(void)doSearch
{
	[self updateStatus];
	
	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"CampusBooks" message:@"No Network Available"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		isSearchInProgress = NO;
		[self refreshView];
	}
	else
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
		//	@try {
/*		strIsbn = @"";
		strAuthor = @"";
		strTitle = @"";
		strKeyword = @"";
		
		if(currentSearchSelection == 0)
		{
			if ([[txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  length] > 0)
			{
				strIsbn = [txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			}
		}
		else if (currentSearchSelection == 1)
		{
			if ([[txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  length] > 0)
			{
				strAuthor = [txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			}
		}
		else if (currentSearchSelection ==2)
		{
			if ([[txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  length] > 0)
			{
				strTitle = [txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			}
		}
		else if (currentSearchSelection ==3)
		{
			if ([[txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  length] > 0)
			{
				strKeyword = [txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			}
		}
		
*/		strKeyword = @"";
		if ([[txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  length] > 0)
		{
			strKeyword = [txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		}
		
		if ( [strKeyword length] > 0)
		{
			progressInd.hidden = NO;
			
			//printf("\n Called Search");
			CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication] delegate]; 
			
			NSMutableArray* booksList = [appDelegate doBookSearch:nil byAuthor:nil withKeyword:strKeyword forISBN:nil forPage:1];
			
			[bookListViewController refreshBookListViewController:booksList fromSource:self];
			//printf("\n No of objects returned :%d", [booksList count]);
			if ([booksList count] >0)
			{
				[[self navigationController] pushViewController:bookListViewController animated:YES];
				//					[appDelegate displayBookListViewController:booksList fromSource:self];
			}
			else
			{
				UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:@"No books match the criteria. \nPlease revise it and try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
				[alertView show];
			}
 
		}
		else
		{
			UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:@"Please provide the search criteria and try again!!! "  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
			[alertView show];
			isSearchInProgress = NO;
			[self refreshView];
		}
		
		
		/*		}
		 @catch (NSException * e) {
		 //printf("\n caught an exception");
		 }
		 @finally {
		 
		 }
		 */
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		isSearchInProgress = NO;
		[self refreshView];
		
	}
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
	//	printf("\n Reached should return ");
	//	[theTextField resignFirstResponder];
	NSString* text = [theTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	if ([text length] > 0)
	{
		[self performSearch];
	}
	[theTextField resignFirstResponder];
	return YES;
}
/*
-(UISegmentedControl *)createSegmentControl{
	
	if (__segmentControl == nil)
	{
		
		NSArray *segmentTextContent = [NSArray arrayWithObjects:@"ISBN",@"Author", @"Title",@"Keyword", nil];
		//		NSArray *segmentTextContent = [NSArray arrayWithObjects:[UIImage imageNamed:@"home-search.png"],[UIImage imageNamed:@"home-search.png"],nil];
		__segmentControl = [[UISegmentedControl alloc] initWithItems:segmentTextContent];
		__segmentControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		__segmentControl.segmentedControlStyle = UISegmentedControlStyleBar;
		//__segmentControl.backgroundColor=[UIColor clearColor];
		__segmentControl.tintColor = [UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];
//		__segmentControl.tintColor = [UIColor lightTextColor];
		//__segmentControl.tintColor = [UIColor redColor];
		__segmentControl.selectedSegmentIndex =0;
		__segmentControl.frame=CGRectMake(25.0, 20.0, 270, 30);
		[__segmentControl addTarget:self action:@selector(segmentControlAction) forControlEvents:UIControlEventValueChanged];
	}
	
	return __segmentControl;
}
-(void)segmentControlAction
{
	currentSearchSelection = __segmentControl.selectedSegmentIndex;
	[self refreshView];
}
 */
-(NSString *)hostName
{
	// Don't include a scheme. 'http://' will break the reachability checking.
	// Change this value to test the reachability of a different host.
	return @"www.apple.com";
}
- (void)updateStatus
{
	
	self.internetConnectionStatus	= [[Reachability sharedReachability] internetConnectionStatus];
	//	[tableView reloadData];
}
/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView  numberOfRowsInSection:(NSInteger)section {
	
	//	printf("\n Total no of award records :%d", [awardsList count]);
	return 5;	
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 45;
}


- (void )tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath 
{
	self.title = @"Home";

	CampusBooksAppDelegate* appDelegate = [[UIApplication sharedApplication]delegate];
	if(newIndexPath.row == 0)
	{
		SearchViewController* searchViewController = [appDelegate searchViewController];
		[self.navigationController pushViewController:searchViewController animated:YES];
	}
	else if (newIndexPath.row == 1)
	{
		[appDelegate.tabBarController setSelectedIndex: 1];
	}
	else if (newIndexPath.row == 2)
	{
		[appDelegate.tabBarController setSelectedIndex: 2];
	}
	else if (newIndexPath.row == 3)
	{
		[appDelegate.tabBarController setSelectedIndex: 3];
	}	
	else if (newIndexPath.row == 4)
	{
		[appDelegate.tabBarController setSelectedIndex: 4];
	}
	[tableView deselectRowAtIndexPath:newIndexPath animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"MasterViewIdentifier"] autorelease];
		cell.selectionStyle = UITableViewCellSelectionStyleGray;
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		UIView* elementView =  [ [UIView alloc] initWithFrame:CGRectMake(5,5,312,480)];
		elementView.tag = 0;
		[cell.contentView addSubview:elementView];
		[elementView release];
	}
	cell.backgroundColor=[UIColor whiteColor];
	UIView* elementView  = [cell.contentView viewWithTag:0];
	for(UIView* subView in elementView.subviews)
	{
		[subView removeFromSuperview];
	}
	
	NSString* content = @"";
	if(indexPath.row ==0)
	{
		content = @"Advanced Search";
	}
	else if(indexPath.row == 1)
	{
		content = @"Top TItles";
	}
	else if(indexPath.row ==2)
	{
		content = @"Award WInners";
	}
	else if(indexPath.row == 3)
	{
		content  = @"My Bookbag";
	}
	else if(indexPath.row == 4)
	{
		content  = @"Help";
	}
	
	CGRect labelFrame = CGRectMake(5,10,300,25);
	UILabel *textView = [[UILabel alloc] initWithFrame:labelFrame];
	textView.backgroundColor = [UIColor clearColor];
	textView.font = [UIFont boldSystemFontOfSize:18];
	textView.textColor=[UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1] ;
	textView.text =content;
	[elementView addSubview:textView];
	[textView release];
	
	return cell;
	
}
 */
-(void)displayAdvancedSearch
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	SearchViewController* searchViewController = [appDelegate searchViewController];
	[self.navigationController pushViewController:searchViewController animated:YES];

}

-(void)displayP2SBarCodeReaderScreen
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	[self.navigationController pushViewController:appDelegate.barcodeViewController animated:YES];
}

-(void)displayLocalOptions
{
	//printf("\n tapped on displayLocalOptions");
	
	PopViewController* popViewController = [[PopViewController alloc] init];
	UINavigationController* popViewNavigationController=[[UINavigationController alloc]initWithRootViewController:popViewController];
	popViewNavigationController.navigationBar.tintColor=[UIColor blackColor];
	[[self navigationController] presentModalViewController:popViewNavigationController animated:YES];
	[popViewController release];
	[popViewNavigationController release];
	 
}

@end
