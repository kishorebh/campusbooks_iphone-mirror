    //
//  RadiusPickerViewControlller.m
//  CampusBooks
//
//  Created by Admin on 17/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "RadiusPickerViewControlller.h"
#import "CampusBooksAppDelegate.h"
@implementation RadiusPickerViewControlller
@synthesize object;
-(id)init
{
	if(self = [super init])
	{
		self.title=@"Choose Radius";
		
		UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]
											   initWithTitle:@"Done"
											   style:UIBarButtonItemStyleBordered	
											   target:self
											   action:@selector(getMainView:)];
		
		self.navigationItem.rightBarButtonItem = rightBarButtonItem;
		[rightBarButtonItem release];
		radiusCountsList=[[NSMutableArray alloc]initWithObjects:@"10",@"15",@"20",@"25",@"30",@"35",@"40",@"45",@"45",@"50",nil];

				
	}
	return self;	
}


- (void)loadView
{
	
	UIView *contentView = [ [UIView alloc] initWithFrame:CGRectMake(0,0,320,440)];
	contentView.backgroundColor = [UIColor clearColor];

	self.view = contentView;
	
	UIView* subView3= [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, 440)];
	subView3.backgroundColor =  [UIColor whiteColor];
	[contentView addSubview:subView3];
	[subView3 release];
	
	myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,100, 320, 420)];
	myPickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	myPickerView.delegate = self;
	myPickerView.showsSelectionIndicator = YES;
	
	
	
	[subView3 addSubview:myPickerView];
	
	UIView* subView1= [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, 105)];
	subView1.backgroundColor =  [UIColor colorWithPatternImage: [UIImage imageNamed:@"pickerBackground1.png"]];
	[subView3 addSubview:subView1];
	[subView1 release];
	
	UIView* subView2= [[UIView alloc] initWithFrame:CGRectMake(0,310, 320, 135)];
	subView2.backgroundColor =  [UIColor colorWithPatternImage: [UIImage imageNamed:@"pickerbackground.png"]];
	[subView3 addSubview:subView2];	
	[subView2 release];
}
-(void)viewWillAppear:(BOOL)animated
{
	NSString* stringRadius=[NSString stringWithFormat:@"%d",object.radiusValue];
	int pickerSelectedRow = 0;
	for (int i=0; i<[radiusCountsList count]; i++) 
	{
		NSString* radiusString=[radiusCountsList objectAtIndex:i];
		if([stringRadius isEqualToString:radiusString])
		{
			pickerSelectedRow=i;
		}
	}
	[myPickerView selectRow:pickerSelectedRow inComponent:0 animated:YES];	
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	
	NSString* title=[radiusCountsList objectAtIndex:row];
	return title;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
	return 40.0;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	return [radiusCountsList count];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	
	rowcount=row;
}
-(void)getMainView:(id)sender
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	NSString* selectedValue=[radiusCountsList objectAtIndex:rowcount];
	LocalOptions* localObj=[appDelegate getLocalOptions];;
	localObj.primaryKey=object.primaryKey;
	localObj.radiusValue=[selectedValue intValue];
	[appDelegate updateLocalOptions:localObj];
	//[localObj release];
	[self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
