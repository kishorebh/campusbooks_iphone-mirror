//
//  AwardsListViewController.h
//  CampusBooks
//
//  Created by Admin on 13/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import"BookListViewController.h"
@interface BookBagViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
	UITableView* theTableView;
	NSMutableArray* awardsList;
	UIImageView* titleView;
	
	BookListViewController* bookListViewController;
	NSInteger selectedType;
	UILabel* titleLabel ;
}
@property(nonatomic) NSInteger selectedType;
@end
