//
//  HomeViewController.m
//  Campusbooks
//
//  Created by sekhar bethalam on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HomeViewController.h"
#import "CampusBooksAppDelegate.h"
#import "PopViewController.h"
#define IPHONE_1G_NAMESTRING @"iPhone 1G"
#define IPHONE_3G_NAMESTRING @"iPhone 3G"
#define IPHONE_3GS_NAMESTRING @"iPhone 3GS"
#define IPOD_1G_NAMESTRING @"iPod touch 1G"
#define IPOD_2G_NAMESTRING @"iPod touch 2G"
@interface HomeViewController ()

@end

@implementation HomeViewController
@synthesize internetConnectionStatus, strKeyword;;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization
        
//        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header-Small-Logo.png"]];

        bookListViewController = [[BookListViewController alloc] initWithBooksList:nil fromSource:self];
        
//        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
//        {
//            orLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 210, 320, 15)];
//            orLabel.text = @"OR";
//            orLabel.font = [UIFont boldSystemFontOfSize:15];
//            orLabel.textColor = [UIColor whiteColor];
//            orLabel.backgroundColor = [UIColor clearColor];
//            orLabel.textAlignment = UITextAlignmentCenter;
//            [contentView addSubview:orLabel];
//            
//            barcodeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 230, 320, 15)];
//            barcodeLabel.text = @"Scan book barcode to compare prices";
//            barcodeLabel.font = [UIFont boldSystemFontOfSize:15];
//            barcodeLabel.textColor = [UIColor whiteColor];
//            barcodeLabel.backgroundColor = [UIColor clearColor];
//            barcodeLabel.textAlignment = UITextAlignmentCenter;
//            [contentView addSubview:orLabel];
//            
//            pic2ShopScanButton = [[UIButton alloc] initWithFrame:CGRectMake(101.5, 250, 117, 35)];
//            [pic2ShopScanButton setImage:[UIImage imageNamed:@"scanbook.png"] forState:UIControlStateNormal];
//            //	[pic2ShopScanButton setImage:[UIImage imageNamed:@"scanbook_press.png"] forState:UIControlStateHighlighted];
//            [pic2ShopScanButton addTarget:self action:@selector(displayP2SBarCodeReaderScreen) forControlEvents:UIControlEventTouchUpInside];
//            [contentView addSubview:pic2ShopScanButton];
//        }
//        
        CGRect activityViewframe = CGRectMake(150.0,0, 35,35);
        progressInd = [[UIActivityIndicatorView alloc] initWithFrame:activityViewframe];
        progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        progressInd.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
                                        UIViewAutoresizingFlexibleRightMargin |
                                        UIViewAutoresizingFlexibleTopMargin |
                                        UIViewAutoresizingFlexibleBottomMargin);
        [progressInd startAnimating];
        
        progressInd.hidden = NO;
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:progressInd ] autorelease];
        
        isLoadOptionsOnWillAppear = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UITapGestureRecognizer* tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:searchTextField action:@selector(resignFirstResponder)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    tapGestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGestureRecognizer];
    [tapGestureRecognizer release];
    
//    [searchTextField setBackground:[UIImage imageNamed:@"home_search_bg.png"]];
//    
//    UIView *paddingView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 43, 20)] autorelease];
//    paddingView.backgroundColor = [UIColor clearColor];
//    searchTextField.leftView = paddingView;
//    searchTextField.leftViewMode = UITextFieldViewModeAlways;

    // Do any additional setup after loading the view from its nib.
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
	{
        orImageLine.hidden = YES;
        pic2ShopScanButton.hidden  = YES;
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



- (void)viewWillAppear:(BOOL)animated {
	
	/*if (isLoadOptionsOnWillAppear) {
     CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate* )[[UIApplication sharedApplication] delegate];
     LocalOptions* localObject=[appDelegate getLocalOptions];
     if (localObject.isFirstTime ==0) {
     [self displayLocalOptions];
     isLoadOptionsOnWillAppear = NO;
     }
     }
	 */
	isSearchInProgress = NO;
	[self refreshView];
	
}

-(void) displaySearchViewController
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	[[self navigationController] pushViewController:appDelegate.searchViewController animated:YES];
}
-(void)refreshView
{
//	self.title = @"CampusBooks";
//	for(UIView* subview in contentView.subviews)
//	{
//		[subview removeFromSuperview];
//	}
//    
//    //	[contentView addSubview:__segmentControl];
//	[contentView addSubview:imageView];
//    
////	[contentView addSubview:txtFldBookBG];
////	[contentView addSubview:txtFldBook];
//	[contentView addSubview:compareLabel];
//	[contentView addSubview:orLabel];
//	[contentView addSubview:barcodeLabel];
//	
//	[contentView addSubview:searchButton];
//	[contentView addSubview:pic2ShopScanButton];
//	[contentView addSubview:localOptionsButton];	
//    
////	txtFldBook.placeholder = @" ISBN, Title, Author or Keyword";
//    /*	if(currentSearchSelection == 0)
//     {
//     txtFldBook.placeholder = @"ISBN";
//     }
//     else 	if(currentSearchSelection == 1)
//     {
//     txtFldBook.placeholder = @"Author";
//     }
//     else 	if(currentSearchSelection == 2)
//     {
//     txtFldBook.placeholder = @"Title";
//     }
//     else 	if(currentSearchSelection == 3)
//     {
//     txtFldBook.placeholder = @"Keyword";
//     }
//     
//     
//     [contentView addSubview:theTableView];
//     */	[progressInd removeFromSuperview];
	if(isSearchInProgress)
	{
		self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:progressInd ] autorelease];
	}
	else
	{
		self.navigationItem.rightBarButtonItem = nil;
	}
}

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */

/*
 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad {
 [super viewDidLoad];
 }
 */

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


- (void)dealloc {
    //	printf("\n In CampusbooksViewController Dealloc");
	[bookListViewController release];
	[imageView release];
    [super dealloc];
	
}
-(void)performSearch
{
	
	
	isSearchInProgress = YES;
	[self refreshView];
	
	[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(doSearch) userInfo:nil repeats:NO];
}

-(void)doSearch
{
	[self updateStatus];
	
	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"CampusBooks" message:@"No Network Available"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		isSearchInProgress = NO;
		[self refreshView];
	}
	else
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
		//	@try {
        /*		strIsbn = @"";
         strAuthor = @"";
         strTitle = @"";
         strKeyword = @"";
         
         if(currentSearchSelection == 0)
         {
         if ([[txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  length] > 0)
         {
         strIsbn = [txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
         }
         }
         else if (currentSearchSelection == 1)
         {
         if ([[txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  length] > 0)
         {
         strAuthor = [txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
         }
         }
         else if (currentSearchSelection ==2)
         {
         if ([[txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  length] > 0)
         {
         strTitle = [txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
         }
         }
         else if (currentSearchSelection ==3)
         {
         if ([[txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  length] > 0)
         {
         strKeyword = [txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
         }
         }
         
         */		strKeyword = @"";
		if ([[searchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  length] > 0)
		{
			strKeyword = [searchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		}
		
		if ( [strKeyword length] > 0)
		{
			progressInd.hidden = NO;
			
			//printf("\n Called Search");
			CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication] delegate]; 
			
			NSMutableArray* booksList = [appDelegate doBookSearch:nil byAuthor:nil withKeyword:strKeyword forISBN:nil forPage:1];
			
			[bookListViewController refreshBookListViewController:booksList fromSource:self];
			//printf("\n No of objects returned :%d", [booksList count]);
			if ([booksList count] >0)
			{
				[[self navigationController] pushViewController:bookListViewController animated:YES];
				//					[appDelegate displayBookListViewController:booksList fromSource:self];
			}
			else
			{
				UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:@"No books match the criteria. \nPlease revise it and try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
				[alertView show];
			}
            
		}
		else
		{
			UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:@"Please provide the search criteria and try again!!! "  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
			[alertView show];
			isSearchInProgress = NO;
			[self refreshView];
		}
		
		
		/*		}
		 @catch (NSException * e) {
		 //printf("\n caught an exception");
		 }
		 @finally {
		 
		 }
		 */
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		isSearchInProgress = NO;
		[self refreshView];
		
	}
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
	//	printf("\n Reached should return ");
	//	[theTextField resignFirstResponder];
	NSString* text = [theTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	if ([text length] > 0)
	{
		[self performSearch];
	}
	[theTextField resignFirstResponder];
	return YES;
}

-(NSString *)hostName
{
	// Don't include a scheme. 'http://' will break the reachability checking.
	// Change this value to test the reachability of a different host.
	return @"www.apple.com";
}
- (void)updateStatus
{
	
	self.internetConnectionStatus	= [[Reachability sharedReachability] internetConnectionStatus];
	//	[tableView reloadData];
}

-(IBAction)displayAdvancedSearch:(id)sender
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	SearchViewController* searchViewController = [appDelegate searchViewController];
	[self.navigationController pushViewController:searchViewController animated:YES];
    
}

-(IBAction)displayP2SBarCodeReaderScreen:(id)sender
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	[self.navigationController pushViewController:appDelegate.barcodeViewController animated:YES];
}

-(IBAction)displayLocalOptions:(id)sender
{
	//printf("\n tapped on displayLocalOptions");
	
	PopViewController* popViewController = [[PopViewController alloc] init];
	UINavigationController* popViewNavigationController=[[UINavigationController alloc]initWithRootViewController:popViewController];
	popViewNavigationController.navigationBar.tintColor=[UIColor blackColor];
	[[self navigationController] presentModalViewController:popViewNavigationController animated:YES];
	[popViewController release];
	[popViewNavigationController release];
    
}
-(IBAction)go:(id)sender{
    [self textFieldShouldReturn:searchTextField];
}

@end
