//
//  CampusBooksViewController.h
//  CampusBooks
//
//  Created by Admin on 13/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"Reachability.h"
#import"BookListViewController.h"
#import"UIDevice-hardware.h"
@interface CampusBooksViewController :UIViewController<UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIAlertViewDelegate>
{
	UIView *contentView;
	
//	UITableView* theTableView;

	UIButton* searchButton;
	
	UIImageView* imageView;
	UILabel* compareLabel ;
	UILabel* orLabel;
	UILabel* barcodeLabel;

	UIImageView* txtFldBookBG;
	UITextField* txtFldBook;
//	NSInteger currentSearchSelection;
	NetworkStatus internetConnectionStatus;

	UIActivityIndicatorView* progressInd;
	BOOL isSearchInProgress;

	NSString* strKeyword;
/*	NSString* strIsbn;
	NSString* strTitle;
	NSString* strAuthor;
*/	
	BookListViewController* bookListViewController;
	
	UIButton* pic2ShopScanButton;
	UIAlertView* analyticsAlert;
	UIButton* localOptionsButton;
	BOOL isLoadOptionsOnWillAppear;
}
@property NetworkStatus internetConnectionStatus;
/*
 @property(nonatomic, retain) NSString* strTitle;
@property(nonatomic, retain) NSString* strAuthor;
@property(nonatomic, retain) NSString* strKeyword;
@property(nonatomic, retain) NSString* strIsbn;
 */
@property(nonatomic, retain)NSString* strKeyword;
- (void)updateStatus;
-(NSString *)hostName;
-(void)displayLocalOptions;
-(void)refreshView;

@end
