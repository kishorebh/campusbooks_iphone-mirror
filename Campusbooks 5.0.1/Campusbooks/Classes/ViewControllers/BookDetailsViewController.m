//
//  BookDetailsViewController.m
//  CampusBooks
//
//  Created by Admin on 19/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "BookDetailsViewController.h"
#import"CampusBooksAppDelegate.h"
#import"BookOfferViewController.h"
#import"BookListViewController.h"
#import"BuyOptionsViewController.h"
@implementation BookDetailsViewController
@synthesize internetConnectionStatus;

-(id)initWithBook:(Book*)aBookInfo fromSource:(UIViewController*)aBookListViewController
{
	if (self = [super init]) {

		currentBookInfo = aBookInfo;
		bookListViewController = aBookListViewController;
		
		contentView = [[TransitionView alloc] initWithFrame:CGRectMake(0,0,320,390)];
		contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];


		CGRect activityViewframe = CGRectMake(145.0,175.5, 35,35);
		progressInd = [[UIActivityIndicatorView alloc] initWithFrame:activityViewframe];
		progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
		progressInd.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
										UIViewAutoresizingFlexibleRightMargin |
										UIViewAutoresizingFlexibleTopMargin |
										UIViewAutoresizingFlexibleBottomMargin);
		[contentView addSubview:progressInd];
		[progressInd startAnimating];
		progressInd.hidden = NO;
		[progressInd stopAnimating];
		
		
		bookSellViewController = [[BookSellViewController alloc] initWithBook:nil  withOffers:nil fromSource:self];
		
		originalDateFormatter = [[NSDateFormatter alloc] init];
		[originalDateFormatter setDateFormat:@"yyyy-MM-dd"];
		
		newDateFormatter = [[NSDateFormatter alloc] init];
		[newDateFormatter setDateStyle:NSDateFormatterMediumStyle];
		[newDateFormatter setTimeStyle:NSDateFormatterNoStyle];
		
		tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 8, 320,360) style:UITableViewStylePlain];
		tableView.delegate = self;
		tableView.dataSource = self;
		tableView.rowHeight = 45;
		tableView.sectionHeaderHeight = 5;
		tableView.sectionFooterHeight = 0;		
		tableView.backgroundColor = [UIColor clearColor];
		tableView.separatorColor=[UIColor clearColor];
		[contentView addSubview:tableView];
		[tableView reloadData];
		[tableView release];
		
		UIBarButtonItem* actionButton = [[[UIBarButtonItem alloc] initWithTitle:@"More"  style:UIBarButtonItemStyleBordered  target:self action:@selector(displayActionSheet:)] autorelease];
		self.navigationItem.rightBarButtonItem=actionButton;
	
	}
	return self;
}


-(void)refreshBookDetailsViewController:(Book*)aBook fromSource:(UIViewController*)aBookListViewController
{
	for (UIView* subView in contentView.subviews)
	{
		[subView removeFromSuperview];
	}
	currentBookInfo = aBook;
	bookListViewController = aBookListViewController;

	
	self.title = @"Book Details";
	NSString* authorName = currentBookInfo.author;
	authorName = [authorName stringByReplacingOccurrencesOfString:@"," withString:@"-"];
	authorNamesList = [authorName componentsSeparatedByString:@"-"];
	[tableView reloadData];

	[contentView addSubview:tableView];
	[contentView addSubview:progressInd];

	[self.view addSubview:contentView];
}


-(NSString *)hostName
{
	// Don't include a scheme. 'http://' will break the reachability checking.
	// Change this value to test the reachability of a different host.
	return @"www.apple.com";
}
- (void)updateStatus
{
	
	self.internetConnectionStatus	= [[Reachability sharedReachability] internetConnectionStatus];
	//	[tableView reloadData];
}
-(void)dealloc
{
		//	printf("\n In BookDetail Dealloc");
	[originalDateFormatter release];
	[newDateFormatter release];
	[super dealloc];
}

#pragma mark Table Delegate and Data Source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tv {
	
	return 1;
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)section {

	NSString* authorName = currentBookInfo.author;
	authorName = [authorName stringByReplacingOccurrencesOfString:@"," withString:@"-"];
	authorNamesList = [authorName componentsSeparatedByString:@"-"];
	
	return [authorNamesList count] + 10;
	
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
	if (cell == nil)
	{
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyIdentifier"] autorelease];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.backgroundColor = [UIColor clearColor];
		
		UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0,0, 300,30)];
		myView.tag = 0;
		myView.backgroundColor = [UIColor clearColor];
		[cell.contentView addSubview:myView];
		[myView release];
	}

	
	UIView* cellContentView = [cell.contentView viewWithTag:0];
	for(UIView* subView in cellContentView.subviews)
	{
		[subView removeFromSuperview];
	}
	//printf("\n IndexPath.row == %d", indexPath.row);
	if (indexPath.row ==0)
	{
		UIView* subView1=[[UIView alloc]initWithFrame:CGRectMake(0,0, 320,110)];
		subView1.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"book_background.png"]];
		
		UILabel* titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 10, 200,40)];
		titleLabel.numberOfLines = 2;
		titleLabel.font = [ UIFont boldSystemFontOfSize:15];
		titleLabel.adjustsFontSizeToFitWidth = YES;
		titleLabel.text = (currentBookInfo.title != nil ? currentBookInfo.title : @"N/A");
		titleLabel.backgroundColor = [UIColor clearColor];
		titleLabel.textAlignment = UITextAlignmentLeft;
		titleLabel.textColor = [UIColor darkTextColor];
		[subView1 addSubview:titleLabel];
		[titleLabel release];
        
        UILabel* authorLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 50, 200,20)];
		authorLabel.font = [ UIFont systemFontOfSize:14];
		authorLabel.adjustsFontSizeToFitWidth = YES;
		authorLabel.text = [NSString stringWithFormat:@"ISBN 10: %@",(currentBookInfo.isbnId10 != nil ? currentBookInfo.isbnId10 : @"N/A")];
		authorLabel.backgroundColor = [UIColor clearColor];
		authorLabel.textAlignment = UITextAlignmentLeft;
		authorLabel.textColor = [UIColor lightGrayColor];
		[subView1 addSubview:authorLabel];
		[authorLabel release];
        
        
        NSString* formattedDateString = @"";
        if (currentBookInfo.publishedDate != nil)
        {
            NSDate* originalDate = [originalDateFormatter dateFromString:currentBookInfo.publishedDate];
            formattedDateString = [newDateFormatter stringFromDate:originalDate];
        }
        if ([formattedDateString length] == 0) {
            formattedDateString = @"N/A";
        }
        UILabel* releaseDateLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 70, 200,20)];
		releaseDateLabel.font = [ UIFont systemFontOfSize:14];
		releaseDateLabel.adjustsFontSizeToFitWidth = YES;
		releaseDateLabel.text = [NSString stringWithFormat:@"ISBN 13: %@",(currentBookInfo.isbnId13 != nil ? currentBookInfo.isbnId13 : @"N/A")];
		releaseDateLabel.backgroundColor = [UIColor clearColor];
		releaseDateLabel.textAlignment = UITextAlignmentLeft;
		releaseDateLabel.textColor = [UIColor lightGrayColor];
		[subView1 addSubview:releaseDateLabel];
		[releaseDateLabel release];
        
        
		UIImageView* imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(35, 17, 60, 80)];
		imageView2.image = currentBookInfo.image != nil ? currentBookInfo.image : [UIImage imageNamed:@"LoadingCover.png"];
		[subView1 addSubview:imageView2];
		[imageView2 release];
		
		[cellContentView addSubview:subView1];
		[subView1 release];		
	}
	else if (indexPath.row ==1) 
	{
        UIView* subView1=[[UIView alloc]initWithFrame:CGRectMake(0,0, 320,28)];
        subView1.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"more_about_this_book.png"]];
        
        [cellContentView addSubview:subView1];
        [subView1 release];
		
	}
	else {
		UIFont* labelFont =  [ UIFont boldSystemFontOfSize:15];
		UIFont* headerFont =  [ UIFont boldSystemFontOfSize:13];
		
		UIColor* backgroundColor = [UIColor clearColor];
		UIColor* headerColor =  [UIColor darkTextColor];//[UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];
		UIColor* singleRowTextColor =[UIColor darkTextColor];//[UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1] ;
		

		cell.accessoryType = UITableViewCellAccessoryNone;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cellContentView.backgroundColor = [UIColor whiteColor];
		NSString* authorName = currentBookInfo.author;
		authorName = [authorName stringByReplacingOccurrencesOfString:@"," withString:@"-"];
		authorNamesList = [authorName componentsSeparatedByString:@"-"];
		//printf("\n Authors Count == %d", [authorNamesList count]);	

		//	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		
		if (indexPath.row == 2)
		{
			UILabel*author =  [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 70,25)];
			author.numberOfLines = 0;
			author.font = headerFont;
			author.text = @"Author(s)";
			author.backgroundColor =backgroundColor;
			author.textAlignment = UITextAlignmentLeft;
			author.textColor = headerColor;
			[cellContentView addSubview:author];
			[author release];
			
			NSString* currentAuthor = [authorNamesList objectAtIndex:0];
			UILabel*authorValue =  [[UILabel alloc]initWithFrame:CGRectMake(145, 10, 155,25)];
			authorValue.numberOfLines = 0;
			authorValue.font = labelFont;
			authorValue.text = currentAuthor;
			authorValue.backgroundColor =backgroundColor;
			authorValue.textAlignment = UITextAlignmentRight;
			authorValue.textColor = singleRowTextColor;
			[cellContentView addSubview:authorValue];
			[authorValue release];
		}
		
		else if( indexPath.row < [authorNamesList count] + 2 ) 
		{
			NSString* currentAuthor = [authorNamesList objectAtIndex:indexPath.row - 2 ];
			
			UILabel*author =  [[UILabel alloc]initWithFrame:CGRectMake(145, 10, 155,25)];
			author.numberOfLines = 0;
			author.font = labelFont;
			author.text = currentAuthor;
			author.backgroundColor =backgroundColor;
			author.textAlignment = UITextAlignmentRight;
			author.textColor = singleRowTextColor;
			[cellContentView addSubview:author];
			[author release];
		}
		else 
		{
			if (  indexPath.row -2 == [authorNamesList count]  )
			{
				UILabel*publishedBy =  [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 70,25)];
				publishedBy.font = headerFont;
				publishedBy.text = @"Publisher";
				publishedBy.backgroundColor =backgroundColor;
				publishedBy.textAlignment = UITextAlignmentLeft;
				publishedBy.textColor = headerColor;
				[cellContentView addSubview:publishedBy];
				[publishedBy release];
				
				UILabel*publisherValue =  [[UILabel alloc]initWithFrame:CGRectMake(145, 10, 155,25)];
				publisherValue.font = labelFont;
				publisherValue.text = [NSString stringWithFormat:@"%s",  (currentBookInfo.publisher != nil ?  [currentBookInfo.publisher UTF8String] : "N/A")] ;
				publisherValue.backgroundColor =backgroundColor;
				publisherValue.textAlignment = UITextAlignmentRight;
				publisherValue.textColor = singleRowTextColor;
				[cellContentView addSubview:publisherValue];
				[publisherValue release];
			}
			else if  (  indexPath.row -3 == [authorNamesList count]  )
			{
				UILabel*publishedOnLabel =  [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 140,25)];
				publishedOnLabel.font = headerFont;
				publishedOnLabel.text = @"Publishing Date";
				publishedOnLabel.backgroundColor =backgroundColor;
				publishedOnLabel.textAlignment = UITextAlignmentLeft;
				publishedOnLabel.textColor = headerColor;
				[cellContentView addSubview:publishedOnLabel];
				[publishedOnLabel release];
				
				NSString* formattedDateString = @"";
				if (currentBookInfo.publishedDate != nil)
				{
					
					
					NSDate* originalDate = [originalDateFormatter dateFromString:currentBookInfo.publishedDate];
					formattedDateString = [newDateFormatter stringFromDate:originalDate];
				}
				else
				{
					formattedDateString = @"N/A";
				}
				
				UILabel* publishedOnValue =  [[UILabel alloc]initWithFrame:CGRectMake(145, 10, 155,25)];
				publishedOnValue.numberOfLines = 0;
				publishedOnValue.font = labelFont;
				publishedOnValue.text = formattedDateString;
				publishedOnValue.backgroundColor =backgroundColor;
				publishedOnValue.textAlignment = UITextAlignmentRight;
				publishedOnValue.textColor = singleRowTextColor;
				[cellContentView addSubview:publishedOnValue];
				[publishedOnValue release];
			}
			else if  (  indexPath.row -4 == [authorNamesList count]  )
			{
				UILabel*isbnId10 =  [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 300,25)];
				isbnId10.numberOfLines = 0;
				isbnId10.font = headerFont;
				isbnId10.text = @"ISBN 10";
				isbnId10.backgroundColor =backgroundColor;
				isbnId10.textAlignment = UITextAlignmentLeft;
				isbnId10.textColor = headerColor;
				[cellContentView addSubview:isbnId10];
				[isbnId10 release];
				
				UILabel* isbnId10Value =  [[UILabel alloc]initWithFrame:CGRectMake(145, 10, 155,25)];
				isbnId10Value.numberOfLines = 0;
				isbnId10Value.font = labelFont;
				isbnId10Value.text = [NSString stringWithFormat:@"%s", (currentBookInfo.isbnId10 != nil ?  [currentBookInfo.isbnId10 UTF8String] : "N/A")] ;
				isbnId10Value.backgroundColor =backgroundColor;
				isbnId10Value.textAlignment = UITextAlignmentRight;
				isbnId10Value.textColor = singleRowTextColor;
				[cellContentView addSubview:isbnId10Value];
				[isbnId10Value release];
			}
			else if  (  indexPath.row -5 == [authorNamesList count]  )
			{	
				UILabel*isbnId13 =  [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 300,25)];
				isbnId13.numberOfLines = 0;
				isbnId13.font = headerFont;
				isbnId13.text = @"ISBN 13";
				isbnId13.backgroundColor =backgroundColor;
				isbnId13.textAlignment = UITextAlignmentLeft;
				isbnId13.textColor = headerColor;
				[cellContentView addSubview:isbnId13];
				[isbnId13 release];
				
				UILabel* isbnId13Value =  [[UILabel alloc]initWithFrame:CGRectMake(145,10, 155,25)];
				isbnId13Value.numberOfLines = 0;
				isbnId13Value.font = labelFont;
				isbnId13Value.text = [NSString stringWithFormat:@"%s",  (currentBookInfo.isbnId13 != nil ?  [currentBookInfo.isbnId13 UTF8String] : "N/A") ] ;
				isbnId13Value.backgroundColor =backgroundColor;
				isbnId13Value.textAlignment = UITextAlignmentRight;
				isbnId13Value.textColor = singleRowTextColor;
				[cellContentView addSubview:isbnId13Value];
				[isbnId13Value release];
			}
			else if  (  indexPath.row -6 == [authorNamesList count]  )
			{
				UILabel*editionLabel =  [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 300,25)];
				editionLabel.numberOfLines = 0;
				editionLabel.font = headerFont;
				editionLabel.text = @"Edition";
				editionLabel.backgroundColor =backgroundColor;
				editionLabel.textAlignment = UITextAlignmentLeft;
				editionLabel.textColor = headerColor;
				[cellContentView addSubview:editionLabel];
				[editionLabel release];
				
				UILabel* editionValue =  [[UILabel alloc]initWithFrame:CGRectMake(145, 10, 155,25)];
				editionValue.numberOfLines = 0;
				editionValue.font = labelFont;
				editionValue.text = [NSString stringWithFormat:@"%s",  (currentBookInfo.edition != nil ?  [currentBookInfo.edition UTF8String] : "N/A") ] ;
				editionValue.backgroundColor =backgroundColor;
				editionValue.textAlignment = UITextAlignmentRight;
				editionValue.textColor = singleRowTextColor;
				[cellContentView addSubview:editionValue];
				[editionValue release];	
			}
			else if  (  indexPath.row -7 == [authorNamesList count]  )
			{
				UILabel*bindingLabel =  [[UILabel alloc]initWithFrame:CGRectMake(10,10, 300,25)];
				bindingLabel.numberOfLines = 0;
				bindingLabel.font = headerFont;
				bindingLabel.text = @"Binding";
				bindingLabel.backgroundColor =backgroundColor;
				bindingLabel.textAlignment = UITextAlignmentLeft;
				bindingLabel.textColor = headerColor;
				[cellContentView addSubview:bindingLabel];
				[bindingLabel release];
				
				UILabel* bindingValue =  [[UILabel alloc]initWithFrame:CGRectMake(145, 10, 155,25)];
				bindingValue.numberOfLines = 0;
				bindingValue.font = labelFont;
				bindingValue.text = currentBookInfo.binding != nil ? [NSString stringWithFormat:@"%@",  currentBookInfo.binding] : @"N/A" ;
				bindingValue.backgroundColor =backgroundColor;
				bindingValue.textAlignment = UITextAlignmentRight;
				bindingValue.textColor = singleRowTextColor;
				[cellContentView addSubview:bindingValue];
				[bindingValue release];
			}
			else if  (  indexPath.row -8 == [authorNamesList count]  )
			{
				UILabel*noOfPagesLabel =  [[UILabel alloc]initWithFrame:CGRectMake(10,10, 300,25)];
				noOfPagesLabel.numberOfLines = 0;
				noOfPagesLabel.font = headerFont;
				noOfPagesLabel.text = @"No.of Pages";
				noOfPagesLabel.backgroundColor =backgroundColor;
				noOfPagesLabel.textAlignment = UITextAlignmentLeft;
				noOfPagesLabel.textColor = headerColor;
				[cellContentView addSubview:noOfPagesLabel];
				[noOfPagesLabel release];
				
				UILabel* noOfPagesValue =  [[UILabel alloc]initWithFrame:CGRectMake(145,10, 155,25)];
				noOfPagesValue.numberOfLines = 0;
				noOfPagesValue.font = labelFont;
				noOfPagesValue.text = currentBookInfo.numberOfPages > 0 ? [NSString stringWithFormat:@"%d", currentBookInfo.numberOfPages] : @"N/A" ;
				noOfPagesValue.backgroundColor =backgroundColor;
				noOfPagesValue.textAlignment = UITextAlignmentRight;
				noOfPagesValue.textColor = singleRowTextColor;
				[cellContentView addSubview:noOfPagesValue];
				[noOfPagesValue release];
			}
		}
		
	}




	
	
	return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row==0)
	{
		return 110.0;
	} else if (indexPath.row ==1)
	{
		return 28;
	}
	return 35.0;
	
}

- (CGFloat)getTextHeightForSystemFontOfSize:(CGFloat)size ofWidth:(CGFloat)maxWidth forText:(NSString*)aText {
    CGFloat maxHeight = 9999;
    CGSize maximumLabelSize = CGSizeMake(maxWidth,maxHeight);
	
    CGSize expectedLabelSize = [aText sizeWithFont:[UIFont systemFontOfSize:size] constrainedToSize:maximumLabelSize lineBreakMode:UILineBreakModeWordWrap]; 
	
    return expectedLabelSize.height;
}


-(void) displayActionSheet:(id) sender
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate]; 
	UIActionSheet *actionSheet = nil;
	NSString* firstOptionTitle = @"Compare Prices";
	NSString* secondOptionTitle = @"Sell It";
	NSString* thirdOptionTitle = @"";
	
	if (currentBookInfo.isInWishList)
	{
		thirdOptionTitle =  @"Remove from My Wishlist";
	}
	else
	{
		thirdOptionTitle = @"Add to My Wishlist";
	}
	actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil
													otherButtonTitles:firstOptionTitle, secondOptionTitle, thirdOptionTitle,  @"Cancel", nil];
	
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	actionSheet.destructiveButtonIndex = actionSheet.numberOfButtons -1 ;	
	actionSheet.delegate = self;
	[actionSheet showInView:appDelegate.window];
	[actionSheet release];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
		[self buyItAction];
	}	
	else if (buttonIndex ==1)
	{

		[self sellItAction];
	}
	else if (buttonIndex == 2)
	{
		if (currentBookInfo.isInWishList)
		{
			[self removeFromWishList];
		}
		else
		{
			[self addToWishList];
		}
	}	
	else 
	{
		NSLog(@"cancel");
	}
}


-(void)addToWishList
{
	WishList* wishList = [[WishList alloc] init];
	wishList.authorName = currentBookInfo.author;
	wishList.bookName = currentBookInfo.title;
	wishList.isbnId10 = currentBookInfo.isbnId10;
	wishList.imageUrl = currentBookInfo.imageUrl;
	
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	[appDelegate addToWishList:wishList];
	[wishList release];
	
	NSString* aMessage = [NSString stringWithFormat:@"'%s' has been successfully added to your wishlist.", [currentBookInfo.title UTF8String]];
	
	UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:aMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
	[alertView show];
	
	currentBookInfo.isInWishList = YES;
	currentBookInfo.isInMyCollection = NO;
}
-(void)removeFromWishList
{
	
	CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [[UIApplication sharedApplication]delegate];
	[appDelegate removeFromWishList:currentBookInfo.isbnId10];
	
	NSString* aMessage = [NSString stringWithFormat:@"'%s' has been successfully removed from your wishlist.", [currentBookInfo.title UTF8String]];
	
	UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:aMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
	[alertView show];
	currentBookInfo.isInWishList = NO;
}
-(void)addToMyCollection
{
	
	MyCollection* collection = [[MyCollection alloc] init];
	collection.authorName = currentBookInfo.author;
	collection.bookName = currentBookInfo.title;
	collection.isbnId10 = currentBookInfo.isbnId10;
	collection.imageUrl = currentBookInfo.imageUrl;
	
	CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [[UIApplication sharedApplication]delegate];
	[appDelegate addToMyCollection:collection];
	[collection release];
	
	NSString* aMessage = [NSString stringWithFormat:@"'%s' has been successfully added to your collection.", [currentBookInfo.title UTF8String]];
	
	UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:aMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
	[alertView show];
	
	currentBookInfo.isInMyCollection = YES;
	
}

-(void)removeFromMyCollection
{
	CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [[UIApplication sharedApplication]delegate];
	[appDelegate removeFromMyCollection:currentBookInfo.isbnId10];
	
	NSString* aMessage = [NSString stringWithFormat:@"'%s' has been successfully removed from your collection.", [currentBookInfo.title UTF8String]];
	
	UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:aMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
	[alertView show];
	currentBookInfo.isInMyCollection = NO;
}


-(void)buyItAction
{
	[self updateStatus];
	
	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!" message:@"No Network Available"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		
	}
	else
	{
		
		//	@try {
		
		[progressInd startAnimating];
		//	[detailsButton setImage:[UIImage imageNamed:@"transparent-clicked.png"] forState:UIControlStateNormal];
		
		[NSThread detachNewThreadSelector:@selector(doPopulateBuyView:) toTarget:self withObject:nil];
		/*			
		 }
		 @catch (NSException * e) {
		 printf("\n caught an exception");
		 }
		 @finally {
		 
		 }
		 */		
	}
}

- (void)doPopulateBuyView:(id)sender
{
	//	@try {
	
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)  [ [UIApplication sharedApplication] delegate];
	
	BookListViewController* myBookListViewController = (BookListViewController*) bookListViewController;
	if (myBookListViewController.sourceViewController== appDelegate.homeViewController || myBookListViewController.sourceViewController == appDelegate.searchViewController || myBookListViewController.sourceViewController == appDelegate.scanHistoryViewController)
	{
		bookOffersList = [appDelegate getBookOffersListForSearch:currentBookInfo.isbnId10];
	}
	else if (myBookListViewController.sourceViewController ==  appDelegate.bookBagViewController)
	{
		bookOffersList = [appDelegate getBookOffersListForBookBag:currentBookInfo.isbnId10];
	}
	

	[self performSelectorOnMainThread:@selector(loadBuyView) withObject:nil waitUntilDone:NO];

	//		[detailsViewControler release];
	/*		}
	 else
	 {
	 UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:@"Unable to retrieve the book details .  \nPlease try another book."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
	 [alertView show];
	 }
	 */		
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	[progressInd stopAnimating];
	
	[pool release];	
	[NSThread exit];
	/*	}
	 @catch (NSException * e) {
	 [e 
	 }
	 @finally {
	 }
	 */
}
-(void)loadBuyView
{
    [self dismissModalViewControllerAnimated:YES];

	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)  [ [UIApplication sharedApplication] delegate];
	BookListViewController* myBookListViewController = (BookListViewController*) bookListViewController;

	LocalOptions* localOptions = [appDelegate getLocalOptions];;
	if (localOptions.optionsValue ==0 )
	{
		//printf("\n Loading BookBuyViewController");
	//	bookBuyViewController = [[BookBuyViewController alloc] initWithBook:currentBookInfo  withOffers:bookOffersList fromSource:myBookListViewController];
		[bookBuyViewController refreshBookBuyViewController:currentBookInfo withOffers:bookOffersList  fromSource:myBookListViewController];
		
		if ([self.navigationController topViewController] == bookBuyViewController)
		{
			//printf("\n cancelling the push");
		}
		else
		{
			[self.navigationController pushViewController:bookBuyViewController animated:YES];
		}
	}
	else 
	{
		//		printf("\n Loading BuyOptionsViewController");
		BuyOptionsViewController* optionsViewController = [[BuyOptionsViewController alloc]initWithBook:currentBookInfo withMode:0 withOffers:bookOffersList fromSource:myBookListViewController];
		[optionsViewController refreshBuyOptionsViewController:currentBookInfo withMode:0 withOffers:bookOffersList fromSource:myBookListViewController];
		if ([self.navigationController topViewController] == optionsViewController)
		{
			//printf("\n cancelling the push");
		}
		else
		{
			[self.navigationController pushViewController:optionsViewController animated:YES];
		}
		[optionsViewController release];
		
	}

}
-(void)sellItAction
{
	[self updateStatus];
	
	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!" message:@"No Network Available"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		
	}
	else
	{
		
		//	@try {
		
		[progressInd startAnimating];
		//	[detailsButton setImage:[UIImage imageNamed:@"transparent-clicked.png"] forState:UIControlStateNormal];
		
		[NSThread detachNewThreadSelector:@selector(doPopulateSellView:) toTarget:self withObject:nil];
		/*			
		 }
		 @catch (NSException * e) {
		 printf("\n caught an exception");
		 }
		 @finally {
		 
		 }
		 */		
	}
}

- (void)doPopulateSellView:(id)sender
{
	//	@try {
	
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)  [ [UIApplication sharedApplication] delegate];
	
	BookListViewController* myBookListViewController = (BookListViewController*) bookListViewController;
	if(myBookListViewController.sourceViewController == appDelegate.homeViewController || myBookListViewController.sourceViewController == appDelegate.searchViewController || myBookListViewController.sourceViewController == appDelegate.scanHistoryViewController)
	{
		bookOffersList = [appDelegate getBookBuybackOffersListForSearch:currentBookInfo.isbnId10];
	}
	else if (myBookListViewController.sourceViewController ==  appDelegate.bookBagViewController)
	{
		bookOffersList = [appDelegate getBookBuybackOffersListForBookBag:currentBookInfo.isbnId10];
	}
	//		[detailsViewControler release];
	/*		}
	 else
	 {
	 UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:@"Unable to retrieve the book details .  \nPlease try another book."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
	 [alertView show];
	 }
	 */		
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	[progressInd stopAnimating];
	
	[pool release];	
	
	[self performSelectorOnMainThread:@selector(loadSellView) withObject:nil waitUntilDone:NO];
	
	[NSThread exit];
	/*	}
	 @catch (NSException * e) {
	 [e 
	 }
	 @finally {
	 }
	 */
}
-(void)loadSellView
{
//	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)  [ [UIApplication sharedApplication] delegate];
	
	BookListViewController* myBookListViewController = (BookListViewController*) bookListViewController;
	
	[bookSellViewController refreshBookSellViewController:currentBookInfo withOffers:bookOffersList  fromSource:myBookListViewController];
	
	if ([self.navigationController topViewController] == bookSellViewController)
	{
		//printf("\n cancelling the push");
	}
	else
	{
		[self.navigationController pushViewController:bookSellViewController animated:YES];
	}
	
}	

@end
