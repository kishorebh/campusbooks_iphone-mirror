//
//  Pic2BarCodeScanViewController.m
//  CampusBooks
//
//  Created by Admin on 21/11/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "Pic2BarCodeScanViewController.h"
#import"CampusBooksAppDelegate.h"
#import"Book.h"
#import"BuyOptionsViewController.h"
#import"RedLaserSDK.h"


@implementation Pic2BarCodeScanViewController

@synthesize internetConnectionStatus, resultsLabel;
- (id)init{
    if (self = [super init]) {
//		overlayController = [[OverlayController alloc] initWithNibName:@"ScanOverlay" bundle:nil];
		
		[UIApplication sharedApplication].statusBarHidden = NO;
		
		contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
		contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
		[self.view addSubview: contentView];
		self.title = @"Scan Book";
		
		allowScan = NO;
		
		currScanedBooksList = [[NSMutableArray alloc] init];
		
		pic2ShopAlert = 
		[[UIAlertView alloc] 
		  initWithTitle:@"CampusBooks" 
		 message:@"Sorry, we our system is only designed to search for ISBN's.\nWe recommend you try using  the free \"RedLaser\" app to search for this item.." 
		  delegate:nil cancelButtonTitle:@"OK" 
		  otherButtonTitles: nil] ;
		pic2ShopAlert.delegate = self;
		
		CGRect activityViewframe = CGRectMake(150.0,0, 35,35);
		progressInd = [[UIActivityIndicatorView alloc] initWithFrame:activityViewframe];
		progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
		progressInd.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
										UIViewAutoresizingFlexibleRightMargin |
										UIViewAutoresizingFlexibleTopMargin |
										UIViewAutoresizingFlexibleBottomMargin);
		[progressInd startAnimating];
		
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
	printf("\n called Pic2Barcode:viewWillAppear");
	[self openBarCodeReader];
	printf("\n completed Pic2Barcode:viewWillAppear");
}
/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
			//printf("\n In Pic2barcode Dealloc");
    [super dealloc];
	[pic2ShopAlert release];
	[currScanedBooksList release];
	[contentView release];
}

-(void)openBarCodeReader
{
	[currScanedBooksList removeAllObjects];
    


	BarcodePickerController * pickerController = [[[BarcodePickerController alloc] init] autorelease];
    pickerController.delegate = self;
    
    [pickerController prepareToScan];

    
	// Make ourselves an overlay controller and tell the SDK about it.	
	OverlayController *overlayController = [[OverlayController alloc] initWithNibName:@"ScanOverlay" bundle:nil];
	[pickerController setOverlay:overlayController];
	[overlayController release];
	
	// hide the status bar and show the scanner view
//	[[UIApplication sharedApplication] setStatusBarHidden:YES];
	[self presentModalViewController:pickerController animated:FALSE];

//	[overlayController setParentPicker:picker];
	[overlayController resetScanning];
//	[picker setOverlayDelegate:overlayController];
//	[picker setView:overlayController.view];
	
	[pickerController setScanUPCE:YES];
	[pickerController setScanEAN8:YES];
	[pickerController setScanEAN13:YES];
	[pickerController setScanSTICKY:YES];
	[pickerController setScanQRCODE:YES];
	[pickerController setScanCODE128:YES];
	[pickerController setScanCODE39:YES];
	[pickerController setScanDATAMATRIX:YES];
	[pickerController setScanITF:YES];
	
	// Optionally, you can change the active scanning region.
	// The region specified below is the default, and lines up
	// with the default overlay.  It is recommended to keep the
	// active region similar in size to the default region.
	// Additionally, the iPhone 3GS may not focus as well if
	// the region is too far away from center.
	//
	// Currently, only the top and bottom of this rectangle is used.
	// The x-position and width specified are ignored.
	
	[pickerController setActiveRegion:CGRectMake(0, 146, 320, 157)];
	
	/* UNCOMMENT THIS SECTION TO SCAN HORIZONTALLY
	 // NOTE: Overlay must be modified to properly support horizontal scanning.
	 
	 // Orientation control:
	 // Only UIImageOrientationUp and UIOrientationRight are supported currently.
	 
	 //[picker setOrientation:UIImageOrientationRight];
	 //[picker setActiveRegion:CGRectMake(80, 52, 157, 320)];
	 */
	
	
	// Choose appropriate overlay.
/*	if([enableUPCESwitch isOn])
		[overlayController.overlayImage setImage:[UIImage imageNamed:@"RedLaserOverlay_UPCE.png"]];
	else
		[overlayController.overlayImage setImage:[UIImage imageNamed:@"RedLaserOverlay.png"]];
*/	
	[overlayController.overlayImage setImage:[UIImage imageNamed:@"RedLaserOverlay.png"]];

//	[self.navigationController presentModalViewController:picker animated:TRUE];
	
//	[self.liveScanner startLiveDecoding];
//	[self presentModalViewController:self.liveScanner animated:YES];
}
-(void) closeViewController
{
	[self.navigationController popViewControllerAnimated:YES];
//	[self.navigationController dismissModalViewControllerAnimated:YES];
}

-(NSMutableArray*)getBooksListFromScannedItems:(NSMutableArray*)aScanItemsList
{
	NSMutableArray* booksList = [[[NSMutableArray alloc] init] autorelease];
	for(ScanItem* scanItem in aScanItemsList)
	{
		Book* book = [[Book alloc] init];
		book.author = scanItem.authorName;
		book.title= scanItem.bookName;
		book.isbnId10 = scanItem.isbnId10;
		book.imageUrl = scanItem.imageUrl;
		[booksList addObject:book];
		[book release];
	}
	return booksList;
}


/*
- (IBAction)grabImage {
	[self.liveScanner startLiveDecoding];
	[self presentModalViewController:self.liveScanner animated:YES];
}
*/


- (void)barcodePickerControllerDidCancel:(BarcodePickerController *)picker
{
	printf("\n Reached Barcode Picker Controller Did Cancel");
	[picker stopScanning]; // Must call this to prevent multiple scan callbacks.
	// Restore main screen
	[self.navigationController dismissModalViewControllerAnimated:TRUE];
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)dismissLiveScanner {
	[self.navigationController dismissModalViewControllerAnimated:TRUE];

//	[[self.liveScanner parentViewController] dismissModalViewControllerAnimated:YES];
}

// User has cancelled
- (void)liveScannerDidCancel {
	//resultsLabel.text = @"cancelled";
	[self performSelector:@selector(dismissLiveScanner) withObject:nil afterDelay:0.0];	
	[self closeViewController];
}

- (void) barcodePickerController:(BarcodePickerController*)picker 
                   returnResults:(NSSet *)results{
    // Restore main screen (and restore title bar for 3.0)
    
    NSLog(@"Results:%@", results);
    // If there's any results, save them in our scan history
    if (results && [results count])
    {
        NSMutableDictionary *scanSession = [[NSMutableDictionary alloc] init];
        [scanSession setObject:[NSDate date] forKey:@"Session End Time"];
        [scanSession setObject:[results allObjects] forKey:@"Scanned Items"];
    }
    
    if ([results count] == 1) {
        for (BarcodeResult* item in results) {
            BOOL isThisBookScannedAlready = NO;
            for(Book* tmpBook in currScanedBooksList)
            {
                if ([tmpBook.isbnId13 isEqualToString:item.barcodeString])
                {
                    isThisBookScannedAlready = YES;
                    break;
                }
            }
            if (!isThisBookScannedAlready)
            {
                scannedEan = [item.barcodeString copy];
                [self buyItAction];
                
            }
            else
            {
                //printf("\n This book is scanned already");
            }
        }

    }
    else {
        [self.navigationController dismissModalViewControllerAnimated:TRUE];
        [self.navigationController popViewControllerAnimated:YES];
    }
    for (BarcodeResult* item in results) {
        NSLog(@"Item:%@", item.barcodeString);
    }
}
-(void) barcodePickerController:(BarcodePickerController*)picker didScanBarcode:(NSString*)ean withInfo:(NSDictionary*)info
{
	//[picker stopScanning]; // Must call this to prevent multiple scan callbacks.


	
	
	
}

// Barcode has been read successfully
- (void)barcodeFound:(NSString*)ean pretty:(NSString*)prettyean  {
	//printf("\n Found a barcode: (%s, %s)",  [ean UTF8String], [prettyean UTF8String]);
	
	BOOL isThisBookScannedAlready = NO;
	for(Book* tmpBook in currScanedBooksList)
	{
		if ([tmpBook.isbnId13 isEqualToString:ean])
		{
			isThisBookScannedAlready = YES;
			break;
		}
	}
	if (!isThisBookScannedAlready)
	{
		scannedEan = [ean copy];
		[self buyItAction];

	}
	else
	{
		//printf("\n This book is scanned already");
	}
	//[self performSelector:@selector(dismissLiveScanner) withObject:nil afterDelay:0.0];
	
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex 
{
	
	
	if(buttonIndex == 1){
		if(alertView==pic2ShopAlert)
		{
			NSString* url = [NSString stringWithFormat:@"redlaser://q=%s", [scannedISBN UTF8String]];
			BOOL isAppAvailable = [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
			
			if (isAppAvailable == NO)
			{
				//printf("\n Application is not available");
				
				NSString* downloadUrl = @"http://itunes.apple.com/us/app/redlaser/id312720263?mt=8";
				 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:downloadUrl]];
			}
			else
			{
				//printf("\n Application is available");
			}
			
			//printf("\n Opening url:%s", [url UTF8String]);
			
		}
		
	}else {
		
		
	}
	
}

-(NSString *)hostName
{
	// Don't include a scheme. 'http://' will break the reachability checking.
	// Change this value to test the reachability of a different host.
	return @"www.apple.com";
}
- (void)updateStatus
{
	
	self.internetConnectionStatus	= [[Reachability sharedReachability] internetConnectionStatus];
	//	[tableView reloadData];
}

-(void)buyItAction
{
	[self updateStatus];
	
	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!" message:@"No Network Available"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		
	}
	else
	{
		
		//	@try {
		
	//	[progressInd startAnimating];
		//	[detailsButton setImage:[UIImage imageNamed:@"transparent-clicked.png"] forState:UIControlStateNormal];
		
		[NSThread detachNewThreadSelector:@selector(doPopulateBuyView:) toTarget:self withObject:nil];
		/*			
		 }
		 @catch (NSException * e) {
		 printf("\n caught an exception");
		 }
		 @finally {
		 
		 }
		 */		
	}
}

- (void)doPopulateBuyView:(id)sender
{
	//	@try {
	
	//printf("\n In DO PopualteBuyView");
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [[UIApplication sharedApplication] delegate];
	Book* book = [appDelegate getBookInfo:scannedEan];
	BookCoverLoader* coverLoader = [BookCoverLoader sharedLoader];
	book.image = [coverLoader getBookCover:book delegate:self];
	
	if( book != nil && [[book.title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] >0)
	{
		//	Book* book = [appDelegate getBookInfo:@"9780321356680"];
		[currScanedBooksList addObject:book];
		
		if ([currScanedBooksList count] > 0)
		{
			ScanList* scanList = [[ScanList alloc] init];
			scanList.scanDate = [NSDate date];
			
			CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [[UIApplication sharedApplication]delegate];
			[appDelegate addToScanList:scanList];
			//printf("\n Scan List Primary Key:%d", scanList.primaryKey);
			
			for(Book* currentBoook in currScanedBooksList)
			{
				ScanItem* scanItem = [[ScanItem alloc] init];
				scanItem.isbnId10 = currentBoook.isbnId10;
				scanItem.bookName = currentBoook.title;
				scanItem.authorName = currentBoook.author;
				scanItem.imageUrl = currentBoook.imageUrl;
				scanItem.scanListId = scanList.primaryKey;
				
				[appDelegate addToScanItem:scanItem];
				[scanItem release];
			}
			[scanList release];
			
			//printf("\n Added %d elements in this iteration", [currScanedBooksList count]);
			
		}
		//	[self doPopulateBuyView:nil];
		
		currentBook = [currScanedBooksList objectAtIndex:0];
		currentBook.image = book.image;
		CampusBooksAppDelegate* appDelegate =  (CampusBooksAppDelegate*) [ [UIApplication sharedApplication] delegate];
		
		
		bookOffersList = [appDelegate getBookOffersListForSearch:currentBook.isbnId10];
		
		
	//	[self buyItAction];
		
		/*			NSString* scanResult = [NSString stringWithFormat:@"%s\n %s", [book.isbnId13 UTF8String], [book.title UTF8String]];
		 
		 UIAlertView* alert = 
		 [[[UIAlertView alloc] 
		 initWithTitle:@"CampusBooks" 
		 message:scanResult 
		 delegate:nil cancelButtonTitle:@"OK" 
		 otherButtonTitles:nil] autorelease];
		 
		 [alert show];
		 */ 
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		
		[progressInd stopAnimating];
		
		[pool release];	
		[self performSelectorOnMainThread:@selector(loadBuyView) withObject:nil waitUntilDone:NO];
		
		[NSThread exit];
		
	}
	else 
	{
		//[self.camOverlay stopProgressIndicator];
		scannedISBN = [scannedEan copy];
		if (book == nil)
		{
			pic2ShopAlert.message = @"Sorry, our system is only designed to scan books. For non-book items, please use the free RedLaser app.";
		}
		else
		{
			pic2ShopAlert.message = @"Sorry, we don't recognize this ISBN. We recommend you try using  the free \"RedLaser\" app to search for this product.";
		}				
		[pic2ShopAlert show];	
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		
		[progressInd stopAnimating];
//		[OverlayController resetScanning];
		
		[pool release];	
		
		[NSThread exit];
		
		
	}
	
    [self dismissModalViewControllerAnimated:YES];


	//		[detailsViewControler release];
	/*		}
	 else
	 {
	 UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:@"Unable to retrieve the book details .  \nPlease try another book."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
	 [alertView show];
	 }
	 */		
	
	/*	}
	 @catch (NSException * e) {
	 [e 
	 }
	 @finally {
	 }
	 */
}
-(void)loadBuyView
{
	CampusBooksAppDelegate* appDelegate =  (CampusBooksAppDelegate*) [ [UIApplication sharedApplication] delegate];
	

	LocalOptions* localOptions = [appDelegate getLocalOptions];
	if (localOptions.optionsValue ==0 )
	{
		BookListViewController* bookListViewController = [[BookListViewController alloc] initWithBooksList:currScanedBooksList fromSource:appDelegate.searchViewController];
		BookBuyViewController* bookBuyViewController = [[BookBuyViewController alloc] initWithBook:currentBook  withOffers:bookOffersList fromSource:bookListViewController];
		[bookBuyViewController refreshBookBuyViewController:currentBook withOffers:bookOffersList  fromSource:bookListViewController];
		
		if ([self.navigationController topViewController] == bookBuyViewController)
		{
			//printf("\n cancelling the push");
		}
		else
		{
			[self.navigationController pushViewController:bookBuyViewController animated:YES];
		}
		[bookBuyViewController release];
		[bookListViewController release];
	}
	else 
	{
		//printf("\n Loading BuyOptionsViewController");
		BookListViewController* bookListViewController = [[BookListViewController alloc] initWithBooksList:currScanedBooksList fromSource:appDelegate.searchViewController];
		BuyOptionsViewController* optionsViewController = [[BuyOptionsViewController alloc]initWithBook:currentBook withMode:0 withOffers:bookOffersList fromSource:bookListViewController];
		[optionsViewController refreshBuyOptionsViewController:currentBook withMode:0 withOffers:bookOffersList fromSource:bookListViewController];
		if ([self.navigationController topViewController] == optionsViewController)
		{
			//printf("\n cancelling the push");
		}
		else
		{
			[self.navigationController pushViewController:optionsViewController animated:YES];
		}
		[optionsViewController release];
		[bookListViewController release];
		
	}
	
	
	//[[self.imgPicker parentViewController] dismissModalViewControllerAnimated:YES];
	[self performSelector:@selector(dismissLiveScanner) withObject:nil afterDelay:0.0];	
	
}
-(void)didReceiveBookCover:(NSString*)aIsbn
{
	printf("\n called didReceiveBookCover in ScanViewController" );
	BookCoverLoader *coverLoader= [BookCoverLoader sharedLoader];
	
	for(Book* book in currScanedBooksList)
	{
		if([book.isbnId10 isEqualToString:aIsbn])
		{
			//printf("\n Loaded an image");
			book.image = [coverLoader getBookCover:book delegate:self];
			if (book.image == nil ||  book.image == [UIImage imageNamed:@"LoadingCover.png"])
			{
				printf("\n Scanner image retrieval in progress");
				book.isImageLoaded = NO;
				book.image = [UIImage imageNamed:@"LoadingCover.png"];
			}
			else
			{
				printf("\n Scanner image is retrieved for %s" , [book.isbnId10 UTF8String]);
				book.isImageLoaded = YES;
	//			book.image = [UIImage imageNamed:@"LoadingCover.png"];

			}
		}
	}
}
@end
