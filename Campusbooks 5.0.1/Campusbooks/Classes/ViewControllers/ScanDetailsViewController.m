//
//  ScanDetailsViewController.m
//  CampusBooks
//
//  Created by Admin on 13/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "ScanDetailsViewController.h"
#import "CampusBooksAppDelegate.h"


@implementation ScanDetailsViewController


@synthesize internetConnectionStatus, strKeyword;;
//static UISegmentedControl *__segmentControl = nil;


 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
-(id)initWithScanList:(ScanList*)aScanList withScanItems:(NSMutableArray*)aScanItemsList
{
	if (self = [super init]) {
        // Custom initialization
		contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 420)];
		contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
		[self.view addSubview: contentView];
		self.title = @"Scan Details";
		
//		booksList
		
		//printf("\n initializing the scan details view");
		scanItemsList = aScanItemsList;
		scanList = aScanList;
		
		bookListViewController = [[BookListViewController alloc] initWithBooksList:nil fromSource:self];
//		__segmentControl = [self createSegmentControl];
		
		txtFldScanListBG =[ [UIImageView alloc] initWithFrame:CGRectMake(35, 60, 251, 54)];
		txtFldScanListBG.image = [UIImage imageNamed:@"textField.png"];
		[contentView addSubview:txtFldScanListBG];
		[txtFldScanListBG release];
		
		dateFormatter = [[NSDateFormatter alloc] init];
		
		[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
		[dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
		
		NSString* dateStr = (scanList.scanDate != nil) ? [dateFormatter stringFromDate:scanList.scanDate] : @"";

		NSString* labelDisplayStr = [scanList.name length] > 0 ? scanList.name : dateStr;
		
		txtFldScanList= [[UITextField alloc ] initWithFrame:CGRectMake(55, 77.5, 200, 28)];
		txtFldScanList.returnKeyType = UIReturnKeySearch;
		txtFldScanList.autocorrectionType = UITextAutocorrectionTypeNo;
		txtFldScanList.autocapitalizationType = UITextAutocapitalizationTypeNone;
		txtFldScanList.clearButtonMode = UITextFieldViewModeWhileEditing;
		txtFldScanList.userInteractionEnabled = YES;
		txtFldScanList.delegate = self;
		txtFldScanList.textAlignment = UITextAlignmentLeft;
		txtFldScanList.font = [UIFont boldSystemFontOfSize:16];
		txtFldScanList.backgroundColor = [UIColor clearColor];
		txtFldScanList.textColor = [UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1] ;
		txtFldScanList.placeholder = @"Scan List Name";
		
		txtFldScanList.text =   labelDisplayStr;
		
		
		[contentView addSubview:txtFldScanList];
		
		viewScannedItemsButton = [[UIButton alloc] initWithFrame:CGRectMake(105, 120, 110, 47)];
		[viewScannedItemsButton setImage:[UIImage imageNamed:@"viewallbooks.png"] forState:UIControlStateNormal];
		viewScannedItemsButton.backgroundColor = [UIColor clearColor];
		[viewScannedItemsButton addTarget:self action:@selector(viewAllBooks) forControlEvents:UIControlEventTouchUpInside];
		[contentView addSubview:viewScannedItemsButton];
		
		
		addToCollectionButton = [[UIButton alloc] initWithFrame:CGRectMake(85, 170, 150, 47)];
		[addToCollectionButton setImage:[UIImage imageNamed:@"addalltowishlist.png"] forState:UIControlStateNormal];
		addToCollectionButton.backgroundColor = [UIColor clearColor];
		[addToCollectionButton addTarget:self action:@selector(addAllToWishList) forControlEvents:UIControlEventTouchUpInside];
		[contentView addSubview:addToCollectionButton];

		addToWishListButton = [[UIButton alloc] initWithFrame:CGRectMake(65, 220, 190, 47)];
		[addToWishListButton setImage:[UIImage imageNamed:@"addalltomycollection.png"] forState:UIControlStateNormal];
		addToWishListButton.backgroundColor = [UIColor clearColor];
		[addToWishListButton addTarget:self action:@selector(addAllToCollection) forControlEvents:UIControlEventTouchUpInside];
		[contentView addSubview:addToWishListButton];

    }
    return self;
}
-(void)refreshWithScanList:(ScanList*)aScanList withScanItems:(NSMutableArray*)aScanItemsList
{
	for(UIView* childView in contentView.subviews)
	{
		[childView removeFromSuperview];
	}
	self.title = @"Scan Details";

	scanItemsList = aScanItemsList;
	scanList = aScanList;
	NSString* dateStr = (scanList.scanDate != nil) ? [dateFormatter stringFromDate:scanList.scanDate] : @"";
	
	NSString* labelDisplayStr = [scanList.name length] > 0 ? scanList.name : dateStr;
	txtFldScanList.text =   labelDisplayStr;

	[contentView addSubview:txtFldScanListBG];
	[contentView addSubview:txtFldScanList];

	[contentView addSubview:addToWishListButton];
	[contentView addSubview:addToCollectionButton];
	[contentView addSubview:viewScannedItemsButton];

	
}
/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)dealloc {
			//printf("\n In ScanDetails Dealloc");
	[bookListViewController release];
    [super dealloc];
	
}
-(NSMutableArray*)getBooksListFromScannedItems:(NSMutableArray*)aScanItemsList
{
	
	NSMutableArray* resultsList =[[NSMutableArray alloc] init] ;
	for(ScanItem* scanItem in aScanItemsList)
	{
		Book* book = [[Book alloc] init];
		book.author = scanItem.authorName;
		book.title= scanItem.bookName;
		book.isbnId10 = scanItem.isbnId10;
		book.imageUrl = scanItem.imageUrl;
		[resultsList addObject:book];
		[book release];
	}
	return resultsList;
}
-(void)viewAllBooks
{
	//printf("\n View all books");
	//CampusBooksAppDelegate* appDelegate = [[UIApplication sharedApplication] delegate]; 
	NSMutableArray* resultsList = [self  getBooksListFromScannedItems:scanItemsList];
	[bookListViewController refreshBookListViewController:resultsList fromSource:self];
	//printf("\n No of objects returned :%d", [booksList count]);
	if ([resultsList count] >0)
	{
		//printf("\n About to display bookList ViewController. \n is Book List view Controller nil ? :%s", (bookListViewController== nil? "Yes" :"No"));
		
		[[self navigationController] pushViewController:bookListViewController animated:YES];
	}
}
-(void) addAllToCollection
{
	//printf("\n This will add all objects to collection");
	
	CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [[UIApplication sharedApplication]delegate];
	for (ScanItem* scanItem in scanItemsList)
	{
		//printf("\n checking with :%s", [scanItem.isbnId10 UTF8String]);
		BOOL isAlreadyInCollection = [appDelegate isItemInMyCollection:scanItem.isbnId10];
		if (!isAlreadyInCollection)
		{
			MyCollection* myCollection = [[[MyCollection alloc ]init]autorelease];
			myCollection.authorName = scanItem.authorName;
			myCollection.bookName = scanItem.bookName;
			myCollection.imageUrl = scanItem.imageUrl;
			myCollection.isbnId10 = scanItem.isbnId10;
			[appDelegate addToMyCollection:myCollection];
		}
	}
	UIAlertView* alert = 
	[[[UIAlertView alloc] 
	  initWithTitle:@"CampusBooks" 
	  message:@"All books have been successfully added to your collection."
	  delegate:nil cancelButtonTitle:@"OK" 
	  otherButtonTitles:nil] autorelease];
	
	[alert show];		
}
-(void) addAllToWishList
{
	//printf("\n This will add all objects to wishlist");
	
//	CampusBooksAppDelegate* appDelegate = [[UIApplication sharedApplication]delegate];
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	for (ScanItem* scanItem in scanItemsList)
	{
		//printf("\n checking with :%s", [scanItem.isbnId10 UTF8String]);
		BOOL isAlreadyInWishList = [appDelegate isItemInWishList:scanItem.isbnId10];
		if (!isAlreadyInWishList)
		{
			WishList* wishList = [[[WishList alloc ]init]autorelease];
			wishList.authorName = scanItem.authorName;
			wishList.bookName = scanItem.bookName;
			wishList.imageUrl = scanItem.imageUrl;
			wishList.isbnId10 = scanItem.isbnId10;
			[appDelegate addToWishList:wishList];
		}
	}
		
	UIAlertView* alert = 
	[[[UIAlertView alloc] 
	  initWithTitle:@"CampusBooks" 
	  message:@"All books have been successfully added to your wishlist."
	  delegate:nil cancelButtonTitle:@"OK" 
	  otherButtonTitles:nil] autorelease];
	
	[alert show];		
}
-(NSString *)hostName
{
	// Don't include a scheme. 'http://' will break the reachability checking.
	// Change this value to test the reachability of a different host.
	return @"www.apple.com";
}
- (void)updateStatus
{
	
	self.internetConnectionStatus	= [[Reachability sharedReachability] internetConnectionStatus];
	//	[tableView reloadData];
}

-(BOOL)textFieldShouldReturn:(UITextField*)theTextField
{
	NSString* text = [theTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	if ([text length] > 0)
	{
		
		scanList.name = theTextField.text;
		CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [[UIApplication sharedApplication]delegate];
		[appDelegate updateScanList:scanList];
	}
	[theTextField resignFirstResponder];
	return YES;
}


@end
