//
//  ScanDetailsViewController.h
//  CampusBooks
//
//  Created by Admin on 13/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"Reachability.h"
#import"BookListViewController.h"
#import"ScanItem.h"
#import"ScanList.h"
@interface ScanDetailsViewController :UIViewController<UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
	UIView *contentView;
	
//	UITableView* theTableView;

	UIButton* searchButton;

	UIImageView* txtFldScanListBG;
	UITextField* txtFldScanList;
	UILabel* txtDateLabel;
	
	NetworkStatus internetConnectionStatus;

	UIActivityIndicatorView* progressInd;
	BOOL isSearchInProgress;

	NSString* strKeyword;

	BookListViewController* bookListViewController;
	
	UIButton* viewScannedItemsButton;
	UIButton* addToCollectionButton;
	UIButton* addToWishListButton;
	NSDateFormatter* dateFormatter;
	NSMutableArray* scanItemsList;
	NSMutableArray* booksList;
	ScanList* scanList;
}
@property NetworkStatus internetConnectionStatus;
/*
 @property(nonatomic, retain) NSString* strTitle;
@property(nonatomic, retain) NSString* strAuthor;
@property(nonatomic, retain) NSString* strKeyword;
@property(nonatomic, retain) NSString* strIsbn;
 */
@property(nonatomic, retain)NSString* strKeyword;
- (void)updateStatus;
-(NSString *)hostName;

-(id)initWithScanList:(ScanList*)aScanList withScanItems:(NSMutableArray*)aScanItemsList;
-(void)refreshWithScanList:(ScanList*)aScanList withScanItems:(NSMutableArray*)aScanItemsList;

@end
