//
//  InfoViewController.h
//  CampusBooks
//
//  Created by Admin on 23/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InfoViewController : UIViewController<UIAlertViewDelegate> {
	UIView* contentView;
	UIButton* backButton;
	UIImageView* titleView;
	
	UIAlertView* campusBooksAlert;
	UIAlertView* reviewAlert;
}
-(void)doOpenSafari;
-(void)openCampusBooks;
-(void)openReviewView;

@end
