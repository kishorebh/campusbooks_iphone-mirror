//
//  InfoViewController.m
//  CampusBooks
//
//  Created by Admin on 23/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "InfoViewController.h"
#import <Twitter/Twitter.h>
#import"CampusBooksAppDelegate.h"
#import "PostViewController.h"
@implementation InfoViewController
- (id)init {
    if (self = [super init]) {
        // Custom initialization
		contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,320, 390)];
		contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
		[self.view addSubview: contentView];
		
/*		UIImageView* titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"CampusBooks.png"]];
		[contentView addSubview:titleView];
		[titleView release];
	*/	
		self.title = @"Info";
//		CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication] delegate];
/*		backButton = [[UIButton alloc] initWithFrame:CGRectMake(0,375, 79,34)];
		[backButton setImage:[UIImage imageNamed:@"all-back.png"] forState:UIControlStateNormal];
		[backButton addTarget:appDelegate action:@selector(closeInfoViewController) forControlEvents:UIControlEventTouchUpInside];
		[contentView addSubview:backButton];
		[backButton release];	
*/		
		NSBundle* mainBundle = [NSBundle mainBundle]; 
		NSString* versionNumber = [mainBundle objectForInfoDictionaryKey:@"CFBundleVersion"];

		
		UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320,30)];
		titleLabel.text = [NSString stringWithFormat:@"Info v%s", [versionNumber UTF8String]];
		titleLabel.textAlignment = UITextAlignmentCenter;
		titleLabel.textColor = [UIColor darkTextColor];//[UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];;
		titleLabel.font = [UIFont systemFontOfSize:15];
		titleLabel.backgroundColor = [UIColor clearColor];
		[contentView addSubview:titleLabel];
		[titleLabel release];
		
		UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 30, 335, 7)];
		imageView.image = [UIImage imageNamed:@"separation line.png"];
		[contentView addSubview:imageView];
		[imageView release];
		
		campusBooksAlert = [[UIAlertView alloc] initWithTitle:@"CampusBooks" message:@"Visiting campusbooks.com will terminate the application. \n Do you want to continue?"
												   delegate:self cancelButtonTitle:@"No" otherButtonTitles: @"Yes",nil];
		campusBooksAlert.delegate =self;
		
		//We hope you are pleased with CampusBooks. Please share your view by “Writing a Review” at the App Store so that others might also consider using CampusBooks. If you are having difficulty with CampusBooks, please write to us so that we can address your concerns.\n\n Thanks for using CampusBooks and thanks for your support. \n\nDo you want to continue?
		reviewAlert = [[UIAlertView alloc] initWithTitle:@"CampusBooks" message:@"Help us spread the word how CampusBooks can help you save money.  Write a review at the app store by clicking \"Yes\".\n\n Not completely happy with CampusBooks or have a feature update you would like to see?  Simply let us know and we will work to address your concern. You can contact support on the iTunes page by clicking \"Yes\" below."
													 delegate:self cancelButtonTitle:@"No" otherButtonTitles: @"Yes",nil];
		reviewAlert.delegate =self;
		
		
/*		UILabel* firstLineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 35, 320,30)];
		firstLineLabel.text = @"CampusBooks is powered by  ";
		firstLineLabel.textAlignment = UITextAlignmentCenter;
		firstLineLabel.textColor = [UIColor darkTextColor];//[UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];;
//		firstLineLabel.textColor = [UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1]  ;
		firstLineLabel.font = [UIFont systemFontOfSize:20];
		firstLineLabel.backgroundColor = [UIColor clearColor];
		[contentView addSubview:firstLineLabel];
		[firstLineLabel release];
*/		
		UIButton* buyButton = [[UIButton alloc] initWithFrame:CGRectMake(60,80,200,50)];
		[buyButton setImage:[UIImage imageNamed:@"CampusBooks.png"] forState:UIControlStateNormal];
		[buyButton addTarget:self action:@selector(openCampusBooks) forControlEvents:UIControlEventTouchUpInside];
		[contentView addSubview:buyButton];
		[buyButton release];
		
		UILabel* contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 135, 290,180)];
		contentLabel.text = @"CampusBooks.com \n was founded in 1998 and is the leading textbook comparison shopping web site, serving hundreds of thousands of students each year. Our goal is to make buying textbooks easy and affordable with the lowest price on new and used textbooks and friendly service. ";
		contentLabel.textAlignment = UITextAlignmentCenter;
		contentLabel.numberOfLines = 0;
		contentLabel.textColor = [UIColor darkTextColor];///[UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];;
//		contentLabel.textColor =  [UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1]  ;
		contentLabel.font = [UIFont systemFontOfSize:15];
		contentLabel.backgroundColor = [UIColor clearColor];
		[contentView addSubview:contentLabel];
		[contentLabel release];
		
		
		UIImageView* imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 310, 335, 7)];
		imageView1.image = [UIImage imageNamed:@"separation line.png"];
		[contentView addSubview:imageView1];
		[imageView1 release];
		
		
		UIButton* reviewButton = [[UIButton alloc] initWithFrame:CGRectMake(110,320,129,41)];
		[reviewButton setImage:[UIImage imageNamed:@"review_campusBooks1.png"] forState:UIControlStateNormal];
		[reviewButton addTarget:self action:@selector(reviewAction) forControlEvents:UIControlEventTouchUpInside];
		[contentView addSubview:reviewButton];
		[reviewButton release];
		
		UIButton* facebookButton = [[UIButton alloc] initWithFrame:CGRectMake(20,315,58,50)];
		[facebookButton setImage:[UIImage imageNamed:@"facebook.png"] forState:UIControlStateNormal];
		//[facebookButton setTitle:@"F" forState:UIControlStateNormal];
		//[facebookButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
		[facebookButton addTarget:self action:@selector(getPostView) forControlEvents:UIControlEventTouchUpInside];
		[contentView addSubview:facebookButton];
		[facebookButton release];
		
		
		UIButton* twitterButton = [[UIButton alloc] initWithFrame:CGRectMake(260,315,50,50)];
		[twitterButton setImage:[UIImage imageNamed:@"twitter.png"] forState:UIControlStateNormal];
		//[twitterButton setTitle:@"T" forState:UIControlStateNormal];
		//[twitterButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
		[twitterButton addTarget:self action:@selector(getTwitterPostView) forControlEvents:UIControlEventTouchUpInside];
		[contentView addSubview:twitterButton];
		[twitterButton release];
		
/*		UIWebView* webView  = [[UIWebView alloc] initWithFrame:CGRectMake(0, 80, 320, 300)];
		webView.backgroundColor = [UIColor clearColor];
		NSString* htmlString = @"<html><head><title><style type=\"text/css\">a {color: red}</style>";
		htmlString = [htmlString stringByAppendingString:@"</title></head><body style='background-color: transparent;color:black'> "];
		htmlString = [htmlString stringByAppendingString:@"<div align='centre' >"];
		htmlString = [htmlString stringByAppendingString:@"<font size='4'>"];
		htmlString = [htmlString stringByAppendingString:@"<div align='centre'> </div><a href='http://www.CampusBooks.com'><img src='CampusBooks.png'></a>. <br>"];
		htmlString = [htmlString stringByAppendingString:@"CampusBooks.com was founded in 1998 and is the leading textbook comparison shopping Web site, serving hundreds of thousands of students each year. Our goal is to make buying textbooks easy and affordable with the lowest price on new and used textbooks and friendly service.<br>"];
		htmlString = [htmlString stringByAppendingString:@" </div></body></html>"];
		
		NSString *imagePath = [[NSBundle mainBundle] resourcePath];
		imagePath = [imagePath stringByReplacingOccurrencesOfString:@"/" withString:@"//"];
		imagePath = [imagePath stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
		
		[webView loadHTMLString:htmlString baseURL:[NSURL URLWithString: [NSString stringWithFormat:@"file:/%@//",imagePath]]];

		[contentView addSubview:webView];
*/		
	/*	UILabel* titleLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 65, 310,30)];
		titleLabel2.text = @"Beta 3 (5th August)";
		titleLabel2.textAlignment = UITextAlignmentRight;
		titleLabel2.textColor = [UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];;
		titleLabel2.font = [UIFont boldSystemFontOfSize:20];
		titleLabel2.backgroundColor = [UIColor clearColor];
		[contentView addSubview:titleLabel2];
		[titleLabel2 release];
	 */
		
		
    }
    return self;
}

-(void)openCampusBooks
{
	[campusBooksAlert show];
}
-(void)reviewAction
{
	[reviewAlert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if(buttonIndex ==1)
	{
		if(alertView == campusBooksAlert)
		{
			[self doOpenSafari];
		}
		else if(alertView == reviewAlert)
		{
			[self openReviewView];
		}
	}
}
-(void)getPostView
{
	PostViewController* postViewController=[[PostViewController alloc]init];
	postViewController.isFromTwitter=NO;
	[self.navigationController pushViewController:postViewController animated:YES];
	//[postViewController release];
}
-(void)getTwitterPostView
{
    TWTweetComposeViewController *twitter = [[TWTweetComposeViewController alloc] init];

    [twitter setInitialText:@"I'm saving money on books using CampusBooks for iPhone! I recommend you do too. Get the free app at http://tinyurl.com/ibookstore"];
                        
    [self presentViewController:twitter animated:YES completion:nil];

    twitter.completionHandler = ^(TWTweetComposeViewControllerResult res) {

        if(res == TWTweetComposeViewControllerResultDone) {

            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Campusbooks" message:@"Thank you very much for recommending CampusBooks to your friends. We appreciate your support." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
              
            [alert show];    
          
        }
        [self dismissModalViewControllerAnimated:YES];

    };
    //	PostViewController* postViewController=[[PostViewController alloc]init];
    //	postViewController.isFromTwitter=YES;
    //	[self.navigationController pushViewController:postViewController animated:YES];
    ////	[postViewController release];
}
	
	
-(void)doOpenSafari
{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.campusbooks.com"]];
}

-(void)openReviewView
{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=326904377&mt=8"]];
}
@end
