//
//  MerchantViewController.h
//  CampusBooks
//
//  Created by Admin on 05/08/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"Reachability.h"
#import"Book.h"
#import"BooksOffer.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface MerchantViewController : UIViewController<UIWebViewDelegate, UIActionSheetDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate> {
	UIView* contentView;
	UIWebView* myWebView;
	NetworkStatus internetConnectionStatus;
	
	UIActivityIndicatorView* progressInd;

	Book* currentBook;
	BooksOffer* currentOffer;
	
	NSString* offerUrl;
	UIAlertView* emailAlertView;
	UIAlertView* safariAlertView;
	UIButton* buyButton;
}
@property(nonatomic) NetworkStatus internetConnectionStatus;
@property(nonatomic,retain) NSString* offerUrl;
-(void)reloadMerchantViewWithOffer:(BooksOffer*)aBookOffer forBook:(Book*)aBook;
- (void)updateStatus;
-(NSString *)hostName;

-(void)openInSafari;
-(void) doEmailContent;
-(NSString*)getEmailString;
-(void)displayComposerSheet ;
-(void) sendEmailFromAppOnDevice;
-(IBAction)sendEmail:(id)sender;

@end
