    //
//  BuyOptionsViewController.m
//  CampusBooks
//
//  Created by admin bluepal on 19/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "BuyOptionsViewController.h"
#import "BookBuyViewController.h"
#import"CampusBooksAppDelegate.h"
#import"BookOfferViewController.h"
#import"BookListViewController.h"
#import"MapViewController.h"
#import"BookDetailsViewController.h"

@implementation BuyOptionsViewController

@synthesize internetConnectionStatus;

static int MODE_BUY = 0;
static int MODE_SELL = 1;

-(id)initWithBook:(Book*)aBookInfo withMode:(NSInteger)aMode withOffers:(NSMutableArray*)aOffersList fromSource:(UIViewController*)aBookListViewController
{
	if (self = [super init]) {
		
		currentBookInfo = aBookInfo;
		bookListViewController = aBookListViewController;
		bookOffersList = aOffersList;
		buyOrSellMode = aMode;
		
		BookCoverLoader* coverLoader = [BookCoverLoader sharedLoader];
		currentBookInfo.image = [coverLoader getBookCover:currentBookInfo delegate:self];

		
		if(buyOrSellMode == MODE_BUY)
		{
			self.title = @"Choose Buy Option";
		}
		else if(buyOrSellMode == MODE_SELL)
		{
			self.title = @"Choose Sell Option";
		}
		contentView = [[TransitionView alloc] initWithFrame:CGRectMake(0,0,320,390)];
		contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
		
		CGRect activityViewframe = CGRectMake(145.0,175.5, 35,35);
		progressInd = [[UIActivityIndicatorView alloc] initWithFrame:activityViewframe];
		progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
		progressInd.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
										UIViewAutoresizingFlexibleRightMargin |
										UIViewAutoresizingFlexibleTopMargin |
										UIViewAutoresizingFlexibleBottomMargin);
		[contentView addSubview:progressInd];
		[progressInd startAnimating];
		progressInd.hidden = NO;
		[progressInd stopAnimating];
		
		
		tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 8, 320,360) style:UITableViewStylePlain];
		tableView.delegate = self;
		tableView.dataSource = self;
		tableView.rowHeight = 35;
		tableView.sectionHeaderHeight = 5;
		tableView.sectionFooterHeight = 0;	
		tableView.separatorColor=[UIColor clearColor];
		tableView.backgroundColor = [UIColor clearColor];
		[contentView addSubview:tableView];
		[tableView reloadData];
		[tableView release];
		
		bestOffersList = [[NSMutableArray alloc] init];
		//UIBarButtonItem* actionControl = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(displayActionSheet:)] autorelease];
//		UIBarButtonItem* actionControl = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"buy.png"] style:UIBarButtonItemStyleDone target:self action:@selector(displayActionSheet:)] autorelease];
		
//		self.navigationItem.rightBarButtonItem = actionControl;
		
		offersByConditionDictionary = [[NSMutableDictionary alloc] init];
		[self organizeOffersByCategory];
	}
	return self;
}


-(void)refreshBuyOptionsViewController:(Book*)aBook withMode:(NSInteger)aMode withOffers:(NSMutableArray*)aOffersList fromSource:(UIViewController*)aBookListViewController
{
	for (UIView* subView in contentView.subviews)
	{
		[subView removeFromSuperview];
	}
	currentBookInfo = aBook;
	bookListViewController = aBookListViewController;
	bookOffersList = aOffersList;

	if(buyOrSellMode == MODE_BUY)
	{
		self.title = @"Choose Buy Option";
		[bookOffersList sortUsingSelector:@selector(compareByConditionAndTotalPrice:)];
		[self getBestPrices];
		[bestOffersList sortUsingSelector:@selector(compareByTotalPrice:)];
		[bookOffersList sortUsingSelector:@selector(compareByTotalPrice:)];
	}
	else if(buyOrSellMode == MODE_SELL)
	{
		self.title = @"Choose Sell Option";
		[bookOffersList sortUsingSelector:@selector(compareByConditionAndTotalPriceReverse:)];
		[self getBestPrices];
		[bestOffersList sortUsingSelector:@selector(compareByTotalPriceReverse:)];
		[bookOffersList sortUsingSelector:@selector(compareByTotalPriceReverse:)];
	}
	[tableView reloadData];
	
//	printf("\n Publisher:%s",[currentBookInfo.publisher UTF8String]);
	[contentView addSubview:tableView];
	
	[self.view addSubview:contentView];
}


-(NSString *)hostName
{
	// Don't include a scheme. 'http://' will break the reachability checking.
	// Change this value to test the reachability of a different host.
	return @"www.apple.com";
}
- (void)updateStatus
{
	
	self.internetConnectionStatus	= [[Reachability sharedReachability] internetConnectionStatus];
	//	[tableView reloadData];
}
-(void)dealloc
{
	//printf("\n In BookBuy Dealloc");
	[offersByConditionDictionary release];
	[super dealloc];
}

#pragma mark Table Delegate and Data Source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tv {
	
	return 1;
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)section {
	return [bestOffersList count]+ 2;
}

- (UITableViewCell *)tableView:(UITableView *)atableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	UITableViewCell *cell = (UITableViewCell *)[atableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MasterViewIdentifier"] autorelease];
		cell.backgroundColor=[UIColor clearColor];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		UIView* elementView =  [[UIView alloc] initWithFrame:CGRectMake(0,0,320,420)];
		elementView.tag = 0;
		elementView.backgroundColor = [UIColor clearColor];
		elementView.autoresizesSubviews = YES;
		elementView.autoresizingMask =  (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
		
		[cell.contentView addSubview:elementView];
		[elementView release];
	}
	UIView* elementView  = [cell.contentView viewWithTag:0];
	for(UIView* subView in elementView.subviews)
	{
		[subView removeFromSuperview];
	}
	
	if (indexPath.row ==0)
	{
		UIView* subView1=[[UIView alloc]initWithFrame:CGRectMake(0,0, 320,110)];
		subView1.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"book_background.png"]];
		
		UILabel* titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 10, 200,40)];
		titleLabel.numberOfLines = 2;
		titleLabel.font = [ UIFont boldSystemFontOfSize:15];
		titleLabel.adjustsFontSizeToFitWidth = YES;
		titleLabel.text = (currentBookInfo.title != nil ? currentBookInfo.title : @"N/A");
		titleLabel.backgroundColor = [UIColor clearColor];
		titleLabel.textAlignment = UITextAlignmentLeft;
		titleLabel.textColor = [UIColor darkTextColor];
		[subView1 addSubview:titleLabel];
		[titleLabel release];
        
        UILabel* authorLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 50, 200,20)];
		authorLabel.font = [ UIFont systemFontOfSize:14];
		authorLabel.adjustsFontSizeToFitWidth = YES;
		authorLabel.text = [NSString stringWithFormat:@"ISBN 10: %@",(currentBookInfo.isbnId10 != nil ? currentBookInfo.isbnId10 : @"N/A")];
		authorLabel.backgroundColor = [UIColor clearColor];
		authorLabel.textAlignment = UITextAlignmentLeft;
		authorLabel.textColor = [UIColor lightGrayColor];
		[subView1 addSubview:authorLabel];
		[authorLabel release];
        
        
        NSString* formattedDateString = @"";
        if (currentBookInfo.publishedDate != nil)
        {
            NSDate* originalDate = [originalDateFormatter dateFromString:currentBookInfo.publishedDate];
            formattedDateString = [newDateFormatter stringFromDate:originalDate];
        }
        if ([formattedDateString length] == 0) {
            formattedDateString = @"N/A";
        }
        UILabel* releaseDateLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 70, 200,20)];
		releaseDateLabel.font = [ UIFont systemFontOfSize:14];
		releaseDateLabel.adjustsFontSizeToFitWidth = YES;
		releaseDateLabel.text = [NSString stringWithFormat:@"ISBN 13: %@",(currentBookInfo.isbnId13 != nil ? currentBookInfo.isbnId13 : @"N/A")];
		releaseDateLabel.backgroundColor = [UIColor clearColor];
		releaseDateLabel.textAlignment = UITextAlignmentLeft;
		releaseDateLabel.textColor = [UIColor lightGrayColor];
		[subView1 addSubview:releaseDateLabel];
		[releaseDateLabel release];
        
        
		UIImageView* imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(35, 17, 60, 80)];
		imageView2.image = currentBookInfo.image != nil ? currentBookInfo.image : [UIImage imageNamed:@"LoadingCover.png"];
		[subView1 addSubview:imageView2];
		[imageView2 release];
		
		[elementView addSubview:subView1];
		[subView1 release];		

		
	}
	else if (indexPath.row ==1) 
	{
        UIView* subView1=[[UIView alloc]initWithFrame:CGRectMake(0,0, 320,28)];
        subView1.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"summary_of_best_offers.png"]];
        
        [elementView addSubview:subView1];
        [subView1 release];
		
	}
	else {
		
		UIFont*  labelFontForPrices =  [ UIFont boldSystemFontOfSize:16];
		
		UIColor* backgroundColor = [UIColor clearColor];
		//UIColor* headerColor =  [UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];
		//	UIColor* singleRowTextColor =[UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1] ;
		
		cell.backgroundColor = backgroundColor;
		elementView.backgroundColor = backgroundColor;
		
		cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		
		BooksOffer* currOffer = [bestOffersList objectAtIndex:indexPath.row -2];		
		NSString* conditionId = currOffer.conditionId;

		NSString* conditionName = @"";
		NSString* conditionImageName = @"";
		if ([conditionId isEqualToString:@"8"])
		{
			conditionName = @"Local Bookstore";
			conditionImageName = @"target.png";
		}
		else if ([conditionId isEqualToString:@"9"])
		{
			conditionName = @"Local Library";
			conditionImageName = @"library.png";
		}
		else {
			conditionName = @"Web";
			conditionImageName = @"world.png";
		}

		NSMutableArray* offersOfConditionList=  [self getOffersFromConditionType:currOffer.conditionId];
		int noOfOffers = [offersOfConditionList count];
//		[offersOfConditionList release];
		UIImageView* merchantLabel =  [[UIImageView alloc]initWithFrame:CGRectMake(20, 5, 35,35)];
		merchantLabel.image = [UIImage imageNamed:conditionImageName];
		[elementView addSubview:merchantLabel];
		[merchantLabel release];
		
		UILabel* conditionLabel =  [[UILabel alloc]initWithFrame:CGRectMake(70, 5, 175,35)];
		conditionLabel.font = labelFontForPrices;
		conditionLabel.text =  [NSString stringWithFormat:@"%s (%d)", [conditionName UTF8String], noOfOffers];
		conditionLabel.backgroundColor =backgroundColor;
		conditionLabel.textAlignment = UITextAlignmentLeft;
		conditionLabel.textColor = [UIColor darkTextColor];
		[elementView addSubview:conditionLabel];
		[conditionLabel release];
		
		NSString* priceValue = [currOffer.totalPrice floatValue] > 0 ? [NSString stringWithFormat:@"$%.2f",[currOffer.totalPrice floatValue] ] : @"Free";
		UILabel* totalPriceLabel =  [[UILabel alloc]initWithFrame:CGRectMake(245,5, 95,35)];
		totalPriceLabel.font = labelFontForPrices;
		totalPriceLabel.text =  priceValue;
		totalPriceLabel.backgroundColor =backgroundColor;
		totalPriceLabel.textAlignment = UITextAlignmentLeft;
		totalPriceLabel.textColor =[UIColor darkTextColor];
		[elementView addSubview:totalPriceLabel];
		[totalPriceLabel release];
			
	}	
	
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row==0)
	{
		return 110;
	} else if (indexPath.row ==1)
	{
		return 28;
	}
	return 45.0;
}
- (NSIndexPath *)tableView:(UITableView *)tv willSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	if (indexPath.row == 0)
	{
		[self populateDetailsView];
	}
	else	if (indexPath.row >=2)
	{
		selectedBookOffer = [bestOffersList objectAtIndex:indexPath.row - 2];	
	}
	if (selectedBookOffer != nil)
	{
		//printf("\n IndexPath Row:%d", indexPath.row);
		
		if (indexPath.row >=2)
		{
			selectedBookOffer = [bestOffersList objectAtIndex:indexPath.row - 2];	
		}
		
		if (selectedBookOffer != nil)
		{
			[self performSelectorOnMainThread:@selector(populateBuyOrSellView) withObject:nil waitUntilDone:NO];
		}
	}
	return nil;
	
}
-(void)loadBuyOrSellView
{
	NSMutableArray* offersOfConditionList = [self getOffersFromConditionType:selectedBookOffer.conditionId];
	
	if (buyOrSellMode == MODE_BUY)
	{
		if (!([selectedBookOffer.conditionId isEqualToString:@"8"] || [selectedBookOffer.conditionId isEqualToString:@"9"]))
		{
			BookBuyViewController* buyViewController = [[BookBuyViewController alloc] initWithBook:currentBookInfo withOffers:offersOfConditionList fromSource:bookListViewController];
			[buyViewController refreshBookBuyViewController:currentBookInfo withOffers:offersOfConditionList fromSource:bookListViewController];
			[self.navigationController pushViewController:buyViewController animated:YES];
			[buyViewController release];
		}
		else {
			CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];					
			LocalOptions* localOptions = [appDelegate getLocalOptions];
			CLLocation* currentLocation = nil;
			if (localOptions.optionsValue ==1) {
				currentLocation = appDelegate.currentLocation;
			}
			else if(localOptions.optionsValue ==2) {
				currentLocation = appDelegate.currentLocation;
			}
			else if(localOptions.optionsValue ==3) {
				Schools* school = [appDelegate getSchoolByName:localOptions.schoolName];
				currentLocation = [[[CLLocation alloc] initWithLatitude:[school.latValue floatValue] longitude:[school.langValue floatValue]] autorelease];
			}
			
			
			MapViewController* viewController=[[MapViewController alloc]initWithCenterLocation:currentLocation forOffers:offersOfConditionList forBook:currentBookInfo  fromBookListController:(BookListViewController*) bookListViewController];
            viewController.conditionId = selectedBookOffer.conditionId;
			[self.navigationController pushViewController:viewController animated:YES];
			[viewController release];
			
		}
		
	}
	else if (buyOrSellMode == MODE_SELL)
	{
		if (!([selectedBookOffer.conditionId isEqualToString:@"8"] || [selectedBookOffer.conditionId isEqualToString:@"9"]))
		{
			BookSellViewController* sellViewController = [[BookSellViewController alloc] initWithBook:currentBookInfo withOffers:offersOfConditionList fromSource:bookListViewController];
			[sellViewController refreshBookSellViewController:currentBookInfo withOffers:offersOfConditionList fromSource:bookListViewController];
			[self.navigationController pushViewController:sellViewController animated:YES];
			[sellViewController release];
		}
		else {
			CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
			LocalOptions* localOptions = [appDelegate getLocalOptions];;
			CLLocation* currentLocation = nil;
			if (localOptions.optionsValue ==1) {
				currentLocation = appDelegate.currentLocation;
			}
			else if(localOptions.optionsValue ==2) {
				currentLocation = appDelegate.currentLocation;
			}
			else if(localOptions.optionsValue ==3) {
				Schools* school = [appDelegate getSchoolByName:localOptions.schoolName];
				currentLocation = [[[CLLocation alloc] initWithLatitude:[school.latValue floatValue] longitude:[school.langValue floatValue]] autorelease];
			}
			MapViewController* viewController=[[MapViewController alloc]initWithCenterLocation:currentLocation forOffers:offersOfConditionList forBook:currentBookInfo  fromBookListController:(BookListViewController*)bookListViewController] ;
            viewController.conditionId = selectedBookOffer.conditionId;

			[self.navigationController pushViewController:viewController animated:YES];
			[viewController release];
			
		}
	}
}
- (void)viewWillAppear:(BOOL)animated
{
	if(buyOrSellMode == MODE_BUY)
	{
		self.title = @"Choose Buy Option";
	}
	else if(buyOrSellMode == MODE_SELL)
	{
		self.title = @"Choose Sell Option";
	}
	[tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
	self.title = @"Back";
}

-(NSMutableArray*)getBestPrices
{
	//	bestOffersList = 
	[bestOffersList removeAllObjects];

//	NSMutableArray* tmpBestOffersList = [[[NSMutableArray alloc] init] autorelease];

    [offersByConditionDictionary enumerateKeysAndObjectsUsingBlock:^(NSString* conditionId, NSMutableArray* offersForThisCondition, BOOL *stop) {
        
        [offersForThisCondition sortUsingSelector:@selector(compareByTotalPrice:)];
        BooksOffer* bestOfferForThisCondition = [offersForThisCondition objectAtIndex:0];
        [bestOffersList addObject:bestOfferForThisCondition];
    }];
	
//	BooksOffer* webOffer = nil;
//	BooksOffer* localOffer = nil;
//	BooksOffer* libraryOffer = nil;
//	
//	for (BooksOffer* aBookOffer in tmpBestOffersList) {
//		NSString* currentCondition = aBookOffer.conditionId;
//        
//		if ([currentCondition isEqualToString:@"8"])
//		{
//			localOffer = aBookOffer;
//		}
//		else if ([currentCondition isEqualToString:@"9"])
//		{
//			libraryOffer = aBookOffer;
//		}
//		else if(webOffer == nil){
//			webOffer = aBookOffer;
//		}
//	}
//	
//	if (webOffer != nil)
//	{
//		[bestOffersList addObject:webOffer];
//	}
//	if (localOffer != nil)
//	{
//		[bestOffersList addObject:localOffer];
//	}
//	if (libraryOffer != nil)
//	{
//		[bestOffersList addObject:libraryOffer];
//	}
	
	return bestOffersList;
}

-(void)organizeOffersByCategory
{
	
	for(BooksOffer* aBookOffer in bookOffersList)
	{
		NSString* currentConditionNum = aBookOffer.conditionId;
		if (!([currentConditionNum isEqualToString:@"8"] || [currentConditionNum isEqualToString:@"9"]))
		{
			currentConditionNum = @"0";
		}
		NSMutableArray* offersForThisCondition = [offersByConditionDictionary valueForKey:currentConditionNum];
		if (offersForThisCondition ==nil) {
			offersForThisCondition = [[NSMutableArray alloc] init];
			[offersByConditionDictionary setValue:offersForThisCondition forKey:currentConditionNum];
			//[offersForThisCondition release];
		}
		[offersForThisCondition addObject:aBookOffer];
	}
}

-(NSMutableArray*)getOffersFromConditionType:(NSString*)aConditionType
{
	if (!([aConditionType isEqualToString:@"8"] || [aConditionType isEqualToString:@"9"]))
	{
		aConditionType = @"0";
	}
	NSMutableArray* offersOfConditionList = [offersByConditionDictionary valueForKey:aConditionType];
    [offersOfConditionList sortUsingSelector:@selector(compareByTotalPrice:)];
	return offersOfConditionList;
}

-(void)populateDetailsView
{
	[self updateStatus];
	
	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!" message:@"No Network Available"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		
	}
	else
	{
		
		//	@try {
		
		[progressInd startAnimating];
		//	[detailsButton setImage:[UIImage imageNamed:@"transparent-clicked.png"] forState:UIControlStateNormal];
		
		[NSThread detachNewThreadSelector:@selector(doPopulateDetailsView:) toTarget:self withObject:nil];
	}
}

- (void)doPopulateDetailsView:(id)sender
{
	//	@try {
	
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	bookForDetails = [appDelegate getBookInfo:currentBookInfo.isbnId10];
	bookForDetails.image = currentBookInfo.image;
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	[progressInd stopAnimating];
	
	[pool release];	
	[self performSelectorOnMainThread:@selector(loadDetailsView) withObject:nil waitUntilDone:NO];
	[NSThread exit];
	
}

-(void)loadDetailsView
{
	BookDetailsViewController* bookDetailsViewController = [[BookDetailsViewController alloc] initWithBook:bookForDetails fromSource:bookListViewController];
	
	[bookDetailsViewController refreshBookDetailsViewController:bookForDetails fromSource:bookListViewController];
	
	if ([self.navigationController topViewController] == bookDetailsViewController)
	{
		//printf("\n cancelling the push");
	}
	else
	{
		//printf("\n about to push to details view controller");
		[self.navigationController pushViewController:bookDetailsViewController animated:YES];
	}
	[bookDetailsViewController release];
}

-(void)didReceiveBookCover:(NSString*)aIsbn
{
	printf("\n called didReceiveBookCover in ScanViewController" );
	BookCoverLoader *coverLoader= [BookCoverLoader sharedLoader];
	currentBookInfo.image = [coverLoader getBookCover:currentBookInfo delegate:self];
	[tableView reloadData];
}	



-(void)populateBuyOrSellView
{
	[self updateStatus];
	
	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!" message:@"No Network Available"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		
	}
	else
	{
		
		//	@try {
		
		[progressInd startAnimating];
		//	[detailsButton setImage:[UIImage imageNamed:@"transparent-clicked.png"] forState:UIControlStateNormal];
		
		[NSThread detachNewThreadSelector:@selector(doPopulateBuyOrSellView:) toTarget:self withObject:nil];
	}
}

- (void)doPopulateBuyOrSellView:(id)sender
{
	//	@try {
	
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	bookForDetails = [appDelegate getBookInfo:currentBookInfo.isbnId10];
	bookForDetails.image = currentBookInfo.image;
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	[progressInd stopAnimating];
	
	[pool release];	
	[self performSelectorOnMainThread:@selector(loadBuyOrSellView) withObject:nil waitUntilDone:NO];
	[NSThread exit];
	
}
@end
