//
//  SearchViewController.m
//  CampusBooks
//
//  Created by Admin on 13/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "SearchViewController.h"
#import "CampusBooksAppDelegate.h"

@implementation SearchViewController
@synthesize internetConnectionStatus, strTitle, strAuthor, strKeyword, strIsbn; 
#define kTransitionDuration	1
#define kOFFSET_FOR_KEYBOARD 65.0
#define IPHONE_1G_NAMESTRING @"iPhone 1G"
#define IPHONE_3G_NAMESTRING @"iPhone 3G"
#define IPHONE_3GS_NAMESTRING @"iPhone 3GS"
#define IPOD_1G_NAMESTRING @"iPod touch 1G"
#define IPOD_2G_NAMESTRING @"iPod touch 2G"

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)init {
    if (self = [super init]) {
        // Custom initialization
		
		contentView = [[TransitionView alloc] initWithFrame:CGRectMake(0,0,320,390)];
		contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
		//UIView* asubView=[[UIView alloc] initWithFrame:CGRectMake(0,0,320,45)];
		//asubView.backgroundColor=[UIColor redColor];
		//[contentView addSubview:asubView];
		CGRect activityViewframe = CGRectMake(150.0,0, 35,35);
		progressInd = [[UIActivityIndicatorView alloc] initWithFrame:activityViewframe];
		progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
		progressInd.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
										UIViewAutoresizingFlexibleRightMargin |
										UIViewAutoresizingFlexibleTopMargin |
										UIViewAutoresizingFlexibleBottomMargin);
		[progressInd startAnimating];
		progressInd.hidden = NO;
		
/*		titleLabelForView = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 320,30)];
		titleLabelForView.text = @"Search";
		titleLabelForView.textAlignment = UITextAlignmentCenter;
		titleLabelForView.textColor =[UIColor darkTextColor];// [UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];;
		titleLabelForView.font = [UIFont boldSystemFontOfSize:25];
		titleLabelForView.backgroundColor = [UIColor clearColor];
		[contentView addSubview:titleLabelForView];
		[titleLabelForView release];
		
		imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 40, 335, 7)];
		imageView1.image = [UIImage imageNamed:@"separation line.png"];
		[contentView addSubview:imageView1];
		[imageView1 release];
		
		imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 305, 335, 7)];
		imageView2.image = [UIImage imageNamed:@"separation line.png"];
		[contentView addSubview:imageView2];
		[imageView2 release];
*/		
		isMoveUp=NO;

		txtFldBookBG =[ [UIImageView alloc] initWithFrame:CGRectMake(20, 10, 280, 54)];
		txtFldBookBG.image = [UIImage imageNamed:@"textField.png"];
		[contentView addSubview:txtFldBookBG];
		[txtFldBookBG release];
		
		txtFldAuthorBG =[ [UIImageView alloc] initWithFrame:CGRectMake(20, 74, 280, 54)];
		txtFldAuthorBG.image = [UIImage imageNamed:@"textField.png"];
		[contentView addSubview:txtFldAuthorBG];
		[txtFldAuthorBG release];
		
		txtFldKeywordBG =[ [UIImageView alloc] initWithFrame:CGRectMake(20, 136, 280, 54)];
		txtFldKeywordBG.image = [UIImage imageNamed:@"textField.png"];
		[contentView addSubview:txtFldKeywordBG];
		[txtFldKeywordBG release];
		
		txtFldISBNBG =[ [UIImageView alloc] initWithFrame:CGRectMake(20, 200, 280, 54)];
		txtFldISBNBG.image = [UIImage imageNamed:@"textField.png"];
		[contentView addSubview:txtFldISBNBG];
		[txtFldISBNBG release];
		
		txtFldBook = [[UITextField alloc ] initWithFrame:CGRectMake(30, 25, 250, 25)];
		txtFldBook.returnKeyType = UIReturnKeySearch;
		txtFldBook.autocorrectionType = UITextAutocorrectionTypeNo;
		txtFldBook.autocapitalizationType = UITextAutocapitalizationTypeNone;
		txtFldBook.delegate = self;
		txtFldBook.clearButtonMode = UITextFieldViewModeWhileEditing;
		txtFldBook.textAlignment = UITextAlignmentLeft;
		txtFldBook.font = [UIFont boldSystemFontOfSize:20];
	//	txtFldBook.backgroundColor = [UIColor redColor];
		txtFldBook.textColor = [UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1] ;
		txtFldBook.placeholder = @"Book Name";

		txtFldAuthor = [[UITextField alloc ] initWithFrame:CGRectMake(30, 89, 250, 25)];
		txtFldAuthor.returnKeyType = UIReturnKeySearch;
		txtFldAuthor.autocorrectionType = UITextAutocorrectionTypeNo;
		txtFldAuthor.autocapitalizationType = UITextAutocapitalizationTypeNone;
		txtFldAuthor.delegate = self;
		txtFldAuthor.clearButtonMode = UITextFieldViewModeWhileEditing;
		txtFldAuthor.textAlignment = UITextAlignmentLeft;
		txtFldAuthor.font = [UIFont boldSystemFontOfSize:20];
		txtFldAuthor.textColor = [UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1] ;
		txtFldAuthor.placeholder = @"Author Name";
	//	txtFldAuthor.backgroundColor = [UIColor redColor];

		txtFldKeyword = [[UITextField alloc ] initWithFrame:CGRectMake(30, 151, 250, 25)];
		txtFldKeyword.returnKeyType = UIReturnKeySearch;
		txtFldKeyword.autocorrectionType = UITextAutocorrectionTypeNo;
		txtFldKeyword.autocapitalizationType = UITextAutocapitalizationTypeNone;
		txtFldKeyword.delegate = self;
		txtFldKeyword.clearButtonMode = UITextFieldViewModeWhileEditing;
		txtFldKeyword.textAlignment = UITextAlignmentLeft;
		txtFldKeyword.font = [UIFont boldSystemFontOfSize:20];
		txtFldKeyword.textColor = [UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1] ;
		txtFldKeyword.placeholder = @"Keyword(s) ";
		//txtFldKeyword.backgroundColor = [UIColor redColor];

		
		txtFldISBN = [[UITextField alloc ] initWithFrame:CGRectMake(30, 215, 250, 25)];
		txtFldISBN.returnKeyType = UIReturnKeySearch;
		txtFldISBN.autocorrectionType = UITextAutocorrectionTypeNo;
		txtFldISBN.autocapitalizationType = UITextAutocapitalizationTypeNone;
		txtFldISBN.delegate = self;
		txtFldISBN.clearButtonMode = UITextFieldViewModeWhileEditing;
		txtFldISBN.textAlignment = UITextAlignmentLeft;
		txtFldISBN.font = [UIFont boldSystemFontOfSize:20];
		txtFldISBN.textColor = [UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1] ;
		txtFldISBN.placeholder = @"ISBN";
	//	txtFldISBN.backgroundColor = [UIColor redColor];

		bookListViewController = [[BookListViewController alloc] initWithBooksList:nil fromSource:self];
		
		if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
		{
			redLaserScanButton = [[UIButton alloc] initWithFrame:CGRectMake(101.5, 317, 117, 35)];
			[redLaserScanButton setImage:[UIImage imageNamed:@"Scan.png"] forState:UIControlStateNormal];
		//	[redLaserScanButton setImage:[UIImage imageNamed:@"scanbook_press.png"] forState:UIControlStateHighlighted];
			redLaserScanButton.backgroundColor = [UIColor clearColor];
			[redLaserScanButton addTarget:self action:@selector(displayP2SBarCodeReaderScreen) forControlEvents:UIControlEventTouchUpInside];
			[contentView addSubview:redLaserScanButton];
		}
		
		self.title = @"Search";
		isSearchInProgress = NO;
		
		[self.view addSubview:contentView];
		[self refreshSearchView];
    }
    return self;
}
-(void)refreshSearchView
{
	for (UIView* subView in contentView.subviews)
	{
		[subView removeFromSuperview];
	}
//	[contentView addSubview:imageView1];
//	[contentView addSubview:imageView2];
//	[contentView addSubview:titleLabelForView];
	[contentView addSubview:titleView];
	[contentView addSubview:txtFldBookBG];
	[contentView addSubview:txtFldAuthorBG];
	[contentView addSubview:txtFldKeywordBG];
	[contentView addSubview:txtFldISBNBG];

	[contentView addSubview:txtFldBook];
	[contentView addSubview:txtFldAuthor];
	[contentView addSubview:txtFldKeyword];
	[contentView addSubview:txtFldISBN];

	[contentView addSubview:redLaserScanButton];
//	[contentView addSubview:backButton];

	if(isSearchInProgress)
	{
		self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:progressInd ] autorelease];
	}
	else
	{
		self.navigationItem.rightBarButtonItem = nil;
	}

}
/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
			//printf("\n In SearchView Dealloc");
    [super dealloc];
}
-(void)performSearch
{
	

	isSearchInProgress = YES;
	[self refreshSearchView];

	[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(doSearch) userInfo:nil repeats:NO];
}
-(void)doSearch
{
	[self updateStatus];

	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"CampusBooks" message:@"No Network Available"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		isSearchInProgress = NO;
		[self refreshSearchView];
	}
	else
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

	//	@try {
			strTitle = @"";
			if ([[txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  length] > 0)
			{
				strTitle = [txtFldBook.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			}
			strAuthor = @"";
			if ([[txtFldAuthor.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  length] > 0)
			{
				strAuthor = [txtFldAuthor.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			}
			strKeyword = @"";
			if ([[txtFldKeyword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  length] > 0)
			{
				strKeyword = [txtFldKeyword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			}
			strIsbn = @"";
			if ([[txtFldISBN.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  length] > 0)
			{
				strIsbn = [txtFldISBN.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			}
			if ([strTitle length] > 0 || [strAuthor length]  > 0 || [strKeyword length] > 0 || [strIsbn length])
			{
				progressInd.hidden = NO;
				CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication] delegate]; 
				NSMutableArray* booksList = [appDelegate doBookSearch:strTitle byAuthor:strAuthor withKeyword:strKeyword forISBN:strIsbn forPage:1];
				
//				printf("\n No of objects returned :%d", [booksList count]);
				if ([booksList count] >0)
				{
				//	BookListViewController* bookListViewController =  [appDelegate getBookListViewController:booksList fromSource:self];
					[bookListViewController refreshBookListViewController:booksList fromSource:self];
					
					if ([self.navigationController topViewController] == bookListViewController)
					{
						//printf("\n cancelling the push");
					}
					else
					{
						[self.navigationController pushViewController:bookListViewController animated:YES];
					}
				}
				else
				{
					UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:@"No books match the criteria. \nPlease revise it and try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
					[alertView show];
				}
			}
			else
			{
				UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:@"Please provide the search criteria and try again!!! "  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
				[alertView show];
			}
			

/*		}
		@catch (NSException * e) {
			printf("\n caught an exception");
		}
		@finally {
		
		}
 */
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		isSearchInProgress = NO;
		[self refreshSearchView];

	}
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
//	printf("\n Reached should return ");
	NSString* text = [theTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	if ([text length] > 0)
	{
		[self performSearch];
	}
	   
	[theTextField resignFirstResponder];
	
	return YES;
}
-(NSString *)hostName
{
	// Don't include a scheme. 'http://' will break the reachability checking.
	// Change this value to test the reachability of a different host.
	return @"www.apple.com";
}
- (void)updateStatus
{
	
	self.internetConnectionStatus	= [[Reachability sharedReachability] internetConnectionStatus];
	//	[tableView reloadData];
}
/*
-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:txtFldISBN])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (contentView.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)sender
{
    if ([sender isEqual:txtFldISBN])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (contentView.frame.origin.y < 0)
        {
            [self setViewMovedUp:NO];
        }
    }	
}
 */
//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5]; // if you want to slide up the view
	
	//printf("\n RESET VIEW");
    CGRect rect = contentView.frame;
    if (movedUp)
    {
		isMoveUp=NO;
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard 
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y = rect.origin.y - kOFFSET_FOR_KEYBOARD;
        rect.size.height = rect.size.height + kOFFSET_FOR_KEYBOARD;
    }
    else
    {
		//isMoveUp=NO;
        // revert back to the normal state.
        rect.origin.y = rect.origin.y + kOFFSET_FOR_KEYBOARD;
        rect.size.height = rect.size.height - kOFFSET_FOR_KEYBOARD;
    }
   contentView.frame = rect;
	
    [UIView commitAnimations];
}


- (void)keyboardWillShow:(NSNotification *)notif
{
//	printf("\n Keyboard Will SHOW");
    //keyboard will be shown now. depending for which textfield is active, move up or move down the view appropriately
	
    if (([txtFldISBN isFirstResponder]  ) && contentView.frame.origin.y >= 0 )
    {
		if(isMoveUp==NO)
		{
			isMoveUp=YES;
			[self setViewMovedUp:YES];
		}
    }
    else if (![txtFldISBN isFirstResponder] && contentView.frame.origin.y < 0 )
    {
		if(isMoveUp==YES)
		{
			isMoveUp=NO;
			[self setViewMovedUp:NO];
		}
       
    }
}

- (void)keyboardWillHide:(NSNotification *)notif
{
//	printf("\n Keyboard Will hide");
    //keyboard will be shown now. depending for which textfield is active, move up or move down the view appropriately
	
    if ([txtFldISBN isFirstResponder] && contentView.frame.origin.y >= 0 )
    {
		
			[self setViewMovedUp:YES];
		
    }
    else if (![txtFldISBN isFirstResponder] && contentView.frame.origin.y < 0 )
    {
		
			[self setViewMovedUp:NO];
		
    }
}


- (void)viewWillAppear:(BOOL)animated
{
	
	progressInd.hidden = YES;
	isSearchInProgress = NO;
//	[self refreshSearchView];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) 
												 name:UIKeyboardWillShowNotification object:self.view.window]; 

     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) 
												 name:UIKeyboardWillHideNotification object:self.view.window]; 

}

- (void)viewWillDisappear:(BOOL)animated
{
	// unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil]; 
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil]; 
	[txtFldKeyword resignFirstResponder];
	[txtFldAuthor resignFirstResponder];
	[txtFldISBN resignFirstResponder];
	[txtFldBook resignFirstResponder];
}


-(void)displayP2SBarCodeReaderScreen
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	[self.navigationController pushViewController:appDelegate.barcodeViewController animated:YES];
}


@end
