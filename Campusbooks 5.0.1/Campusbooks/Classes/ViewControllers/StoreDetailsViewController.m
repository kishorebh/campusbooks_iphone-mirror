    //
//  StoreDetailsViewController.m
//  CampusBooks
//
//  Created by Admin on 19/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "StoreDetailsViewController.h"
#import"CampusBooksAppDelegate.h"

@implementation StoreDetailsViewController
@synthesize booksOffer;
@synthesize internetConnectionStatus;

static int  SHOPPING_MODE_BUY = 0;
static int  SHOPPING_MODE_SELL = 1;

- (id)initWithOffer:(BooksOffer*)aBookOffer forBook:(Book*)aBook fromBookListController:(BookListViewController*)aBookListViewController {
	if (self = [super init]) {
		theTableView = nil;
		
		book = aBook;
		booksOffer = aBookOffer;
		bookListViewController = aBookListViewController;

		optionTitles = [[NSMutableArray alloc] init];
		contentView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
		contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
		
		UITableView* tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,0 ,320,440) style:UITableViewStylePlain];	
		tableView.delegate = self;
		tableView.dataSource = self;
		tableView.backgroundColor = [UIColor clearColor];
		tableView.separatorColor = [UIColor clearColor];
		tableView.rowHeight = 35;
		[tableView setSectionHeaderHeight:5];
		[tableView setSectionFooterHeight:5];
		[contentView addSubview:tableView];
		self.view =contentView;
		[tableView release];
		[contentView release];
		
		
        
		self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]initWithTitle:@"More" style:UIBarButtonItemStyleBordered target:self action:@selector(displayActionSheet:)]autorelease];
		
		addressString=@"";
		if([booksOffer.storeAddress1 length]>0)
		{
			addressString=[addressString stringByAppendingString:booksOffer.storeAddress1];
			addressString=[addressString stringByAppendingString:@"#@#"];
		}
		if([booksOffer.storeAddress2 length]>0)
		{
			addressString=[addressString stringByAppendingString:booksOffer.storeAddress2];
			addressString=[addressString stringByAppendingString:@"#@#"];
		}
		if([booksOffer.storeCity length]>0)
		{
			addressString=[addressString stringByAppendingString:booksOffer.storeCity];
			addressString=[addressString stringByAppendingString:@"#@#"];
		}
		if([booksOffer.storeState length]>0)
		{
			addressString=[addressString stringByAppendingString:booksOffer.storeState];
		}
		addressArray = [addressString componentsSeparatedByString:@"#@#"];

		CGRect activityViewframe = CGRectMake(145.0,175.5, 35,35);
		progressInd = [[UIActivityIndicatorView alloc] initWithFrame:activityViewframe];
		progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
		progressInd.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
										UIViewAutoresizingFlexibleRightMargin |
										UIViewAutoresizingFlexibleTopMargin |
										UIViewAutoresizingFlexibleBottomMargin);
		[contentView addSubview:progressInd];
		[progressInd startAnimating];
		progressInd.hidden = NO;
		[progressInd stopAnimating];
		
	}
	return self;
}
- (void)viewWillAppear:(BOOL)animated 
{
	if ([booksOffer.conditionId isEqualToString:@"8"] || [booksOffer.conditionId isEqualToString:@"9"])
	{
		self.title = @"Store Details";
	}
	else {
		self.title = @"Library Details";
	}

	
	[theTableView reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView  numberOfRowsInSection:(NSInteger)section {
	
	return 6;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
	return 1;
	
}
/*		
		NSString* addressString=@"";
		printf("\n store address:%s",[booksOffer.storeAddress1 UTF8String]);
		printf("\n store address2:%s",[booksOffer.storeAddress2 UTF8String]);
		printf("\n store city:%s",[booksOffer.storeCity UTF8String]);
		printf("\n store state:%s",[booksOffer.storeState UTF8String]);
		if([booksOffer.storeAddress1 length]>0)
		{
			addressString=[addressString stringByAppendingString:booksOffer.storeAddress1];
			addressString=[addressString stringByAppendingString:@", "];
			addressString=[addressString stringByAppendingString:@"\n "];
		}
		if([booksOffer.storeAddress2 length]>0)
		{
			addressString=[addressString stringByAppendingString:booksOffer.storeAddress2];
			addressString=[addressString stringByAppendingString:@", "];
			addressString=[addressString stringByAppendingString:@"\n "];
		}
		if([booksOffer.storeCity length]>0)
		{
			addressString=[addressString stringByAppendingString:booksOffer.storeCity];
			addressString=[addressString stringByAppendingString:@", "];
			addressString=[addressString stringByAppendingString:@"\n "];
		}
		if([booksOffer.storeState length]>0)
		{
			addressString=[addressString stringByAppendingString:booksOffer.storeState];
		}
*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row==0)
	{
		return 110;
	}
	else if (indexPath.row ==3)
	{
		addressString=@"";
		if([booksOffer.storeAddress1 length]>0)
		{
			addressString=[addressString stringByAppendingString:booksOffer.storeAddress1];
			addressString=[addressString stringByAppendingString:@"#@#"];
		}
		if([booksOffer.storeAddress2 length]>0)
		{
			addressString=[addressString stringByAppendingString:booksOffer.storeAddress2];
			addressString=[addressString stringByAppendingString:@"#@#"];
		}
		if([booksOffer.storeCity length]>0)
		{
			addressString=[addressString stringByAppendingString:booksOffer.storeCity];
			addressString=[addressString stringByAppendingString:@"#@#"];
		}
		if([booksOffer.storeState length]>0)
		{
			addressString=[addressString stringByAppendingString:booksOffer.storeState];
		}
		addressArray = [addressString componentsSeparatedByString:@"#@#"];
		int noOfElements = [addressArray count];
		return 25.0 * noOfElements;
	}
	return 35.0;
	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
	if (cell == nil)
	{
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyIdentifier"] autorelease];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.backgroundColor = [UIColor clearColor];
		UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0,0,290,50)];
		myView.tag = 0;
		[cell.contentView addSubview:myView];
		[myView release];
	}
	
	UIView* cellContentView = [cell.contentView viewWithTag:0];
	for(UIView* subView in cellContentView.subviews)
	{
		[subView removeFromSuperview];
	}
	
	cell.accessoryType = UITableViewCellAccessoryNone;
	
	if (indexPath.row ==0)
	{
		UIView* subView1=[[UIView alloc]initWithFrame:CGRectMake(0,0, 320,110)];
		subView1.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"book_background.png"]];
		
		UILabel* titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 10, 200,40)];
		titleLabel.numberOfLines = 2;
		titleLabel.font = [ UIFont boldSystemFontOfSize:15];
		titleLabel.adjustsFontSizeToFitWidth = YES;
		titleLabel.text = (bookForDetails.title != nil ? bookForDetails.title : @"N/A");
		titleLabel.backgroundColor = [UIColor clearColor];
		titleLabel.textAlignment = UITextAlignmentLeft;
		titleLabel.textColor = [UIColor darkTextColor];
		[subView1 addSubview:titleLabel];
		[titleLabel release];
        
        UILabel* authorLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 50, 200,20)];
		authorLabel.font = [ UIFont systemFontOfSize:14];
		authorLabel.adjustsFontSizeToFitWidth = YES;
		authorLabel.text = [NSString stringWithFormat:@"ISBN 10: %@",(bookForDetails.isbnId10 != nil ? bookForDetails.isbnId10 : @"N/A")];
		authorLabel.backgroundColor = [UIColor clearColor];
		authorLabel.textAlignment = UITextAlignmentLeft;
		authorLabel.textColor = [UIColor lightGrayColor];
		[subView1 addSubview:authorLabel];
		[authorLabel release];
        
        
        NSString* formattedDateString = @"";
        if (bookForDetails.publishedDate != nil)
        {
            NSDate* originalDate = [originalDateFormatter dateFromString:bookForDetails.publishedDate];
            formattedDateString = [newDateFormatter stringFromDate:originalDate];
        }
        if ([formattedDateString length] == 0) {
            formattedDateString = @"N/A";
        }
        UILabel* releaseDateLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 70, 200,20)];
		releaseDateLabel.font = [ UIFont systemFontOfSize:14];
		releaseDateLabel.adjustsFontSizeToFitWidth = YES;
		releaseDateLabel.text = [NSString stringWithFormat:@"ISBN 13: %@",(bookForDetails.isbnId13 != nil ? bookForDetails.isbnId13 : @"N/A")];
		releaseDateLabel.backgroundColor = [UIColor clearColor];
		releaseDateLabel.textAlignment = UITextAlignmentLeft;
		releaseDateLabel.textColor = [UIColor lightGrayColor];
		[subView1 addSubview:releaseDateLabel];
		[releaseDateLabel release];
        
        
		UIImageView* imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(35, 17, 60, 80)];
		imageView2.image = bookForDetails.image != nil ? bookForDetails.image : [UIImage imageNamed:@"LoadingCover.png"];
		[subView1 addSubview:imageView2];
		[imageView2 release];
		
		[cellContentView addSubview:subView1];
		[subView1 release];
	}
	else if (indexPath.row ==1) 
	{
		UIFont*  headerFontForPrices = [UIFont boldSystemFontOfSize:15];
		
		cellContentView.backgroundColor=[UIColor blackColor];
		cell.accessoryType = UITableViewCellAccessoryNone;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		UILabel*merchantLabel=  [[UILabel alloc]initWithFrame:CGRectMake(10, 5, 310,25)];
		merchantLabel.numberOfLines = 0;
		merchantLabel.font = headerFontForPrices;
		
		if ([booksOffer.conditionId isEqualToString:@"8"]) {
			merchantLabel.text = @"BookStore Details";
		}
		else {
			merchantLabel.text = @"Library Details";
		}
		
		merchantLabel.backgroundColor =[UIColor clearColor];
		merchantLabel.textAlignment = UITextAlignmentCenter;
		merchantLabel.textColor = [UIColor whiteColor];
		[cellContentView addSubview:merchantLabel];
		[merchantLabel release];
		
		
	}
	else {
		
		UIFont* headerFont =  [ UIFont boldSystemFontOfSize:13];
		
		
		UIColor* backgroundColor = [UIColor whiteColor];
		UIColor* singleRowTextColor =[UIColor darkTextColor];//[UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1] ;
		
		cellContentView.backgroundColor = backgroundColor;
		cell.backgroundColor = backgroundColor;
		
		if (indexPath.row == 2)
		{
			cell.accessoryType = UITableViewCellAccessoryNone;
			
			UILabel *textView = [[UILabel alloc] initWithFrame:CGRectMake(10,5,60,25)];
			[textView setFont:headerFont];
			textView.textColor=singleRowTextColor ;
			textView.backgroundColor = backgroundColor;
			textView.text =@"Name";
			[cellContentView addSubview:textView];
			[textView release];
			
			UIImageView* imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(74,0,1, 35)];
			imageView1.image = [UIImage imageNamed:@"sep.png"];
			[cellContentView addSubview:imageView1];
			[imageView1 release];
			
			UILabel* valueLabel =  [[UILabel alloc]initWithFrame:CGRectMake(75, 5, 235,25)];
			valueLabel.font =headerFont;
			valueLabel.text =  booksOffer.storeName;
			valueLabel.backgroundColor =[UIColor clearColor];
			valueLabel.textAlignment = UITextAlignmentRight;
			valueLabel.textColor = singleRowTextColor;
			[cellContentView addSubview:valueLabel];
			[valueLabel release];
			
		}
		if (indexPath.row == 3)
		{
			cell.accessoryType = UITableViewCellAccessoryNone;
			
			UILabel *textView = [[UILabel alloc] initWithFrame:CGRectMake(10,5,60,25)];
			[textView setFont:headerFont];
			textView.textColor=singleRowTextColor ;
			textView.backgroundColor = backgroundColor;
			textView.text =@"Address";
			[cellContentView addSubview:textView];
			[textView release];
			
			addressString=@"";
			if([booksOffer.storeAddress1 length]>0)
			{
				addressString=[addressString stringByAppendingString:booksOffer.storeAddress1];
				addressString=[addressString stringByAppendingString:@"#@#"];
			}
			if([booksOffer.storeAddress2 length]>0)
			{
				addressString=[addressString stringByAppendingString:booksOffer.storeAddress2];
				addressString=[addressString stringByAppendingString:@"#@#"];
			}
			if([booksOffer.storeCity length]>0)
			{
				addressString=[addressString stringByAppendingString:booksOffer.storeCity];
				addressString=[addressString stringByAppendingString:@"#@#"];
			}
			if([booksOffer.storeState length]>0)
			{
				addressString=[addressString stringByAppendingString:booksOffer.storeState];
			}
			addressArray = [addressString componentsSeparatedByString:@"#@#"];
			for (int addressElement = 0; addressElement < [addressArray count]; addressElement++) {
				
				UIImageView* imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(74,addressElement * 35,1, 35)];
				imageView1.image = [UIImage imageNamed:@"sep.png"];
				[cellContentView addSubview:imageView1];
				[imageView1 release];
				
				UILabel* valueLabel =  [[UILabel alloc]initWithFrame:CGRectMake(75, addressElement * 20 + 5, 235,25)];
				valueLabel.font =headerFont;
				valueLabel.text =  [addressArray objectAtIndex:addressElement];
				valueLabel.backgroundColor =[UIColor clearColor];
				valueLabel.textAlignment = UITextAlignmentRight;
				valueLabel.textColor = singleRowTextColor;
				[cellContentView addSubview:valueLabel];
				[valueLabel release];
			}
			
		}
		if (indexPath.row == 4)
		{
			cell.accessoryType = UITableViewCellAccessoryNone;
			
			if ([[booksOffer.storePhone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] >0)
			{
				cell.selectionStyle = UITableViewCellSelectionStyleBlue;
			}
			
			UILabel *textView = [[UILabel alloc] initWithFrame:CGRectMake(10,5,60,25)];
			[textView setFont:headerFont];
			textView.textColor=singleRowTextColor ;
			textView.backgroundColor = backgroundColor;
			textView.text =@"Phone";
			[cellContentView addSubview:textView];
			[textView release];
			
			UIImageView* imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(74,0,1, 35)];
			imageView1.image = [UIImage imageNamed:@"sep.png"];
			[cellContentView addSubview:imageView1];
			[imageView1 release];
			
			NSString* phoneNumber = [[booksOffer.storePhone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] >0 ? booksOffer.storePhone : @"N/A";
			UILabel* valueLabel =  [[UILabel alloc]initWithFrame:CGRectMake(75, 5, 235,25)];
			valueLabel.font =headerFont;
			valueLabel.text =  phoneNumber;
			valueLabel.backgroundColor =[UIColor clearColor];
			valueLabel.textAlignment = UITextAlignmentRight;
			valueLabel.textColor = [UIColor redColor];
			[cellContentView addSubview:valueLabel];
			[valueLabel release];
			
		}
		if (indexPath.row == 5)
		{
			cell.accessoryType = UITableViewCellAccessoryNone;
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
			
			UILabel *textView = [[UILabel alloc] initWithFrame:CGRectMake(10,5,60,25)];
			[textView setFont:headerFont];
			textView.textColor=singleRowTextColor ;
			textView.backgroundColor = backgroundColor;
			textView.text =@"Web";
			[cellContentView addSubview:textView];
			[textView release];
			
			UIImageView* imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(74,0,1, 35)];
			imageView1.image = [UIImage imageNamed:@"sep.png"];
			[cellContentView addSubview:imageView1];
			[imageView1 release];
			UILabel* valueLabel =  [[UILabel alloc]initWithFrame:CGRectMake(75, 5, 235,25)];
			valueLabel.font =headerFont;
			valueLabel.text =  @"Visit Website";
			valueLabel.backgroundColor =[UIColor clearColor];
			valueLabel.textAlignment = UITextAlignmentRight;
			valueLabel.textColor = [UIColor blueColor];
			[cellContentView addSubview:valueLabel];
			[valueLabel release];
			
		}
		
		
	}
	return cell;
}
/*
- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	BooksOffer* booksOffer = [offerObjList objectAtIndex:indexPath.row-2];
	[self showDetailsAction:booksOffer];
	
}
*/
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	if (indexPath.row ==0) {
		[self populateDetailsView];
	}
	else if (indexPath.row ==4) {
		if ([[booksOffer.storePhone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] >0)
		{
			[self callBookStore];
		}
	}
	else {
		if ([[booksOffer.link stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] >0)
		{
			[self viewBookStore];
		}
	}

	[aTableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)dealloc {
    [super dealloc];
}

-(void)viewBookStore
{
	MerchantViewController* merchantViewController = [[MerchantViewController alloc] init];
	[merchantViewController reloadMerchantViewWithOffer:booksOffer forBook:book];
	[self.navigationController pushViewController:merchantViewController animated:YES];
	[merchantViewController release];
}

-(void) displayActionSheet:(id) sender
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate]; 
	UIActionSheet *actionSheet = nil;

	[optionTitles removeAllObjects];
	
	if ([booksOffer.conditionId isEqualToString:@"8"] )
	{
		[optionTitles addObject: @"Call Bookstore"];
	}
	else {
		[optionTitles addObject: @"Call Library"];
	}

	[optionTitles addObject:@"Get Directions"]	;
	[optionTitles addObject:@"Email Friend"];
	[optionTitles addObject:@"Cancel"];
	
	actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil
									 otherButtonTitles: nil];
	
	for(NSString* option in optionTitles)
	{
		[actionSheet addButtonWithTitle:option];
	}
	
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	actionSheet.destructiveButtonIndex = actionSheet.numberOfButtons -1 ;	
	actionSheet.delegate = self;
	[actionSheet showInView:appDelegate.window];
	[actionSheet release];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if(buttonIndex >=0 && buttonIndex < [optionTitles count])
	{
		NSString* option = [optionTitles objectAtIndex:buttonIndex];
		
		if ([option isEqualToString:@"Call Bookstore"] || [option isEqualToString:@"Call Library"])
		{
			[self callBookStore];
		}	
		else if ([option isEqualToString:@"Get Directions"])
		{
			[self showDirections];
		}
		else if ([option isEqualToString:@"Email Friend"])
		{
			[self sendEmail:nil];
		}
		else 
		{
			NSLog(@"cancel");
		}
	}
}
-(void)callBookStore
{
	
	NSString* phoneNumber = [booksOffer.storePhone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
	if ([phoneNumber length] >0) {
		NSString* urlStr = [NSString stringWithFormat:@"tel://%s", [phoneNumber UTF8String]];
		printf("\n URL:%s",[urlStr UTF8String]);
		NSURL* url = [NSURL URLWithString:urlStr];
		[[UIApplication sharedApplication] openURL:url];
	}
}
-(void)showDirections
{

	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	
	
	if(appDelegate.currentLocation == nil)
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Campusbooks" message:@"Couldn't retrieve the directions as the curent location can't be determined. "
													   delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		return;
	}
	else {
		NSString* urStr = [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f&daddr=%s,%s", appDelegate.currentLocation.coordinate.latitude,appDelegate.currentLocation.coordinate.longitude, [booksOffer.storeLat UTF8String], [booksOffer.storeLong UTF8String]];
		NSURL* url = [NSURL URLWithString:urStr];
		[[UIApplication sharedApplication] openURL:url];
	}

	
}

-(void) sendEmailFromAppOnDevice
{
	NSString* emailSubject =  @"";
	
	if (booksOffer.shoppingMode == SHOPPING_MODE_BUY)
	{
		emailSubject = [NSString stringWithFormat:@" Best place to grab your copy of '%s'" , [book.title UTF8String]];
	}
	else
	{
		emailSubject = [NSString stringWithFormat:@" Best place to get the best price for your copy of '%s'" , [book.title UTF8String]];
	}
	NSString* emailContent  = [self getEmailString];
	NSString* emailURLString = [NSString stringWithFormat:@"mailto:?subject=%s&body=%s",[emailSubject UTF8String], [emailContent UTF8String]];
	emailURLString = [emailURLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	NSURL* url = [NSURL URLWithString:emailURLString];
	[[UIApplication sharedApplication] openURL:url];	
}
-(IBAction)sendEmail:(id)sender
{
	// This sample can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	
	//	[self showAlertForEmail];
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheet];
			//[self sendEmailFromAppOnDevice];
		}
		else
		{
			[self sendEmailFromAppOnDevice];
		}
	}
	else
	{
		[self sendEmailFromAppOnDevice];
	}
	
	
}

-(NSString*)getEmailString
{
	NSString* emailContent = @"";
	
	emailContent =  [NSString stringWithFormat:@"  <html><head><title></title></head><body> "];
	if (booksOffer.shoppingMode == SHOPPING_MODE_BUY)
	{
		emailContent = [emailContent stringByAppendingString:@"<p>Check out the best place to grab your copy of  '<i>'"];
	}
	else if(booksOffer.shoppingMode = SHOPPING_MODE_SELL)
	{
		emailContent = [emailContent stringByAppendingString:@"<p>Check out the best place to get the best price for your copy of  '<i>'"];
	}
	emailContent = [emailContent stringByAppendingString:book.title];
	emailContent = [emailContent stringByAppendingString:@"'</i>'.</p>"];
	
	
	NSString* storeType = @"";
	
	if ([booksOffer.conditionId isEqualToString:@"8"]) {
		storeType = @"Bookstore";
	}
	else {
		storeType =  @"Library";
	}
	
	addressString=@"";
	if([booksOffer.storeAddress1 length]>0)
	{
		addressString=[addressString stringByAppendingString:booksOffer.storeAddress1];
		addressString=[addressString stringByAppendingString:@"#@#"];
	}
	if([booksOffer.storeAddress2 length]>0)
	{
		addressString=[addressString stringByAppendingString:booksOffer.storeAddress2];
		addressString=[addressString stringByAppendingString:@"#@#"];
	}
	if([booksOffer.storeCity length]>0)
	{
		addressString=[addressString stringByAppendingString:booksOffer.storeCity];
		addressString=[addressString stringByAppendingString:@"#@#"];
	}
	if([booksOffer.storeState length]>0)
	{
		addressString=[addressString stringByAppendingString:booksOffer.storeState];
	}
	NSString* fullAddressString = [addressString stringByReplacingOccurrencesOfString:@"#@#" withString:@", "];
	
	NSString* addressUrlString = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%s",[fullAddressString UTF8String]];
	
	
	NSString* priceString = [booksOffer.totalPrice floatValue] > 0  ? [NSString stringWithFormat:@"$%.2f", [booksOffer.totalPrice floatValue]] : @" Free";

	emailContent = [emailContent stringByAppendingString:@"<table ><tr><th>"];
	emailContent = [emailContent stringByAppendingString:storeType];	
	emailContent = [emailContent stringByAppendingString:@"</th><th>Address</th><th>Price</th></tr>"];
	emailContent = [emailContent stringByAppendingString:@" <tr><td><a href='"];
	emailContent = [emailContent stringByAppendingString:booksOffer.link];
	emailContent = [emailContent stringByAppendingString:@"'>"];
	emailContent = [emailContent stringByAppendingString:booksOffer.storeName];
	emailContent = [emailContent stringByAppendingString:@"</a></td><td><a href='"];
	emailContent = [emailContent stringByAppendingString:addressUrlString];
	emailContent = [emailContent stringByAppendingString:@"'>"];
	emailContent = [emailContent stringByAppendingString:fullAddressString];
	emailContent = [emailContent stringByAppendingString:@"</a></td><td>"];
	emailContent = [emailContent stringByAppendingString:priceString];
	emailContent = [emailContent stringByAppendingString:@"</td></tr>"];
	emailContent = [emailContent stringByAppendingString:@"</table> "];
	
	emailContent = [emailContent stringByAppendingString:@"<p>This information is presented by '<a href='http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=326904377&mt=8'> CampusBooks</a>', , a price comparison tool on iPhone.<br></p>"];	
	emailContent = [emailContent stringByAppendingString:@"</body></html>"];
	
	emailContent = [emailContent stringByReplacingOccurrencesOfString:@"&" withString:@""];
	
	return emailContent;
}
#pragma mark -
#pragma mark Compose Mail

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
	NSString* emailSubject = @"";
	
	if (booksOffer.shoppingMode == SHOPPING_MODE_BUY)
	{
		emailSubject = [NSString stringWithFormat:@" Best place to grab your copy of '%s'" , [book.title UTF8String]];
	}
	else if (booksOffer.shoppingMode == SHOPPING_MODE_SELL)
	{
		emailSubject = [NSString stringWithFormat:@" Best place to get the best price for your copy of '%s'" , [book.title UTF8String]];
	}
	
	NSString* emailContent = [self getEmailString];
	
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	
	[picker setSubject:emailSubject];
	
	
	// Attach an image to the email
	NSString *path = [[NSBundle mainBundle] pathForResource:@"icon" ofType:@"png"];
	NSData *myData = [NSData dataWithContentsOfFile:path];
	[picker addAttachmentData:myData mimeType:@"image/png" fileName:@"iBookStore"];
	
	// Fill out the email body text
	NSString *emailBody = emailContent;
	[picker setMessageBody:emailBody isHTML:YES];
	
	[self presentModalViewController:picker animated:YES];
    [picker release];
}


// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			//	message.text = @"Result: saved";
			break;
		case MFMailComposeResultSent:
		{
			UIAlertView* alert  = [[UIAlertView alloc] initWithTitle:@"CampusBooks" message:@"Email is sent sucesfully" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] ;
			[alert show];
			[alert release];
			
			break;
		}
		case MFMailComposeResultFailed:
		{
			UIAlertView* alert  = [[UIAlertView alloc] initWithTitle:@"CampusBooks" message:@"Unable to send email. Please try again" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil] ;
			[alert show];
			[alert release];
			break;
		}
		default:
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}

-(NSString *)hostName
{
	// Don't include a scheme. 'http://' will break the reachability checking.
	// Change this value to test the reachability of a different host.
	return @"www.apple.com";
}
- (void)updateStatus
{
	
	self.internetConnectionStatus	= [[Reachability sharedReachability] internetConnectionStatus];
	//	[tableView reloadData];
}

-(void)populateDetailsView
{
	[self updateStatus];
	
	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!" message:@"No Network Available"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		
	}
	else
	{
		
		//	@try {
		
		[progressInd startAnimating];
		//	[detailsButton setImage:[UIImage imageNamed:@"transparent-clicked.png"] forState:UIControlStateNormal];
		
		[NSThread detachNewThreadSelector:@selector(doPopulateDetailsView:) toTarget:self withObject:nil];
		/*			
		 }
		 @catch (NSException * e) {
		 printf("\n caught an exception");
		 }
		 @finally {
		 
		 }
		 */		
	}
}

- (void)doPopulateDetailsView:(id)sender
{
	//	@try {
	
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	
	bookForDetails = [appDelegate getBookInfo:book.isbnId10];
	bookForDetails.image = book.image;
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	[progressInd stopAnimating];
	
	[pool release];	
	[self performSelectorOnMainThread:@selector(loadDetailsView) withObject:nil waitUntilDone:NO];
	
	[NSThread exit];
	/*	}
	 @catch (NSException * e) {
	 [e 
	 }
	 @finally {
	 }
	 */
}

-(void)loadDetailsView
{
	BookDetailsViewController* bookDetailsViewController = [[BookDetailsViewController alloc] initWithBook:bookForDetails fromSource:bookListViewController];
	
	[bookDetailsViewController refreshBookDetailsViewController:bookForDetails fromSource:bookListViewController];
	
	if ([self.navigationController topViewController] == bookDetailsViewController)
	{
		//printf("\n cancelling the push");
	}
	else
	{
		//printf("\n about to push to details view controller");
		[self.navigationController pushViewController:bookDetailsViewController animated:YES];
	}
	[bookDetailsViewController release];
	
}


@end