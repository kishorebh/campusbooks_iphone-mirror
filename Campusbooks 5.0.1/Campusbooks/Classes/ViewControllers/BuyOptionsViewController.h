//
//  BuyOptionsViewController.h
//  CampusBooks
//
//  Created by admin bluepal on 19/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Book.h"
#import "BookBuyViewController.h"
#import"BookCoverLoader.h"

@interface BuyOptionsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, BookCoverLoaderDelegate> {
	Book* currentBookInfo;
	UIViewController* bookListViewController;
	UIView* contentView ;
	UIActivityIndicatorView* progressInd;
	
	NSMutableArray* bookOffersList;
	
	UITableView* tableView;
	
	int currentDisplayCriterion;
	UIImageView* titleView;
	NSArray* authorNamesList;
	
	NSMutableArray* bestOffersList;
	Book* bookForDetails;
	BooksOffer* selectedBookOffer;

	NetworkStatus internetConnectionStatus;
	NSInteger buyOrSellMode; 
	NSMutableDictionary* offersByConditionDictionary;
    
    NSDateFormatter* originalDateFormatter;
	NSDateFormatter* newDateFormatter;

}
@property NetworkStatus internetConnectionStatus;
- (void)updateStatus;
-(NSString *)hostName;
-(NSMutableArray*)getBestPrices;
-(NSMutableArray*)getOffersFromConditionType:(NSString*)aConditionType;
-(id)initWithBook:(Book*)aBookInfo withMode:(NSInteger)aMode withOffers:(NSMutableArray*)aOffersList fromSource:(UIViewController*)aBookListViewController;
-(void)refreshBuyOptionsViewController:(Book*)aBook withMode:(NSInteger)aMode withOffers:(NSMutableArray*)aOffersList fromSource:(UIViewController*)aBookListViewController;
-(void)populateDetailsView;
-(void)loadDetailsView;
-(void)organizeOffersByCategory;
@end
