//
//  BookOffersViewController.m
//  CampusBooks
//
//  Created by Admin on 19/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "BookOfferViewController.h"
#import"CampusBooksAppDelegate.h"

@implementation BookOfferViewController
@synthesize internetConnectionStatus;

static int  SHOPPING_MODE_BUY = 0;
static int  SHOPPING_MODE_SELL = 1;

-(id)initWithOffer:(BooksOffer*)aOffer  forBook:(Book*)aBook fromSource:(UIViewController*)aSourceViewController
{
	if (self = [super init]) {
		
		offer = aOffer;
		bookDetailsViewController = aSourceViewController;
		currentBook = aBook;
		
		contentView = [[TransitionView alloc] initWithFrame:CGRectMake(0,0,320,390)];
		contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
		
		CGRect activityViewframe = CGRectMake(145.0,175.5, 35,35);
		progressInd = [[UIActivityIndicatorView alloc] initWithFrame:activityViewframe];
		progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
		progressInd.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
										UIViewAutoresizingFlexibleRightMargin |
										UIViewAutoresizingFlexibleTopMargin |
										UIViewAutoresizingFlexibleBottomMargin);
		[progressInd startAnimating];
		progressInd.hidden = NO;
		[progressInd stopAnimating];
		
		
        originalDateFormatter = [[NSDateFormatter alloc] init];
		[originalDateFormatter setDateFormat:@"yyyy-MM-dd"];
		
		newDateFormatter = [[NSDateFormatter alloc] init];
		[newDateFormatter setDateStyle:NSDateFormatterMediumStyle];
		[newDateFormatter setTimeStyle:NSDateFormatterNoStyle];

		tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 8, 320,360) style:UITableViewStylePlain];
		tableView.delegate = self;
		tableView.dataSource = self;
		tableView.backgroundColor = [UIColor clearColor];
		tableView.sectionHeaderHeight = 5.0;
		tableView.sectionFooterHeight = 0.0;
		tableView.separatorColor=[UIColor clearColor];
		
		self.title = @"Offer";
		
		[self.view addSubview:contentView];
		
		merchantViewController = [[MerchantViewController alloc] init];

		[self refreshBookOfferViewController:aOffer forBook:aBook fromSource:aSourceViewController];
	}
	return self;
	
}
-(void)refreshBookOfferViewController:(BooksOffer*)aOffer  forBook:(Book*)aBook fromSource:(UIViewController*)aSourceViewController
{
	for(UIView* subView in contentView.subviews)
	{
		[subView removeFromSuperview];
	}
	offer = aOffer;
	bookDetailsViewController = aSourceViewController;
	currentBook = aBook;
	
	//[contentView addSubview:titleView];
	
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]initWithTitle:@"Buy" style:UIBarButtonItemStyleBordered target:self action:@selector(purchaseBook)]autorelease];

//	if (offer.shoppingMode == SHOPPING_MODE_BUY)
//	{
////		UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
////		[button setBackgroundImage:[UIImage imageNamed:@"buy.png"] forState:UIControlStateNormal];
////		[button addTarget:self action:@selector(purchaseBook) forControlEvents:UIControlEventTouchUpInside];
////		UIBarButtonItem* actionControl = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
////		[button release];
//	}
//	else if(offer.shoppingMode == SHOPPING_MODE_SELL)
//	{
//		UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
//		[button setBackgroundImage:[UIImage imageNamed:@"sell.png"] forState:UIControlStateNormal];
//		[button addTarget:self action:@selector(purchaseBook) forControlEvents:UIControlEventTouchUpInside];
//		UIBarButtonItem* actionControl = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
//		self.navigationItem.rightBarButtonItem = actionControl;
//		[button release];
//	}			
	
	[contentView addSubview:progressInd];
	[contentView addSubview:tableView];
	[tableView reloadData];

}


-(void) purchaseBook
{
	[merchantViewController reloadMerchantViewWithOffer:offer forBook:currentBook];
	
	[self.navigationController pushViewController:merchantViewController animated:YES];
	
}


/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

#pragma mark Table Delegate and Data Source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tv {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)section {
    
    if (offer.shoppingMode == SHOPPING_MODE_BUY)
    {
        return 7;
    }
    return 6;
	
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
	if (cell == nil)
	{
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyIdentifier"] autorelease];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.backgroundColor = [UIColor clearColor];
		UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0,0,290,50)];
		myView.tag = 0;
		[cell.contentView addSubview:myView];
		[myView release];
	}
	
	UIView* cellContentView = [cell.contentView viewWithTag:0];
	for(UIView* subView in cellContentView.subviews)
	{
		[subView removeFromSuperview];
	}
	
	cell.accessoryType = UITableViewCellAccessoryNone;
	
	if (indexPath.row ==0)
	{
		UIView* subView1=[[UIView alloc]initWithFrame:CGRectMake(0,0, 320,110)];
		subView1.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"book_background.png"]];
		
		UILabel* titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 10, 200,40)];
		titleLabel.numberOfLines = 2;
		titleLabel.font = [ UIFont boldSystemFontOfSize:15];
		titleLabel.adjustsFontSizeToFitWidth = YES;
		titleLabel.text = (currentBook.title != nil ? currentBook.title : @"N/A");
		titleLabel.backgroundColor = [UIColor clearColor];
		titleLabel.textAlignment = UITextAlignmentLeft;
		titleLabel.textColor = [UIColor darkTextColor];
		[subView1 addSubview:titleLabel];
		[titleLabel release];
        
        UILabel* authorLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 50, 200,20)];
		authorLabel.font = [ UIFont systemFontOfSize:14];
		authorLabel.adjustsFontSizeToFitWidth = YES;
		authorLabel.text = [NSString stringWithFormat:@"ISBN 10: %@",(currentBook.isbnId10 != nil ? currentBook.isbnId10 : @"N/A")];
		authorLabel.backgroundColor = [UIColor clearColor];
		authorLabel.textAlignment = UITextAlignmentLeft;
		authorLabel.textColor = [UIColor lightGrayColor];
		[subView1 addSubview:authorLabel];
		[authorLabel release];
        
        
        NSString* formattedDateString = @"";
        if (currentBook.publishedDate != nil)
        {
            NSDate* originalDate = [originalDateFormatter dateFromString:currentBook.publishedDate];
            formattedDateString = [newDateFormatter stringFromDate:originalDate];
        }
        if ([formattedDateString length] == 0) {
            formattedDateString = @"N/A";
        }
        UILabel* releaseDateLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 70, 200,20)];
		releaseDateLabel.font = [ UIFont systemFontOfSize:14];
		releaseDateLabel.adjustsFontSizeToFitWidth = YES;
		releaseDateLabel.text = [NSString stringWithFormat:@"ISBN 13: %@",(currentBook.isbnId13 != nil ? currentBook.isbnId13 : @"N/A")];
		releaseDateLabel.backgroundColor = [UIColor clearColor];
		releaseDateLabel.textAlignment = UITextAlignmentLeft;
		releaseDateLabel.textColor = [UIColor lightGrayColor];
		[subView1 addSubview:releaseDateLabel];
		[releaseDateLabel release];
        
        
		UIImageView* imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(35, 17, 60, 80)];
		imageView2.image = currentBook.image != nil ? currentBook.image : [UIImage imageNamed:@"LoadingCover.png"];
		[subView1 addSubview:imageView2];
		[imageView2 release];
		
		[cellContentView addSubview:subView1];
		[subView1 release];
		
	}
	else if (indexPath.row ==1) 
	{
        UIView* subView1=[[UIView alloc]initWithFrame:CGRectMake(0,0, 320,28)];
        subView1.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"more_about_this_offer.png"]];
        
        [cellContentView addSubview:subView1];
        [subView1 release];
		
	}
	else {
			
		UIFont* labelFont =  [ UIFont boldSystemFontOfSize:15];
		UIFont* headerFont =  [ UIFont boldSystemFontOfSize:13];
		
		
		UIColor* backgroundColor = [UIColor whiteColor];
		UIColor* headerColor =[UIColor darkTextColor];//  [UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];
		UIColor* singleRowTextColor =[UIColor darkTextColor];//[UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1] ;
		
		cellContentView.backgroundColor = backgroundColor;
		cell.backgroundColor = backgroundColor;
		
		if (indexPath.row == 2)
		{
			UILabel*merchant =  [[UILabel alloc]initWithFrame:CGRectMake(10, 7.5, 275,20)];
			merchant.numberOfLines = 0;
			merchant.font = headerFont;
			merchant.text = @"Merchant";
			merchant.backgroundColor =backgroundColor;
			merchant.textAlignment = UITextAlignmentLeft;
			merchant.textColor = headerColor;//[UIColor colorWithRed:0.92578125 green:0.92578125 blue:0.92578125 alpha:1];;
			[merchant setFont:[UIFont boldSystemFontOfSize:15]];
			[cellContentView addSubview:merchant];
			[merchant release];
		
			UIImageView* merchantLabel =  [[UIImageView alloc]initWithFrame:CGRectMake(145, 20, 140,30)];
			merchantLabel.image = [UIImage imageNamed:[NSString stringWithFormat:@"%s.png",[offer.merchantId UTF8String]]];
			[cellContentView addSubview:merchantLabel];
			[merchantLabel release];
		}

		else if (indexPath.row == 3)
		{

			UILabel*isbnId10 =  [[UILabel alloc]initWithFrame:CGRectMake(10, 7.5, 100,20)];
			isbnId10.numberOfLines = 0;
			isbnId10.font = headerFont;
			isbnId10.text = @"Total Price";
			isbnId10.backgroundColor =backgroundColor;
			isbnId10.textAlignment = UITextAlignmentLeft;
			isbnId10.textColor = headerColor;
			[isbnId10 setFont:[UIFont boldSystemFontOfSize:15]];
			[cellContentView addSubview:isbnId10];
			[isbnId10 release];
			
			UILabel*totalPriceLabel =  [[UILabel alloc]initWithFrame:CGRectMake(205, 7.5, 80,20)];
			totalPriceLabel.numberOfLines = 0;
			totalPriceLabel.font = headerFont;
			totalPriceLabel.text = [NSString stringWithFormat:@"$%.2f", [offer.totalPrice floatValue]];
			totalPriceLabel.backgroundColor =backgroundColor;
			totalPriceLabel.textAlignment = UITextAlignmentRight;
			totalPriceLabel.textColor = singleRowTextColor;
			[totalPriceLabel setFont:[UIFont systemFontOfSize:15]];
			[cellContentView addSubview:totalPriceLabel];
			[totalPriceLabel release];
			
			if( offer.shoppingMode == SHOPPING_MODE_BUY)
			{
				UILabel* isbnId10Value =  [[UILabel alloc]initWithFrame:CGRectMake(10, 30, 275,20)];
				isbnId10Value.numberOfLines = 0;
				isbnId10Value.font = labelFont;
				isbnId10Value.text = [NSString stringWithFormat:@"$%.2f (Original) + $%.2f (Shipping)",[offer.price floatValue], [offer.shippingPrice floatValue]] ;
				isbnId10Value.backgroundColor =backgroundColor;
				isbnId10Value.textAlignment = UITextAlignmentRight;
				isbnId10Value.textColor = singleRowTextColor;
				[isbnId10Value setFont:[UIFont systemFontOfSize:15]];
				[cellContentView addSubview:isbnId10Value];
				[isbnId10Value release];
			}
		}

		 else if(indexPath.row ==4)
		{
			UILabel*publishedOnLabel =  [[UILabel alloc]initWithFrame:CGRectMake(10, 7.5, 100,20)];
			publishedOnLabel.font = headerFont;
			publishedOnLabel.text = @"Condition";
			publishedOnLabel.backgroundColor =backgroundColor;
			publishedOnLabel.textAlignment = UITextAlignmentLeft;
			publishedOnLabel.textColor = headerColor;
			[publishedOnLabel setFont:[UIFont boldSystemFontOfSize:15]];
			[cellContentView addSubview:publishedOnLabel];
			[publishedOnLabel release];
		
			UILabel* publishedOnValue =  [[UILabel alloc]initWithFrame:CGRectMake(110, 7.5, 175,20)];
			publishedOnValue.numberOfLines = 0;
			publishedOnValue.font = labelFont;
			publishedOnValue.text = [NSString stringWithFormat:@"%s",[offer.offerConditionText UTF8String]] ;
			publishedOnValue.backgroundColor =backgroundColor;
			publishedOnValue.textAlignment = UITextAlignmentRight;
			publishedOnValue.textColor = singleRowTextColor;
			[publishedOnValue setFont:[UIFont systemFontOfSize:15]];
			[cellContentView addSubview:publishedOnValue];
			[publishedOnValue release];
		}
        else if( indexPath.row == 5 && offer.shoppingMode == SHOPPING_MODE_BUY)
		{
			UILabel*editionLabel =  [[UILabel alloc]initWithFrame:CGRectMake(10, 7.5, 100,20)];
			editionLabel.numberOfLines = 0;
			editionLabel.font = headerFont;
			editionLabel.text = @"Coupons";
			editionLabel.backgroundColor =backgroundColor;
			editionLabel.textAlignment = UITextAlignmentLeft;
			editionLabel.textColor = headerColor;
			[editionLabel setFont:[UIFont boldSystemFontOfSize:15]];
			[cellContentView addSubview:editionLabel];
			[editionLabel release];
            
            
			CGFloat height =  [self getTextHeightForSystemFontOfSize:15 ofWidth:300 forText:offer.couponText ]  + 20 ;
			UILabel* editionValue =  [[UILabel alloc]initWithFrame:CGRectMake(10, 30, 265,height)];
			editionValue.numberOfLines = 0;
			editionValue.font = labelFont;
			editionValue.text = [NSString stringWithFormat:@"%s",[offer.couponText UTF8String]] ;
			editionValue.backgroundColor =backgroundColor;
			editionValue.textAlignment = UITextAlignmentLeft;
			editionValue.textColor = singleRowTextColor;
			[editionValue setFont:[UIFont systemFontOfSize:15]];
			[cellContentView addSubview:editionValue];
			[editionValue release];	
		}
		
		else 
		{
			UILabel*editionLabel =  [[UILabel alloc]initWithFrame:CGRectMake(10, 7.5, 100,20)];
			editionLabel.numberOfLines = 0;
			editionLabel.font = headerFont;
			editionLabel.text = @"Comments";
			editionLabel.backgroundColor =backgroundColor;
			editionLabel.textAlignment = UITextAlignmentLeft;
			editionLabel.textColor = headerColor;
			[editionLabel setFont:[UIFont boldSystemFontOfSize:15]];
			[cellContentView addSubview:editionLabel];
			[editionLabel release];
		
			CGFloat height =  [self getTextHeightForSystemFontOfSize:15 ofWidth:300 forText:offer.comments ]  + 20 ;
			UILabel* editionValue =  [[UILabel alloc]initWithFrame:CGRectMake(10, 30, 265,height)];
			editionValue.numberOfLines = 0;
			editionValue.font = labelFont;
			editionValue.text = [NSString stringWithFormat:@"%s",[offer.comments UTF8String]] ;
			editionValue.backgroundColor =backgroundColor;
			editionValue.textAlignment = UITextAlignmentLeft;
			editionValue.textColor = singleRowTextColor;
			[editionValue setFont:[UIFont systemFontOfSize:15]];
			[cellContentView addSubview:editionValue];
			[editionValue release];	
		}
	}
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if(indexPath.row == 0 )
	{
		return 110;
	}
	if(indexPath.row == 1 )
	{
		return 28;
	}
	else if(indexPath.row == 2 )
	{
		return 60;
	}
	else if (indexPath.row ==3 && offer.shoppingMode == SHOPPING_MODE_BUY)
	{
		return 60;
	}
	else if (indexPath.row == 5 )
	{
		return  [self getTextHeightForSystemFontOfSize:15.0 ofWidth:300 forText:offer.couponText ] + 55 ;
	}
	else if (indexPath.row == 6 )
	{
		return  [self getTextHeightForSystemFontOfSize:15.0 ofWidth:300 forText:offer.comments ] + 55 ;
	}
	else
	{
		return 35.0;
	}
	return 35.0;
}

- (void)dealloc {
		//	printf("\n In BookOffers Dealloc");
    [super dealloc];
}
-(NSString *)hostName
{
	// Don't include a scheme. 'http://' will break the reachability checking.
	// Change this value to test the reachability of a different host.
	return @"www.apple.com";
}
- (void)updateStatus
{
	
	self.internetConnectionStatus	= [[Reachability sharedReachability] internetConnectionStatus];
	//	[tableView reloadData];
}

- (CGFloat)getTextHeightForSystemFontOfSize:(CGFloat)size ofWidth:(CGFloat)maxWidth forText:(NSString*)aText {
    CGFloat maxHeight = 9999;
    CGSize maximumLabelSize = CGSizeMake(maxWidth,maxHeight);
	
    CGSize expectedLabelSize = [aText sizeWithFont:[UIFont systemFontOfSize:size] constrainedToSize:maximumLabelSize lineBreakMode:UILineBreakModeWordWrap]; 
	
    return expectedLabelSize.height;
}

-(void)loadMerchantSite
{
	
	//printf("\n This will load merchant site");
}
		   

@end
