    //
//  SchoolsListViewController.m
//  CampusBooks
//
//  Created by Admin on 17/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SchoolsListViewController.h"
#import "CampusBooksAppDelegate.h"

static NSArray* indexTitlesArray = nil;
static NSMutableArray* schoolsList = nil;
@implementation SchoolsListViewController
@synthesize object;

- (id)init {
	
	if (self = [super init]) {
		
		if ( indexTitlesArray == nil)
		{
			indexTitlesArray = [ [NSArray alloc] initWithObjects:@"A", @"B",@"C",@"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z",nil];
		}
		CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*) [ [UIApplication sharedApplication] delegate];

		if(schoolsList == nil)
		{
			schoolsList=[appDelegate getSchoolsList];
		}
		
		theTableView = nil;
		isfirstTime=YES;
		
		
		self.title = @"Schools List";
		
		UIView 	*myView = [[UIView alloc] initWithFrame:CGRectMake(0,-10,320,440)];
		subView1= [[UIView alloc] initWithFrame:CGRectMake(0,0,320,440)];
		
		
		UIImageView* imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(268,5,30,315)];
		[subView1 addSubview:imageView2];
		[imageView2 release];
		theTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,45,320,360) style:UITableViewStylePlain];	
		theTableView.delegate = self;
		theTableView.dataSource = self;	
		theTableView.rowHeight = 45;
//		theTableView.backgroundColor=[UIColor clearColor];
		theTableView.sectionFooterHeight = 5;
		theTableView.sectionHeaderHeight = 5;
		[subView1 addSubview:theTableView];	
		[myView addSubview:subView1];
		self.view =myView;
		//[myView release];
		
		
		
		
		mySearchBar=[[UISearchBar alloc]init];
		mySearchBar.tintColor = [UIColor blackColor];
		[mySearchBar setFrame:CGRectMake(0, 0,320,42)];
		mySearchBar.delegate=self;
		mySearchBar.showsCancelButton=NO;
		[subView1 addSubview:mySearchBar];
		
		self.title = @"Schools List";
		
		//[filteredListContent release];
		filteredListContent = [[NSMutableArray alloc] initWithCapacity: [schoolsList count]];
		[filteredListContent addObjectsFromArray: schoolsList];
		
		//[filteredListContent release];
		filteredListContent = [[NSMutableArray alloc] initWithCapacity: [schoolsList count]];
		[filteredListContent addObjectsFromArray: schoolsList];
		[self organizeAisleItemsIntoIndexes];
	}
	return self;
}
-(void)reloadView
{
	[theTableView reloadData];
}
-(void)setToPortrait:(BOOL)isPortrait
{
	if(isPortrait == YES)
	{
		
		[self shouldAutorotateToInterfaceOrientation:UIInterfaceOrientationPortrait];
	}
	else
	{
		[self shouldAutorotateToInterfaceOrientation:UIInterfaceOrientationLandscapeLeft];
	}
}
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
	
	
	return NO;
}
- (void)viewWillAppear:(BOOL)animated 
{
	
	//[self reloadView];
	[theTableView reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
	searchBar.showsCancelButton=NO;
	[searchBar resignFirstResponder];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	if([searchBar.text length]>0)
	{
		if([[searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] !=0)
		{
			searchBar.showsCancelButton=YES;
			[filteredListContent removeAllObjects];	// clear the filtered array first
			Schools* obj;
			for (obj in schoolsList)
			{
				NSRange range = [obj.schoolName rangeOfString:searchText options:NSCaseInsensitiveSearch];
				if(range.location != NSNotFound)
				{
					[filteredListContent addObject:obj];
				}	
			}
		}
	}
	else
	{
		filteredListContent = [[NSMutableArray alloc] initWithCapacity: [schoolsList count]];
		[filteredListContent addObjectsFromArray: schoolsList];
	}
		[self organizeAisleItemsIntoIndexes];
		
		
	
	
	[theTableView reloadData];
	
}

-(void) organizeAisleItemsIntoIndexes
{
	[masterCategoryListDictionary release];
	masterCategoryListDictionary = [[NSMutableDictionary alloc] init];
	Schools* obj;
	
	for ( obj in filteredListContent)
	{
	//	printf("\n Name:%s",[obj.schoolName UTF8String]);
		NSString *firstLetter = [[obj.schoolName substringToIndex:1] capitalizedString];
		
		NSMutableArray *indexArray = [masterCategoryListDictionary objectForKey:firstLetter];
		if (indexArray == nil) {
			indexArray = [[NSMutableArray alloc] init];
			[masterCategoryListDictionary setObject:indexArray forKey:firstLetter];
			[indexArray release];
		}
		[indexArray addObject:obj.schoolName];
	}
	
	masterCategoryListIndexArray = (NSMutableArray*)[ [masterCategoryListDictionary allKeys] sortedArrayUsingSelector:@selector(compare:)];
}
-(void)reloadTableView
{
	
	[theTableView reloadData];
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
	
	return indexTitlesArray;
}
- (NSInteger)tableView:(UITableView *)tableView  numberOfRowsInSection:(NSInteger)section {
	
	if([schoolsList count]!=0)
	{
		if([masterCategoryListIndexArray count]!=0)
		{
			NSString* sectionName = [masterCategoryListIndexArray objectAtIndex:section];
			NSArray* aisleItemsArray = [masterCategoryListDictionary valueForKey:sectionName];
			
			return [aisleItemsArray count];
		}
		
		return[filteredListContent count];
	}
	else
	{
		return 0;
	}
	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MasterViewIdentifier"] autorelease];
		cell.selectionStyle = UITableViewCellSelectionStyleGray;
		cell.autoresizesSubviews = YES;
		cell.backgroundColor=[UIColor colorWithRed:1 green:1 blue:1 alpha:0.5];
		UIView* elementView =  [ [UIView alloc] initWithFrame:CGRectMake(20,170,280,280)];
		elementView.tag = 0;
		[cell.contentView addSubview:elementView];
		[elementView release];
		
		
	}
	UIView* elementView  = [cell.contentView viewWithTag:0];
	for(UIView* subView in elementView.subviews)
	{
		[subView removeFromSuperview];
	}
	//cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
	
	masterCategoryListIndexArray = (NSMutableArray*)[ [masterCategoryListDictionary allKeys] sortedArrayUsingSelector:@selector(compare:)];
	NSString* sectionName = [masterCategoryListIndexArray objectAtIndex:indexPath.section];
	NSMutableArray* categoriesArray = [masterCategoryListDictionary valueForKey:sectionName];
	
	NSString* currencySelectedString= [categoriesArray objectAtIndex:indexPath.row];
	
	if([object.schoolName isEqualToString:currencySelectedString])
	{
		cell.accessoryType=UITableViewCellAccessoryCheckmark;
	}
	elementView.backgroundColor=[UIColor colorWithRed:1 green:1 blue:1 alpha:0.25];
	
	//printf("\n current string :%s",[currencySelectedString UTF8String]);
	UILabel *textView1 = [[UILabel alloc] initWithFrame:CGRectMake(10,2.5,290,40)];
	[textView1 setFont:[UIFont boldSystemFontOfSize:15]];
	textView1.backgroundColor=[UIColor clearColor];
	textView1.textColor =[UIColor blackColor];//[[[UIColor alloc]initWithRed:0.265625 green:0.46484375 blue:0.53125 alpha:1.0]autorelease];//[UIColor blackColor];//[[[UIColor alloc]initWithRed:0.1015625 green:0.1640625 blue:0.0703125 alpha:1.0]autorelease];//[UIColor darkTextColor];
	textView1.numberOfLines = 0;
	textView1.text =currencySelectedString;	
	[elementView addSubview:textView1];
	[textView1 release];
	
	
	return cell;
}	
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	if([masterCategoryListDictionary count]!=0)
	{
		return [masterCategoryListDictionary count];
	}
	else 
	{
		return 1;
	}
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
	
	masterCategoryListIndexArray = (NSMutableArray*)[ [masterCategoryListDictionary allKeys] sortedArrayUsingSelector:@selector(compare:)];
	
	int indexFromMainArray = [indexTitlesArray indexOfObject:title];
	int indexFromAisleItemArray = [masterCategoryListIndexArray indexOfObject:title];
	
	NSString* currentTitle = @" ";
	if (indexFromAisleItemArray >= 0 && indexFromAisleItemArray < [masterCategoryListIndexArray count])
	{
		currentTitle = [masterCategoryListIndexArray objectAtIndex:indexFromAisleItemArray];
		printf("\n Current TItle:%s", [currentTitle UTF8String]);
	}
	
	
	int currentIndex = indexFromMainArray + 1; //Start checking from 'Z';
	BOOL isPreviousElementFound = NO;
	while (isPreviousElementFound == NO && currentIndex > 0 && currentIndex <= [indexTitlesArray count] )
	{
		
		currentIndex = currentIndex -1 ;
		currentTitle = [indexTitlesArray objectAtIndex:currentIndex];
		
		indexFromAisleItemArray =  [masterCategoryListIndexArray indexOfObject:currentTitle];
		isPreviousElementFound =  (indexFromAisleItemArray >= 0 && indexFromAisleItemArray < [masterCategoryListIndexArray count]);
	}
	if (isPreviousElementFound == NO)
	{
		return 0;
	}
	
	return indexFromAisleItemArray;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	
	masterCategoryListIndexArray = (NSMutableArray*)[ [masterCategoryListDictionary allKeys] sortedArrayUsingSelector:@selector(compare:)];
	if ([masterCategoryListDictionary count] > 0)
	{
		masterCategoryListIndexArray = (NSMutableArray*)[ [masterCategoryListDictionary allKeys] sortedArrayUsingSelector:@selector(compare:)];
		NSString* sectionName = [masterCategoryListIndexArray objectAtIndex:section];
		return sectionName;
	}
	else
	{
		
		return @"No Records Found";
	}		
	return @"";
	
}
/*
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	if ([masterCategoryListDictionary count] > 0)
	{
		masterCategoryListIndexArray = (NSMutableArray*)[ [masterCategoryListDictionary allKeys] sortedArrayUsingSelector:@selector(compare:)];
		NSString* sectionName = [masterCategoryListIndexArray objectAtIndex:section];
		
		UIView* view = [[UIView alloc]initWithFrame:CGRectMake(20, 0, 280, 43)];
		view.backgroundColor =[UIColor grayColor];
		
		UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(15, -10,240 , 40)];
		[label setFont:[UIFont boldSystemFontOfSize:18]];
		label.numberOfLines = 0;
		label.backgroundColor = [UIColor clearColor];
		UIColor* textColor1 = [UIColor whiteColor];
		label.textColor =textColor1;
		
		label.text =sectionName ;
		
		[view addSubview:label];
		return view;
		
	}
	else
	{
		UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
		view.backgroundColor =[UIColor grayColor];//[[[UIColor alloc]initWithRed:0.12890625 green:0.625 blue:0.8359375 alpha:1.0]autorelease];
		
		UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(10, 0,240 , 40)];
		[label setFont:[UIFont boldSystemFontOfSize:18]];
		label.numberOfLines = 0;
		label.backgroundColor = [UIColor clearColor];
		UIColor* textColor1 =[UIColor whiteColor];
		label.textColor =textColor1;
		label.text=@"No Records Found";
		[view addSubview:label];
		return view;
		
	}		
	
}
*/
- (void )tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath 
{
	[theTableView deselectRowAtIndexPath:newIndexPath animated:YES];
	NSString* selectedString;
	if([filteredListContent count]!=0)
	{
		masterCategoryListIndexArray = (NSMutableArray*)[ [masterCategoryListDictionary allKeys] sortedArrayUsingSelector:@selector(compare:)];
		NSString* sectionName = [masterCategoryListIndexArray objectAtIndex:newIndexPath.section];
		NSMutableArray* categoriesArray = [masterCategoryListDictionary valueForKey:sectionName];
		
		selectedString = [categoriesArray objectAtIndex:newIndexPath.row];
	}
	else
	{
		selectedString = [schoolsList objectAtIndex:newIndexPath.row];
		
	}
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	LocalOptions* localObj=[appDelegate getLocalOptions];
	localObj.primaryKey=object.primaryKey;
	localObj.schoolName=selectedString;
	[appDelegate updateLocalOptions:localObj];
	//[localObj release];	
	
	[self.navigationController popViewControllerAnimated:YES];
	
	[tableView deselectRowAtIndexPath:newIndexPath animated:YES];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)aSearchBar
{
	aSearchBar.text=@"";
	aSearchBar.showsCancelButton = NO;
    [aSearchBar resignFirstResponder];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return ([masterCategoryListDictionary count] > 0  ? 20 : 40);
}
- (void)dealloc {
    [super dealloc];
}
@end



