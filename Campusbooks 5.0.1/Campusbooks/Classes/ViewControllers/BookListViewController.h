//
//  BookListViewController.h
//  CampusBooks
//
//  Created by Admin on 19/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FlowCoverView.h"
#import"Reachability.h"
#import"TransitionView.h"
#import"Book.h"
#import"BookCoverLoader.h"
#import"BookInfo.h"
#import"BookDetailsViewController.h"
#import"BookBuyViewController.h"
#import"BooKSellViewController.h"
@interface BookListViewController : UIViewController <FlowCoverViewDelegate, UISearchBarDelegate, BookCoverLoaderDelegate, UIActionSheetDelegate>{
	
	UIViewController* sourceViewController;
	UILabel* titleLabel;
	UILabel* authorLabel;
	UILabel* isbnLabel;
	UILabel* noItemsLabel;

	UIImageView* titleView;
	UILabel* byLabel;
	UIActivityIndicatorView* progressInd;
	NetworkStatus internetConnectionStatus;

	BOOL displayResults;

	NSMutableArray* booksList;
	UIView* coversView;
	FlowCoverView* flowCoverView;
	Book* currentBook;
	
	NSInteger currentPage;
	NSInteger currentCategory;
	
	
	BookDetailsViewController* bookDetailsViewController;
	BookBuyViewController* bookBuyViewController;
	BookSellViewController* bookSellViewController;
	
	UIButton* detailsButton;
	UIButton* moreResultsButton;
	
	NSMutableArray* actionSheetItems;
	int selectedImageIndex;
    BOOL isDisplayingWishList;

	
}
@property(nonatomic,retain) NSMutableArray* booksList;;
@property(nonatomic,retain) UIViewController* sourceViewController;
@property(nonatomic)NSInteger currentCategory;
@property NetworkStatus internetConnectionStatus;

- (void)updateStatus;
-(NSString *)hostName;
- (id)initWithBooksList:(NSMutableArray*) aBooksList fromSource:(UIViewController*)aSourceViewController;
-(void)displayTitles;
-(void)refreshBookListViewController:(NSMutableArray*)aBooksList fromSource:(UIViewController*)aSourceViewController;
- (CGFloat)getTextHeightForSystemFontOfSize:(CGFloat)size ofWidth:(CGFloat)maxWidth forText:(NSString*)aText ;

-(void)reloadToolbars;

-(void)removeFromWishList;
-(void)addToWishlistAction;
-(void)sellItAction;
-(void)buyItAction;

-(void) displayActionSheet:(id) sender;
@end