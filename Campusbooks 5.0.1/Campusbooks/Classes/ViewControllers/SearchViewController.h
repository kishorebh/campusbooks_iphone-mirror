//
//  SearchViewController.h
//  CampusBooks
//
//  Created by Admin on 13/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"BookListViewController.h"
@interface SearchViewController : UIViewController <UITextFieldDelegate>
{
	UITableView* tableView;
	UIActivityIndicatorView* progressInd;
	
	UIImageView* titleView;
	UIImageView* txtFldBookBG;
	UIImageView* txtFldAuthorBG;
	UIImageView* txtFldKeywordBG;
	UIImageView* txtFldISBNBG;
	UIImageView* imageView1;
	UIImageView* imageView2;
	
	UITextField* txtFldISBN;
	UITextField* txtFldAuthor; 
	UITextField* txtFldBook;
	UITextField* txtFldKeyword;
	
	UIButton* backButton;
	UIButton* searchButton ;
	UIButton* redLaserScanButton;
	TransitionView *contentView;
	BOOL isSearchInProgress;
	NetworkStatus internetConnectionStatus;
	UILabel* titleLabelForView;
	NSString* strKeyword;
	NSString* strIsbn;
	NSString* strTitle;
	NSString* strAuthor;
	BOOL isMoveUp;
	
	BookListViewController* bookListViewController;

}
@property NetworkStatus internetConnectionStatus;
@property(nonatomic, retain) NSString* strTitle;
@property(nonatomic, retain) NSString* strAuthor;
@property(nonatomic, retain) NSString* strKeyword;
@property(nonatomic, retain) NSString* strIsbn;
- (void)updateStatus;
-(NSString *)hostName;
- (void)performSearch;
-(void)refreshSearchView;
-(void)setViewMovedUp:(BOOL)movedUp;

@end
