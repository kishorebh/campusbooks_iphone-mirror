//
//  AwardsListViewController.m
//  CampusBooks
//
//  Created by Admin on 13/06/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "BookBagViewController.h"
#import "CampusBooksAppDelegate.h"

@implementation BookBagViewController
@synthesize selectedType;

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)init {
    if (self = [super init]) {
        // Custom initialization
		UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,320, 390)];
		contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
		[self.view addSubview: contentView];
		[contentView release];
		bookListViewController = [[BookListViewController alloc] initWithBooksList:nil fromSource:self];
								  
		
/*		titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home-title.png"]];
		[contentView addSubview:titleView];
		[titleView release];
*/		
		theTableView = [[UITableView alloc]initWithFrame:CGRectMake(5, 65, 310,250) style:UITableViewStyleGrouped];
		theTableView.delegate = self;
		theTableView.dataSource = self;
		theTableView.backgroundColor = [UIColor clearColor];
		[contentView addSubview:theTableView];
		[theTableView release];
		
		titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, 320,30)];
		titleLabel.text =@"My Bookbag";;
		titleLabel.textAlignment = UITextAlignmentCenter;
		titleLabel.textColor = [UIColor darkTextColor];//[UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];
		titleLabel.font = [UIFont boldSystemFontOfSize:25];
		titleLabel.backgroundColor = [UIColor clearColor];
		[contentView addSubview:titleLabel];
		
/*		CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [[UIApplication sharedApplication] delegate];
		awardsList = [appDelegate getAwardsList];
		[theTableView reloadData];
*/		
		UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 48, 335, 7)];
		imageView.image = [UIImage imageNamed:@"separation line.png"];
		[contentView addSubview:imageView];
		[imageView release];
		
		UIImageView* imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 325, 335, 7)];
		imageView1.image = [UIImage imageNamed:@"separation line.png"];
		[contentView addSubview:imageView1];
		[imageView1 release];
		
		self.title = @"My Bookbag";
/*		UIButton* backButton = [[UIButton alloc] initWithFrame:CGRectMake(0,375, 79,34)];
		[backButton setImage:[UIImage imageNamed:@"all-back.png"] forState:UIControlStateNormal];
		[backButton addTarget:appDelegate action:@selector(closeBookBagViewController) forControlEvents:UIControlEventTouchUpInside];
		[contentView addSubview:backButton];
		[backButton release];
 */
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
	self.title = @"My Bookbag";
}
	

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
			//printf("\n In Bookbagview Dealloc");
    [super dealloc];
}


- (void )tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath 
{

	
	
	CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	
	NSMutableArray* booksList = nil;
	if(newIndexPath.row ==0)
	{
		booksList = [appDelegate getMyWishList:0];
		selectedType = 1;
	}
	else
	{
		booksList = [appDelegate getMyCollection:0];
		selectedType = 2;
	}
	if ([booksList count] !=0)
	{
		//	[wishList sortUsingSelector:@selector(compareByFavoriteDate:)];
	//	CampusBooksAppDelegate* appDelegate = [[UIApplication sharedApplication]delegate];
	//	[appDelegate displayBookListViewController:booksList fromSource:self];
		
		//BookListViewController* bookListViewController =  [appDelegate getBookListViewController:booksList fromSource:self];
		[bookListViewController refreshBookListViewController:booksList fromSource:self];
		
		if ([self.navigationController topViewController] == bookListViewController)
		{
			//printf("\n cancelling the push");
		}
		else
		{
			[self.navigationController pushViewController:bookListViewController animated:YES];
		}
	}
	else
	{
		NSString* alertContent = @"There are no books in your wishlist.";
		
		if(newIndexPath.row ==0)
		{
			alertContent = @"There are no books in your wishlist.";
		}
		else
		{
			alertContent = @"There are no books in your collection.";
		}
		UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:alertContent delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
		[alertView show];
	}
	
	[tableView deselectRowAtIndexPath:newIndexPath animated:YES];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MasterViewIdentifier"] autorelease];
		cell.selectionStyle = UITableViewCellSelectionStyleGray;
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		UIView* elementView =  [ [UIView alloc] initWithFrame:CGRectMake(5,5,312,480)];
		elementView.tag = 0;
		[cell.contentView addSubview:elementView];
		[elementView release];
	}
	cell.backgroundColor=[UIColor colorWithRed:0.92578125 green:0.92578125 blue:0.92578125 alpha:1];
	UIView* elementView  = [cell.contentView viewWithTag:0];
	for(UIView* subView in elementView.subviews)
	{
		[subView removeFromSuperview];
	}
	
	NSString* content = (indexPath.row ==0 ?  @"My Wishlist ": @"My Collection");
	
	CGRect labelFrame = CGRectMake(5,10,300,25);
	UILabel *textView = [[UILabel alloc] initWithFrame:labelFrame];
	textView.backgroundColor = [UIColor clearColor];
	textView.font = [UIFont boldSystemFontOfSize:15];
	textView.textColor=[UIColor darkTextColor];//[UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1] ;
	textView.text = content;
	[elementView addSubview:textView];
	[textView release];
	
	return cell;
	
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView  numberOfRowsInSection:(NSInteger)section {
	
	return 2;	
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 45;
}

@end
