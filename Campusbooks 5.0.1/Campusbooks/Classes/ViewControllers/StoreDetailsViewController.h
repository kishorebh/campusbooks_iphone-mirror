//
//  StoreDetailsViewController.h
//  CampusBooks
//
//  Created by Admin on 19/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"BooksOffer.h"
#import"Book.h"
#import <MessageUI/MFMailComposeViewController.h>
#import"BookListViewController.h"
#import"Reachability.h"

@interface StoreDetailsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource, UIActionSheetDelegate,MFMailComposeViewControllerDelegate> {
	UITableView *theTableView;
	BooksOffer* booksOffer;
	UIView* contentView;
	NSString* addressString;
	NSArray* addressArray;
	Book* book;
	NSMutableArray* optionTitles;
	BookListViewController* bookListViewController;	
	NetworkStatus internetConnectionStatus;
	UIActivityIndicatorView* progressInd;
	Book* bookForDetails;
	
    NSDateFormatter* originalDateFormatter;
	NSDateFormatter* newDateFormatter;

}
@property(nonatomic,retain)BooksOffer* booksOffer;
@property NetworkStatus internetConnectionStatus;

-(IBAction)sendEmail:(id)sender;
-(void)callBookStore;
-(void)showDirections;
-(NSString*)getEmailString;
-(void)displayComposerSheet ;
-(void)viewBookStore;
- (id)initWithOffer:(BooksOffer*)aBookOffer forBook:(Book*)aBook fromBookListController:(BookListViewController*)aBookListViewController;
-(void)populateDetailsView;
-(void)loadDetailsView;
@end
