//
//  BookDetailsViewController.m
//  CampusBooks
//
//  Created by Admin on 19/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "BookBuyViewController.h"
#import"CampusBooksAppDelegate.h"
#import"BookOfferViewController.h"
#import"BookListViewController.h"
@implementation BookBuyViewController
@synthesize internetConnectionStatus;

static int DISPLAY_BEST_OFFERS =0;
static int DISPLAY_ALL_OFFERS= 1;

-(id)initWithBook:(Book*)aBookInfo withOffers:(NSMutableArray*)aOffersList fromSource:(UIViewController*)aBookListViewController
{
	if (self = [super init]) {

		currentBookInfo = aBookInfo;
		bookListViewController = aBookListViewController;
		bookOffersList = aOffersList;
		currentDisplayCriterion = DISPLAY_BEST_OFFERS;
		
		contentView = [[TransitionView alloc] initWithFrame:CGRectMake(0,0,320,390)];
		contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];


		CGRect activityViewframe = CGRectMake(145.0,175.5, 35,35);
		progressInd = [[UIActivityIndicatorView alloc] initWithFrame:activityViewframe];
		progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
		progressInd.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
										UIViewAutoresizingFlexibleRightMargin |
										UIViewAutoresizingFlexibleTopMargin |
										UIViewAutoresizingFlexibleBottomMargin);
		[contentView addSubview:progressInd];
		[progressInd startAnimating];
		progressInd.hidden = NO;
		[progressInd stopAnimating];
		
		originalDateFormatter = [[NSDateFormatter alloc] init];
		[originalDateFormatter setDateFormat:@"yyyy-MM-dd"];
		
		newDateFormatter = [[NSDateFormatter alloc] init];
		[newDateFormatter setDateStyle:NSDateFormatterMediumStyle];
		[newDateFormatter setTimeStyle:NSDateFormatterNoStyle];
		
		tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320,360) style:UITableViewStylePlain];
		tableView.delegate = self;
		tableView.dataSource = self;
		tableView.rowHeight = 35;
		tableView.sectionHeaderHeight = 5;
		tableView.sectionFooterHeight = 0;	
		tableView.separatorColor=[UIColor clearColor];
		tableView.backgroundColor = [UIColor clearColor];
		[contentView addSubview:tableView];
		[tableView reloadData];
		[tableView release];
		
		bestOffersList = [[NSMutableArray alloc] init];
		
//        UIImageView  *titlesubView= [[UIImageView alloc] initWithFrame:CGRectMake(0,20,300,45)];
//		titlesubView.backgroundColor=[UIColor clearColor];
//		segmentControl = [self createSegmentControl];
//		[titlesubView addSubview:segmentControl];
//		self.navigationItem.titleView = titlesubView;
        
        self.title = @"Compare Prices";

		self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"More" style:UIBarButtonItemStyleBordered target:self action:@selector(displayActionSheet:)]autorelease];

		
		emailAlertView = [[UIAlertView alloc] initWithTitle:@"CampusBooks" message:@"Emailing will terminate the application. \n Do you want to continue?"
												   delegate:self cancelButtonTitle:@"No" otherButtonTitles: @"Yes",nil];
		emailAlertView.delegate =self;
		
		offerViewController = [[BookOfferViewController alloc] initWithOffer:nil forBook:nil fromSource:self];
		
	}
	return self;
}


-(void)refreshBookBuyViewController:(Book*)aBook withOffers:(NSMutableArray*)aOffersList fromSource:(UIViewController*)aBookListViewController
{
	for (UIView* subView in contentView.subviews)
	{
		[subView removeFromSuperview];
	}
	currentBookInfo = aBook;
	bookListViewController = aBookListViewController;
	bookOffersList = aOffersList;
	currentDisplayCriterion = DISPLAY_BEST_OFFERS;
	segmentControl.selectedSegmentIndex =0;
	[bookOffersList sortUsingSelector:@selector(compareByConditionAndTotalPrice:)];
	[self getBestPrices:bookOffersList];
	[bestOffersList sortUsingSelector:@selector(compareByTotalPrice:)];
	[bookOffersList sortUsingSelector:@selector(compareByTotalPrice:)];
	[tableView reloadData];

//	printf("\n Publisher:%s",[currentBookInfo.publisher UTF8String]);
	[contentView addSubview:tableView];
	[self.view addSubview:contentView];
}


-(NSString *)hostName
{
	// Don't include a scheme. 'http://' will break the reachability checking.
	// Change this value to test the reachability of a different host.
	return @"www.apple.com";
}
- (void)updateStatus
{
	
	self.internetConnectionStatus	= [[Reachability sharedReachability] internetConnectionStatus];
	//	[tableView reloadData];
}
-(void)dealloc
{
		//	printf("\n In BookBuy Dealloc");
	[originalDateFormatter release];
	[newDateFormatter release];
	[super dealloc];
}

#pragma mark Table Delegate and Data Source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tv {
	
	return 1;
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)section {
	 if (currentDisplayCriterion == DISPLAY_BEST_OFFERS)
	{
		return [bestOffersList count]+ 3;
	}
	else if(currentDisplayCriterion ==DISPLAY_ALL_OFFERS)
	{
		return [bookOffersList count] + 3;
	}
	return 3;
}

- (UITableViewCell *)tableView:(UITableView *)atableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	UITableViewCell *cell = (UITableViewCell *)[atableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MasterViewIdentifier"] autorelease];
		cell.backgroundColor=[UIColor clearColor];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		UIView* elementView =  [[UIView alloc] initWithFrame:CGRectMake(0,0,320,420)];
		elementView.tag = 0;
		elementView.backgroundColor = [UIColor clearColor];
		elementView.autoresizesSubviews = YES;
		elementView.autoresizingMask =  (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
		
		[cell.contentView addSubview:elementView];
		[elementView release];
	}
	UIView* elementView  = [cell.contentView viewWithTag:0];
	for(UIView* subView in elementView.subviews)
	{
		[subView removeFromSuperview];
	}
    elementView.backgroundColor = [UIColor clearColor];

	if (indexPath.row ==0)
	{
		UIView* subView1=[[UIView alloc]initWithFrame:CGRectMake(0,0, 320,110)];
		subView1.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"book_background.png"]];
		
		UILabel* titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 10, 200,40)];
		titleLabel.numberOfLines = 2;
		titleLabel.font = [ UIFont boldSystemFontOfSize:15];
		titleLabel.adjustsFontSizeToFitWidth = YES;
		titleLabel.text = (currentBookInfo.title != nil ? currentBookInfo.title : @"N/A");
		titleLabel.backgroundColor = [UIColor clearColor];
		titleLabel.textAlignment = UITextAlignmentLeft;
		titleLabel.textColor = [UIColor darkTextColor];
		[subView1 addSubview:titleLabel];
		[titleLabel release];
        
        UILabel* authorLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 50, 200,20)];
		authorLabel.font = [ UIFont systemFontOfSize:14];
		authorLabel.adjustsFontSizeToFitWidth = YES;
		authorLabel.text = [NSString stringWithFormat:@"ISBN 10: %@",(currentBookInfo.isbnId10 != nil ? currentBookInfo.isbnId10 : @"N/A")];
		authorLabel.backgroundColor = [UIColor clearColor];
		authorLabel.textAlignment = UITextAlignmentLeft;
		authorLabel.textColor = [UIColor lightGrayColor];
		[subView1 addSubview:authorLabel];
		[authorLabel release];
        
        
        NSString* formattedDateString = @"";
        if (currentBookInfo.publishedDate != nil)
        {
            NSDate* originalDate = [originalDateFormatter dateFromString:currentBookInfo.publishedDate];
            formattedDateString = [newDateFormatter stringFromDate:originalDate];
        }
        if ([formattedDateString length] == 0) {
            formattedDateString = @"N/A";
        }
        UILabel* releaseDateLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 70, 200,20)];
		releaseDateLabel.font = [ UIFont systemFontOfSize:14];
		releaseDateLabel.adjustsFontSizeToFitWidth = YES;
		releaseDateLabel.text = [NSString stringWithFormat:@"ISBN 13: %@",(currentBookInfo.isbnId13 != nil ? currentBookInfo.isbnId13 : @"N/A")];
		releaseDateLabel.backgroundColor = [UIColor clearColor];
		releaseDateLabel.textAlignment = UITextAlignmentLeft;
		releaseDateLabel.textColor = [UIColor lightGrayColor];
		[subView1 addSubview:releaseDateLabel];
		[releaseDateLabel release];
        
        
		UIImageView* imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(35, 17, 60, 80)];
		imageView2.image = currentBookInfo.image != nil ? currentBookInfo.image : [UIImage imageNamed:@"LoadingCover.png"];
		[subView1 addSubview:imageView2];
		[imageView2 release];
		
		[elementView addSubview:subView1];
		[subView1 release];		
	}
    else if (indexPath.row == 1)
	{
		UIView* subView1=[[UIView alloc]initWithFrame:CGRectMake(0,0, 320,42)];
		subView1.backgroundColor=[UIColor clearColor];
		[elementView addSubview:subView1];
		[subView1 release];
        
        MyCustomButton* bestPricesButton =  [ [MyCustomButton alloc] initWithFrame:CGRectMake(0, 0, 320,42)];	
        [bestPricesButton addTarget:self action:@selector(segmentControlAction:) forControlEvents:UIControlEventTouchUpInside];
        
        if (currentDisplayCriterion == DISPLAY_BEST_OFFERS) {
            bestPricesButton.tag = DISPLAY_BEST_OFFERS;
            [bestPricesButton setImage:[UIImage imageNamed:@"best_prices_selected.png"] forState:UIControlStateNormal];
            [bestPricesButton setImage:[UIImage imageNamed:@"all_prices_selected.png"] forState:UIControlStateHighlighted];
        }
        else {
            bestPricesButton.tag = DISPLAY_ALL_OFFERS;
            [bestPricesButton setImage:[UIImage imageNamed:@"all_prices_selected.png"] forState:UIControlStateNormal];
            [bestPricesButton setImage:[UIImage imageNamed:@"best_prices_selected.png"] forState:UIControlStateHighlighted];
        }
        [subView1 addSubview:bestPricesButton];
        [bestPricesButton release];
        
	}
    else if (indexPath.row == 2)
    {
        UIView* subView1=[[UIView alloc]initWithFrame:CGRectMake(0,0, 320,38)];
        subView1.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"book_price_list_header.png"]];
        
        [elementView addSubview:subView1];
        [subView1 release];
        
    }
	else {
		
		UIFont*  labelFontForPrices =  [ UIFont boldSystemFontOfSize:13];

		UIColor* backgroundColor = [UIColor clearColor];
		//UIColor* headerColor =  [UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];
	//	UIColor* singleRowTextColor =[UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1] ;

		cell.backgroundColor = backgroundColor;
		elementView.backgroundColor = backgroundColor;

		NSString* authorName = currentBookInfo.author;
		authorName = [authorName stringByReplacingOccurrencesOfString:@"," withString:@"-"];
		authorNamesList = [authorName componentsSeparatedByString:@"-"];
		
		cell.accessoryType = UITableViewCellAccessoryNone;

        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        
        elementView.frame = CGRectMake(0, 0, 320, 38);
        
        UIView* subView1=[[UIView alloc]initWithFrame:CGRectMake(0,0, 320,elementView.frame.size.height)];
        NSLog(@"Subview1 height;%f", subView1.frame.size.height);
        subView1.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"buy_or_sell_row_bg_with_lines.png"]];
        
        [elementView addSubview:subView1];
        [subView1 release];

        BooksOffer* currOffer = nil;
        
		if (currentDisplayCriterion == DISPLAY_BEST_OFFERS )
		{

            currOffer = [bestOffersList objectAtIndex:indexPath.row  - 3];	
        }
        else {
            currOffer = [bookOffersList objectAtIndex:indexPath.row  - 3];	
        }
        
        NSString* conditionName = currOffer.conditionName;
        
        UIImage* merchantLogo = [UIImage imageNamed:[NSString stringWithFormat:@"%s.png",[currOffer.merchantId UTF8String]]];
        
        if (merchantLogo != nil )
        {
            UIImageView* merchantLabel =  [[UIImageView alloc]initWithFrame:CGRectMake(3.5, 6.5, 110,25)];
            merchantLabel.image = merchantLogo;
            [subView1 addSubview:merchantLabel];
            [merchantLabel release];
        }
        else
        {
            UILabel* merchantLabel =  [[UILabel alloc]initWithFrame:CGRectMake(3.5, 6.5, 110,25)];
            merchantLabel.font = [UIFont boldSystemFontOfSize:11];
            merchantLabel.text =  currOffer.merchantName ;
            merchantLabel.backgroundColor =backgroundColor;
            merchantLabel.textAlignment = UITextAlignmentLeft;
            merchantLabel.textColor = [UIColor darkTextColor];
            [subView1 addSubview:merchantLabel];
            [merchantLabel release];
        }
        
        UILabel* conditionLabel =  [[UILabel alloc]initWithFrame:CGRectMake(118, 6.5, 87,25)];
        conditionLabel.font = [UIFont systemFontOfSize:11];
        conditionLabel.text =  conditionName ;
        conditionLabel.backgroundColor =backgroundColor;
        conditionLabel.textAlignment = UITextAlignmentCenter;
        conditionLabel.textColor = [UIColor darkGrayColor];
        [subView1 addSubview:conditionLabel];
        [conditionLabel release];
        
        UILabel* totalPriceLabel =  [[UILabel alloc]initWithFrame:CGRectMake(210, 5.5, 55,22)];
        totalPriceLabel.font = labelFontForPrices;
        totalPriceLabel.text =  [NSString stringWithFormat:@"$%.2f",[currOffer.totalPrice floatValue] ] ;;
        totalPriceLabel.backgroundColor =backgroundColor;
        totalPriceLabel.textAlignment = UITextAlignmentLeft;
        totalPriceLabel.textColor =[UIColor darkTextColor];
        [subView1 addSubview:totalPriceLabel];
        [totalPriceLabel release];
        
        MyCustomButton* customButton =  [ [MyCustomButton alloc] initWithIndexPath:indexPath];
        [customButton setFrame:CGRectMake(266, 5.5, 52,27)];	
        [customButton addTarget:self action:@selector(buyHereAction:) forControlEvents:UIControlEventTouchUpInside];
        [customButton setImage:[UIImage imageNamed:@"buy_off.png"] forState:UIControlStateNormal];
        [customButton setImage:[UIImage imageNamed:@"buy_on.png"] forState:UIControlStateHighlighted];
        [subView1 addSubview:customButton];
        [customButton release];
    }
	
	return cell;
}
	
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row==0)
	{
		return 110.0;
	} 
    else if (indexPath.row == 1)
	{
		return 42;
	}
    else if(indexPath.row == 2) {
        return 39;
    }
	return 38;
}
- (NSIndexPath *)tableView:(UITableView *)tv willSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	BooksOffer* selectedBookOffer= nil;
	if(currentDisplayCriterion == DISPLAY_BEST_OFFERS)
	{
		if (indexPath.row >= 3)
		{
			selectedBookOffer = [bestOffersList objectAtIndex:indexPath.row - 3];	
		}
	}
	else if (currentDisplayCriterion == DISPLAY_ALL_OFFERS )
	{
		if (indexPath.row >= 3)
		{
			selectedBookOffer = [bookOffersList objectAtIndex:indexPath.row - 3];	
		}
	}
	if (selectedBookOffer != nil)
	{
		[offerViewController refreshBookOfferViewController:selectedBookOffer forBook:currentBookInfo fromSource:self];
		[self.navigationController pushViewController:offerViewController animated:YES];
	}
	[tv deselectRowAtIndexPath:indexPath animated:YES];
	return nil;

}

-(UISegmentedControl *)createSegmentControl{
	
	if (segmentControl == nil)
	{
		
		currentDisplayCriterion = DISPLAY_BEST_OFFERS;
		NSArray *segmentTextContent = [NSArray arrayWithObjects:@"Best Prices", @"All Prices",nil];
//		NSArray *segmentTextContent = [NSArray arrayWithObjects:[UIImage imageNamed:@"home-search.png"],[UIImage imageNamed:@"home-search.png"],nil];
		segmentControl = [[UISegmentedControl alloc] initWithItems:segmentTextContent];
		segmentControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		segmentControl.frame = CGRectMake(0, 10, 300, 35);
		segmentControl.segmentedControlStyle = UISegmentedControlStyleBar;
		segmentControl.backgroundColor=[UIColor clearColor];
		segmentControl.tintColor = [UIColor lightGrayColor];//[UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];
		segmentControl.selectedSegmentIndex =0;
		segmentControl.frame=CGRectMake(0.0, 8, 300, 30);
		segmentControl.highlighted=YES;
		segmentControl.tintColor = [UIColor lightGrayColor];
		[segmentControl addTarget:self action:@selector(segmentControlAction:) forControlEvents:UIControlEventValueChanged];
	}
	
	return segmentControl;
}

/*
-(UIToolbar*)getToolbar
{
	if (toolbar == nil)
	{
		toolbar = [UIToolbar new];
		toolbar.barStyle = UIBarStyleBlackOpaque;
		toolbar.tintColor=[UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];
		toolbar.backgroundColor=[UIColor clearColor];
		[toolbar sizeToFit];
		
		CGFloat toolbarHeight = [toolbar frame].size.height;
		CGRect mainViewBounds = contentView.bounds;
		[toolbar setFrame:CGRectMake(CGRectGetMinX(mainViewBounds),
									   CGRectGetMinY(mainViewBounds) + CGRectGetHeight(mainViewBounds) - (toolbarHeight * 2.0) + 21 ,
									   CGRectGetWidth(mainViewBounds),
									   toolbarHeight)];
		
		
		UISegmentedControl *segCont = [self createSegmentControl];
		segCont.tag=1;
		//		UIView* segControlView = [ [UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 160, 30)];
		//		[segControlView addSubview:segCont];
		
		UIBarButtonItem* flexContr = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil ] autorelease];
		UIBarButtonItem* segContr = [[UIBarButtonItem alloc] initWithCustomView:segCont];
		UIBarButtonItem* actionControl = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(displayActionSheet:)] autorelease];

		
		NSArray *items = [NSArray arrayWithObjects:flexContr, segContr, flexContr, actionControl,nil];
		[toolbar setItems:items animated:NO];
		//		[segControlView release];
		//[segCont release];
		[segContr release];
	}
	return toolbar;
}
 */
-(void)segmentControlAction:(id)sender
{
    contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
	currentDisplayCriterion = ((UIButton*)sender).tag;
	if(currentDisplayCriterion == DISPLAY_BEST_OFFERS)
	{
		currentDisplayCriterion = DISPLAY_ALL_OFFERS;
	}
	else if(currentDisplayCriterion == DISPLAY_ALL_OFFERS)
	{
		currentDisplayCriterion = DISPLAY_BEST_OFFERS;
	}
	
	[tableView reloadData];
}
- (CGFloat)getTextHeightForSystemFontOfSize:(CGFloat)size ofWidth:(CGFloat)maxWidth forText:(NSString*)aText {
    CGFloat maxHeight = 9999;
    CGSize maximumLabelSize = CGSizeMake(maxWidth,maxHeight);
	
    CGSize expectedLabelSize = [aText sizeWithFont:[UIFont systemFontOfSize:size] constrainedToSize:maximumLabelSize lineBreakMode:UILineBreakModeWordWrap]; 
	
    return expectedLabelSize.height;
}


-(void) displayActionSheet:(id) sender
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate]; 
	UIActionSheet *actionSheet = nil;
	NSString* firstOptionTitle = @"Book Details";
	NSString* secondOptionTitle = @"Sell It";	
	NSString* thirdOptionTitle = @"";
	if (currentBookInfo.isInWishList)
	{
		thirdOptionTitle =  @"Remove from Wishlist";
	}
	else
	{
		thirdOptionTitle = @"Add to Wishlist";
	}
	
	actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil
													otherButtonTitles:firstOptionTitle, secondOptionTitle,thirdOptionTitle, @"Cancel", nil];
	
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	actionSheet.destructiveButtonIndex = actionSheet.numberOfButtons -1 ;	
	actionSheet.delegate = self;
	[actionSheet showInView:appDelegate.window];
	[actionSheet release];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
		[self populateDetailsView];
	}	
	else if (buttonIndex ==1)
	{
		[self sellItAction];
	}
	else if (buttonIndex == 2)
	{
		if (currentBookInfo.isInWishList)
		{
			[self removeFromWishList];
		}
		else
		{
			[self addToWishList];
		}
	}
	else 
	{
		NSLog(@"cancel");
	}
}


-(void)addToWishList
{
	WishList* wishList = [[WishList alloc] init];
	wishList.authorName = currentBookInfo.author;
	wishList.bookName = currentBookInfo.title;
	wishList.isbnId10 = currentBookInfo.isbnId10;
	wishList.imageUrl = currentBookInfo.imageUrl;
	
	CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [[UIApplication sharedApplication]delegate];
	[appDelegate addToWishList:wishList];
	[wishList release];
	
	NSString* aMessage = [NSString stringWithFormat:@"'%s' has been successfully added to your wishlist.", [currentBookInfo.title UTF8String]];
	
	UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:aMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
	[alertView show];
	
	currentBookInfo.isInWishList = YES;
	currentBookInfo.isInMyCollection = NO;
}
-(void)removeFromWishList
{
	
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	[appDelegate removeFromWishList:currentBookInfo.isbnId10];
	
	NSString* aMessage = [NSString stringWithFormat:@"'%s' has been successfully removed from your wishlist.", [currentBookInfo.title UTF8String]];
	
	UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:aMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
	[alertView show];
	currentBookInfo.isInWishList = NO;
}

-(void)getBestPrices:(NSMutableArray*)aOffersList
{
//	bestOffersList = 
	[bestOffersList removeAllObjects];
	
	NSString* previousOfferCondition = @"";
	for(BooksOffer* aBookOffer in bookOffersList)
	{
		NSString* currentOfferCondition = aBookOffer.conditionName;
		if (![currentOfferCondition isEqualToString:previousOfferCondition])
		{
			previousOfferCondition = currentOfferCondition;
			[bestOffersList addObject:aBookOffer];
		}
        NSLog(@"Comparing:%@ with %@", currentOfferCondition, previousOfferCondition );
	}	
}


-(void)populateDetailsView
{
	[self updateStatus];
	
	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!" message:@"No Network Available"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		
	}
	else
	{
		
		//	@try {
		
		[progressInd startAnimating];
		//	[detailsButton setImage:[UIImage imageNamed:@"transparent-clicked.png"] forState:UIControlStateNormal];
		
		[NSThread detachNewThreadSelector:@selector(doPopulateDetailsView:) toTarget:self withObject:nil];
		/*			
		 }
		 @catch (NSException * e) {
		 printf("\n caught an exception");
		 }
		 @finally {
		 
		 }
		 */		
	}
}

- (void)doPopulateDetailsView:(id)sender
{
	//	@try {
	
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate* )[[UIApplication sharedApplication]delegate];
	
	currentBook = [appDelegate getBookInfo:currentBookInfo.isbnId10];
	currentBook.image = currentBookInfo.image;
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	[progressInd stopAnimating];
	
	[pool release];	
	
	[self performSelectorOnMainThread:@selector(loadDetailsView) withObject:nil waitUntilDone:NO];

	[NSThread exit];
	/*	}
	 @catch (NSException * e) {
	 [e 
	 }
	 @finally {
	 }
	 */
}

-(void)loadDetailsView
{
	BookDetailsViewController* bookDetailsViewController = [[BookDetailsViewController alloc] initWithBook:currentBook fromSource:bookListViewController];
	
	[bookDetailsViewController refreshBookDetailsViewController:currentBook fromSource:bookListViewController];
	
	if ([self.navigationController topViewController] == bookDetailsViewController)
	{
		//printf("\n cancelling the push");
	}
	else
	{
		//	printf("\n about to push to details view controller");
		[self.navigationController pushViewController:bookDetailsViewController animated:YES];
	}
	[bookDetailsViewController release];

}

-(void)sellItAction
{
	[self updateStatus];
	
	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!" message:@"No Network Available"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		
	}
	else
	{
		
		//	@try {
		
		[progressInd startAnimating];
		//	[detailsButton setImage:[UIImage imageNamed:@"transparent-clicked.png"] forState:UIControlStateNormal];
		
		[NSThread detachNewThreadSelector:@selector(doPopulateSellView:) toTarget:self withObject:nil];
		/*			
		 }
		 @catch (NSException * e) {
		 printf("\n caught an exception");
		 }
		 @finally {
		 
		 }
		 */		
	}
}

- (void)doPopulateSellView:(id)sender
{
	//	@try {
	
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	

	

	CampusBooksAppDelegate* appDelegate =  (CampusBooksAppDelegate*) [ [UIApplication sharedApplication] delegate];

	currentBook = [appDelegate getBookInfo:currentBookInfo.isbnId10];
	currentBook.image = currentBookInfo.image;
	
	sellOffersList = [appDelegate getBookBuybackOffersListForSearch:currentBook.isbnId10];
	
	[self performSelectorOnMainThread:@selector(loadSellView) withObject:nil waitUntilDone:NO];

	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	[progressInd stopAnimating];
	
	[pool release];	
	[NSThread exit];
	/*	}
	 @catch (NSException * e) {
	 [e 
	 }
	 @finally {
	 }
	 */
}
-(void)loadSellView
{
	BookSellViewController* bookSellViewController = [[BookSellViewController alloc] initWithBook:currentBook withOffers:sellOffersList fromSource:bookListViewController];
	
	[bookSellViewController refreshBookSellViewController:currentBook withOffers:sellOffersList  fromSource:bookListViewController];
	
	if ([self.navigationController topViewController] == bookSellViewController)
	{
		//printf("\n cancelling the push");
	}
	else
	{
		//	printf("\n about to push to sell view controller");
		
		[self.navigationController pushViewController:bookSellViewController animated:YES];
	}
	[bookSellViewController release];

}
-(void)buyHereAction:(id)sender
{
	NSIndexPath* indexPath = [sender indexPath];
	//printf("\n IndexPath Row:%d", indexPath.row);
	
	BooksOffer* selectedBookOffer= nil;
	if(currentDisplayCriterion == DISPLAY_BEST_OFFERS)
	{
		if (indexPath.row >= 3)
		{
			selectedBookOffer = [bestOffersList objectAtIndex:indexPath.row - 3];	
		}
	}
	else if (currentDisplayCriterion == DISPLAY_ALL_OFFERS )
	{
		if (indexPath.row >=3)
		{
			selectedBookOffer = [bookOffersList objectAtIndex:indexPath.row - 3];	
		}
	}
	if (selectedBookOffer != nil)
	{
		MerchantViewController* merchantViewController = [[MerchantViewController alloc] init];
		[merchantViewController reloadMerchantViewWithOffer:selectedBookOffer forBook:currentBookInfo];
		[self.navigationController pushViewController:merchantViewController animated:YES];
		[merchantViewController release];
	}
	
}
					 
					 

@end
