    //
//  MapViewController.m
//  CampusBooks
//
//  Created by Admin on 19/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "MapViewController.h"
#import"CampusBooksAppDelegate.h"
#import"StoreDetailsViewController.h"

#define SHOW_LIST 0
#define SHOW_MAP 1

@implementation MapViewController
@synthesize offerObjList, conditionId;

@synthesize internetConnectionStatus;
- (id)initWithCenterLocation:(CLLocation*)aCenterLocation forOffers:(NSMutableArray*)aOffersList forBook:(Book*)aBook fromBookListController:(BookListViewController*)aBookListViewController{
	if (self = [super init]) {
		
		bookListViewController = aBookListViewController;
		optionTitles = [[NSMutableArray alloc] init];
		
		contentView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
		contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
		self.view = contentView;
		[contentView release];
		
		centerLocation = aCenterLocation;
		offerObjList = aOffersList;
		book = aBook;
		
		firstView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,460)];
		firstView.backgroundColor = [UIColor clearColor];
		
		
		secondView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,460)];
		secondView.backgroundColor = [UIColor clearColor];
		//[contentView addSubview:secondView];
		
		tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,0 ,320,360) style:UITableViewStylePlain];	
		tableView.delegate = self;
		tableView.dataSource = self;
		tableView.backgroundColor = [UIColor clearColor];
		tableView.separatorColor = [UIColor clearColor];
		tableView.rowHeight = 35;
		[tableView setSectionHeaderHeight:5];
		[tableView setSectionFooterHeight:5];
		
		[secondView addSubview:tableView];
		[contentView addSubview:secondView];
		
		cellProgressInd = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(145.0,175.5, 35,35)];
		cellProgressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
		cellProgressInd.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
											UIViewAutoresizingFlexibleRightMargin |
											UIViewAutoresizingFlexibleTopMargin |
											UIViewAutoresizingFlexibleBottomMargin);
		[cellProgressInd startAnimating];
		cellProgressInd.hidden = NO;
		[cellProgressInd stopAnimating];
	//	[cellProgressInd sizeToFit];	
		
//		UIView  *titlesubView= [[UIView alloc] initWithFrame:CGRectMake(0,20,300,45)];
//		titlesubView.backgroundColor=[UIColor clearColor];
//		segmentControl = [self createSegmentControl];
//		[titlesubView addSubview:segmentControl];
//		self.navigationItem.titleView = titlesubView;
        

		if ([conditionId isEqualToString:@"8"]) {
            self.title = @"Bookstores";
        }
        else {
            self.title = @"Libraries";
        }
		if (centerLocation != nil && [offerObjList count] > 0)
		{
			BooksOffer* offer = [ offerObjList objectAtIndex:0];
			centerLocation = [[[CLLocation alloc] initWithLatitude:[offer.storeLat floatValue] longitude:[offer.storeLong floatValue]] autorelease];
		}
		
		 [offerObjList sortUsingSelector:@selector(compareByMiles:)];	
		
		if([offerObjList count] > 0)
		{
			farthestLocation = [offerObjList objectAtIndex:[offerObjList count]-1];
		}
		
        currentDisplayCriterion = SHOW_LIST;
		[self getMapView];
		[self centerMap];
		
	}
	return self;
}

- (void)viewWillAppear:(BOOL)animated {
	
	[self updateStatus];
	
	if (internetConnectionStatus == NotReachable) 
	{
		[cellProgressInd stopAnimating];
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Campusbooks" message:@"No Network Available. \n This Application requires network connectivity. \n This application will terminate now."
													   delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		return;
	}
			
}
-(void)segmentControlAction:(id)sender
{
    contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
	currentDisplayCriterion = ((UIButton*)sender).tag;
	if(currentDisplayCriterion == SHOW_LIST)
	{
        currentDisplayCriterion = SHOW_MAP;
	}
	else if(currentDisplayCriterion == SHOW_MAP)
	{
        currentDisplayCriterion = SHOW_LIST;
	}
    
    [tableView reloadData];

}
-(UISegmentedControl *)createSegmentControl{
	
	if (segmentControl == nil)
	{
		
		NSArray *segmentTextContent = [NSArray arrayWithObjects:@"Show List", @"Show Map",nil];
		//		NSArray *segmentTextContent = [NSArray arrayWithObjects:[UIImage imageNamed:@"home-search.png"],[UIImage imageNamed:@"home-search.png"],nil];
		segmentControl = [[UISegmentedControl alloc] initWithItems:segmentTextContent];
		segmentControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		segmentControl.frame = CGRectMake(0, 10, 300, 35);
		segmentControl.segmentedControlStyle = UISegmentedControlStyleBar;
		segmentControl.backgroundColor=[UIColor clearColor];
		segmentControl.tintColor = [UIColor lightGrayColor];//[UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];
		segmentControl.selectedSegmentIndex =0;
		segmentControl.frame=CGRectMake(0.0, 8, 300, 30);
		segmentControl.highlighted=YES;
		segmentControl.tintColor = [UIColor lightGrayColor];
		[segmentControl addTarget:self action:@selector(segmentControlAction:) forControlEvents:UIControlEventValueChanged];
	}
	
	return segmentControl;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex 
{
	
	if(buttonIndex == 0){
		
		exit(0);
	}
	
}


-(void)exit:(NSTimer*)timer { 
	exit(0);
	
}


-(void)getMapView{
	
	
	CGRect frame = CGRectMake(0,0, 320,200);
	
	mapView = [[MKMapView alloc] initWithFrame:frame]; 
	mapView.mapType = MKMapTypeStandard;
	mapView.delegate = self;
	mapView.scrollEnabled = YES; 
	mapView.zoomEnabled = YES; 
	MKCoordinateRegion mkCoordinateRegion;
	
	//If we're not able to determine the current location, use the first item in the list as the current location
	
	CLLocationCoordinate2D center;
	center.latitude = centerLocation.coordinate.latitude;
	center.longitude = centerLocation.coordinate.longitude;
	mkCoordinateRegion.center=center;
	
	
	
	MKCoordinateSpan spn;
	spn.latitudeDelta=0.001;
	spn.longitudeDelta=0.001;
	
	mkCoordinateRegion.span=spn;
	
	/*locationManager = [[CLLocationManager alloc] init];
	locationManager.delegate = self;
	locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
	
	[locationManager startUpdatingLocation];*/
	
	
	//printf("\n count:%d",[offerObjList count]);
	for(int i = 0;i < [offerObjList count];i++)
	{
		
		BooksOffer* offerObject = [offerObjList objectAtIndex:i];
		
		CLLocationCoordinate2D currentCoordinate ;
		currentCoordinate.latitude = [offerObject.storeLat doubleValue];
		currentCoordinate.longitude = [offerObject.storeLong doubleValue];
		
		POI *poi = [[POI alloc] initWithCoords:currentCoordinate forOffer:offerObject ]; 
		[mapView addAnnotation:poi];
		[poi release];
		
	}
	[firstView addSubview:mapView];
}

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation
{
	MKPinAnnotationView *annView = nil;
	
	
	annView= [[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"Current Location"] autorelease];
	
	annView.animatesDrop=TRUE;
	annView.canShowCallout = YES;
	annView.calloutOffset = CGPointMake(-5, 5);
	annView.rightCalloutAccessoryView=[UIButton buttonWithType:UIButtonTypeDetailDisclosure];
	[annView setPinColor:MKPinAnnotationColorGreen];
	
/*	for(int i=0;i<[offerObjList count];i++)
	{
		BooksOffer *object=[offerObjList objectAtIndex:i];
		if([[annView.annotation title] isEqualToString:object.storeName])
		{
			annView.animatesDrop=TRUE;
			annView.canShowCallout = YES;
			annView.calloutOffset = CGPointMake(-5, 5);
			annView.rightCalloutAccessoryView=[UIButton buttonWithType:UIButtonTypeDetailDisclosure];
			[annView setPinColor:MKPinAnnotationColorGreen];
			
		}
		else 
		{	
			annView.animatesDrop=TRUE;
			annView.canShowCallout = YES;
		}
	}
*/	
	return annView ;
}


- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
	
	CSImageAnnotationView* imageAnnotationView = (CSImageAnnotationView*) view;
	POI* annotation = (POI*)[imageAnnotationView annotation];
	
	BooksOffer* offerObject = annotation.booksOffer;
	
	[self showDetailsAction:offerObject];
	
}

-(void)showDetailsAction:(BooksOffer*)offerObject
{
	//printf("\n coffee title ;%s",[offerObject.storeName UTF8String]);

	StoreDetailsViewController* detailViewController = [[StoreDetailsViewController alloc]initWithOffer:offerObject forBook:book fromBookListController:bookListViewController];
	[[self navigationController] pushViewController:detailViewController animated:YES];
	[detailViewController release];
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)section
{ 
    if (currentDisplayCriterion == SHOW_LIST) {
        return [offerObjList count] + 3;
    }
    else {
        return 3;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tv 
{
	return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row==0)
	{
		return 110.0;
	} 
    else if (indexPath.row ==1)
	{
		return 43;
	}
    else if (indexPath.row == 2 )
	{
        if (currentDisplayCriterion == SHOW_MAP) {
            return 176;
        }
        else {
            return 38;
        }
	}
	
	return 52;
	
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
	if (cell == nil)
	{
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyIdentifier"] autorelease];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.backgroundColor = [UIColor clearColor];
		UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0,0,290,50)];
		myView.tag = 0;
		[cell.contentView addSubview:myView];
		[myView release];
	}
	
	UIView* elementView = [cell.contentView viewWithTag:0];
	for(UIView* subView in elementView.subviews)
	{
		[subView removeFromSuperview];
	}
	
	cell.accessoryType = UITableViewCellAccessoryNone;
	
	if (indexPath.row ==0)
	{
		UIView* subView1=[[UIView alloc]initWithFrame:CGRectMake(0,0, 320,110)];
		subView1.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"book_background.png"]];
		
		UILabel* titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 10, 200,40)];
		titleLabel.numberOfLines = 2;
		titleLabel.font = [ UIFont boldSystemFontOfSize:15];
		titleLabel.adjustsFontSizeToFitWidth = YES;
		titleLabel.text = (bookForDetails.title != nil ? bookForDetails.title : @"N/A");
		titleLabel.backgroundColor = [UIColor clearColor];
		titleLabel.textAlignment = UITextAlignmentLeft;
		titleLabel.textColor = [UIColor darkTextColor];
		[subView1 addSubview:titleLabel];
		[titleLabel release];
        
        UILabel* authorLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 50, 200,20)];
		authorLabel.font = [ UIFont systemFontOfSize:14];
		authorLabel.adjustsFontSizeToFitWidth = YES;
		authorLabel.text = [NSString stringWithFormat:@"ISBN 10: %@",(bookForDetails.isbnId10 != nil ? bookForDetails.isbnId10 : @"N/A")];
		authorLabel.backgroundColor = [UIColor clearColor];
		authorLabel.textAlignment = UITextAlignmentLeft;
		authorLabel.textColor = [UIColor lightGrayColor];
		[subView1 addSubview:authorLabel];
		[authorLabel release];
        
        
        NSString* formattedDateString = @"";
        if (bookForDetails.publishedDate != nil)
        {
            NSDate* originalDate = [originalDateFormatter dateFromString:bookForDetails.publishedDate];
            formattedDateString = [newDateFormatter stringFromDate:originalDate];
        }
        if ([formattedDateString length] == 0) {
            formattedDateString = @"N/A";
        }
        UILabel* releaseDateLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 70, 200,20)];
		releaseDateLabel.font = [ UIFont systemFontOfSize:14];
		releaseDateLabel.adjustsFontSizeToFitWidth = YES;
		releaseDateLabel.text = [NSString stringWithFormat:@"ISBN 13: %@",(bookForDetails.isbnId13 != nil ? bookForDetails.isbnId13 : @"N/A")];
		releaseDateLabel.backgroundColor = [UIColor clearColor];
		releaseDateLabel.textAlignment = UITextAlignmentLeft;
		releaseDateLabel.textColor = [UIColor lightGrayColor];
		[subView1 addSubview:releaseDateLabel];
		[releaseDateLabel release];
        
        
		UIImageView* imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(35, 17, 60, 80)];
		imageView2.image = bookForDetails.image != nil ? bookForDetails.image : [UIImage imageNamed:@"LoadingCover.png"];
		[subView1 addSubview:imageView2];
		[imageView2 release];
		
		[elementView addSubview:subView1];
		[subView1 release];		
	}
    else if (indexPath.row == 1)
	{
		UIView* subView1=[[UIView alloc]initWithFrame:CGRectMake(0,0, 320,42)];
		subView1.backgroundColor=[UIColor clearColor];
		[elementView addSubview:subView1];
		[subView1 release];
        
        MyCustomButton* showListOrMapButton =  [ [MyCustomButton alloc] initWithFrame:CGRectMake(0, 0, 320,42)];	
        [showListOrMapButton addTarget:self action:@selector(segmentControlAction:) forControlEvents:UIControlEventTouchUpInside];
        
        if (currentDisplayCriterion == SHOW_MAP) {
            showListOrMapButton.tag = SHOW_MAP;
            [showListOrMapButton setImage:[UIImage imageNamed:@"show_map.png"] forState:UIControlStateNormal];
            [showListOrMapButton setImage:[UIImage imageNamed:@"show_list.png"] forState:UIControlStateHighlighted];
        }
        else {
            showListOrMapButton.tag = SHOW_LIST;
            [showListOrMapButton setImage:[UIImage imageNamed:@"show_list.png"] forState:UIControlStateNormal];
            [showListOrMapButton setImage:[UIImage imageNamed:@"show_map.png"] forState:UIControlStateHighlighted];
        }
        [subView1 addSubview:showListOrMapButton];
        [showListOrMapButton release];
	}
    else if (indexPath.row == 2)
    {
        if (currentDisplayCriterion == SHOW_MAP) {
            [elementView addSubview:mapView];
        }
        else {
            UIView* subView1=[[UIView alloc]initWithFrame:CGRectMake(0,0, 320,38)];
            subView1.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"show_list_header.png"]];
            
            [elementView addSubview:subView1];
            [subView1 release];
        }        
    }
	else {
		
		UIFont* headerFont =  [ UIFont boldSystemFontOfSize:13];
		
		
		UIColor* backgroundColor = [UIColor clearColor];
		UIColor* singleRowTextColor =[UIColor darkTextColor];//[UIColor colorWithRed:66/255.0 green:141/255.0 blue:149/255.0 alpha:1] ;
		
		elementView.backgroundColor = backgroundColor;
		cell.backgroundColor = backgroundColor;
		cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		if (indexPath.row >= 3)
		{
			cell.accessoryType = UITableViewCellAccessoryNone;
			
            elementView.frame = CGRectMake(0, 0, 320, 52);
            
            UIView* subView1=[[UIView alloc]initWithFrame:CGRectMake(0,0, 320,elementView.frame.size.height)];
            
            if ([selectedOffer.conditionId isEqualToString:@"8"]) {
                subView1.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"show_list_row_bg.png"]];
            }
            else {
                subView1.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"library_distance_price.png"]];
            }
            
            [elementView addSubview:subView1];
            [subView1 release];

			BooksOffer* offerObject=[offerObjList objectAtIndex:indexPath.row-3];
			
			UILabel *textView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,140,49)];
			[textView setFont:headerFont];
			textView.textColor=singleRowTextColor ;
			textView.numberOfLines = 0;
			textView.backgroundColor = backgroundColor;
            textView.textAlignment = UITextAlignmentCenter;


			textView.text =offerObject.storeName;
			[subView1 addSubview:textView];
			[textView release];

//			UIImageView* imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(184,0,1, 70)];
//			imageView1.image = [UIImage imageNamed:@"sep.png"];
//			[elementView addSubview:imageView1];
//			[imageView1 release];
			
			NSString* distanceValue  = (offerObject.storeMiles != nil? offerObject.storeMiles  : @"0");
			distanceValue = [NSString stringWithFormat:@"%.2f miles",[distanceValue floatValue]];
			
			UILabel* distanceLabel =  [[UILabel alloc]initWithFrame:CGRectMake(155, 12, 88,25)];
			distanceLabel.font =headerFont;
			distanceLabel.text = distanceValue ;
			distanceLabel.backgroundColor =[UIColor clearColor];
			distanceLabel.textAlignment = UITextAlignmentCenter;
			distanceLabel.textColor = singleRowTextColor;
			[subView1 addSubview:distanceLabel];
			[distanceLabel release];

//			UIImageView* imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(261,0,1, 70)];
//			imageView2.image = [UIImage imageNamed:@"sep.png"];
//			[elementView addSubview:imageView2];
//			[imageView2 release];
			
			UILabel* totalPriceLabel =  [[UILabel alloc]initWithFrame:CGRectMake(243, 12, 72,25)];
			totalPriceLabel.font = [UIFont boldSystemFontOfSize:13];;
			totalPriceLabel.text =  (offerObject.totalPrice != nil && [offerObject.totalPrice floatValue] > 0 ? [NSString stringWithFormat:@"$%.2f",[offerObject.totalPrice floatValue] ] : @"  Free");
			totalPriceLabel.backgroundColor =backgroundColor;
			totalPriceLabel.textAlignment = UITextAlignmentLeft;
			totalPriceLabel.textColor = [UIColor darkTextColor];
			[subView1 addSubview:totalPriceLabel];
			[totalPriceLabel release];

//			UILabel* conditionLabel =  [[UILabel alloc]initWithFrame:CGRectMake(270, 30, 45,25)];
//			conditionLabel.font = [UIFont boldSystemFontOfSize:13];;
//			conditionLabel.text =  [NSString stringWithFormat:@"(%s)", [offerObject.offerConditionText UTF8String]];
//			conditionLabel.backgroundColor =backgroundColor;
//			conditionLabel.textAlignment = UITextAlignmentLeft;
//			conditionLabel.textColor = [UIColor darkTextColor];
//			[elementView addSubview:conditionLabel];
//			[conditionLabel release];
			
			
		}
		
	}
	return cell;
}

-(void)action:(MyCustomButton*)aCustomButton
{
	NSIndexPath* indexPath = aCustomButton.indexPath;
	BooksOffer* currentOffer = [offerObjList objectAtIndex:indexPath.row-3];
	[self showDetailsAction:currentOffer];
}
- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	if(indexPath.row ==0)
	{
		[self populateDetailsView];
	}
	else if(indexPath.row != 2)
	{
		BooksOffer* currentOffer = [offerObjList objectAtIndex:indexPath.row-3];
		[tableView deselectRowAtIndexPath:indexPath animated:YES];
		[self displayActionSheetForOffer:currentOffer];
	}
}
-(void)displayActionSheetForOffer:(BooksOffer*)aBookOffer
{
	selectedOffer = aBookOffer;
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate]; 
	UIActionSheet *actionSheet = nil;
	
	
	[optionTitles removeAllObjects];
	
	if ([[selectedOffer.storePhone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] >0)
	{
		if ([selectedOffer.conditionId isEqualToString:@"8"]) {
			[optionTitles addObject:@"Call Bookstore"];
		}
		else {
			[optionTitles addObject:@"Call Library"];
		}
	}
	if ([[selectedOffer.link stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] >0)
	{
		if ([selectedOffer.conditionId isEqualToString:@"8"]) {
			[optionTitles addObject:@"Visit Bookstore"];
		}
		else {
			[optionTitles addObject:@"Visit Library"];
		}
	}
	[optionTitles addObject:@"View Details"];
	[optionTitles addObject:@"Get Directions"];
	[optionTitles addObject:@"Email Friend"];
	[optionTitles addObject:@"Cancel"];
	
	actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil
									 otherButtonTitles: nil];
	
	
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	actionSheet.destructiveButtonIndex = actionSheet.numberOfButtons -1 ;	
	actionSheet.delegate = self;
	
	for (NSString* option in optionTitles) {
		[actionSheet addButtonWithTitle:option];
	}
	
	[actionSheet showInView:appDelegate.window];
	[actionSheet release];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex >= 0 && buttonIndex < [optionTitles count]) {

		NSString* selectedOption = [optionTitles objectAtIndex:buttonIndex];
		if ([selectedOption isEqualToString:@"Call Bookstore"] || [selectedOption isEqualToString:@"Call Library"]) {
			[self callBookStore];
		}
		else if ([selectedOption isEqualToString:@"Visit Bookstore"] || [selectedOption isEqualToString:@"Visit Library"]) {
			[self viewBookStore];
			printf("\n Visit Bookstore");
		}
		else if ([selectedOption isEqualToString:@"View Details"])
		{
			[self showDetailsAction:selectedOffer];
		}
		else if ([selectedOption isEqualToString:@"Get Directions"])
		{
			[self showDirections];
		}
		else if ([selectedOption isEqualToString:@"Email Friend"])
		{
			[self sendEmail:nil];
		}
		else 
		{
			NSLog(@"cancel");
		}
		
	}
}
-(void)viewBookStore
{
	MerchantViewController* merchantViewController = [[MerchantViewController alloc] init];
	[merchantViewController reloadMerchantViewWithOffer:selectedOffer forBook:book];
	[self.navigationController pushViewController:merchantViewController animated:YES];
	[merchantViewController release];
}	
-(void)callBookStore
{
	
	NSString* phoneNumber = [selectedOffer.storePhone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
	if ([phoneNumber length] >0) {
		NSString* urlStr = [NSString stringWithFormat:@"tel://%s", [phoneNumber UTF8String]];
		printf("\n URL:%s",[urlStr UTF8String]);
		NSURL* url = [NSURL URLWithString:urlStr];
		[[UIApplication sharedApplication] openURL:url];
	}
}
-(void)showDirections
{
	
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	
	
	if(appDelegate.currentLocation == nil)
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Campusbooks" message:@"Couldn't retrieve the directions as the curent location can't be determined. "
													   delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		return;
	}
	else {
		NSString* urStr = [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f&daddr=%s,%s", appDelegate.currentLocation.coordinate.latitude,appDelegate.currentLocation.coordinate.longitude, [selectedOffer.storeLat UTF8String], [selectedOffer.storeLong UTF8String]];
		NSURL* url = [NSURL URLWithString:urStr];
		[[UIApplication sharedApplication] openURL:url];
	}
	
	
}

-(void) sendEmailFromAppOnDevice
{
	NSString* emailSubject = [NSString stringWithFormat:@" Best place to grab your copy of '%s'" , [book.title UTF8String]];
	NSString* emailContent  = [self getEmailString];
	NSString* emailURLString = [NSString stringWithFormat:@"mailto:?subject=%s&body=%s",[emailSubject UTF8String], [emailContent UTF8String]];
	emailURLString = [emailURLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	NSURL* url = [NSURL URLWithString:emailURLString];
	[[UIApplication sharedApplication] openURL:url];	
}
-(IBAction)sendEmail:(id)sender
{
	// This sample can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	
	//	[self showAlertForEmail];
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheet];
			//[self sendEmailFromAppOnDevice];
		}
		else
		{
			[self sendEmailFromAppOnDevice];
		}
	}
	else
	{
		[self sendEmailFromAppOnDevice];
	}
	
	
}

-(NSString*)getEmailString
{
	NSString* emailContent = @"";
	
	NSString* addressString=@"";
	if([selectedOffer.storeAddress1 length]>0)
	{
		addressString=[addressString stringByAppendingString:selectedOffer.storeAddress1];
		addressString=[addressString stringByAppendingString:@"#@#"];
	}
	if([selectedOffer.storeAddress2 length]>0)
	{
		addressString=[addressString stringByAppendingString:selectedOffer.storeAddress2];
		addressString=[addressString stringByAppendingString:@"#@#"];
	}
	if([selectedOffer.storeCity length]>0)
	{
		addressString=[addressString stringByAppendingString:selectedOffer.storeCity];
		addressString=[addressString stringByAppendingString:@"#@#"];
	}
	if([selectedOffer.storeState length]>0)
	{
		addressString=[addressString stringByAppendingString:selectedOffer.storeState];
	}
	NSString* fullAddressString = [addressString stringByReplacingOccurrencesOfString:@"#@#" withString:@", "];
	
	NSString* addressUrlString = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%s",[fullAddressString UTF8String]];
	
	
	emailContent =  [NSString stringWithFormat:@"  <html><head><title></title></head><body> "];
	emailContent = [emailContent stringByAppendingString:@"<p>Check out the best place to grab your copy of  '<i>'"];

	emailContent = [emailContent stringByAppendingString:book.title];
	emailContent = [emailContent stringByAppendingString:@"'</i>'.</p>"];
	
	NSString* storeType = @"";
	
	if ([selectedOffer.conditionId isEqualToString:@"8"]) {
		storeType = @"Bookstore";
	}
	else {
		storeType =  @"Library";
	}
	
	NSString* priceString = [selectedOffer.totalPrice floatValue] > 0  ? [NSString stringWithFormat:@"$%.2f", [selectedOffer.totalPrice floatValue]] : @" Free";
	emailContent = [emailContent stringByAppendingString:@"<table ><tr><th>"];
	emailContent = [emailContent stringByAppendingString:storeType];	
	emailContent = [emailContent stringByAppendingString:@"</th><th>Address</th><th>Price</th></tr>"];
	emailContent = [emailContent stringByAppendingString:@" <tr><td><a href='"];
	emailContent = [emailContent stringByAppendingString:selectedOffer.link];
	emailContent = [emailContent stringByAppendingString:@"'>"];
	emailContent = [emailContent stringByAppendingString:selectedOffer.storeName];
	emailContent = [emailContent stringByAppendingString:@"</a></td><td><a href='"];
	emailContent = [emailContent stringByAppendingString:addressUrlString];
	emailContent = [emailContent stringByAppendingString:@"'>"];
	emailContent = [emailContent stringByAppendingString:fullAddressString];
	emailContent = [emailContent stringByAppendingString:@"</a></td><td>"];
	emailContent = [emailContent stringByAppendingString:priceString];
	emailContent = [emailContent stringByAppendingString:@"</td></tr>"];
	emailContent = [emailContent stringByAppendingString:@"</table> "];
	
	emailContent = [emailContent stringByAppendingString:@"<p>This information is presented by '<a href='http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=326904377&mt=8'> CampusBooks</a>', , a price comparison tool on iPhone.<br></p>"];	
	emailContent = [emailContent stringByAppendingString:@"</body></html>"];
	
	emailContent = [emailContent stringByReplacingOccurrencesOfString:@"&" withString:@""];
	
	return emailContent;
}
#pragma mark -
#pragma mark Compose Mail

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
	NSString* emailSubject  = [NSString stringWithFormat:@" Best place to grab your copy of '%s'" , [book.title UTF8String]];
	
	NSString* emailContent = [self getEmailString];
	
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	
	[picker setSubject:emailSubject];
	
	
	// Attach an image to the email
	NSString *path = [[NSBundle mainBundle] pathForResource:@"icon" ofType:@"png"];
	NSData *myData = [NSData dataWithContentsOfFile:path];
	[picker addAttachmentData:myData mimeType:@"image/png" fileName:@"iBookStore"];
	
	// Fill out the email body text
	NSString *emailBody = emailContent;
	[picker setMessageBody:emailBody isHTML:YES];
	
	[self presentModalViewController:picker animated:YES];
    [picker release];
}


// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			//	message.text = @"Result: saved";
			break;
		case MFMailComposeResultSent:
		{
			UIAlertView* alert  = [[UIAlertView alloc] initWithTitle:@"CampusBooks" message:@"Email is sent sucesfully" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] ;
			[alert show];
			[alert release];
			
			break;
		}
		case MFMailComposeResultFailed:
		{
			UIAlertView* alert  = [[UIAlertView alloc] initWithTitle:@"CampusBooks" message:@"Unable to send email. Please try again" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil] ;
			[alert show];
			[alert release];
			break;
		}
		default:
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)updateStatus
{
	internetConnectionStatus= [[Reachability sharedReachability] internetConnectionStatus];
}



- (void)dealloc {
	
	[mapView release];
	
	[firstView release];
	[cellProgressInd release];
	
    [super dealloc];
}
-(void) centerMap {
	MKCoordinateRegion region;
	
	CLLocationDegrees maxLat = -90;
	CLLocationDegrees maxLon = -180;
	CLLocationDegrees minLat = 90;
	CLLocationDegrees minLon = 180;
	for(BooksOffer* currentOffer in offerObjList)
	{
		float latitude = [currentOffer.storeLat floatValue];
		float longitude = [currentOffer.storeLong floatValue];
		
		if(latitude > maxLat)
			maxLat = latitude;
		if(latitude < minLat)
			minLat = latitude;
		if(longitude > maxLon)
			maxLon = longitude;
		if(longitude < minLon)
			minLon = longitude;
	}
	region.center.latitude     = (maxLat + minLat) / 2;
	region.center.longitude    = (maxLon + minLon) / 2;
	region.span.latitudeDelta  = maxLat - minLat;
	region.span.longitudeDelta = maxLon - minLon;
	
	
	[mapView setRegion:region animated:NO];
}



-(void)populateDetailsView
{
	[self updateStatus];
	
	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!" message:@"No Network Available"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		
	}
	else
	{
		
		//	@try {
		
		[cellProgressInd startAnimating];
		//	[detailsButton setImage:[UIImage imageNamed:@"transparent-clicked.png"] forState:UIControlStateNormal];
		
		[NSThread detachNewThreadSelector:@selector(doPopulateDetailsView:) toTarget:self withObject:nil];
		/*			
		 }
		 @catch (NSException * e) {
		 printf("\n caught an exception");
		 }
		 @finally {
		 
		 }
		 */		
	}
}

- (void)doPopulateDetailsView:(id)sender
{
	//	@try {
	
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	
	bookForDetails = [appDelegate getBookInfo:book.isbnId10];
	bookForDetails.image = book.image;
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	[cellProgressInd stopAnimating];
	
	[pool release];	
	[self performSelectorOnMainThread:@selector(loadDetailsView) withObject:nil waitUntilDone:NO];
	[NSThread exit];
	
}

-(void)loadDetailsView
{
	BookDetailsViewController* bookDetailsViewController = [[BookDetailsViewController alloc] initWithBook:bookForDetails fromSource:bookListViewController];
	
	[bookDetailsViewController refreshBookDetailsViewController:bookForDetails fromSource:bookListViewController];
	
	if ([self.navigationController topViewController] == bookDetailsViewController)
	{
		//printf("\n cancelling the push");
	}
	else
	{
		//printf("\n about to push to details view controller");
		[self.navigationController pushViewController:bookDetailsViewController animated:YES];
	}
	[bookDetailsViewController release];
	
}


@end
