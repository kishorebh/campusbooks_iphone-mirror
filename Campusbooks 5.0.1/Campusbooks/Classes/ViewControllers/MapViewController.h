//
//  MapViewController.h
//  CampusBooks
//
//  Created by Admin on 19/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Reachability.h"
#import <CoreLocation/CoreLocation.h>
#import "AddressAnnotation.h"
#import "CSImageAnnotationView.h"
#import "BooksOffer.h"
#import"MyCustomButton.h"
#import"POI.h"
#import"Book.h"
#import <MessageUI/MFMailComposeViewController.h>
#import"BookListViewController.h"
#import"Reachability.h"

@interface MapViewController :UIViewController<MKMapViewDelegate,UITableViewDelegate,UITableViewDataSource, UIActionSheetDelegate, MFMailComposeViewControllerDelegate>

{	
	UITableView* tableView;
	Book* book;
	MKMapView *mapView;
	
	UIView* firstView;
	UIView* secondView;
	
	UIView *contentView;

	NSMutableArray* offerObjList;
	
	UIActivityIndicatorView* cellProgressInd;
	
	MKPinAnnotationView *newAnnotation;
	
	UISegmentedControl* segmentControl;
	CLLocation* centerLocation;
	CLLocation* farthestLocation;
	
	BooksOffer* selectedOffer;
	
	NSMutableArray* optionTitles;

	BookListViewController* bookListViewController;
	NetworkStatus internetConnectionStatus;
	UIActivityIndicatorView* progressInd;
	
	Book* bookForDetails;
    
    NSDateFormatter* originalDateFormatter;
    NSDateFormatter* newDateFormatter;
    
    int currentDisplayCriterion;
	
}
@property(nonatomic,retain)NSMutableArray* offerObjList;
@property(nonatomic,retain)NSString* conditionId;
@property NetworkStatus internetConnectionStatus;

- (void)updateStatus;
-(UISegmentedControl *)createSegmentControl;
-(void)getMapView;
-(void)showDetailsAction:(BooksOffer*)aBookOffer;
- (id)initWithCenterLocation:(CLLocation*)aCenterLocation forOffers:(NSMutableArray*)aOffersList forBook:(Book*)aBook fromBookListController:(BookListViewController*)aBookListViewController;
-(void)displayActionSheetForOffer:(BooksOffer*)aBookOffer;
-(void) centerMap;
-(void)callBookStore;
-(void)showDirections;

-(NSString*)getEmailString;
-(void)displayComposerSheet ;
-(IBAction)sendEmail:(id)sender;
-(void)viewBookStore;
-(void)populateDetailsView;
-(void)loadDetailsView;
@end
