//
//  MerchantViewController.m
//  CampusBooks
//
//  Created by Admin on 05/08/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "MerchantViewController.h"
#import"CampusBooksAppDelegate.h"

@implementation MerchantViewController
@synthesize offerUrl, internetConnectionStatus;

static int  SHOPPING_MODE_BUY = 0;
static int  SHOPPING_MODE_SELL = 1;

- (id)init
{
	self = [super init];
	if (self)
	{
		contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 420)];
		contentView.backgroundColor = [UIColor whiteColor];
		[self.view addSubview: contentView];
		[contentView release];
		
		myWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 360)];
		myWebView.backgroundColor = [UIColor whiteColor];
		myWebView.scalesPageToFit =YES;
		myWebView.delegate = self;
		[self.view addSubview: myWebView];
		
		CGRect activityViewframe = CGRectMake(145.0,175.5, 35,35);
		
		progressInd = [[UIActivityIndicatorView alloc] initWithFrame:activityViewframe];
		
		progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
		progressInd.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
										UIViewAutoresizingFlexibleRightMargin |
										UIViewAutoresizingFlexibleTopMargin |
										UIViewAutoresizingFlexibleBottomMargin);
		
		[myWebView addSubview:progressInd];
		[progressInd startAnimating];
		progressInd.hidden = NO;
		
        
		self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]initWithTitle:@"More" style:UIBarButtonItemStyleBordered target:self action:@selector(displayActionSheet:)]autorelease];

		
	}
	return self;
}

- (void)dealloc
{
		//	printf("\n In MerchantView Dealloc");
	//	[dealItem release];
	[myWebView release];
	//	[leftBarItem release];
	//	[selectedRowIndexPath release];
	[progressInd release];
	[super dealloc];
}
-(void)reloadMerchantViewWithOffer:(BooksOffer*)aBookOffer forBook:(Book*)aBook;
{
	printf("\n MerchantView URL :%s", [aBookOffer.link UTF8String]);
	currentOffer  = aBookOffer;
	currentBook = aBook;
	NSURL* url = [NSURL URLWithString:aBookOffer.link];
	[myWebView addSubview:progressInd];
	myWebView.backgroundColor = [UIColor whiteColor];
	[myWebView loadRequest:[NSURLRequest requestWithURL:url]];
	// [myWebView removeFromSuperview];
	
	self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"More" style:UIBarButtonItemStyleBordered target:self action:@selector(displayActionSheet:)] autorelease];

	if (!([currentOffer.conditionId isEqualToString:@"8"] || [currentOffer.conditionId isEqualToString:@"9"]))
	{
		self.title = aBookOffer.merchantName;

	}
	else {
		self.title = aBookOffer.storeName;

	}

}


-(void) displayActionSheet:(id) sender
{
	
	////printf("\n in the ");
	CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [[UIApplication sharedApplication]delegate];
	
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@""
															 delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil
													otherButtonTitles:@"Email Friend", @"Open in Safari", @"Cancel", nil];
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	actionSheet.destructiveButtonIndex = 2;	// make the second button red (destructive)
	
	
	[actionSheet showInView:appDelegate.window]; // show from our table view (pops up in the middle of the table)
	[actionSheet release];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
		[self sendEmail:nil];
	}
	else if (buttonIndex == 1)
	{
		[self openInSafari];
	}
	else if (buttonIndex == 2)
	{
		NSLog(@"cancel");
	}
	
}



#pragma mark UIWebView delegate methods

- (void)webViewDidStartLoad:(UIWebView *)webView
{
	//printf("\n Start loading");
//	[self displayInProgressRightBarButton];
	[progressInd startAnimating];
	// starting the load, show the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	printf("\n Completed loading");
	//	[self displayOrdinaryRightBarButton];
//	[contentView addSubview:webView];
	[progressInd removeFromSuperview];
	// finished loading, hide the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)viewWillDisappear:(BOOL) animated 
{
	if ([myWebView isLoading]) {
		[myWebView stopLoading];
	}
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
	if ([error code] == NSURLErrorCancelled) return ;

	// load error, hide the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	// report the error inside the webview
	NSString* errorString = [NSString stringWithFormat:
							 @"<html><center><font size=+5 color='red'>An error occurred: Please try again<br>%@</font></center></html>",
							 error.localizedDescription];
	[myWebView loadHTMLString:errorString baseURL:nil];
//	[contentView addSubview:webView];
	[progressInd removeFromSuperview];

}
-(NSString *)hostName
{
	// Don't include a scheme. 'http://' will break the reachability checking.
	// Change this value to test the reachability of a different host.
	return @"www.apple.com";
}

- (void)updateStatus
{
	internetConnectionStatus	= [[Reachability sharedReachability] internetConnectionStatus];
}

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

-(void)openInSafari{
	NSString* url = currentOffer.link;
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];	
	
}

-(void) doEmailContent
{
	
	NSString* emailSubject =  @"";
	
	if (currentOffer.shoppingMode == SHOPPING_MODE_BUY)
	{
		emailSubject = [NSString stringWithFormat:@" Best offer to grab your copy of '%s'" , [currentBook.title UTF8String]];
	}
	else
	{
		emailSubject = [NSString stringWithFormat:@" Best offer to get the best price for your copy of '%s'" , [currentBook.title UTF8String]];
	}
	NSString* emailContent  = [self getEmailString];
	NSString* emailURLString = [NSString stringWithFormat:@"mailto:?subject=%s&body=%s",[emailSubject UTF8String], [emailContent UTF8String]];
	emailURLString = [emailURLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	NSURL* url = [NSURL URLWithString:emailURLString];
	[[UIApplication sharedApplication] openURL:url];	
	
}


-(IBAction)sendEmail:(id)sender
{
	// This sample can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	
	//	[self showAlertForEmail];
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheet];
			//[self sendEmailFromAppOnDevice];
		}
		else
		{
			[self sendEmailFromAppOnDevice];
		}
	}
	else
	{
		[self sendEmailFromAppOnDevice];
	}
	
	
}
-(void) sendEmailFromAppOnDevice
{
	[self doEmailContent];
}
-(NSString*)getEmailString
{
	NSString* emailContent = @"";
	
	emailContent =  [NSString stringWithFormat:@"  <html><head><title></title></head><body> "];
	if (currentOffer.shoppingMode == SHOPPING_MODE_BUY)
	{
		emailContent = [emailContent stringByAppendingString:@"<p>Check out the best offer to grab your copy of  '<i>'"];
	}
	else if(currentOffer.shoppingMode = SHOPPING_MODE_SELL)
	{
		emailContent = [emailContent stringByAppendingString:@"<p>Check out the best offer to get the best price for your copy of  '<i>'"];
	}
	
	emailContent = [emailContent stringByAppendingString:currentBook.title];
	emailContent = [emailContent stringByAppendingString:@"'</i>'.</p>"];

	NSString* merchantHeader = @"";
	if ([currentOffer.conditionId isEqualToString:@"8"])
	{
		merchantHeader = @"Bookstore";
	}
	else if ([currentOffer.conditionId isEqualToString:@"9"])
	{
		merchantHeader = @"Library";
	}
	else {
			merchantHeader = @"Merchant";
	}
	
	
	NSString* addressString=@"";
	if([currentOffer.storeAddress1 length]>0)
	{
		addressString=[addressString stringByAppendingString:currentOffer.storeAddress1];
		addressString=[addressString stringByAppendingString:@"#@#"];
	}
	if([currentOffer.storeAddress2 length]>0)
	{
		addressString=[addressString stringByAppendingString:currentOffer.storeAddress2];
		addressString=[addressString stringByAppendingString:@"#@#"];
	}
	if([currentOffer.storeCity length]>0)
	{
		addressString=[addressString stringByAppendingString:currentOffer.storeCity];
		addressString=[addressString stringByAppendingString:@"#@#"];
	}
	if([currentOffer.storeState length]>0)
	{
		addressString=[addressString stringByAppendingString:currentOffer.storeState];
	}
	NSString* fullAddressString = [addressString stringByReplacingOccurrencesOfString:@"#@#" withString:@", "];
	
	NSString* addressUrlString = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%s",[fullAddressString UTF8String]];
	
	NSString* priceString = [currentOffer.totalPrice floatValue] > 0  ? [NSString stringWithFormat:@"$%.2f", [currentOffer.totalPrice floatValue]] : @" Free";
	
	if ([currentOffer.conditionId isEqualToString:@"8"] || [currentOffer.conditionId isEqualToString:@"9"])
	{
		emailContent = [emailContent stringByAppendingString:@"<table ><tr><th>"];
		emailContent = [emailContent stringByAppendingString:merchantHeader];
		emailContent = [emailContent stringByAppendingString:@"</th><th>Address</th><th>Price</th></tr>"];
		emailContent = [emailContent stringByAppendingString:@" <tr><td><a href='"];
		emailContent = [emailContent stringByAppendingString:currentOffer.link];
		emailContent = [emailContent stringByAppendingString:@"'>"];
		emailContent = [emailContent stringByAppendingString:currentOffer.storeName];
		emailContent = [emailContent stringByAppendingString:@"</a></td><td><a href='"];
		emailContent = [emailContent stringByAppendingString:addressUrlString];
		emailContent = [emailContent stringByAppendingString:@"'>"];
		emailContent = [emailContent stringByAppendingString:fullAddressString];
		emailContent = [emailContent stringByAppendingString:@"</a></td><td>"];
		emailContent = [emailContent stringByAppendingString:priceString];
		emailContent = [emailContent stringByAppendingString:@"</td></tr>"];
		emailContent = [emailContent stringByAppendingString:@"</table> "];
		
	}
	else {
		emailContent = [emailContent stringByAppendingString:@"<table ><tr><th>"];
		emailContent = [emailContent stringByAppendingString:merchantHeader];
		emailContent = [emailContent stringByAppendingString:@"</th><th>Price</th></tr>"];
		emailContent = [emailContent stringByAppendingString:@" <tr><td><a href='"];
		emailContent = [emailContent stringByAppendingString:currentOffer.link];
		emailContent = [emailContent stringByAppendingString:@"'>"];
		emailContent = [emailContent stringByAppendingString:currentOffer.merchantName];
		emailContent = [emailContent stringByAppendingString:@"</a></td><td>"];
		emailContent = [emailContent stringByAppendingString:priceString];
		emailContent = [emailContent stringByAppendingString:@"</td></tr>"];
		emailContent = [emailContent stringByAppendingString:@"</table> "];
		
	}

	emailContent = [emailContent stringByAppendingString:@"<p>This information is presented by '<a href='http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=326904377&mt=8'> CampusBooks</a>', , a price comparison tool on iPhone.<br></p>"];	
	emailContent = [emailContent stringByAppendingString:@"</body></html>"];
	
	emailContent = [emailContent stringByReplacingOccurrencesOfString:@"&" withString:@""];
	
	return emailContent;
}
#pragma mark -
#pragma mark Compose Mail

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
	NSString* emailSubject = @"";
	
	if (currentOffer.shoppingMode == SHOPPING_MODE_BUY)
	{
		emailSubject = [NSString stringWithFormat:@" Best offer to grab your copy of '%s'" , [currentBook.title UTF8String]];
	}
	else if (currentOffer.shoppingMode == SHOPPING_MODE_SELL)
	{
		emailSubject = [NSString stringWithFormat:@" Best offer to get the best price for your copy of '%s'" , [currentBook.title UTF8String]];
	}
	
	NSString* emailContent = [self getEmailString];
	
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	
	[picker setSubject:emailSubject];
	
	
	// Attach an image to the email
	NSString *path = [[NSBundle mainBundle] pathForResource:@"icon" ofType:@"png"];
	NSData *myData = [NSData dataWithContentsOfFile:path];
	[picker addAttachmentData:myData mimeType:@"image/png" fileName:@"iBookStore"];
	
	// Fill out the email body text
	NSString *emailBody = emailContent;
	[picker setMessageBody:emailBody isHTML:YES];
	
	[self presentModalViewController:picker animated:YES];
    [picker release];
}


// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			//	message.text = @"Result: saved";
			break;
		case MFMailComposeResultSent:
		{
			UIAlertView* alert  = [[UIAlertView alloc] initWithTitle:@"CampusBooks" message:@"Email is sent sucesfully" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] ;
			[alert show];
			[alert release];
			
			break;
		}
		case MFMailComposeResultFailed:
		{
			UIAlertView* alert  = [[UIAlertView alloc] initWithTitle:@"CampusBooks" message:@"Unable to send email. Please try again" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil] ;
			[alert show];
			[alert release];
			break;
		}
		default:
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}

@end
