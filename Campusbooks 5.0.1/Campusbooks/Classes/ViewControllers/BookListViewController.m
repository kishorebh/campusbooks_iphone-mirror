//
//  BookListViewController.m
//  CampusBooks
//
//  Created by Admin on 19/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "BookListViewController.h"
#import"CampusBooksAppDelegate.h"
#import "BuyOptionsViewController.h"
@implementation BookListViewController
@synthesize internetConnectionStatus, booksList, sourceViewController,currentCategory; 
#define kTransitionDuration	1

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithBooksList:(NSMutableArray*) aBooksList  fromSource:(UIViewController*)aSourceViewController{
    if (self = [super init]) {
        // Custom initialization
		coversView = [[UIView alloc]  initWithFrame:CGRectMake(0,0,320,390)];
		coversView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
		[self.view addSubview:coversView];

		CGRect activityViewframe = CGRectMake(150.0,0, 35,35);
		progressInd = [[UIActivityIndicatorView alloc] initWithFrame:activityViewframe];
		progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
		progressInd.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
										UIViewAutoresizingFlexibleRightMargin |
										UIViewAutoresizingFlexibleTopMargin |
										UIViewAutoresizingFlexibleBottomMargin);
		[progressInd startAnimating];
		
		self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:progressInd ] autorelease];


		noItemsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, 320,50)];
		noItemsLabel.text = @"You have no items in your wishlist";
		noItemsLabel.numberOfLines = 0;
		noItemsLabel.textAlignment = UITextAlignmentCenter;
		noItemsLabel.textColor =[UIColor darkTextColor];// [UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];;
		noItemsLabel.font = [UIFont boldSystemFontOfSize:18];
		noItemsLabel.backgroundColor = [UIColor clearColor];
		
		titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 210, 300,35)];
		byLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 242, 300,14)];
		authorLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 255, 300,16)];
		isbnLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 265, 310,20)];

		flowCoverView = [[FlowCoverView alloc]initWithFrame:CGRectMake(0, -85, 320, 420)];
		flowCoverView.delegate = self;
		
		bookDetailsViewController = [[BookDetailsViewController alloc] initWithBook:nil  fromSource:self];
		bookBuyViewController = [[BookBuyViewController alloc] initWithBook:nil  withOffers:nil fromSource:self];
		bookSellViewController = [[BookSellViewController alloc] initWithBook:nil  withOffers:nil fromSource:self];
		
		detailsButton = [[UIButton alloc] initWithFrame:CGRectMake(105,10, 105,150)];
		[detailsButton setImage:[UIImage imageNamed:@"transparent.png"] forState:UIControlStateNormal];	
		detailsButton.backgroundColor = [UIColor clearColor];
		[detailsButton addTarget:self action:@selector(displayActionSheet:) forControlEvents:UIControlEventTouchUpInside];
		
		moreResultsButton = [[UIButton alloc] initWithFrame:CGRectMake(113,330,129,39)];
		[moreResultsButton setImage:[UIImage imageNamed:@"more_titles1.png"] forState:UIControlStateNormal];
		[moreResultsButton addTarget:self action:@selector(loadMoreResults) forControlEvents:UIControlEventTouchUpInside];
		
		actionSheetItems = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
	CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [[UIApplication sharedApplication]delegate];
	if(sourceViewController == appDelegate.bookBagViewController && appDelegate.bookBagViewController.selectedType == 1)
	{
		booksList = [appDelegate getMyWishList:0];	
		[self refreshBookListViewController:booksList fromSource:sourceViewController];
	}
}

-(void)refreshBookListViewController:(NSMutableArray*)aBooksList fromSource:(UIViewController*)aSourceViewController
{
	for (UIView* subView in coversView.subviews)
	{
		[subView removeFromSuperview];
	}
	[actionSheetItems removeAllObjects];
	
	booksList = aBooksList;
	sourceViewController = aSourceViewController;
	[coversView addSubview:titleView];

	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	
	if( (sourceViewController == appDelegate.searchViewController  || sourceViewController == appDelegate.homeViewController ))
	{
        isDisplayingWishList = NO;
		self.title  = @"Search Results";
	}
	else if ([booksList count] >= 10 )
	{
        isDisplayingWishList = YES;
		self.title  = @"Books List";
	}
	else 
	{
		self.title = @"Book Info";
	}


	if(sourceViewController == appDelegate.bookBagViewController && appDelegate.bookBagViewController.selectedType == 1 && [booksList count] ==0)
	{
		[coversView addSubview: noItemsLabel];
	}
	
	UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 48, 335, 7)];
	imageView.image = [UIImage imageNamed:@"separation line.png"];
	[coversView addSubview:imageView];
	[imageView release];
	
	UIImageView* imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 325, 335, 7)];
	imageView1.image = [UIImage imageNamed:@"separation line.png"];
	[coversView addSubview:imageView1];
	[imageView1 release];

	if(sourceViewController == appDelegate.bookBagViewController)
	{
        isDisplayingWishList=YES;

		if(appDelegate.bookBagViewController.selectedType == 1)
		{
			self.title = @"Wishlist";
		}
		else if(appDelegate.bookBagViewController.selectedType == 2)
		{
			self.title = @"Collection";
		}
	}
	currentPage = 1;
	
	[progressInd startAnimating];
	progressInd.hidden = NO;
	[progressInd stopAnimating];
	
	[coversView addSubview:flowCoverView];

	BookCoverLoader *coverLoader= [BookCoverLoader sharedLoader];

	for(Book* book in booksList)
	{
		book.image = [coverLoader getBookCover:book delegate:self];
		if (book.image == nil)
		{
			book.isImageLoaded = NO;
			book.image = [UIImage imageNamed:@"LoadingCover.png"];
		}
		else
		{
			book.isImageLoaded = YES;
		}
	}
	[flowCoverView reDraw:YES];
	if([booksList count] > 0)
	{
		currentBook = [booksList objectAtIndex:0];
		[self displayTitles];
	}
	
}
-(void)reloadToolbars
{
	[actionSheetItems removeAllObjects];
	
	[actionSheetItems addObject:@"Book Details"];
	[actionSheetItems addObject:@"Compare Prices"];
	[actionSheetItems addObject:@"Sell It"];

    if(isDisplayingWishList==NO && selectedImageIndex == [booksList count] -1)
    {
        [actionSheetItems addObject:@"More Titles"];
    }

	if(!currentBook.isInWishList)
	{
		[actionSheetItems addObject:@"Add to Wishlist"];
	}
	else
	{
		[actionSheetItems addObject:@"Remove from Wishlist"];
	}
	
	[actionSheetItems addObject:@"Cancel"];
	
}
/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */

/*
 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad {
 [super viewDidLoad];
 }
 */

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
		//	printf("\n In BooksListView Dealloc");
    [super dealloc];
}

-(void)populateDetailsView
{
	[self updateStatus];
	
	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!" message:@"No Network Available"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		
	}
	else
	{
		
	//	@try {
			
			[progressInd startAnimating];
		//	[detailsButton setImage:[UIImage imageNamed:@"transparent-clicked.png"] forState:UIControlStateNormal];

			[NSThread detachNewThreadSelector:@selector(doPopulateDetailsView:) toTarget:self withObject:nil];
/*			
		}
		@catch (NSException * e) {
			printf("\n caught an exception");
		}
		@finally {
			
		}
*/		
	}
}

- (void)doPopulateDetailsView:(id)sender
{
//	@try {
		
		NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
		CampusBooksAppDelegate* appDelegate =   (CampusBooksAppDelegate*)[ [UIApplication sharedApplication] delegate];
		
		Book* currentBookInfo =	nil;
		if(sourceViewController == appDelegate.homeViewController || sourceViewController == appDelegate.searchViewController || sourceViewController == appDelegate.scanHistoryViewController)
		{
			//printf("\n Calling objects for Search");
			currentBookInfo = [appDelegate getBookInfoForSearch:currentBook.isbnId10];
		//	bookOffersList = [appDelegate getBookOffersListForSearch:currentBook.isbnId10];
		}
		else if (sourceViewController ==  appDelegate.bookBagViewController)
		{
			//printf("\n Calling objects for Bookbag");
			currentBookInfo = [appDelegate getBookInfoForBookBag:currentBook.isbnId10];
		///	bookOffersList = [appDelegate getBookOffersListForBookBag:currentBook.isbnId10];
		}
//		if (currentBookInfo.isbnId13 != nil)
//		{
			
			BookCoverLoader* coverLoader = [BookCoverLoader sharedLoader];
			currentBookInfo.image = [coverLoader getBookCover:currentBookInfo delegate:self];
			currentBookInfo.isInWishList = [appDelegate isItemInWishList:currentBookInfo.isbnId10];
			currentBookInfo.isInMyCollection = [appDelegate isItemInMyCollection:currentBookInfo.isbnId10];
//			[bookDetailsViewController refreshBookDetailsViewController:currentBookInfo withOffers:bookOffersList fromSource:self];
			[bookDetailsViewController refreshBookDetailsViewController:currentBookInfo fromSource:self];
			
			if ([self.navigationController topViewController] == bookDetailsViewController)
			{
				//printf("\n cancelling the push");
			}
			else
			{
				[self.navigationController pushViewController:bookDetailsViewController animated:YES];
			}
	//		[detailsViewControler release];
/*		}
		else
		{
			UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:@"Unable to retrieve the book details .  \nPlease try another book."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
			[alertView show];
		}
*/		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		
		[progressInd stopAnimating];
		
		[pool release];	
		[NSThread exit];
/*	}
	@catch (NSException * e) {
		[e 
	}
	@finally {
	}
 */
}


-(void)loadMoreResults
{
	[self updateStatus];
	
	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!" message:@"No Network Available"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		
	}
	else
	{
//		printf("\n Load more results action");
		//	@try {
		
		currentPage++;
		CampusBooksAppDelegate* appDelegate =  (CampusBooksAppDelegate*) [ [UIApplication sharedApplication] delegate];
		if(sourceViewController == appDelegate.searchViewController )
		{
			SearchViewController* viewController = appDelegate.searchViewController;
			NSString* strKeyword = viewController.strKeyword;
			NSString* strIsbn = viewController.strIsbn;
			NSString* strAuthor = viewController.strAuthor;
			NSString* strTitle = viewController.strTitle;
			
			booksList = [appDelegate doBookSearch:strTitle byAuthor:strAuthor withKeyword:strKeyword forISBN:strIsbn forPage:currentPage];
		//	[booksList sortUsingSelector:@selector(compareByRankAndRating:)];
		}
		else if(sourceViewController == appDelegate.homeViewController)
		{
			HomeViewController* viewController = appDelegate.homeViewController;
			NSString* strKeyword = viewController.strKeyword;
			/*			NSString* strIsbn = viewController.strIsbn;
			 NSString* strAuthor = viewController.strAuthor;
			 NSString* strTitle = viewController.strTitle;
			 */			
			booksList = [appDelegate doBookSearch:nil byAuthor:nil withKeyword:strKeyword forISBN:nil forPage:currentPage];
			//	[booksList sortUsingSelector:@selector(compareByRankAndRating:)];
		}
		else if(sourceViewController == appDelegate.bookBagViewController)
		{
			NSInteger offset = (currentPage - 1) * 10;
			BookBagViewController* viewController =  appDelegate.bookBagViewController;
			NSInteger selectedType = viewController.selectedType;
			if(selectedType == 1)
			{
				booksList = [appDelegate getMyWishList:offset];
			}
			else
			{
				booksList = [appDelegate getMyCollection:offset];
			}
		}
		
		BookCoverLoader* coverLoader = [BookCoverLoader sharedLoader];
		for(Book* book in booksList)
		{
			//printf("\n Image Source:%s for :%s", [book.imageUrl UTF8String], [book.isbnId10 UTF8String]);
			book.image = [coverLoader getBookCover:book delegate:self];
			if (book.image == nil)
			{
				book.isImageLoaded = NO;
				book.image = [UIImage imageNamed:@"LoadingCover.png"];
			}
			else
			{
				book.isImageLoaded = YES;
			}
		}
		[flowCoverView reDraw:NO];

		
		/*			
		 }
		 @catch (NSException * e) {
		 printf("\n caught an exception");
		 }
		 @finally {
		 
		 }
		 */		
	}
}


/************************************************************************/
/*																		*/
/*	FlowCover Callbacks													*/
/*																		*/
/************************************************************************/

- (int)flowCoverNumberImages:(FlowCoverView *)view
{
	return [booksList count];
}

- (UIImage *)flowCover:(FlowCoverView *)view cover:(int)image
{
//	printf("flowCover cover");
	if( image <[booksList count])
	{
		Book* book = [booksList objectAtIndex:image];
		return book.image;
	}
	return [UIImage imageNamed:@"MissingCover.png"];
	
}
-(void)displayTitles
{
	//printf("In Display Title");	
	
	[titleLabel removeFromSuperview];
	[byLabel removeFromSuperview];
	[authorLabel removeFromSuperview];
	[isbnLabel removeFromSuperview];
	[moreResultsButton removeFromSuperview];
	[detailsButton removeFromSuperview];
	
	if ([booksList count] >0)
	{

		CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [[UIApplication sharedApplication]delegate];
		
		
		float additionalInfoLabelStart = 200;
		float titleLabelStart = additionalInfoLabelStart +  5;;
		float titleLabelHeight =  [self getTextHeightForSystemFontOfSize:15 ofWidth:300 forText:currentBook.title];
		titleLabel.frame = CGRectMake(10, titleLabelStart, 300,titleLabelHeight);

		float byLabelStart = titleLabelStart + titleLabelHeight + 5;
		float byLabelHeight =  [self getTextHeightForSystemFontOfSize:11 ofWidth:300 forText:@"by"];
		byLabel.frame = CGRectMake(10, byLabelStart, 300,byLabelHeight);

		float authorLabelStart = byLabelStart + byLabelHeight + 5;
		float authorLabelHeight =  [self getTextHeightForSystemFontOfSize:14 ofWidth:300 forText:currentBook.author];
		authorLabel.frame = CGRectMake(10, authorLabelStart, 300,authorLabelHeight);

		float isbnLabelStart = authorLabelStart + authorLabelHeight + 5;
		float isbnLabelHeight =  [self getTextHeightForSystemFontOfSize:15 ofWidth:300 forText:currentBook.isbnId10];
		isbnLabel.frame = CGRectMake(10, isbnLabelStart, 300,isbnLabelHeight);

		titleLabel.numberOfLines = 0;
		titleLabel.adjustsFontSizeToFitWidth = YES;
		titleLabel.font = [ UIFont boldSystemFontOfSize:15];
		titleLabel.text = currentBook.title;
		titleLabel.backgroundColor = [UIColor clearColor];
		titleLabel.textAlignment = UITextAlignmentCenter;
		titleLabel.textColor = [UIColor darkTextColor];//[UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];
		[coversView addSubview:titleLabel];
		
		//printf("\n title in list %s", [currentBook.title UTF8String]);

		byLabel.numberOfLines = 0;
		byLabel.font = [ UIFont boldSystemFontOfSize:11];
		byLabel.text = @"by";
		byLabel.backgroundColor = [UIColor clearColor];
		byLabel.textAlignment = UITextAlignmentCenter;
		byLabel.textColor = [UIColor darkTextColor];//[UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];
		[coversView addSubview:byLabel];
		
		authorLabel.numberOfLines = 0;
		authorLabel.adjustsFontSizeToFitWidth = YES;
		authorLabel.font = [ UIFont boldSystemFontOfSize:14];
		authorLabel.text = currentBook.author;
		authorLabel.backgroundColor = [UIColor clearColor];
		authorLabel.textAlignment = UITextAlignmentCenter;
		authorLabel.textColor = [UIColor darkTextColor];//[UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];
		[coversView addSubview:authorLabel];
		
		
		NSString* isbnStr = [NSString stringWithFormat:@"ISBN - %s", [currentBook.isbnId10 UTF8String]];
		
		isbnLabel.numberOfLines = 0;
		isbnLabel.font = [ UIFont boldSystemFontOfSize:14];
		isbnLabel.backgroundColor = [UIColor clearColor];
		isbnLabel.textAlignment = UITextAlignmentCenter;
		isbnLabel.textColor = [UIColor darkTextColor];//[UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];
		isbnLabel.text = isbnStr;
		[coversView addSubview:isbnLabel];
		
		if( (sourceViewController == appDelegate.searchViewController   
			 || sourceViewController == appDelegate.homeViewController || sourceViewController == appDelegate.bookBagViewController) && [booksList count]%10 ==0 && selectedImageIndex == [booksList count] -1)
		{
			[coversView addSubview:moreResultsButton];
		}

		[coversView addSubview:detailsButton];
		
		currentBook.isInWishList = [appDelegate isItemInWishList:currentBook.isbnId10];
		currentBook.isInMyCollection = [appDelegate isItemInMyCollection:currentBook.isbnId10];
		[self reloadToolbars];	
	}	
}
- (void)flowCover:(FlowCoverView *)view didChangeImage:(int)image
{	
	//printf("\n FlowCover didChangeImage");
	if ([booksList count] >0)
	{
		currentBook  = [booksList objectAtIndex:image];
		selectedImageIndex = image;
	}
	[self displayTitles];
}	

- (void)flowCover:(FlowCoverView *)view didSelect:(int)image
{
	if ([booksList count] >0)
	{
		currentBook  = [booksList objectAtIndex:image];
	}
	[self populateDetailsView];
}
-(void)didReceiveBookCover:(NSString*)aIsbn
{
	BookCoverLoader *coverLoader= [BookCoverLoader sharedLoader];
	
	for(Book* book in booksList)
	{
		if([book.isbnId10 isEqualToString:aIsbn])
		{
			//printf("\n Loaded an image");
			book.image = [coverLoader getBookCover:book delegate:self];
			if (book.image == nil ||  book.image == [UIImage imageNamed:@"LoadingCover.png"])
			{
				//printf("\n image retrieval in progress");
				book.isImageLoaded = NO;
				book.image = [UIImage imageNamed:@"LoadingCover.png"];
			}
			else
			{
				//printf("\n image is retrieved for %s" , [book.isbnId10 UTF8String]);
				book.isImageLoaded = YES;
			}
		}
	}
	BOOL areAllImagesLoaded = YES;
	for(Book* book in booksList)
	{
		if(!book.isImageLoaded)
		{
			//printf("\n not reloading the covers as cover for %s is not loaded", [book.isbnId10 UTF8String] );
			areAllImagesLoaded = NO;
			break;
		}
	}
	if(areAllImagesLoaded)
	{
		//printf("\n Loading all images at once");
		[flowCoverView reDraw:NO];
	}
	else
	{
		//printf("\n Wait as not all images are loaded");
	}
}
-(NSString *)hostName
{
	// Don't include a scheme. 'http://' will break the reachability checking.
	// Change this value to test the reachability of a different host.
	return @"www.apple.com";
}
- (void)updateStatus
{
	
	self.internetConnectionStatus	= [[Reachability sharedReachability] internetConnectionStatus];
	//	[tableView reloadData];
}
- (CGFloat)getTextHeightForSystemFontOfSize:(CGFloat)size ofWidth:(CGFloat)maxWidth forText:(NSString*)aText {
    CGFloat maxHeight = 9999;
    CGSize maximumLabelSize = CGSizeMake(maxWidth,maxHeight);
	
    CGSize expectedLabelSize = [aText sizeWithFont:[UIFont systemFontOfSize:size] constrainedToSize:maximumLabelSize lineBreakMode:UILineBreakModeWordWrap]; 
	
    return expectedLabelSize.height;
}

-(void) displayActionSheet:(id) sender
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate]; 
	UIActionSheet *actionSheet = nil;
	
	actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil
									 otherButtonTitles:nil];

	
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	actionSheet.destructiveButtonIndex = actionSheet.numberOfButtons -1 ;	
	actionSheet.delegate = self;
	
	for (NSString* actionItem in actionSheetItems)
	{
		[actionSheet addButtonWithTitle:actionItem];
	}
	
	[actionSheet showInView:appDelegate.window];
	[actionSheet release];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	NSString* optionTitle = [actionSheetItems objectAtIndex:buttonIndex];
	
	if ([optionTitle isEqualToString:@"Book Details"])
	{
		[self populateDetailsView];
	}
	else if ([optionTitle isEqualToString:@"Compare Prices"])
	{
		[self buyItAction];
	}
	else if ([optionTitle isEqualToString:@"Sell It"])
	{
		[self sellItAction];
	}
	else if ([optionTitle isEqualToString:@"Add to Wishlist"])
	{
		[self addToWishlistAction];
	}
	else if ([optionTitle isEqualToString:@"Remove from Wishlist"])
	{
		[self removeFromWishList];
	}
	else if ([optionTitle isEqualToString:@"More Titles"])
	{
		[self loadMoreResults];
	}
	else if ([optionTitle isEqualToString:@"Cancel"])
	{
		NSLog(@"Cancel");
	}
	
	

}



-(void)buyItAction
{
	[self updateStatus];
	
	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!" message:@"No Network Available"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		
	}
	else
	{
		
		//	@try {
		
		[progressInd startAnimating];
		//	[detailsButton setImage:[UIImage imageNamed:@"transparent-clicked.png"] forState:UIControlStateNormal];
		
		[NSThread detachNewThreadSelector:@selector(doPopulateBuyView:) toTarget:self withObject:nil];
		/*			
		 }
		 @catch (NSException * e) {
		 printf("\n caught an exception");
		 }
		 @finally {
		 
		 }
		 */		
	}
}

- (void)doPopulateBuyView:(id)sender
{
	//	@try {
	
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	CampusBooksAppDelegate* appDelegate =  (CampusBooksAppDelegate*) [ [UIApplication sharedApplication] delegate];
	
	NSMutableArray* bookOffersList = nil;
	if(sourceViewController == appDelegate.homeViewController || sourceViewController == appDelegate.searchViewController || sourceViewController == appDelegate.scanHistoryViewController)
	{
		bookOffersList = [appDelegate getBookOffersListForSearch:currentBook.isbnId10];
	}
	else if (sourceViewController ==  appDelegate.bookBagViewController)
	{
		bookOffersList = [appDelegate getBookOffersListForBookBag:currentBook.isbnId10];
	}
	
	BookCoverLoader* coverLoader = [BookCoverLoader sharedLoader];
	currentBook.image = [coverLoader getBookCover:currentBook delegate:self];
	
	LocalOptions* localOptions =[appDelegate getLocalOptions];;
	if (localOptions.optionsValue ==0 )
	{
		//printf("\n Loading BookBuyViewController");
		[bookBuyViewController refreshBookBuyViewController:currentBook withOffers:bookOffersList  fromSource:self];
		
		if ([self.navigationController topViewController] == bookBuyViewController)
		{
			//printf("\n cancelling the push");
		}
		else
		{
			[self.navigationController pushViewController:bookBuyViewController animated:YES];
		}
	}
	else 
	{
		//printf("\n Loading BuyOptionsViewController");
		BuyOptionsViewController* optionsViewController = [[BuyOptionsViewController alloc]initWithBook:currentBook withMode:0 withOffers:bookOffersList fromSource:self];
		[optionsViewController refreshBuyOptionsViewController:currentBook withMode:0 withOffers:bookOffersList fromSource:self];
		if ([self.navigationController topViewController] == optionsViewController)
		{
			//printf("\n cancelling the push");
		}
		else
		{
			[self.navigationController pushViewController:optionsViewController animated:YES];
		}
		[optionsViewController release];
		
	}

	//		[detailsViewControler release];
	/*		}
	 else
	 {
	 UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:@"Unable to retrieve the book details .  \nPlease try another book."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
	 [alertView show];
	 }
	 */		
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	[progressInd stopAnimating];
	
	[pool release];	
	[NSThread exit];
	/*	}
	 @catch (NSException * e) {
	 [e 
	 }
	 @finally {
	 }
	 */
}

-(void)sellItAction
{
	[self updateStatus];
	
	if (self.internetConnectionStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!" message:@"No Network Available"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];	
		[alert release];
		
	}
	else
	{
		
		//	@try {
		
		[progressInd startAnimating];
		//	[detailsButton setImage:[UIImage imageNamed:@"transparent-clicked.png"] forState:UIControlStateNormal];
		
		[NSThread detachNewThreadSelector:@selector(doPopulateSellView:) toTarget:self withObject:nil];
		/*			
		 }
		 @catch (NSException * e) {
		 printf("\n caught an exception");
		 }
		 @finally {
		 
		 }
		 */		
	}
}

- (void)doPopulateSellView:(id)sender
{
	//	@try {
	
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	CampusBooksAppDelegate* appDelegate =  (CampusBooksAppDelegate*) [ [UIApplication sharedApplication] delegate];
	
	NSMutableArray* bookOffersList = nil;
	if(sourceViewController == appDelegate.homeViewController || sourceViewController == appDelegate.searchViewController || sourceViewController == appDelegate.scanHistoryViewController)
	{
		bookOffersList = [appDelegate getBookBuybackOffersListForSearch:currentBook.isbnId10];
	}
	else if (sourceViewController ==  appDelegate.bookBagViewController)
	{
		bookOffersList = [appDelegate getBookBuybackOffersListForBookBag:currentBook.isbnId10];
	}
	
	BookCoverLoader* coverLoader = [BookCoverLoader sharedLoader];
	currentBook.image = [coverLoader getBookCover:currentBook delegate:self];
	[bookSellViewController refreshBookSellViewController:currentBook withOffers:bookOffersList  fromSource:self];
	
	if ([self.navigationController topViewController] == bookSellViewController)
	{
		//printf("\n cancelling the push");
	}
	else
	{
		[self.navigationController pushViewController:bookSellViewController animated:YES];
	}
	//		[detailsViewControler release];
	/*		}
	 else
	 {
	 UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:@"Unable to retrieve the book details .  \nPlease try another book."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
	 [alertView show];
	 }
	 */		
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	[progressInd stopAnimating];
	
	[pool release];	
	[NSThread exit];
	/*	}
	 @catch (NSException * e) {
	 [e 
	 }
	 @finally {
	 }
	 */
}

-(void)addToWishlistAction
{
	WishList* wishList = [[WishList alloc] init];
	wishList.authorName = currentBook.author;
	wishList.bookName = currentBook.title;
	wishList.isbnId10 = currentBook.isbnId10;
	wishList.imageUrl = currentBook.imageUrl;
	
	CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [[UIApplication sharedApplication]delegate];
	[appDelegate addToWishList:wishList];
	[wishList release];
	
	NSString* aMessage = [NSString stringWithFormat:@"'%s' has been successfully added to your wishlist.", [currentBook.title UTF8String]];
	
	UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:aMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
	[alertView show];
	
	currentBook.isInWishList = YES;
	[self reloadToolbars];

	//currentBook.isInMyCollection = NO;
}
-(void)removeFromWishList
{
	
	CampusBooksAppDelegate* appDelegate =(CampusBooksAppDelegate*) [[UIApplication sharedApplication]delegate];
	[appDelegate removeFromWishList:currentBook.isbnId10];
	
	NSString* aMessage = [NSString stringWithFormat:@"'%s' has been successfully removed from your wishlist.", [currentBook.title UTF8String]];
	
	UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:aMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
	[alertView show];
	currentBook.isInWishList = NO;
	[self reloadToolbars];

}
-(void)addToCollectionAction
{
	
	MyCollection* collection = [[MyCollection alloc] init];
	collection.authorName = currentBook.author;
	collection.bookName = currentBook.title;
	collection.isbnId10 = currentBook.isbnId10;
	collection.imageUrl = currentBook.imageUrl;
	
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	[appDelegate addToMyCollection:collection];
	[collection release];
	
	NSString* aMessage = [NSString stringWithFormat:@"'%s' has been successfully added to your collection.", [currentBook.title UTF8String]];
	
	UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:aMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
	[alertView show];
	
	currentBook.isInMyCollection = YES;
	[self reloadToolbars];

}

-(void)removeFromCollectionAction
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	[appDelegate removeFromMyCollection:currentBook.isbnId10];
	
	NSString* aMessage = [NSString stringWithFormat:@"'%s' has been successfully removed from your collection.", [currentBook.title UTF8String]];
	
	UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:@"CampusBooks" message:aMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
	[alertView show];
	currentBook.isInMyCollection = NO;
	[self reloadToolbars];

}

@end
