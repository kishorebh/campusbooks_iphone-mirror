//
//  SchoolsListViewController.h
//  CampusBooks
//
//  Created by Admin on 17/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"Schools.h"
#import"LocalOptions.h"
@interface SchoolsListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UISearchBarDelegate>{
	
	UITextField *titleField;
	UITableView* theTableView;
	Schools* schoolObj;
	UIImageView* imageView;
	BOOL isfirstTime;
	NSMutableDictionary* masterCategoryListDictionary;
	NSMutableArray* filteredListContent;
	NSMutableArray* masterCategoryListIndexArray;
	UISearchBar* mySearchBar;
	UIView  *subView1;;
	NSString* selctedString;
	LocalOptions* object;
}
@property(nonatomic,retain)LocalOptions* object;

-(void)setToPortrait:(BOOL)isPortrait;
-(void) organizeAisleItemsIntoIndexes;

@end