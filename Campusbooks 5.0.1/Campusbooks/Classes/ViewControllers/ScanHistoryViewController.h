//
//  Pic2BarCodeScanViewController.h
//  CampusBooks
//
//  Created by Admin on 21/11/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"MyCustomButton.h"
#import"ScanItem.h"
#import"ScanList.h"
@interface ScanHistoryViewController : UIViewController<UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate> {
//	IBOutlet OverlayController * overlayController;
	UIView* contentView;
	UIButton* singleScanButton;
	BOOL allowScan;
	
	NSMutableArray* currScanedBooksList;
	
	NSMutableArray* scanListList;
	UITableView* tableView;
	NSDateFormatter* dateFormatter;
	
	UILabel* noItemsLabel;
	NSMutableArray* booksList;

}
-(void)displayBooksFromScanList:(ScanList*)aScanList;
-(NSMutableArray*)getBooksListFromScannedItems:(NSMutableArray*)aScanItemsList;
@end
