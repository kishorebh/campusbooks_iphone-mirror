//
//  PopViewController.h
//  CampusBooks
//
//  Created by Admin on 17/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"LocalOptions.h"

@interface PopViewController :  UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
	UITableView *theTableView;
	UITextField* zipCodeField;
	UIView *myView;
	UIView* subView;
//	BOOL isSelected;
	int indexValue;
	BOOL isMoveUp;
	LocalOptions* localOptions;
	UILabel *schoolNameLabel;
	UILabel* radiusLabel;
	BOOL isTimerIntiated;
	
	UIActivityIndicatorView* activityIndicator;
	UILabel* fetchingLabel;
	NSIndexPath* selectedIndexPath;
}
-(void)setViewMovedUp:(BOOL)movedUp;
-(void)updateLocalOptionsAndClose;
-(void)populateSchoolsView;
@end