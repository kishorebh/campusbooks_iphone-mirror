//
//  BookOffersViewController.h
//  CampusBooks
//
//  Created by Admin on 19/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"Book.h"
#import"BooksOffer.h"
#import"TransitionView.h"
#import"HTMLUtil.h"
#import"Reachability.h"
#import"MerchantViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface BookOfferViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, UIActionSheetDelegate,MFMailComposeViewControllerDelegate>{
	UIView* contentView;
	UITableView* tableView;
	Book* currentBook;
	BooksOffer* offer;
	UIViewController* bookDetailsViewController;
	UIActivityIndicatorView* progressInd;
	NetworkStatus internetConnectionStatus;
	
	MerchantViewController* merchantViewController ;
    
    NSDateFormatter* originalDateFormatter;
    NSDateFormatter* newDateFormatter;
}
@property NetworkStatus internetConnectionStatus;
- (void)updateStatus;
-(NSString *)hostName;
- (CGFloat)getTextHeightForSystemFontOfSize:(CGFloat)size ofWidth:(CGFloat)maxWidth forText:(NSString*)aText ;
-(void)refreshBookOfferViewController:(BooksOffer*)aOffer  forBook:(Book*)aBook fromSource:(UIViewController*)aSourceViewController;
-(id)initWithOffer:(BooksOffer*)aOffer  forBook:(Book*)aBook fromSource:(UIViewController*)aSourceViewController;

@end
