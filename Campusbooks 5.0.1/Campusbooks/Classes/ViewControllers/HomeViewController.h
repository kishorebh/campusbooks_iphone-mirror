//
//  HomeViewController.h
//  Campusbooks
//
//  Created by sekhar bethalam on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"Reachability.h"
#import"BookListViewController.h"
#import"UIDevice-hardware.h"

@interface HomeViewController : UIViewController<UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIAlertViewDelegate>
{
	UIView *contentView;
	
    //	UITableView* theTableView;
    
	IBOutlet UIButton* advancedSearchButton;
	
	UIImageView* imageView;
	UILabel* compareLabel ;
	UILabel* orLabel;
	UILabel* barcodeLabel;
    
	IBOutlet UITextField* searchTextField;
    IBOutlet UIButton* goButton;
    IBOutlet UIImageView* orImageLine;
    //	NSInteger currentSearchSelection;
	NetworkStatus internetConnectionStatus;
    
	UIActivityIndicatorView* progressInd;
	BOOL isSearchInProgress;
    
	NSString* strKeyword;
    /*	NSString* strIsbn;
     NSString* strTitle;
     NSString* strAuthor;
     */	
	BookListViewController* bookListViewController;
	
	IBOutlet UIButton* pic2ShopScanButton;
	UIAlertView* analyticsAlert;
	IBOutlet UIButton* localOptionsButton;
	BOOL isLoadOptionsOnWillAppear;
}
@property NetworkStatus internetConnectionStatus;
/*
 @property(nonatomic, retain) NSString* strTitle;
 @property(nonatomic, retain) NSString* strAuthor;
 @property(nonatomic, retain) NSString* strKeyword;
 @property(nonatomic, retain) NSString* strIsbn;
 */
@property(nonatomic, retain)NSString* strKeyword;
- (void)updateStatus;
-(NSString *)hostName;
-(void)refreshView;

-(IBAction)displayAdvancedSearch:(id)sender;
-(IBAction)displayP2SBarCodeReaderScreen:(id)sender;
-(IBAction)displayLocalOptions:(id)sender;
-(IBAction)go:(id)sender;
@end
