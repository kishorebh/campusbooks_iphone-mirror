    //
//  PopViewController.m
//  CampusBooks
//
//  Created by Admin on 17/07/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "PopViewController.h"
#import "CampusBooksAppDelegate.h"
#import "SchoolsListViewController.h"
#import"RadiusPickerViewControlller.h"
#import"MapViewController.h"
#define kOFFSET_FOR_KEYBOARD 100.0
@implementation PopViewController

- (id)init {
	if (self = [super init]) {
		
		
		self.title=@"Local Options";
		
		UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]
											   initWithTitle:@"Done"
											   style:UIBarButtonItemStyleBordered	
											   target:self
											   action:@selector(Done:)];
		
		self.navigationItem.rightBarButtonItem = rightBarButtonItem;
		[rightBarButtonItem release];
		
		myView = [[UIView alloc] initWithFrame:CGRectMake(0,5,020,440)];
		myView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
		myView.autoresizesSubviews = YES;
		
		theTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,0, 320, 440) style:UITableViewStyleGrouped];	
		theTableView.rowHeight = 45;
		theTableView.delegate = self;
		theTableView.dataSource = self;
		theTableView.sectionFooterHeight=20;
		theTableView.sectionHeaderHeight=20;
		theTableView.backgroundColor=[UIColor clearColor];

		[myView addSubview:theTableView];
		

		indexValue=5;
		zipCodeField = [[UITextField alloc] initWithFrame:CGRectMake(120,11, 160, 30)];
		zipCodeField .borderStyle = UITextBorderStyleNone;
		zipCodeField.textColor = [UIColor blackColor];//[[[UIColor alloc]initWithRed:0.250 green:0.300 blue:0.30 alpha:1.0]autorelease];;
		zipCodeField.backgroundColor = [UIColor whiteColor];
		zipCodeField.font = [UIFont boldSystemFontOfSize:17];
		zipCodeField.delegate=self;
		zipCodeField.textAlignment=UITextAlignmentRight;
		zipCodeField.placeholder=@"Enter Zipcode";
		zipCodeField.textColor =[UIColor darkTextColor];
		zipCodeField.autocorrectionType = UITextAutocorrectionTypeNo;	
		zipCodeField.keyboardType = UIKeyboardTypeDefault;	
		zipCodeField.returnKeyType = UIReturnKeyDone;
		zipCodeField.autocapitalizationType=UITextAutocapitalizationTypeNone;
		zipCodeField.clearButtonMode =  UITextFieldViewModeWhileEditing;	
		
		self.view=myView;
	
		[NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(timerAction:) userInfo:nil repeats:NO];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) 
													 name:UIKeyboardWillShowNotification object:self.view.window]; 
		CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*) [ [UIApplication sharedApplication] delegate];
		
		localOptions=[appDelegate getLocalOptions];
		isTimerIntiated=YES;
		indexValue = localOptions.optionsValue;
		
		activityIndicator  = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(145.0,175.5, 35,35)] ;
		activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
		activityIndicator.backgroundColor = [UIColor clearColor];
		[subView addSubview:activityIndicator];
		activityIndicator.hidesWhenStopped = YES;
		
		fetchingLabel= [[UILabel alloc] initWithFrame:CGRectMake(60,330,140,20)];
		[fetchingLabel setFont:[UIFont boldSystemFontOfSize:15]];
		fetchingLabel.backgroundColor=[UIColor clearColor];
		fetchingLabel.textColor =[UIColor grayColor];
		fetchingLabel.numberOfLines = 0;
		fetchingLabel.hidden=YES;
		fetchingLabel.text =@""; 
		[subView addSubview:fetchingLabel];
		//[fetchingLabel release];
	}
	return self;
}

-(void)viewWillAppear:(BOOL)animated
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*) [ [UIApplication sharedApplication] delegate];
	localOptions=[appDelegate getLocalOptions];
	[theTableView reloadData];

}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField becomeFirstResponder];
	[textField resignFirstResponder];
	
	[self setViewMovedUp:NO];
	isMoveUp=NO;
	return YES;
	
	
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if(indexPath.section==1)
	{
		if(indexPath.row==0  && indexValue==3)
		{
			return 60;
		}
	}
	return 45;
}

- (NSInteger)tableView:(UITableView *)tableView  numberOfRowsInSection:(NSInteger)section {
	
	if(section==0)
	{
		return 4;
	}
	if(section==1 && indexValue>0)
	{
		return 1;
	}
	if (section ==2)
	{
		return 1;
	}
	return 0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
	return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MasterViewIdentifier"] autorelease];
		cell.backgroundColor=[UIColor whiteColor];
		cell.accessoryType = UITableViewCellAccessoryNone;
		
		cell.selectionStyle=UITableViewCellSelectionStyleGray;
		UIView* elementView =  [[UIView alloc] initWithFrame:CGRectMake(20,170,320,280)];
		elementView.tag = 0;
		//elementView.backgroundColor=[UIColor clearColor];
		elementView.backgroundColor=[UIColor whiteColor];
		[cell.contentView addSubview:elementView];
		[elementView release];
	}
	UIView* elementView  = [cell.contentView viewWithTag:0];
	for(UIView* subView1 in elementView.subviews)
	{
		[subView1 removeFromSuperview];
	}
	
	UIColor* textColor = [UIColor colorWithRed:31/255.0 green:70/255.0 blue:82/255.0 alpha:1];
	if(indexPath.section==0)
	{
		if(indexPath.row==0)
		{
			
			UILabel *textView1 = [[UILabel alloc] initWithFrame:CGRectMake(10,12.5,300,20)];
			[textView1 setFont:[UIFont boldSystemFontOfSize:15]];
			textView1.backgroundColor=[UIColor clearColor];
			textView1.textColor = textColor;
			textView1.numberOfLines = 0;
			textView1.text =@"Do not include";	
			[elementView addSubview:textView1];
			[textView1 release];
			
			if(indexValue==0)
			{
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
		}
		else if(indexPath.row==1)
		{
			UILabel *textView1 = [[UILabel alloc] initWithFrame:CGRectMake(10,12.5,300,20)];
			[textView1 setFont:[UIFont boldSystemFontOfSize:15]];
			textView1.backgroundColor=[UIColor clearColor];
			textView1.textColor = textColor;
			textView1.numberOfLines = 0;
			textView1.text =@"Based on current location";	
			[elementView addSubview:textView1];
			[textView1 release];
			if(indexValue==1)
			{
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
		}
		else if(indexPath.row==2)
		{
			UILabel *textView1 = [[UILabel alloc] initWithFrame:CGRectMake(10,12.5,300,20)];
			[textView1 setFont:[UIFont boldSystemFontOfSize:15]];
			textView1.backgroundColor=[UIColor clearColor];
			textView1.textColor =textColor;
			textView1.numberOfLines = 0;
			textView1.text =@"Based on zipcode";	
			[elementView addSubview:textView1];
			[textView1 release];
			
			if(indexValue==2)
			{
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
		}
		else if(indexPath.row==3)
		{
			UILabel *textView1 = [[UILabel alloc] initWithFrame:CGRectMake(10,12.5,300,20)];
			[textView1 setFont:[UIFont boldSystemFontOfSize:15]];
			textView1.backgroundColor=[UIColor clearColor];
			textView1.textColor = textColor;
			textView1.numberOfLines = 0;
			textView1.text =@"Based on my school";	
			[elementView addSubview:textView1];
			[textView1 release];
			
			if(indexValue==3)
			{
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
		}
		
	}
	else if(indexPath.section==1)
	{
		if(indexValue==1)
		{
			
			UILabel *textView1 = [[UILabel alloc] initWithFrame:CGRectMake(10,12.5,100,20)];
			[textView1 setFont:[UIFont boldSystemFontOfSize:15]];
			textView1.backgroundColor=[UIColor clearColor];
			textView1.textColor =textColor;
			textView1.textAlignment=UITextAlignmentLeft;
			textView1.numberOfLines = 0;
			textView1.text =@"Radius";	
			[elementView addSubview:textView1];
			[textView1 release];
			
			radiusLabel = [[UILabel alloc] initWithFrame:CGRectMake(130,12.5,130,20)];
			[radiusLabel setFont:[UIFont boldSystemFontOfSize:15]];
			radiusLabel.backgroundColor=[UIColor clearColor];
			radiusLabel.textColor =textColor;
			radiusLabel.numberOfLines = 0;
			radiusLabel.textAlignment=UITextAlignmentRight;
			if (localOptions.radiusValue> 0)
			{
				radiusLabel.text =[NSString stringWithFormat:@"%d miles",localOptions.radiusValue];	
			}
			else
			{
				radiusLabel.text =@"Choose Radius Value";	
			}
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			[elementView addSubview:radiusLabel];
		}
		else if(indexValue==2)
		{
			UILabel *textView1 = [[UILabel alloc] initWithFrame:CGRectMake(10,12.5,100,20)];
			[textView1 setFont:[UIFont boldSystemFontOfSize:15]];
			textView1.backgroundColor=[UIColor clearColor];
			textView1.textColor =textColor;
			textView1.textAlignment=UITextAlignmentLeft;
			textView1.numberOfLines = 0;
			textView1.text =@"Zipcode";	
			[elementView addSubview:textView1];
			[textView1 release];
			
			if ([[localOptions.zipCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0)
			{
				
				zipCodeField.text =localOptions.zipCode;	
			}
			else 
			{
				CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
				
				zipCodeField.text = (appDelegate.currentZipcode != nil ? appDelegate.currentZipcode : @"");	
			}
			[elementView addSubview:zipCodeField];
		}
		else if(indexValue==3)
		{
			schoolNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,12.5,250,40)];
			[schoolNameLabel setFont:[UIFont boldSystemFontOfSize:15]];
			schoolNameLabel.backgroundColor=[UIColor clearColor];
			schoolNameLabel.textColor = textColor;
			schoolNameLabel.numberOfLines = 2;
		//	printf("\n selected string:%s",[localOptions.schoolName UTF8String]);
			if ([[localOptions.schoolName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0)
			{
				schoolNameLabel.text =localOptions.schoolName;	
			}
			else
			{
				schoolNameLabel.text =@"Choose school";	
			}

			[elementView addSubview:schoolNameLabel];
			
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		}
	}
	else if(indexPath.section==2)
	{
		if(indexPath.row==0)
		{
			
			UILabel *textView1 = [[UILabel alloc] initWithFrame:CGRectMake(10,12.5,300,20)];
			[textView1 setFont:[UIFont boldSystemFontOfSize:15]];
			textView1.backgroundColor=[UIColor clearColor];
			textView1.textColor = textColor;
			textView1.numberOfLines = 0;
			textView1.text =@"Don't ask me again";	
			[elementView addSubview:textView1];
			[textView1 release];
			
			if(localOptions.isFirstTime)
			{
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
		}
	}
		
	
	return cell;
}
- (void )tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath 
{
	isTimerIntiated=NO;
	if(newIndexPath.section==0)
	{
		indexValue=newIndexPath.row;
		localOptions.optionsValue = indexValue;

		[theTableView reloadData];
	}
	else if(newIndexPath.section==1)
	{
		if(indexValue==3)
		{
			fetchingLabel.text =@"Loading Schools"; 
			selectedIndexPath = newIndexPath;
			[self populateSchoolsView];
				
		}
		else if(indexValue==1)
		{
			RadiusPickerViewControlller* viewController=[[RadiusPickerViewControlller alloc]init];
			viewController.object=localOptions;
			[self.navigationController pushViewController:viewController animated:YES];
			[viewController release];
		}
	}
	else if(newIndexPath.section==2)
	{
		localOptions.isFirstTime = !localOptions.isFirstTime;
		
		[theTableView reloadData];
	}
	if (!(newIndexPath.section == 1 && indexValue == 3))
	{
		[theTableView deselectRowAtIndexPath:newIndexPath animated:YES];
	}

	
}

-(void)Done:(id)sender
{
	//printf("\n Tapped on Done");
	[self updateLocalOptionsAndClose];
}

-(void)timerAction:(id)sender
{
	if(isTimerIntiated==YES)
	{
		[self updateLocalOptionsAndClose];
	}
}

-(void)updateLocalOptions
{
	CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];
	
	LocalOptions* updateObj=[appDelegate getLocalOptions];
	updateObj.optionsValue=indexValue;
	updateObj.primaryKey=localOptions.primaryKey;
	updateObj.radiusValue=localOptions.radiusValue;
	updateObj.zipCode=zipCodeField.text;
	updateObj.isFirstTime=localOptions.isFirstTime;
	if ([[localOptions.schoolName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0)
	{
		updateObj.schoolName=schoolNameLabel.text;
	}
	else 
	{
		updateObj.schoolName=@"";
	}
	
	[appDelegate updateLocalOptions:updateObj];
//	[updateObj release];

}

-(void)updateLocalOptionsAndClose
{
//	BOOL tabBarNotLoaded = (localOptions.isFirstTime == 0);
	[self updateLocalOptions];
/*	if (tabBarNotLoaded)
	{
		printf("\n Dismissing localOptions and load tabbar");
		CampusBooksAppDelegate* appDelegate = (CampusBooksAppDelegate*)[[UIApplication sharedApplication]delegate];

		[appDelegate getToolBarItems];
	}
	else {
		printf("\n Dismissing localOptions as popview controller");
		[self.navigationController dismissModalViewControllerAnimated:NO];
	}
 */
	[self.navigationController dismissModalViewControllerAnimated:YES];
}
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5]; // if you want to slide up the view
	
	//printf("\n RESET VIEW");
	CGRect rect = self.view.frame;
    if (movedUp)
    {
		isMoveUp=YES;
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard 
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
		
		rect.origin.y = rect.origin.y - kOFFSET_FOR_KEYBOARD;
		
        rect.size.height = rect.size.height + kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y = rect.origin.y + kOFFSET_FOR_KEYBOARD;
        rect.size.height = rect.size.height - kOFFSET_FOR_KEYBOARD;
    }
	self.view.frame = rect;
	
    [UIView commitAnimations];
	
}


- (void)keyboardWillShow:(NSNotification *)notif
{
	//	printf("\n Keyboard Will SHOW");
    //keyboard will be shown now. depending for which textfield is active, move up or move down the view appropriately
	
 if ([zipCodeField isFirstResponder] && self.view.frame.origin.y >= 0 ) 
    {
		if(isMoveUp==NO)
		{
			[self setViewMovedUp:YES];
		}
    }
}



- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
	[activityIndicator release];
}

-(void)populateSchoolsView
{
	[activityIndicator startAnimating];
	fetchingLabel.text =@"Loading Schools"; 

	[NSThread detachNewThreadSelector:@selector(doPopulateSchoolsView:) toTarget:self withObject:nil];
}

- (void)doPopulateSchoolsView:(id)sender
{
	
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	SchoolsListViewController* viewController=[[SchoolsListViewController alloc]init];
	viewController.object=localOptions;

	if ([self.navigationController topViewController] == viewController)
	{
		//printf("\n cancelling the push");
	}
	else
	{
		[self.navigationController pushViewController:viewController animated:YES];
	}

	[viewController release];
	[activityIndicator stopAnimating];
	[theTableView deselectRowAtIndexPath:selectedIndexPath animated:YES];

	fetchingLabel.text =@""; 
	[pool release];	
	[NSThread exit];
	/*	}
	 @catch (NSException * e) {
	 [e 
	 }
	 @finally {
	 }
	 */
}



@end

