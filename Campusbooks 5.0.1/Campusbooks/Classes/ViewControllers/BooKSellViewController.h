//
//  BookSellViewController.h
//  CampusBooks
//
//  Created by Admin on 19/07/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import"Reachability.h"
#import"Book.h"
#import"WishList.h"
#import"MyCollection.h"
#import"HTMLUtil.h"
#import"BookOfferViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface BookSellViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, MFMailComposeViewControllerDelegate>{
	Book* currentBookInfo;
	UIViewController* bookListViewController;
	UIView* contentView ;
	UIActivityIndicatorView* progressInd;

	NSMutableArray* bookOffersList;
	NetworkStatus internetConnectionStatus;
	
	UITableView* tableView;
	
	int currentDisplayCriterion;
	UIImageView* titleView;
	NSArray* authorNamesList;
	
	NSMutableArray* bestOffersList;

	NSDateFormatter* originalDateFormatter;
	NSDateFormatter* newDateFormatter;
	
	UIAlertView* emailAlertView;
	
	BookOfferViewController* offerViewController;
	UIToolbar* toolbar;
	UISegmentedControl* segmentControl;
	
	Book* currentBook;
	NSMutableArray* offersList ;
}
@property NetworkStatus internetConnectionStatus;
- (void)updateStatus;
-(NSString *)hostName;

-(id)initWithBook:(Book*)aBook withOffers:(NSMutableArray*)aOffersList fromSource:(UIViewController*)aBookListViewController;
-(UIToolbar*)getToolbar;
-(void)refreshBookSellViewController:(Book*)aBook withOffers:(NSMutableArray*)aOffersList fromSource:(UIViewController*)aBookListViewController;

- (CGFloat)getTextHeightForSystemFontOfSize:(CGFloat)size ofWidth:(CGFloat)maxWidth forText:(NSString*)aText ;


-(void)addToWishList;
-(void)removeFromWishList;
-(void)getBestPrices:(NSMutableArray*)aOffersList;

-(UISegmentedControl *)createSegmentControl;
-(void)populateDetailsView;
-(void)buyItAction;

@end
