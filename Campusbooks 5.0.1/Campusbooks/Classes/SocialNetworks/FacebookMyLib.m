
//
//  FacebookMyLib.m
//  FunnyQuotes
//
//  Created by Damian Danielczyk on 2009-11-28.
//  Copyright 2009 SuntecDevelopment.com All rights reserved.
//

#import "FacebookMyLib.h"

//FACEBOOK API KEYS /////////////
#define FacebookApplicationID @"209393655855851"
//#define FacebookAPIKey @"ec6c286a2bb0d667d292e16e3046c829"
//#define FacebookSecret @"3ef8a51768d8c1aa2a7531d5577d0525"

/*
 #define FacebookApplicationID 188287781149
#define FacebookAPIKey @"908d25325cb956171ba2ac402d296083"
#define FacebookSecret @"211ccf927347af1bd7b11e61464e4052"
 */
/////////////////////////////////

static FacebookMyLib* _sharedInstance;
@implementation FacebookMyLib

@synthesize facebook;
@synthesize pendingStatus;

+(FacebookMyLib*)sharedInstance {
    if (!_sharedInstance) {
        _sharedInstance = [[FacebookMyLib alloc] init];
    }
    return _sharedInstance;
}
-(id)init{
    if ( (self = [super init])) {
        
        facebook = [[Facebook alloc] initWithAppId:FacebookApplicationID andDelegate:self];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults objectForKey:@"FBAccessTokenKey"] 
            && [defaults objectForKey:@"FBExpirationDateKey"]) {
            facebook.accessToken = [defaults objectForKey:@"FBAccessTokenKey"];
            facebook.expirationDate = [defaults objectForKey:@"FBExpirationDateKey"];
        }
    }
    return self;
}

- (void)fbDidLogin {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[facebook accessToken] forKey:@"FBAccessTokenKey"];
    [defaults setObject:[facebook expirationDate] forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
    
    if (self.pendingStatus) {
        [self tryToUpdateStatus:self.pendingStatus];
    }
    
}

////update user facebook status
//-(void)setUsersStatus:(NSString *)status {
//	NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
//							status, @"status",
//							@"true", @"status_includes_verb",
//							nil];
//	[[FBRequest requestWithDelegate:self] call:@"facebook.Users.setStatus" params:params];
//	/*NSString *path = @"http://www.facebook.com/images/devsite/iphone_connect_btn.jpg";
//	NSURL    *url  = [NSURL URLWithString:path];
//	NSData   *data = [NSData dataWithContentsOfURL:url];
//	UIImage  *img  = [[UIImage alloc] initWithData:data];
//	
//	//NSDictionary *params = nil;
//	[[FBRequest requestWithDelegate:self] call:@"facebook.photos.upload" params:params dataParam:(NSData*)img];*/
//	
//}


//updated user status - show alert box informing user.
- (void)request:(FBRequest*)request didLoad:(id)result {
	self.pendingStatus = nil;
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"CampusBooks" message:@"Thank you very much for recommending CampusBooks to your friends on Facebook."  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	[alert show];
	[alert release];
	
}

//failed to update states because of lack of permissions, show dialog box..
- (void)request:(FBRequest*)request didFailWithError:(NSError*)error {
    NSLog(@"Received Error:%@", error.userInfo);
    
//    if ([[[error.userInfo objectForKey:@"error"]objectForKey:@"code"] intValue] == 506) {
//
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"CampusBooks" message:@"You already recommended Campusbooks to your friends on Facebook."  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//        [alert release];
//    
//    }
}

-(BOOL)handleOpenURL:(NSURL *)url{
    
   return [facebook handleOpenURL:url];

}

////permissions updated, call doLastAction again
//- (void)dialogDidSucceed:(FBDialog*)dialog {
//	[self doLastAction];
//}

////in pendingAction we keep last status which should be updated, in case user is not logged in or has no persmissions, then we can try to update status again.
//-(void)doLastAction
//{
//	if(self.pendingStatus){
//		[self setUsersStatus:self.pendingStatus];
//	}
//}


-(void)tryToUpdateStatus:(NSString*)statusString
{
	self.pendingStatus = statusString;
    
    if (![facebook isSessionValid]) {
        
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"status_update",
                                nil];
        [facebook authorize:permissions];
        [permissions release];
    } 
    else {
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       self.pendingStatus,@"message",
                                       nil];
        
        [facebook requestWithGraphPath:@"me/feed"
                              andParams:params
                          andHttpMethod:@"POST"
                            andDelegate:self];
    }
//	if(!session)
//	{
//		[self initSession];
//		[self showLoginBox];
//	}else{
//		if(![session uid]){
//			[self showLoginBox];
//		}else{
//			
//			[self doLastAction];
//		}
//	}
//}
}


-(void)dealloc
{
//			printf("\n In FacebookMyLib Dealloc");
	[super dealloc];
//	[self.session.delegates removeObject:self];
}


@end
