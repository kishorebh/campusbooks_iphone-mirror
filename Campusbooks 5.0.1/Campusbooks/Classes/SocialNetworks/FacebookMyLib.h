//
//  FacebookMyLib.h
//  FunnyQuotes
//
//  Created by Damian Danielczyk on 2009-11-28.
//  Copyright 2009 SuntecDevelopment.com All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FBConnect.h"


@interface FacebookMyLib : NSObject <FBSessionDelegate, FBRequestDelegate>{
	Facebook* facebook;
    
	NSString *pendingStatus;
}
@property(nonatomic,retain) Facebook *facebook;
@property(nonatomic,retain) NSString *pendingStatus;

+(FacebookMyLib*)sharedInstance;
//-(void)setUsersStatus:(NSString *)status;
//-(void)doLastAction;
-(void)tryToUpdateStatus:(NSString*)statusString;
-(BOOL)handleOpenURL:(NSURL *)url;
@end
