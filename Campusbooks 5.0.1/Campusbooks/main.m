//
//  main.m
//  Campusbooks
//
//  Created by sekhar bethalam on 07/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CampusBooksAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CampusBooksAppDelegate class]));
    }
}
